package com.libra.sinvoice;

import java.util.ArrayList;
import java.util.List;

import android.media.AudioFormat;

public class RecordAsync implements Record.Listener, Record.Callback {
    private final static String TAG = "RecordAsync";

    private final static int STATE_STOP = 0;
    private final static int STATE_START = 1;

    private final static int BUFFER_COUNT = 3;

    private Record mRecord;
    private Thread mRecordThread;

    private static RecordAsync sThis;
    private int mState;
    private List<Listener> mListeners = new ArrayList<Listener>();
    private BufferData[] mBufferData;
    private int mCurDataIndex;

    public static interface Listener {
        void onStartRecordAsync();

        void onDataRecordAsync(BufferData data);

        void onStopRecordAsync();
    }

    private RecordAsync(int sampleRate, int bufferSize) {
        mRecord = new Record(this, sampleRate, AudioFormat.CHANNEL_IN_MONO,
                AudioFormat.ENCODING_PCM_16BIT, bufferSize);

        mState = STATE_STOP;
        mCurDataIndex = 0;
        mBufferData = new BufferData[BUFFER_COUNT];
        for (int i = 0; i < BUFFER_COUNT; ++i) {
            mBufferData[i] = new BufferData(bufferSize);
        }
    }

    public static RecordAsync getThis(int sampleRate, int bufferSize) {
        if (null == sThis) {
            synchronized (RecordAsync.class) {
                if (null == sThis) {
                    sThis = new RecordAsync(sampleRate, bufferSize);
                }
            }
        }

        return sThis;
    }

    public void addListener(Listener listener) {
        if (null != listener) {
            mListeners.add(listener);
        }
    }

    public void removeListener(Listener listener) {
        mListeners.remove(listener);
    }

    public void start(final boolean isReadFromFile) {
        if (STATE_STOP == mState) {
            mRecordThread = new Thread() {
                @Override
                public void run() {
                    mRecord.start(isReadFromFile);

                    LogHelper.d(TAG, "record thread end");

                    LogHelper.d(TAG, "stop recognition start");
                    // stopRecognition();
                    LogHelper.d(TAG, "stop recognition end");
                }
            };
            if (null != mRecordThread) {
                mRecordThread.start();

                mState = STATE_START;
            }
        } else {
            LogHelper.e(TAG, "already started!");
        }
    }

    public void stop() {
        if (STATE_START == mState) {
            mRecord.stop();
            if (null != mRecordThread) {
                try {
                    mRecordThread.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    mRecordThread = null;
                }
            }

            mState = STATE_STOP;
        }
    }

    @Override
    public BufferData getRecordBuffer() {
        BufferData ret = mBufferData[mCurDataIndex++];
        if (mCurDataIndex >= BUFFER_COUNT) {
            mCurDataIndex = 0;
        }
        return ret;
    }

    @Override
    public void freeRecordBuffer(BufferData buffer) {
        for (Listener listener : mListeners) {
            listener.onDataRecordAsync(buffer);
        }
    }

    @Override
    public void onStartRecord() {
        for (Listener listener : mListeners) {
            listener.onStartRecordAsync();
        }
    }

    @Override
    public void onStopRecord() {
        for (Listener listener : mListeners) {
            listener.onStopRecordAsync();
        }
    }

}
