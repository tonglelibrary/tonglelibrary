package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Administrator on 2015/5/11.
 */
public class BodyJson implements Serializable {

    // 首页广告位列表数据
//    public ArrayList<PromotionListItem> top_promotion_list;

    public String has_update;
    public String force_update;
    public String recently_version_no;
    public String recently_version_tips;
    public String recently_version_link;

    // Home forum response data
    public String data_total_count;
//    public ArrayList<UserFollowPostList> user_follow_post_list;

    // Collect expert data
    public ArrayList<FriendListItem> user_follow_friend_list;
    public ArrayList<FriendListItem> user_fans_list;

    //Clooect mstoredetailpic data
//    public ArrayList<MStorePicList> ad_list;

    // Collect product data
//    public ArrayList<ProductListItemData> prod_list;

    // Collect forum data
//    public ArrayList<UserFollowPostList> post_list;

    // Collect mstore data
//    public ArrayList<MStoreListItem> mstore_list;

    // Update tab page data in Expert tab page of Collect fragment
//    public ArrayList<UserFollowPostList> user_post_list;

    // Post detail data
    public UserPostInfo user_post_info;

    // Product detail data info
    public ProductInfo prod_info;

    // Product picture text detail list
    public ArrayList<ProductPicTextListItem> prod_pic_text_list;

    // Product specification parameter list
    public ArrayList<ProdParameterListItem> prod_parameter_list;

    // Product comment output parameter
    public ArrayList<ProductRatingListItem> product_rating_list;

    //达人二维码
    public String user_qr_code_url;
    //魔店二维码
    public String mstore_qr_code_url;
    //商品二维码
    public String prod_qr_code_url;
    //魔店名
    public String mstore_name;

    public String token;

    //购物车
//    public ArrayList<ShopCartListItem> my_shopping_cart;
}
