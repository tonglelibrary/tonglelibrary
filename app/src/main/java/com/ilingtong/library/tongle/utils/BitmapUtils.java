package com.ilingtong.library.tongle.utils;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.ilingtong.library.tongle.TongleAppInstance;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;


/**
 * Bitmap工具类
 * 
 * @author GaiQS E-mail:gaiqs@sina.com
 * @Date: 2015年2月3日
 * @Time: 上午10:44:04
 */
public class BitmapUtils {
	public static final int LEFT = 0;
	public static final int RIGHT = 1;
	public static final int TOP = 3;
	public static final int BOTTOM = 4;
	public static final int quality_100 = 100;
	public static final int quality_50 = 50;
	public static final int quality_30 = 30;
	public static final int quality_10 = 10;

	private static Bitmap bitmap(int resId) {
		Options options = new Options();
		options.inInputShareable = true;
		options.inPurgeable = true;
		options.inJustDecodeBounds = false;
		options.inPreferredConfig = Config.RGB_565;
		options.inSampleSize = 3; // width，hight设为原来的1/3
		// 获取资源图片流
		InputStream is = TongleAppInstance.getAppContext().getResources().openRawResource(resId);
		// BitmapFactory.decodeStream(is,null,options);
		return BitmapFactory.decodeStream(is, null, options);
	}

	/**
	 * 根据uri获取路径-方法一
	 * 
	 * @param context
	 * @param uri
	 * @return
	 */
	public static String getRealFilePathFromUri1(Context context, Uri uri) {
		String path = null;
		if (null != uri) {
			ContentResolver cr = context.getContentResolver();
			Cursor cursor = cr.query(uri, new String[] { "_data" }, null, null, null);
			if (cursor.moveToFirst()) {
				path = cursor.getString(0);
			}
		}
		return path;
	}

//	/**
//	 * 根据uri获取路径-方法二
//	 * 
//	 * @param context
//	 * @param uri
//	 * @return
//	 */
//	public static String getRealFilePathFromUri2(final Context context, final Uri fileUrl) {
//		final String scheme = fileUrl.getScheme();
//		String data = null;
//		if (scheme == null)
//			data = fileUrl.getPath();
//		else if (ContentResolver.SCHEME_FILE.equals(scheme)) {
//			data = fileUrl.getPath();
//		} else if (ContentResolver.SCHEME_CONTENT.equals(scheme)) {
//			Cursor cursor = context.getContentResolver().query(fileUrl, new String[] { ImageColumns.DATA }, null, null, null);
//			if (null != cursor) {
//				if (cursor.moveToFirst()) {
//					int index = cursor.getColumnIndex(ImageColumns.DATA);
//					if (index > -1) {
//						data = cursor.getString(index);
//					}
//				}
//				cursor.close();
//			}
//		}
//		return data;
//	}
//
//	/**
//	 * 根据uri获取路径-方法三
//	 * 
//	 * @param paramContext
//	 * @param fileUrl
//	 * @return
//	 */
//	public static String getRealFilePathFromUri3(Context paramContext, Uri fileUrl) {
//		String fileName = null;
//		Uri filePathUri = fileUrl;
//		if (fileUrl != null) {
//			if (fileUrl.getScheme().toString().compareTo("content") == 0) {
//				// content://开头的uri
//				Cursor cursor = paramContext.getContentResolver().query(fileUrl, null, null, null, null);
//				if (cursor != null && cursor.moveToFirst()) {
//					int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
//					fileName = cursor.getString(column_index); // 取出文件路径
//					// Android 4.1 更改了SD的目录，sdcard映射到/storage/sdcard0
//					if (!fileName.startsWith("/storage") && !fileName.startsWith("/mnt")) {
//						// 检查是否有”/mnt“前缀
//						fileName = "/mnt" + fileName;
//					}
//					cursor.close();
//				}
//			} else if (fileUrl.getScheme().compareTo("file") == 0) {
//				// file:///开头的uri
//				fileName = filePathUri.toString();// 替换file://
//				fileName = filePathUri.toString().replace("file://", "");
//				int index = fileName.indexOf("/sdcard");
//				fileName = index == -1 ? fileName : fileName.substring(index);
//				if (!fileName.startsWith("/mnt")) {
//					// 加上"/mnt"头
//					fileName += "/mnt";
//				}
//			}
//		}
//		if (TextUtils.isEmpty(fileName) && null != fileUrl) {
//			fileName = getRealFilePathFromUri4(paramContext, filePathUri);
//		}
//		return fileName;
//	}
//
//	/**
//	 * 根据uri获取路径-方法四
//	 * 
//	 * @param paramContext
//	 * @param fileUrl
//	 * @return
//	 */
//	public static String getRealFilePathFromUri4(final Context paramContext, final Uri uri) {
//		int actual_image_column_index;
//		String img_path;
//		String[] proj = { MediaStore.Images.Media.DATA };
//		Cursor cursor = paramContext.getContentResolver().query(uri, proj, null, null, null);
//		actual_image_column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
//		cursor.moveToFirst();
//		img_path = cursor.getString(actual_image_column_index);
//		return img_path;
//	}

	/**
	 * 创建一个文字Bitmap
	 * 
	 * @param ttf
	 * @return
	 */
	public static Bitmap createBitmapTTF(String ttf) {
		if (TextUtils.isEmpty(ttf)) {
			ttf = "测试字体";
		}
		Bitmap bmp = Bitmap.createBitmap(300, 200, Config.ARGB_8888);
		Canvas canvas = new Canvas(bmp);
		Paint paint = new Paint();
		// Typeface typeface = Typeface.createFromAsset(this.getAssets(),
		// "fonts/CONSOLA.TTF");
		// paint.setTypeface(typeface);
		paint.setColor(Color.RED);
		paint.setTextSize(40);
		canvas.drawText(ttf, 0, 100, paint);
		return bmp;
	}

	/**
	 *
	 * @param bitmap
	 * @param widthScale
	 *            宽度缩放比例
	 * @param hightScale
	 *            长度缩放比例
	 * @return
	 */
	public static Bitmap scale(Bitmap bitmap, float widthScale, float hightScale) {
		Matrix matrix = new Matrix();
		matrix.postScale(widthScale, hightScale); // 长和宽放大缩小的比例
		Bitmap resizeBmp = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
		return resizeBmp;
	}

	/** */
	/**
	 * 图片去色,返回灰度图片
	 * 
	 * @param bmpOriginal
	 *            传入的图片
	 * @return 去色后的图片
	 */
	public static Bitmap toGrayscale(Bitmap bmpOriginal) {
		int width, height;
		height = bmpOriginal.getHeight();
		width = bmpOriginal.getWidth();
		Bitmap bmpGrayscale = Bitmap.createBitmap(width, height, Config.RGB_565);
		Canvas c = new Canvas(bmpGrayscale);
		Paint paint = new Paint();
		ColorMatrix cm = new ColorMatrix();
		cm.setSaturation(0);
		ColorMatrixColorFilter f = new ColorMatrixColorFilter(cm);
		paint.setColorFilter(f);
		c.drawBitmap(bmpOriginal, 0, 0, paint);
		return bmpGrayscale;
	}

	/** */
	/**
	 * 去色同时加圆角
	 * 
	 * @param bmpOriginal
	 *            原图
	 * @param pixels
	 *            圆角弧度
	 * @return 修改后的图片
	 */
	public static Bitmap toGrayscale(Bitmap bmpOriginal, int pixels) {
		return toRoundCorner(toGrayscale(bmpOriginal), pixels);
	}

	/** */
	/**
	 * 把图片变成圆角
	 * 
	 * @param bitmap
	 *            需要修改的图片
	 * @param pixels
	 *            圆角的弧度
	 * @return 圆角图片
	 */
	public static Bitmap toRoundCorner(Bitmap bitmap, int pixels) {
		Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Config.ARGB_8888);
		Canvas canvas = new Canvas(output);
		final int color = 0xff424242;
		final Paint paint = new Paint();
		final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
		final RectF rectF = new RectF(rect);
		final float roundPx = pixels;
		paint.setAntiAlias(true);
		canvas.drawARGB(0, 0, 0, 0);
		paint.setColor(color);
		canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
		paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
		canvas.drawBitmap(bitmap, rect, rect, paint);
		return output;
	}

	/**
	 * 创建圆边图片
	 * 
	 * @param bitmap
	 * @return
	 */
	public static Bitmap toRoundedCorner(Bitmap bitmap) {
		// 创建新的位图
		Bitmap bgBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Config.ARGB_8888);
		// 把创建的位图作为画板
		Canvas mCanvas = new Canvas(bgBitmap);
		Paint mPaint = new Paint();
		Rect mRect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
		RectF mRectF = new RectF(mRect);
		// 设置圆角半径为50
		float roundPx = DipUtils.dip2px(6);
		mPaint.setAntiAlias(true);
		// 先绘制圆角矩形
		mCanvas.drawRoundRect(mRectF, roundPx, roundPx, mPaint);
		// 设置图像的叠加模式
		mPaint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
		// 绘制图像
		mCanvas.drawBitmap(bitmap, mRect, mRect, mPaint);
		return bgBitmap;
	}

	/** */
	/**
	 * 使圆角功能支持BitampDrawable
	 * 
	 * @param bitmapDrawable
	 * @param pixels
	 * @return
	 */
	public static BitmapDrawable toRoundCorner(BitmapDrawable bitmapDrawable, int pixels) {
		Bitmap bitmap = bitmapDrawable.getBitmap();
		bitmapDrawable = new BitmapDrawable(toRoundCorner(bitmap, pixels));
		return bitmapDrawable;
	}

	/**
	 * 读取路径中的图片，然后将其转化为缩放后的bitmap
	 * 
	 * @param path
	 */
	public static void saveBefore(String path) {
		Options options = new Options();
		options.inJustDecodeBounds = true;
		// 获取这个图片的宽和高
		Bitmap bitmap = BitmapFactory.decodeFile(path, options); // 此时返回bm为空
		options.inJustDecodeBounds = false;
		// // 计算缩放比
		// int be = (int) (options.outHeight / (float) 200);
		// if (be <= 0)
		// be = 1;
		options.inSampleSize = 2; // 图片长宽各缩小二分之一
		// 重新读入图片，注意这次要把options.inJustDecodeBounds 设为 false哦
		bitmap = BitmapFactory.decodeFile(path, options);
		int w = bitmap.getWidth();
		int h = bitmap.getHeight();
		System.out.println(w + " " + h);
		// savePNG_After(bitmap,path);
		saveJPGE_After(bitmap, path, BitmapUtils.quality_100);
	}

	/**
	 * 根据路径的图片，然后将其转化为bitmap
	 * 
	 * @param path
	 * @return
	 */
	public static Bitmap readBitmapByPath(String path) {
		Options options = new Options();
		// 设置为true 只会返回图片的宽高
		options.inJustDecodeBounds = true;
		// 获取这个图片的宽和高
		Bitmap bitmap = BitmapFactory.decodeFile(path, options); // 此时返回bm为空
		options.inJustDecodeBounds = false;
		// 计算缩放比
		if (options.outWidth > TongleAppInstance.sWidth || options.outHeight > TongleAppInstance.sHeight) {
			options.inSampleSize = 2; // 图片长宽各缩小二分之一
		}
		if (options.outWidth < TongleAppInstance.sWidth / 2) {
			// 重新读入图片，注意这次要把options.inJustDecodeBounds 设为 false哦
			options.inJustDecodeBounds = false;
			bitmap = BitmapFactory.decodeFile(path, options);
			// 图片太小，重新设置图片宽高
			bitmap = BitmapUtils.createBitmapBySize(bitmap, (int) (options.outWidth + TongleAppInstance.sWidth / 2), (int) (options.outHeight + TongleAppInstance.sWidth / 2));
		} else {
			// 重新读入图片，注意这次要把options.inJustDecodeBounds 设为 false哦
			options.inJustDecodeBounds = false;
			bitmap = BitmapFactory.decodeFile(path, options);
		}
		return bitmap;
	}

	/**
	 * 保存图片为PNG
	 * 
	 * @param bitmap
	 * @param name
	 */
	public static boolean savePNG_After(Bitmap bitmap, String name, int quality) {
		File file = new File(name);
		try {
			FileOutputStream out = new FileOutputStream(file);
			if (bitmap.compress(Bitmap.CompressFormat.PNG, quality, out)) {
				out.flush();
				out.close();
			}
			return true;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * 保存图片为JPEG
	 * 
	 * @param bitmap
	 * @param path
	 */
	public static boolean saveJPGE_After(Bitmap bitmap, String path, int quality) {
		File file = new File(path);
		try {
			FileOutputStream out = new FileOutputStream(file);
			if (bitmap.compress(Bitmap.CompressFormat.JPEG, quality, out)) {
				out.flush();
				out.close();
			}
			return true;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * 水印
	 * 
	 * @return
	 */
	public static Bitmap createBitmapForWatermark(Bitmap src, Bitmap watermark) {
		if (src == null) {
			return null;
		}
		int w = src.getWidth();
		int h = src.getHeight();
		int ww = watermark.getWidth();
		int wh = watermark.getHeight();
		// create the new blank bitmap
		Bitmap newb = Bitmap.createBitmap(w, h, Config.ARGB_8888);// 创建一个新的和SRC长度宽度一样的位图
		Canvas cv = new Canvas(newb);
		// draw src into
		cv.drawBitmap(src, 0, 0, null);// 在 0，0坐标开始画入src
		// draw watermark into
		cv.drawBitmap(watermark, w - ww - 10, h - wh + 5, null);// 在src的右下角画入水印
		// save all clip
		cv.save(Canvas.ALL_SAVE_FLAG);// 保存
		// store
		cv.restore();// 存储
		return newb;
	}

	/**
	 * 图片合成
	 * 
	 * @return
	 */
	public static Bitmap potoMix(int direction, Bitmap... bitmaps) {
		if (bitmaps.length <= 0) {
			return null;
		}
		if (bitmaps.length == 1) {
			return bitmaps[0];
		}
		Bitmap newBitmap = bitmaps[0];
		// newBitmap = createBitmapForFotoMix(bitmaps[0],bitmaps[1],direction);
		for (int i = 1; i < bitmaps.length; i++) {
			newBitmap = createBitmapForFotoMix(newBitmap, bitmaps[i], direction);
		}
		return newBitmap;
	}

	private static Bitmap createBitmapForFotoMix(Bitmap first, Bitmap second, int direction) {
		if (first == null) {
			return null;
		}
		if (second == null) {
			return first;
		}
		int fw = first.getWidth();
		int fh = first.getHeight();
		int sw = second.getWidth();
		int sh = second.getHeight();
		Bitmap newBitmap = null;
		if (direction == LEFT) {
			newBitmap = Bitmap.createBitmap(fw + sw, fh > sh ? fh : sh, Config.ARGB_8888);
			Canvas canvas = new Canvas(newBitmap);
			canvas.drawBitmap(first, sw, 0, null);
			canvas.drawBitmap(second, 0, 0, null);
		} else if (direction == RIGHT) {
			newBitmap = Bitmap.createBitmap(fw + sw, fh > sh ? fh : sh, Config.ARGB_8888);
			Canvas canvas = new Canvas(newBitmap);
			canvas.drawBitmap(first, 0, 0, null);
			canvas.drawBitmap(second, fw, 0, null);
		} else if (direction == TOP) {
			newBitmap = Bitmap.createBitmap(sw > fw ? sw : fw, fh + sh, Config.ARGB_8888);
			Canvas canvas = new Canvas(newBitmap);
			canvas.drawBitmap(first, 0, sh, null);
			canvas.drawBitmap(second, 0, 0, null);
		} else if (direction == BOTTOM) {
			newBitmap = Bitmap.createBitmap(sw > fw ? sw : fw, fh + sh, Config.ARGB_8888);
			Canvas canvas = new Canvas(newBitmap);
			canvas.drawBitmap(first, 0, 0, null);
			canvas.drawBitmap(second, 0, fh, null);
		}
		return newBitmap;
	}

	/**
	 * 将Bitmap转换成指定大小
	 * 
	 * @param bitmap
	 * @param width
	 * @param height
	 * @return
	 */
	public static Bitmap createBitmapBySize(Bitmap bitmap, int width, int height) {
		return Bitmap.createScaledBitmap(bitmap, width, height, true);
	}

	/**
	 * Drawable 转 Bitmap
	 * 
	 * @param drawable
	 * @return
	 */
	public static Bitmap drawableToBitmapByBD(Drawable drawable) {
		BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
		return bitmapDrawable.getBitmap();
	}

	/**
	 * Bitmap 转 Drawable
	 * 
	 * @param bitmap
	 * @return
	 */
	public static Drawable bitmapToDrawableByBD(Bitmap bitmap) {
		Drawable drawable = new BitmapDrawable(bitmap);
		return drawable;
	}

	/**
	 * view 转bitmap
	 * 
	 * @param view
	 * @return
	 */
	// public static Bitmap convertViewToBitmap(View view) {
	// view.measure(MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED),
	// MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED));
	// view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
	// view.buildDrawingCache();
	// Bitmap bitmap = view.getDrawingCache();
	// return bitmap;
	// }
	/**
	 * view 转bitmap
	 * 
	 * @return
	 */
	public static Bitmap convertViewToBitmap(View v) {
		v.clearFocus();
		v.setPressed(false);

		boolean willNotCache = v.willNotCacheDrawing();
		v.setWillNotCacheDrawing(false);

		// Reset the drawing cache background color to fully transparent
		// for the duration of this operation
		int color = v.getDrawingCacheBackgroundColor();
		v.setDrawingCacheBackgroundColor(0);

		if (color != 0) {
			v.destroyDrawingCache();
		}
		v.buildDrawingCache();
		Bitmap cacheBitmap = v.getDrawingCache();
		if (cacheBitmap == null) {
			Log.e("TTTTTTTTActivity", "failed getViewBitmap(" + v + ")", new RuntimeException());
			return null;
		}

		Bitmap bitmap = Bitmap.createBitmap(cacheBitmap);

		// Restore the view
		v.destroyDrawingCache();
		v.setWillNotCacheDrawing(willNotCache);
		v.setDrawingCacheBackgroundColor(color);

		return bitmap;
	}

	/**
	 * byte[] 转 bitmap
	 * 
	 * @param b
	 * @return
	 */
	public static Bitmap bytesToBimap(byte[] b) {
		if (b.length != 0) {
			return BitmapFactory.decodeByteArray(b, 0, b.length);
		} else {
			return null;
		}
	}

	/**
	 * bitmap 转 byte[]
	 * 
	 * @param bm
	 * @return
	 */
	public static byte[] bitmapToBytes(Bitmap bm) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		bm.compress(Bitmap.CompressFormat.PNG, 100, baos);
		return baos.toByteArray();
	}
	
	/**
	 * 获得图像
	 * 
	 * @param path
	 * @return
	 * @throws java.io.FileNotFoundException
	 */
	public static Bitmap getBitmapByPath(String path, int screenWidth, int screenHeight) {
		Bitmap bm = null;
		File file = new File(path);
		if (!file.exists()) {
			return bm;
		}
		FileInputStream in = null;
		try {
			in = new FileInputStream(file);
			Options options = getOptions(path);
			if (options != null) {
				int w = options.outWidth;
				int h = options.outHeight;
				options.inSampleSize = w > h ? w / screenWidth : h / screenHeight;
				options.inJustDecodeBounds = false;
			}
			bm = BitmapFactory.decodeStream(in, null, options);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				in.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return bm;
	}
	/*
	 * 获得设置信息
	 */
	private static Options getOptions(String path) {
		Options options = new Options();
		options.inJustDecodeBounds = true;// 只描边，不读取数据
		BitmapFactory.decodeFile(path, options);
		return options;
	}

}
