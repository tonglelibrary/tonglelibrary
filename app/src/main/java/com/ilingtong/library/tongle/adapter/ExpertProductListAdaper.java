package com.ilingtong.library.tongle.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.protocol.ProductListItemData;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

/**
 * User: shuailei
 * Date: 2015/5/19
 * Time: 10:45
 * Email: leishuai@isoftstone.com
 * Dest:达人二级产品
 */
public class ExpertProductListAdaper extends BaseAdapter {
    private LayoutInflater inflater;
    private List<ProductListItemData> list;
    ProductListItemData dataItem = new ProductListItemData();
    Context mContext;

    public ExpertProductListAdaper(Context context, ArrayList list) {
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mContext = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder holder;
        if (view == null) {
            view = inflater.inflate(R.layout.list_item_product, null);
            holder = new ViewHolder();
            holder.listItemImageView = (ImageView) view.findViewById(R.id.product_list_item_image);
            holder.desc = (TextView) view.findViewById(R.id.product_list_item_desc);
            holder.price = (TextView) view.findViewById(R.id.product_list_item_price);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        dataItem = list.get(position);
        ImageLoader.getInstance().displayImage(dataItem.prod_thumbnail_pic_url, holder.listItemImageView, TongleAppInstance.options);
        holder.desc.setText(dataItem.prod_name);
        holder.price.setText(mContext.getString(R.string.RMB) + dataItem.prod_price);
        return view;
    }

    static class ViewHolder {
        TextView price;
        TextView desc;
        ImageView listItemImageView;
    }
}
