package com.ilingtong.library.tongle.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.RelativeLayout;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.activity.CollectProductMoreActivity;
import com.ilingtong.library.tongle.adapter.PicTextDetailListAdapter;
import com.ilingtong.library.tongle.external.maxwin.view.XListView;
import com.ilingtong.library.tongle.protocol.PicTxtDetailResult;
import com.ilingtong.library.tongle.protocol.PicTxtEDetailModle;
import com.ilingtong.library.tongle.utils.ToastUtils;

/**
 * User: lengjiqiang
 * Date: 2015/5/25
 * Time: 13:54
 * Email: jqleng@isoftstone.com
 * Desc: 商品图文详情页
 */
public class ProductDetailFragment extends FragmentAppBase {
    private XListView detailListView;
    private RelativeLayout rl_replace;
    private PicTextDetailListAdapter detailListAdapter;
    private PicTxtEDetailModle pictxtedetailmodle  = new PicTxtEDetailModle();

    View mainView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(mainView == null){
            mainView = inflater.inflate(R.layout.fragment_product_detail_layout, null);
            initView(mainView);
            doRequest();
        }
        return mainView;
    }
    public void initView(View mainView) {
        detailListView = (XListView) mainView.findViewById(R.id.product_detail_listview);
        rl_replace = (RelativeLayout) mainView.findViewById(R.id.rl_replace);

        detailListView.setPullLoadEnable(false);
        detailListView.setPullRefreshEnable(false);
    }
    public void doRequest(){
        ServiceManager.productDetailRequest(TongleAppInstance.getInstance().getUserID(), CollectProductMoreActivity.prodID, successListener(), errorListener());
    }
    /**
     * 功能：商品图文详情网络响应成功，返回数据
     */
    private Response.Listener successListener() {
        return new Response.Listener<PicTxtDetailResult>() {
            @Override
            public void onResponse(PicTxtDetailResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    showRequestCallbackDialog();
                     pictxtedetailmodle.prod_pic_text_list =response.getBody().prod_pic_text_list ;
                    if (pictxtedetailmodle.prod_pic_text_list.size()==0){
                        detailListView.setVisibility(View.GONE);
                        rl_replace.setVisibility(View.VISIBLE);
                    }else{
                        detailListAdapter = new PicTextDetailListAdapter(getActivity(), pictxtedetailmodle.prod_pic_text_list);
                        detailListView.setAdapter(detailListAdapter);
                    }
                } else {
                    ToastUtils.toastShort(getString(R.string.para_exception) + response.getHead().getReturn_message());
                }
                cancelRequestCallbackDialog();
            }
        };
    }
    /**
     * 功能：商品图文详情请求网络响应失败
     */
    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ToastUtils.toastShort(getString(R.string.sys_exception));
                cancelRequestCallbackDialog();
            }
        };
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ViewParent parent = mainView.getParent();
        if(parent !=null)
        {
            ((ViewGroup)parent).removeView(mainView);
        }
    }
}
