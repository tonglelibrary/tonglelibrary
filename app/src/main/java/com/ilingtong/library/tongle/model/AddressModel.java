package com.ilingtong.library.tongle.model;

import com.ilingtong.library.tongle.protocol.AddressListItem;

import java.util.ArrayList;

/**
 * User: lengjiqiang
 * Date: 2015/6/9
 * Time: 22:19
 * Email: jqleng@isoftstone.com
 * Desc: 保存收货地址信息的地址类模型
 */
public class AddressModel {
    public static ArrayList<AddressListItem> my_address_list;
    public static AddressListItem checkAddress = new AddressListItem();
}
