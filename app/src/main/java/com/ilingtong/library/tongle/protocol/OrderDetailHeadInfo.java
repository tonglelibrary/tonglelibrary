package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: shuailei
 * Date: 2015/6/10
 * Time: 13:01
 * Email: leishuai@isoftstone.com
 * Desc:
 */
public class OrderDetailHeadInfo implements Serializable {
    public String order_no;
    public String order_time;
    public String order_memo;
    public String quantity;
    public String amount;  //实付金额
    public String fee_amount;
    public String status;
    public String pay_method;
    public String modify_flag;
    public String cancle_flag;
    public String evaluate_flag;
    public String tariff;
    public String customs_flag;
    public String customs_fail_reason;
    public String goods_amount;    //add at 2016/04/07  商品总额
    public String pay_amount;    //add at 2016/04/07    应付金额

    @Override
    public String toString() {
        return "order_no:" + order_no + "\r\n" +
                "order_time:" + order_time + "\r\n" +
                "order_memo:" + order_memo + "\r\n" +
                "quantity:" + quantity + "\r\n" +
                "amount:" + amount + "\r\n" +
                "fee_amount:" + fee_amount + "\r\n" +
                "status:" + status + "\r\n" +
                "pay_method:" + pay_method + "\r\n" +
                "modify_flag:" + modify_flag + "\r\n" +
                "cancle_flag:" + cancle_flag + "\r\n" +
                "tariff:" + tariff + "\r\n" +
                "customs_fail_reason:" + customs_fail_reason + "\r\n" +
                "customs_flag:" + customs_flag + "\r\n" +
                "evaluate_flag:" + evaluate_flag;
    }
}
