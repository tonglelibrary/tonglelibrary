package com.ilingtong.library.tongle.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by wuqian on 2015/10/29.
 * mail: wuqian@ilingtong.com
 * Description:收藏选项下viewager的adapter./我的订单页面viewpager的adapter
 */
public class CollectFragmentPagerAdapter extends FragmentPagerAdapter {
    private Fragment[] fragments;
    public CollectFragmentPagerAdapter(FragmentManager fm, Fragment[] fragments) {
        super(fm);
        this.fragments = fragments;
    }

    @Override
    public Fragment getItem(int position) {
        return fragments[position];
    }

    @Override
    public int getCount() {
        return fragments.length;
    }
}
