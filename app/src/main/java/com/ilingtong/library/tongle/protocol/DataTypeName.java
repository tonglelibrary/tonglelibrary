package com.ilingtong.library.tongle.protocol;

/**
 * Created by wuqian on 2016/3/29.
 * mail: wuqian@ilingtong.com
 * Description:基础数据  2018接口入口参数对象化用。
 */
public class DataTypeName {
    private String data_type;  //基础数据类型

    public DataTypeName(String data_type) {
        this.data_type = data_type;
    }

    public String getData_type() {
        return data_type;
    }

    public void setData_type(String data_type) {
        this.data_type = data_type;
    }
}
