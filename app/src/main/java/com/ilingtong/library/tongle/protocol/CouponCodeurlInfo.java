package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/3/11.
 * mail: wuqian@ilingtong.com
 * Description: 团购券一维码二维码信息
 * use by 6010 6011 6013
 */
public class CouponCodeurlInfo implements Serializable {
    private String coupon_2d_qrcode_url;    //订单二维码图片URL
    private String coupon_1d_qrcode_url;           //订单一维码图片URL

    public String getCoupon_2d_qrcode_url() {
        return coupon_2d_qrcode_url;
    }

    public void setCoupon_2d_qrcode_url(String coupon_2d_qrcode_url) {
        this.coupon_2d_qrcode_url = coupon_2d_qrcode_url;
    }

    public String getCoupon_1d_qrcode_url() {
        return coupon_1d_qrcode_url;
    }

    public void setCoupon_1d_qrcode_url(String coupon_1d_qrcode_url) {
        this.coupon_1d_qrcode_url = coupon_1d_qrcode_url;
    }
}
