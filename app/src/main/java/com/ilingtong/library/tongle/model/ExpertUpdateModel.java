package com.ilingtong.library.tongle.model;

import com.ilingtong.library.tongle.protocol.UserFollowPostList;

import java.util.ArrayList;

/**
 * User: lengjiqiang
 * Date: 2015/6/2
 * Time: 13:48
 * Email: jqleng@isoftstone.com
 * Dest:
 */
public class ExpertUpdateModel {
    //  收藏 -> 达人 -> 更新属性也数据
    public static String expert_update_data_total_count;
    public static ArrayList<UserFollowPostList> user_post_list;
}
