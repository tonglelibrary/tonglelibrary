package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/6/4
 * Time: 11:50
 * Email: jqleng@isoftstone.com
 * Desc:
 */
public class CollectExpertResult implements Serializable {
    private BaseInfo head;
    private CollectExpertInfo body;

    public BaseInfo getHead() {
        return head;
    }

    public void setHead(BaseInfo head) {
        this.head = head;
    }

    public CollectExpertInfo getBody() {
        return body;
    }

    public void setBody(CollectExpertInfo body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return body.toString();
    }
}
