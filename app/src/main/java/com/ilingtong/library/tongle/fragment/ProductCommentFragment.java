package com.ilingtong.library.tongle.fragment;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.RelativeLayout;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.adapter.ProdCommentListAdapter;
import com.ilingtong.library.tongle.external.maxwin.view.XListView;
import com.ilingtong.library.tongle.protocol.ProductCommentResult;
import com.ilingtong.library.tongle.utils.DialogUtils;
import com.ilingtong.library.tongle.utils.ToastUtils;

/**
 * User: lengjiqiang
 * Date: 2015/5/25
 * Time: 13:56
 * Email: jqleng@isoftstone.com
 * Dest: 产品评价页
 */
public class ProductCommentFragment extends FragmentAppBase implements XListView.IXListViewListener {
    private XListView commentListView;
    private RelativeLayout rl_replace;
    private ProdCommentListAdapter commentListAdapter;
    //private ProgressDialog pDlg;
    private Dialog mDialog;
    private String prodID;

    View mainView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (mainView == null) {
            mainView = inflater.inflate(R.layout.fragment_product_comment_layout, null);
            getData();
            initView(mainView);
            doRequest();
        }
        return mainView;
    }

    public void getData() {
        prodID = getActivity().getIntent().getExtras().getString("product_id");
    }

    public void initView(View mainView) {
        commentListView = (XListView) mainView.findViewById(R.id.product_comment_listview);
        rl_replace = (RelativeLayout) mainView.findViewById(R.id.rl_replace);
        //创建并弹出“加载中Dialog”
        mDialog = DialogUtils.createLoadingDialog(getActivity());
//        pDlg = new ProgressDialog(getActivity());
//        pDlg.setCancelable(false);
//        pDlg.setMessage(getString(R.string.loading));
//        pDlg.show();
    }

    public void doRequest() {
        mDialog.show();
        ServiceManager.prdCommentRequest(TongleAppInstance.getInstance().getUserID(), prodID, "", "", "", TongleAppConst.FETCH_COUNT, successListener(), errorListener());
    }

    @Override
    public void onRefresh(int id) {
        doRequest();
    }

    @Override
    public void onLoadMore(int id) {

    }

    /**
     * 功能：获取产品评价数据网络响应成功，返回数据
     */
    private Response.Listener successListener() {
        return new Response.Listener<ProductCommentResult>() {
            @Override
            public void onResponse(ProductCommentResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    if (response.getBody().product_rating_list.size() == 0) {
                        commentListView.setVisibility(View.GONE);
                        rl_replace.setVisibility(View.VISIBLE);
                    }
                    commentListView.setPullLoadEnable(false);
                    commentListView.setPullRefreshEnable(true);
                    commentListAdapter = new ProdCommentListAdapter(getActivity(), response.getBody().product_rating_list);
                    commentListView.setRefreshTime();
                    commentListView.setXListViewListener(ProductCommentFragment.this, 0);
                    commentListView.setAdapter(commentListAdapter);

                } else {
                    ToastUtils.toastShort(getString(R.string.para_exception) + response.getHead().getReturn_message());
                }
                //pDlg.dismiss();
                mDialog.dismiss();
            }
        };
    }

    /**
     * 功能：获取产品评价请求网络响应失败
     */
    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ToastUtils.toastShort(getString(R.string.sys_exception));
                //pDlg.dismiss();
                mDialog.dismiss();
            }
        };
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ViewParent parent = mainView.getParent();
        if(parent !=null)
        {
            ((ViewGroup)parent).removeView(mainView);
        }
    }
}
