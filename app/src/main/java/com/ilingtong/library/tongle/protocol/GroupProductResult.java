package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/3/11.
 * mail: wuqian@ilingtong.com
 * Description:6007接口返回json 对应entity
 */
public class GroupProductResult extends BaseResult implements Serializable {

    private GroupProductBodyInfo body;

    public GroupProductBodyInfo getBody() {
        return body;
    }

    public void setBody(GroupProductBodyInfo body) {
        this.body = body;
    }
}
