package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/6/17
 * Time: 14:41
 * Email: jqleng@isoftstone.com
 * Desc:
 */
public class CodeResult implements Serializable {
    private BaseInfo head;
    private CodeInfo body;

    public BaseInfo getHead() {
        return head;
    }

    public void setHead(BaseInfo head) {
        this.head = head;
    }

    public CodeInfo getBody() {
        return body;
    }

    public void setBody(CodeInfo body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return body.toString();
    }
}
