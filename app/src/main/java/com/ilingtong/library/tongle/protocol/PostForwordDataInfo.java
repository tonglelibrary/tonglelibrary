package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: cuishuang
 * Date: 2015/7/13
 * Time: 14:22
 * Email: shuangcui@isoftstone.com
 * Desc: 帖子转发数据类
 */
public class PostForwordDataInfo implements Serializable {
    private String my_repost_id;
    private String post_link_url;
    private String post_image_url;
    private String magazine_title;  //帖子标题
    private String wx_profile;   //微信转帖说明  add at 2016/3/21


    public String getMy_repost_id() {
        return my_repost_id;
    }

    public void setMy_repost_id(String my_repost_id) {
        this.my_repost_id = my_repost_id;
    }

    public String getPost_link_url() {
        return post_link_url;
    }

    public void setPost_link_url(String post_link_url) {
        this.post_link_url = post_link_url;
    }

    public String getPost_image_url() {
        return post_image_url;
    }

    public void setPost_image_url(String post_image_url) {
        this.post_image_url = post_image_url;
    }
    public String getMagazine_title() {
        return magazine_title;
    }

    public void setMagazine_title(String magazine_title) {
        this.magazine_title = magazine_title;
    }

    public String getWx_profile() {
        return wx_profile;
    }

    public void setWx_profile(String wx_profile) {
        this.wx_profile = wx_profile;
    }
}
