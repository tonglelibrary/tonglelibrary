package com.ilingtong.library.tongle.utils;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;

import com.ilingtong.library.tongle.TongleAppInstance;

public class SelectPicUtils {
	/** custom icon */
	public static final int REQUEST_CUSTOM_ICON = 1;
	/** custom style */
	public static final int REQUEST_CUSTOM_CAMERA = 2;
	/** custom theme */
	public static final int REQUEST_CUSTOM_PHOTO = 3;

	/**
	 * 打开相册选择图片
	 * 
	 * @param paramActivity
	 * @param requestCode
	 */
	public static void openPhoto(Activity paramActivity, int requestCode) {
		Intent intent = new Intent(Intent.ACTION_PICK, null);
		intent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");
		paramActivity.startActivityForResult(intent, requestCode);
	}

	/**
	 * 打开相册选择图片
	 * 
	 * @param paramFragment
	 * @param requestCode
	 */
	public static void openPhoto(Fragment paramFragment, int requestCode) {
		Intent intent = new Intent(Intent.ACTION_PICK, null);
		intent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");
		paramFragment.startActivityForResult(intent, requestCode);
	}

	/**
	 * 裁剪图片
	 * 
	 * @param uri
	 */
	public static void startPhotoZoom(Fragment framgnet, int requestCode, Uri uri) {
		Intent intent = new Intent("com.android.camera.action.CROP");
		intent.setDataAndType(uri, "image/*");
		// crop为true是设置在开启的intent中设置显示的view可以剪裁
		intent.putExtra("crop", "true");
		// aspectX aspectY 是宽高的比例
		intent.putExtra("aspectX", 1);
		intent.putExtra("aspectY", 1);
		// outputX,outputY 是剪裁图片的宽高
		intent.putExtra("outputX", TongleAppInstance.sWidth);
		intent.putExtra("outputY", TongleAppInstance.sHeight);
		intent.putExtra("return-data", true);
		// intent.putExtra("return-data", false); //不返回图片的bitmap
		// intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
		// intent.putExtra("outputFormat",
		// Bitmap.CompressFormat.JPEG.toString());
		// intent.putExtra("noFaceDetection", true); // no face detection
		framgnet.startActivityForResult(intent, requestCode);
	}
}
