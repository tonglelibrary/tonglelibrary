package com.ilingtong.library.tongle.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.adapter.PostContentListAdapter;
import com.ilingtong.library.tongle.external.maxwin.view.XListView;
import com.ilingtong.library.tongle.fragment.MForumFragment;
import com.ilingtong.library.tongle.model.PostDetailModel;
import com.ilingtong.library.tongle.protocol.FavoriteListItem;
import com.ilingtong.library.tongle.protocol.MStoreDetailCollectResult;
import com.ilingtong.library.tongle.protocol.PostDetailResult;
import com.ilingtong.library.tongle.utils.ToastUtils;
import com.ilingtong.library.tongle.utils.DipUtils;
import com.nostra13.universalimageloader.core.ImageLoader;
import java.util.ArrayList;
import java.util.List;

import cn.sharesdk.framework.Platform;
/**
 * User: lengjiqiang
 * Date: 2015/5/20
 * Time: 13:44
 * Email: jqleng@isoftstone.com
 * Dest: 帖子详情
 */
public class CollectForumDetailActivity extends BaseActivity implements View.OnClickListener, XListView.IXListViewListener {
    private TextView top_name;
    private ImageView left_arrow_btn;
    private ImageView collect_btn;//收藏
    private ImageView QR_share_btn;//分享
    private XListView listview;
    private RelativeLayout rl_replace;
    private TextView tv_replace;
    private View contentView;
    private ImageView coverImage;//帖子顶部图片
    private TextView postTitle;//帖子名字
    private LinearLayout post_linear;//帖子布局
    private ImageView headIcon;//发帖人头像
    private TextView nickName;//昵称
    private TextView time;//发帖时间

    private String post_id;//帖子id
    private String user_nick_name;//名称
    private boolean bIsFavorate = false;

    private PostDetailModel postDetailModel;
    private ProgressDialog pDlg;
    private int intoType = 0; //表示从哪个页面跳转来

    private Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            List<Bitmap> newlist = (List<Bitmap>) msg.obj;
            PostContentListAdapter adapter = new PostContentListAdapter(CollectForumDetailActivity.this, postDetailModel.user_post_info, newlist);
            listview.setPullLoadEnable(false);
            listview.setPullRefreshEnable(true);
            listview.setXListViewListener(CollectForumDetailActivity.this, 0);
            listview.setRefreshTime();
            listview.setAdapter(adapter);
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.xlistview_comm_layout);
        getData();
        initView();
        doRequest();
    }

    public void getData() {
        post_id = getIntent().getExtras().getString("post_id");
        intoType = getIntent().getIntExtra(TongleAppConst.INTO_TYPE, 0);
    }

    public void initView() {
        top_name = (TextView) findViewById(R.id.top_name);
        left_arrow_btn = (ImageView) findViewById(R.id.left_arrow_btn);
        collect_btn = (ImageView) findViewById(R.id.collect_btn);
        QR_share_btn = (ImageView) findViewById(R.id.QR_share_btn);
        listview = (XListView) findViewById(R.id.xlistview);
        rl_replace = (RelativeLayout) findViewById(R.id.rl_replace);
        tv_replace = (TextView) findViewById(R.id.tv_replace);

        left_arrow_btn.setOnClickListener(this);
        collect_btn.setOnClickListener(this);
        QR_share_btn.setOnClickListener(this);

        rl_replace.setVisibility(View.VISIBLE);
        tv_replace.setVisibility(View.VISIBLE);
        listview.setVisibility(View.GONE);

        contentView = LayoutInflater.from(this).inflate(R.layout.collect_forum_detail_header, null);
        coverImage = (ImageView) contentView.findViewById(R.id.cover_pic);
        //重新计算帖子详情顶部图片的高度  使图片宽充满整个屏幕  高宽比为16:9
        ViewGroup.LayoutParams params1 = coverImage.getLayoutParams();
        params1.height = (DipUtils.getScreenWidth(this) * 9 / 16);
        coverImage.setLayoutParams(params1);

        headIcon = (ImageView) contentView.findViewById(R.id.head_photo);
        post_linear = (LinearLayout) contentView.findViewById(R.id.post_linear);
        post_linear.setOnClickListener(this);
        headIcon.setOnClickListener(this);
        nickName = (TextView) contentView.findViewById(R.id.nick_name);
        postTitle = (TextView) contentView.findViewById(R.id.post_title);

        top_name.setVisibility(View.VISIBLE);
        time = (TextView) contentView.findViewById(R.id.post_time);
        left_arrow_btn.setVisibility(View.VISIBLE);
        QR_share_btn.setImageDrawable(getResources().getDrawable(R.drawable.btn_share_select));
        QR_share_btn.setScaleType(ImageView.ScaleType.CENTER);

        listview.addHeaderView(contentView);
        postDetailModel = new PostDetailModel();

        //创建并弹出“加载中Dialog”
        pDlg = new ProgressDialog(this);
        pDlg.setCancelable(false);
        pDlg.setMessage(getString(R.string.loading));
        pDlg.show();
    }

    public void doRequest() {
        ServiceManager.doPostDetailRequest(TongleAppInstance.getInstance().getUserID(), post_id, successListener(), errorListener());
    }

    @Override
    public void onRefresh(int id) {
        doRequest();
    }

    @Override
    public void onLoadMore(int id) {

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.left_arrow_btn) {
            finish();
        } else if (v.getId() == R.id.collect_btn) {
            if (ToastUtils.isFastClick()) {
                return;
            } else {
                if (bIsFavorate == false) {
                    //帖子收藏
                    ArrayList<FavoriteListItem> favList = new ArrayList<FavoriteListItem>();
                    FavoriteListItem favListItem = new FavoriteListItem();
                    favListItem.collection_type = TongleAppConst.POST;
                    favListItem.key_value = post_id;
                    favList.add(favListItem);
                    ServiceManager.doMStoreCollectRequest(TongleAppInstance.getInstance().getUserID(), favList, "", collectListener(), errorListener());
                } else {
                    //取消收藏
                    ServiceManager.doMStoreCancelCollectRequest(TongleAppInstance.getInstance().getUserID(), post_id, TongleAppConst.POST, collectListener(), errorListener());
                }
            }
        } else if (v.getId() == R.id.QR_share_btn) {
            ShareDialogActivity.launch(CollectForumDetailActivity.this,ShareDialogActivity.TYPE_SHARE_POST,post_id,"");
        } else if (v.getId() == R.id.post_linear) {
            if (!TextUtils.isEmpty(postDetailModel.user_post_info.first_user_id) && !TextUtils.isEmpty(postDetailModel.user_post_info.user_favorite_by_me)) {
                Intent i = new Intent(this, CollectExpertDetailActivity.class);
                i.putExtra("user_id", postDetailModel.user_post_info.first_user_id);
                i.putExtra("user_favorited_by_me", postDetailModel.user_post_info.user_favorite_by_me);
                startActivity(i);
            }
        } else if (v.getId() == R.id.head_photo) {
            Intent i = new Intent(this, CollectExpertDetailActivity.class);
            i.putExtra("user_id", postDetailModel.user_post_info.first_user_id);
            i.putExtra("user_favorited_by_me", postDetailModel.user_post_info.user_favorite_by_me);
            startActivity(i);
        }
    }

    /**
     * 功能：订单详情请求网络响应失败
     */

    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ToastUtils.toastShort(getString(R.string.sys_exception));
                pDlg.dismiss();
            }
        };
    }

    /**
     * 功能：获取帖子详情网络响应成功，返回数据
     */
    private Response.Listener successListener() {
        return new Response.Listener<PostDetailResult>() {
            @Override
            public void onResponse(PostDetailResult response) {
                if (response.getHead().getReturn_flag().equals(TongleAppConst.SUCCESS)) {
                    postDetailModel.user_post_info = response.getBody().getUser_post_info();
                    //如果没数据，显示空白页
                    if (TextUtils.isEmpty(postDetailModel.user_post_info.user_id)) {
                        collect_btn.setVisibility(View.GONE);
                        QR_share_btn.setVisibility(View.GONE);
                    } else {
                        collect_btn.setVisibility(View.VISIBLE);
                        QR_share_btn.setVisibility(View.VISIBLE);
                        listview.setVisibility(View.VISIBLE);
                    }
                    try {

                        ImageLoader.getInstance().displayImage(postDetailModel.user_post_info.cover_picture_url, coverImage, TongleAppInstance.options);
                        ImageLoader.getInstance().displayImage(postDetailModel.user_post_info.first_user_head_photo_url, headIcon, TongleAppInstance.headIcon_options);
                        postTitle.setText(postDetailModel.user_post_info.first_post_title);
                        nickName.setText(postDetailModel.user_post_info.first_user_nick_name);
                        time.setText(postDetailModel.user_post_info.post_time);
                        // imageload(postDetailModel.user_post_info.cover_picture_url);
                        //设置标题名
                        user_nick_name = postDetailModel.user_post_info.first_user_nick_name;
                        top_name.setText(user_nick_name);

                        //0已收藏
                        if (postDetailModel.user_post_info.post_favorited_by_me.equals("0")) {
                            collect_btn.setBackgroundResource(R.drawable.discollect_button_style);
                            bIsFavorate = true;
                        } else {
                            collect_btn.setBackgroundResource(R.drawable.collect_button_style);
                            bIsFavorate = false;
                        }
                    } catch (Exception e) {

                    }
                    mHandler.sendEmptyMessage(0);
                } else {
                    ToastUtils.toastShort(getString(R.string.para_exception) + response.getHead().getReturn_message());
                }
                pDlg.dismiss();

            }
        };
    }

    /**
     * 功能：添加收藏请求网络响应成功，返回数据
     */
    private Response.Listener collectListener() {
        return new Response.Listener<MStoreDetailCollectResult>() {
            @Override
            public void onResponse(MStoreDetailCollectResult response) {
                if (TongleAppConst.ADD_FAVORITE.equals(response.getHead().getFunction_id())) {
                    if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                        collect_btn.setBackgroundResource(R.drawable.discollect_button_style);
                        ToastUtils.toastShort(getString(R.string.favorite_success));
                        bIsFavorate = true;
                        if (intoType == TongleAppConst.HOME_INTO) {
                            MForumFragment.MFORUMFRAGMENT_UPDATE_FLAG = true;
                        } else if (intoType == TongleAppConst.COLLECTFRAGMENT_INTO) { //表示从收藏页跳转来。收藏成功不刷新
                            MForumFragment.UPDATE_LIST_FLAG = false;
                        } else {
                            MForumFragment.MFORUMFRAGMENT_UPDATE_FLAG = true; //表示显示MForumFragment时刷新
                        }
                    } else {
                        ToastUtils.toastShort(getString(R.string.para_exception) + response.getHead().getReturn_message());
                    }
                } else if (TongleAppConst.CANCEL_FAVORITE.equals(response.getHead().getFunction_id())) {
                    if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                        collect_btn.setBackgroundResource(R.drawable.collect_button_style);
                        ToastUtils.toastShort(getString(R.string.favorite_cancel));
                        bIsFavorate = false;
                        if (intoType == TongleAppConst.HOME_INTO) {
                            MForumFragment.MFORUMFRAGMENT_UPDATE_FLAG = true;
                        } else if (intoType == TongleAppConst.COLLECTFRAGMENT_INTO) {//表示从收藏页跳转来。取消收藏刷新
                            MForumFragment.UPDATE_LIST_FLAG = true;
                        } else {
                            MForumFragment.MFORUMFRAGMENT_UPDATE_FLAG = true; //表示显示MForumFragment时刷新
                        }
                    } else {
                        ToastUtils.toastShort(getString(R.string.para_exception) + response.getHead().getReturn_message());
                    }
                }

            }
        };
    }

}
