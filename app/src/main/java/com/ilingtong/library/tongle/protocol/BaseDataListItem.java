package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * User: lengjiqiang
 * Date: 2015/6/9
 * Time: 11:04
 * Email: jqleng@isoftstone.com
 * Desc: 基础数据列表中的元素类型，
 */
public class BaseDataListItem implements Serializable {
    public String base_data_type;
    public ArrayList<DataListItem> data_list;
}
