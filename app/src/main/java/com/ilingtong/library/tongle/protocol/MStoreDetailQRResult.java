package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/6/4
 * Time: 16:57
 * Email: jqleng@isoftstone.com
 * Desc:
 */
public class MStoreDetailQRResult implements Serializable {
    private BaseInfo head;
    private MStoreDetailQRInfo body;

    public BaseInfo getHead() {
        return head;
    }

    public void setHead(BaseInfo head) {
        this.head = head;
    }

    public MStoreDetailQRInfo getBody() {
        return body;
    }

    public void setBody(MStoreDetailQRInfo body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return body.toString();
    }
}
