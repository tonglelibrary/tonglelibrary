package com.ilingtong.library.tongle.model;

import com.ilingtong.library.tongle.protocol.ShopCartListItem;

import java.util.ArrayList;

/**
 * User: lengjiqiang
 * Date: 2015/6/2
 * Time: 13:59
 * Email: jqleng@isoftstone.com
 * Desc: 保存购物车数据的模型类
 */
public class CartModel {
    // 购物车
    public ArrayList<ShopCartListItem> my_shopping_cart;
    public static ArrayList<ShopCartListItem> checkList = new ArrayList<>();
}
