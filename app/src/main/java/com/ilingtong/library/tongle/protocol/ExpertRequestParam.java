package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/5/9
 * Time: 15:22
 * Email: jqleng@isoftstone.com
 * Dest: 达人
 */
public class ExpertRequestParam implements Serializable {
    public String user_id;
    public String expert_user_id;
    public String forward;
    public String fetch_count;
    public String fans_user_id;
}
