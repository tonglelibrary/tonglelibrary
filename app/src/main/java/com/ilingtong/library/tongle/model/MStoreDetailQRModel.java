package com.ilingtong.library.tongle.model;

import android.app.Activity;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.protocol.ExpertDetailData;
import com.ilingtong.library.tongle.protocol.GsonRequest;
import com.ilingtong.library.tongle.protocol.ParametersJson;

import java.util.HashMap;
import java.util.Map;
import java.util.Observable;

/**
 * User: shuailei
 * Date: 2015/5/122
 * Time: 15:54
 * Email:leishuai@isoftstone.com
 * Dest:
 */
public class MStoreDetailQRModel extends Observable {
    final private String productUrl = TongleAppConst.SERVER_ADDRESS + ":" + TongleAppInstance.getInstance().getToken() + "/qrcode/mstore_show";
    private String TAG = "MSTOREDETAILQRMODEL";
    Activity appActivity;
    public ExpertDetailData productData = new ExpertDetailData();

    public MStoreDetailQRModel(Activity activity) {
        appActivity = activity;
    }

    public void requestProductModel(String user_id, String mstore_id) {
        RequestQueue queue = Volley.newRequestQueue(appActivity);
        Map<String, String> requestParam = new HashMap<String, String>();
        requestParam.put("user_id", user_id);
        requestParam.put("mstore_id", mstore_id);
        Gson mGson = new Gson();
        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);

        GsonRequest<ParametersJson> gsonRequest = new GsonRequest<ParametersJson>(Request.Method.POST, productUrl, ParametersJson.class, params_gson, new Response.Listener<ParametersJson>() {
            @Override
            public void onResponse(ParametersJson parameters_json) {
                productData.mstore_qr_code_url = parameters_json.body.mstore_qr_code_url;
                setChanged();
                notifyObservers(TAG);
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG + " - ERROR", error.getMessage(), error);
            }
        });
        queue.add(gsonRequest);
    }
}
