package com.ilingtong.library.tongle.model;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.protocol.GsonRequest;
import com.ilingtong.library.tongle.protocol.ParametersJson;
import com.ilingtong.library.tongle.protocol.ProductRatingListItem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;

/**
 * User: lengjiqiang
 * Date: 2015/5/26
 * Time: 10:01
 * Email: jqleng@isoftstone.com
 * Dest:
 */
public class ProdCommentModel extends Observable {
    Context contextModel;
    private String TAG = "PRODUCT_COMMENT";
    // input parameters
    public String user_id;
    public String product_id;
    public String level;
    public String rating_doc_no;
    public String forward;
    public String fetch_count;

    //output parameters
    public ArrayList<ProductRatingListItem> product_rating_list;

    final private String commentUrl = TongleAppConst.SERVER_ADDRESS + ":" + TongleAppInstance.getInstance().getToken() + "/products/evaluation_show";

    public ProdCommentModel(Context context) {
        contextModel = context;
    }

    public void HttpRequest() {
        RequestQueue queue = Volley.newRequestQueue(contextModel);
        Map<String, String> requestParam = new HashMap<String, String>();
        requestParam.put("user_id", user_id);
        requestParam.put("product_id", product_id);
        requestParam.put("level", level);
        requestParam.put("rating_doc_no", rating_doc_no);
        requestParam.put("forward", forward);
        requestParam.put("fetch_count", fetch_count);
        Gson mGson = new Gson();
        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);

        GsonRequest<ParametersJson> gsonRequest = new GsonRequest<ParametersJson>(Request.Method.POST, commentUrl, ParametersJson.class, params_gson, new Response.Listener<ParametersJson>() {
            @Override
            public void onResponse(ParametersJson parameters_json) {
                product_rating_list = parameters_json.body.product_rating_list;

                setChanged();
                notifyObservers(TAG);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG + " - ERROR", error.getMessage(), error);
            }
        });
        queue.add(gsonRequest);
    }

}