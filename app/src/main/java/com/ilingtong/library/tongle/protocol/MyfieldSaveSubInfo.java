package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: shuailei
 * Date: 2015/6/15
 * Time: 12:05
 * Email: leishuai@isoftstone.com
 * Desc: 领域子界面封装类
 */
public class MyfieldSaveSubInfo implements Serializable {
    public String type_id;
    public String type_name;

    @Override
    public String toString() {
        return "type_id:" + type_id + "\r\n" +
                "type_name:" + type_name;
    }
}
