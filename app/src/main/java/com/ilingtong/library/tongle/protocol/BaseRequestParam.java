package com.ilingtong.library.tongle.protocol;

import com.ilingtong.library.tongle.TongleAppInstance;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/5/6.
 * mail: wuqian@ilingtong.com
 * Description:
 */
public class BaseRequestParam implements Serializable{
    private String user_id;
    private String user_token;
    private String app_inner_no;

    public BaseRequestParam(String user_id, String user_token) {
        this.user_id = user_id;
        this.app_inner_no = TongleAppInstance.getApp_inner_no();
        this.user_token = user_token;
    }
}
