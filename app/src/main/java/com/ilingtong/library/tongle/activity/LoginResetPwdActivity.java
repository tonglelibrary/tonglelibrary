package com.ilingtong.library.tongle.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.protocol.ResetPwdParam;
import com.ilingtong.library.tongle.protocol.ResetPwdResult;
import com.ilingtong.library.tongle.utils.ToastUtils;
import com.ilingtong.library.tongle.widget.ClearEditText;

/**
 * User: lengjiqiang
 * Date: 2015/6/17
 * Time: 17:15
 * Email: jqleng@isoftstone.com
 * Desc: 密码重置页
 */
public class LoginResetPwdActivity extends BaseActivity implements View.OnClickListener {
    private TextView top_name;
    private ImageView left_arrow_btn;
    private ClearEditText new_pwd_edit;
    private ClearEditText confirm_edit;
    private Button reset_btn;
    private String strNewPwd;
    private String strConfirm;
    private ProgressDialog pDlg;
    private String strCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_reset_pwd_second_layout);
        getData();
        initView();
    }
    public void getData(){
        strCode = getIntent().getStringExtra("verification_code");
    }
    public void initView() {
        top_name = (TextView) findViewById(R.id.top_name);
        left_arrow_btn = (ImageView) findViewById(R.id.left_arrow_btn);
        new_pwd_edit = (ClearEditText) findViewById(R.id.new_pwd_edit);
        confirm_edit = (ClearEditText) findViewById(R.id.confirm_edit);
        reset_btn = (Button) findViewById(R.id.reset_btn);
        left_arrow_btn.setOnClickListener(this);
        reset_btn.setOnClickListener(this);
        left_arrow_btn.setVisibility(View.VISIBLE);
        top_name.setText(R.string.reset_password);
        top_name.setVisibility(View.VISIBLE);

        new_pwd_edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                strNewPwd = new_pwd_edit.getText().toString();
                strConfirm = confirm_edit.getText().toString();

                if (strNewPwd.equals("") || strConfirm.equals("")) {
                    reset_btn.setBackgroundResource(R.drawable.button_forbid_style);
                } else {
                    reset_btn.setBackgroundResource(R.drawable.button_style);
                }
            }
        });
        confirm_edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                strNewPwd = new_pwd_edit.getText().toString();
                strConfirm = confirm_edit.getText().toString();

                if (strNewPwd.equals("") || strConfirm.equals("")) {
                    reset_btn.setBackgroundResource(R.drawable.button_forbid_style);
                } else {
                    reset_btn.setBackgroundResource(R.drawable.button_style);
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.left_arrow_btn) {
            finish();
        } else if (v.getId() == R.id.reset_btn) {
            strNewPwd = new_pwd_edit.getText().toString();
            strConfirm = confirm_edit.getText().toString();
            pDlg = new ProgressDialog(this);
            pDlg.setCancelable(false);
            pDlg.setMessage(getString(R.string.loading));
            ResetPwdParam param = new ResetPwdParam();
            param.phone_number = TongleAppInstance.getInstance().getUser_phone();
            param.password = strNewPwd;
            param.confirm_password = strConfirm;
            param.verification_code = strCode;
            //新密码长度验证
            if (strNewPwd.length() < TongleAppConst.PASSWORD_LENGTH) {
                ToastUtils.toastShort(getString(R.string.regist_password_error));
                return;
            }
            //确认新密码长度验证
            if (strConfirm.length() < TongleAppConst.PASSWORD_LENGTH) {
                ToastUtils.toastShort(getString(R.string.regist_password_error));
                return;
            }
            //两次密码比较
            if (!strNewPwd.equals(strConfirm)) {
                ToastUtils.toastShort(getResources().getString(R.string.modify_newpass_error));
                return;
            }
            if (!TextUtils.isEmpty(strNewPwd) & !TextUtils.isEmpty(strNewPwd) && !TextUtils.isEmpty(strConfirm))
                if (!"".equals(strNewPwd) && !"".equals(strConfirm)) {
                    ServiceManager.doRestPwdRquest(param, successListener(), errorListener());
                } else {
                    ToastUtils.toastShort(getString(R.string.reset_pwd_different_error));
                }
        }
    }

    /**
     * 功能：网络响应成功，返回数据
     */
    private Response.Listener<ResetPwdResult> successListener() {
        return new Response.Listener<ResetPwdResult>() {
            @Override
            public void onResponse(ResetPwdResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    SharedPreferences sp = getApplicationContext().getSharedPreferences("login", Context.MODE_PRIVATE);
                    sp.edit().putString("Password", "").commit();
                    sp.edit().putBoolean("IfLogin", false).commit();
                    sp.edit().putBoolean("IfRember", false).commit();

                    ToastUtils.toastShort(getString(R.string.modify_success));
                    LoginResetPwdActivity.this.finish();
                    if (LoginPwdRecoveryActivity.instance != null) {
                        LoginPwdRecoveryActivity.instance.finish();
                    }
                } else {
                    ToastUtils.toastShort(getResources().getString(R.string.para_exception) + response.getHead().getReturn_message());
                }
                pDlg.dismiss();
            }
        };
    }

    /**
     * 功能：网络响应失败
     */
    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ToastUtils.toastShort(getResources().getString(R.string.sys_exception));
                pDlg.dismiss();
            }
        };
    }
}
