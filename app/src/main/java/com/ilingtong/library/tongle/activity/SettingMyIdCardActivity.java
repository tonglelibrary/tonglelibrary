package com.ilingtong.library.tongle.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.utils.ToastUtils;
import com.ilingtong.library.tongle.utils.Utils;

/**
 * User: lengjiqiang
 * Date: 2015/6/7
 * Time: 22:41
 * Email: jqleng@isoftstone.com
 * Desc: 修改身份证号码
 */
public class SettingMyIdCardActivity extends BaseActivity implements View.OnClickListener {
    private ImageView left_arrow_btn;
    private TextView top_name;
    private TextView top_btn_text;
    private EditText edit_name;
    private String name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_idcard_layout);
        initView();
    }

    public void initView() {
        top_name = (TextView) findViewById(R.id.top_name);
        top_btn_text = (TextView) findViewById(R.id.top_btn_text);
        edit_name = (EditText)findViewById(R.id.edit_name);
        left_arrow_btn = (ImageView) findViewById(R.id.left_arrow_btn);

        top_name.setText(getString(R.string.setting_my_id_card_top_name));
        top_btn_text.setText(getString(R.string.setting_my_id_card_top_txt));
        top_name.setVisibility(View.VISIBLE);
        top_name.setVisibility(View.VISIBLE);
        top_btn_text.setVisibility(View.VISIBLE);
        left_arrow_btn.setVisibility(View.VISIBLE);
        top_btn_text.setOnClickListener(this);
        left_arrow_btn.setOnClickListener(this);

        Bundle bundle = getIntent().getExtras();
        name = bundle.getString("name");
        edit_name.setText(name+"");
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.left_arrow_btn) {
            finish();
        } else if (v.getId() == R.id.top_btn_text) {
            String text = edit_name.getText().toString();
            if (text.length() != TongleAppConst.ID_NUMBER_LENGTH_five && text.length() != TongleAppConst.ID_NUMBER_LENGTH_eight) {
                ToastUtils.toastShort(getString(R.string.setting_my_id_card_error));
            } else if (!Utils.isIdCardcode(text)) {
                ToastUtils.toastShort(getString(R.string.setting_my_id_card_error));
            } else {
                TongleAppInstance.getInstance().setUser_id_no(text);
                Intent intent = new Intent();
                intent.putExtra("text", text);
                setResult(RESULT_OK, intent);
                finish();
            }

        }
    }
}
