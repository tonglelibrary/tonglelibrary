package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;
import java.util.ArrayList;


/**
 * User: shuailei
 * Date: 2015/6/10
 * Time: 13:01
 * Email: leishuai@isoftstone.com
 * Desc:
 */
public class OrderDetailOrderDetailInfo implements Serializable {
    public String order_detail_no;
    public String prod_pic_url;
    public String prod_id;
    public String post_id;
    public String prod_name;
    public String price;
    public String quantity;
    public String amount;
    public String fee_amount;
    public ArrayList<OrderDetaiProdSpecListInfo> prod_spec_list;
    public String country_pic_url;
    public String import_info_desc;
    public String transfer_fee_desc;
    public String tariff_desc;
    public String import_goods_flag;
    public String goods_eva_flag;  //可评价标志 add at 2016/04/22 （之前漏加）
    public String relation_id;  //add at 2016/04/07

    @Override
    public String toString() {
        return "order_detail_no:" + order_detail_no + "\r\n" +
                "prod_pic_url:" + prod_pic_url + "\r\n" +
                "prod_id:" + prod_id + "\r\n" +
                "post_id:" + post_id + "\r\n" +
                "prod_name:" + prod_name + "\r\n" +
                "price:" + price + "\r\n" +
                "quantity:" + quantity + "\r\n" +
                "amount:" + amount + "\r\n" +
                "fee_amount:" + fee_amount + "\r\n" +
                "country_pic_url:" + country_pic_url + "\r\n" +
                "import_info_desc:" + import_info_desc + "\r\n" +
                "transfer_fee_desc:" + transfer_fee_desc + "\r\n" +
                "tariff_desc:" + tariff_desc + "\r\n" +
                "import_goods_flag:" + import_goods_flag + "\r\n" +
                "prod_spec_list:" + prod_spec_list;
    }
}
