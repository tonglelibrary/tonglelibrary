package com.ilingtong.library.tongle.fragment;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.adapter.ProductSpecListAdapter;
import com.ilingtong.library.tongle.protocol.ProductSpecResult;
import com.ilingtong.library.tongle.utils.DialogUtils;
import com.ilingtong.library.tongle.utils.ToastUtils;

/**
 * User: lengjiqiang
 * Date: 2015/5/25
 * Time: 13:56
 * Email: jqleng@isoftstone.com
 * Dest: 产品规格
 */
public class ProductSpecFragment extends Fragment {
    private ProductSpecListAdapter specListAdapter, specListAdapter1;
    private ListView specListView1;
    private ListView specListView2;
    private TextView product_spec_textview1;
    private TextView product_spec_textview2;
    private RelativeLayout rl_replace;
    private LinearLayout allLayout;
    // private ProgressDialog pDlg;
    private Dialog mDialog;
    private String prodID;

    View mainView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (mainView == null) {
            mainView = inflater.inflate(R.layout.fragment_product_spce_layout, null);
            getData();
            initView(mainView);
            doRequest();
        }
        return mainView;
    }

    public void getData() {
        prodID = getActivity().getIntent().getExtras().getString("product_id");
    }

    public void initView(View mainView) {
        specListView1 = (ListView) mainView.findViewById(R.id.product_spec_listview1);
        specListView2 = (ListView) mainView.findViewById(R.id.product_spec_listview2);
        product_spec_textview1 = (TextView) mainView.findViewById(R.id.product_spec_textview1);
        product_spec_textview2 = (TextView) mainView.findViewById(R.id.product_spec_textview2);
        rl_replace = (RelativeLayout) mainView.findViewById(R.id.rl_replace);
        allLayout = (LinearLayout) mainView.findViewById(R.id.all_layout);
        //创建并弹出“加载中Dialog”
        mDialog = DialogUtils.createLoadingDialog(getActivity());
//        pDlg = new ProgressDialog(getActivity());
//        pDlg.setCancelable(false);
//        pDlg.setMessage(getString(R.string.loading));
//        pDlg.show();
    }

    public void doRequest() {
        mDialog.show();
        ServiceManager.productSpecRequest(TongleAppInstance.getInstance().getUserID(), prodID, successListener(), errorListener());
    }

    /**
     * 功能：魔店详情网络响应成功，返回数据
     */
    private Response.Listener successListener() {
        return new Response.Listener<ProductSpecResult>() {
            @Override
            public void onResponse(ProductSpecResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    if (response.getBody().getProd_parameter_list().size() > 0) {
                        product_spec_textview1.setText(response.getBody().getProd_parameter_list().get(0).module_name);
                        specListAdapter = new ProductSpecListAdapter(getActivity(), response.getBody().getProd_parameter_list().get(0).parameters);
                        specListView1.setAdapter(specListAdapter);

                        if (response.getBody().getProd_parameter_list().size() > 1) {
                            product_spec_textview2.setText(response.getBody().getProd_parameter_list().get(1).module_name);
                            specListAdapter1 = new ProductSpecListAdapter(getActivity(), response.getBody().getProd_parameter_list().get(1).parameters);
                            specListView2.setAdapter(specListAdapter1);
                        }
                    } else {
                        allLayout.setVisibility(View.GONE);
                        rl_replace.setVisibility(View.VISIBLE);
                    }
                } else {
                    ToastUtils.toastShort(getString(R.string.para_exception) + response.getHead().getReturn_message());
                }
                // pDlg.dismiss();
                mDialog.dismiss();
            }
        };
    }

    /**
     * 功能：魔店详情请求网络响应失败
     */
    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ToastUtils.toastShort(getString(R.string.sys_exception));
                //pDlg.dismiss();
                mDialog.dismiss();
            }
        };
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ViewParent parent = mainView.getParent();
        if(parent !=null)
        {
            ((ViewGroup)parent).removeView(mainView);
        }
    }
}
