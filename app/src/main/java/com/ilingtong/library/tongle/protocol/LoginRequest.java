package com.ilingtong.library.tongle.protocol;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * User: lengjiqiang
 * Date: 2015/5/5
 * Time: 18:53
 * Email: jqleng@isoftstone.com
 * Dest:
 */
public class LoginRequest extends BaseRequest {
    public String strPhoneNumber;
    public String strPassword;

    public void fromJson(JSONObject jsonObject) throws JSONException {
        if (null == jsonObject) {
            return;
        }
        this.strPhoneNumber = jsonObject.optString("phone_number");
        this.strPassword = jsonObject.optString("password");
        return;
    }

    public JSONObject toJson() throws JSONException {
        Map<String, String> map = new HashMap<String, String>();
        map.put("phone_number", strPhoneNumber);
        map.put("password", strPassword);
//        JSONObject jsonObject = new JSONObject(map);
        return toParam(map);
    }
}
