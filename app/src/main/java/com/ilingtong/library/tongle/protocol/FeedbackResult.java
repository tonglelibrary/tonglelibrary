package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/6/23
 * Time: 10:36
 * Email: jqleng@isoftstone.com
 * Desc: 意见反馈 类
 */
public class FeedbackResult implements Serializable {
    private BaseInfo head;
    private FeedbackInfo body;

    public BaseInfo getHead() {
        return head;
    }

    public void setHead(BaseInfo head) {
        this.head = head;
    }

    public FeedbackInfo getBody() {
        return body;
    }

    public void setBody(FeedbackInfo body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return body.toString();
    }
}
