package com.ilingtong.library.tongle.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.protocol.AddressListItem;

import java.util.ArrayList;
import java.util.List;

/**
 * User: lengjiqiang
 * Date: 2015/6/9
 * Time: 22:30
 * Email: jqleng@isoftstone.com
 * Desc:
 */
public class AddressListAdapter extends BaseAdapter {
    private LayoutInflater inflater;
    private List<AddressListItem> list;
    private AddressListItem item = new AddressListItem();
    private Context mContext;

    public AddressListAdapter(Context context, ArrayList addressList) {
        mContext = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.list = addressList;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder holder;
        if (view == null) {
            view = inflater.inflate(R.layout.list_item_address, null);
            holder = new ViewHolder();
           // holder.listItemImageView = (ImageView) view.findViewById(R.id.cart_list_item_image);
            holder.personName = (TextView) view.findViewById(R.id.person_name);
            holder.phoneNumber = (TextView) view.findViewById(R.id.phone_number);
            holder.address = (TextView) view.findViewById(R.id.address);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        item = (AddressListItem) getItem(position);
        holder.personName.setText(mContext.getString(R.string.adapter_address_list_person_name)+item.consignee);
        holder.phoneNumber.setText(mContext.getString(R.string.adapter_address_list_phone_number)+item.tel);
        holder.address.setText(item.province_name+item.city_name+item.area_name+item.address);
        return view;
    }
    static class ViewHolder {
        TextView personName;
        TextView phoneNumber;
        TextView address;
       // ImageView listItemImageView;
    }
}

