package com.ilingtong.library.tongle.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.external.MySpinnerView;
import com.ilingtong.library.tongle.model.AddressModel;
import com.ilingtong.library.tongle.model.ConfirmOrderModel;
import com.ilingtong.library.tongle.protocol.AddInfo;
import com.ilingtong.library.tongle.protocol.AddressResult;
import com.ilingtong.library.tongle.protocol.BaseDataNode;
import com.ilingtong.library.tongle.protocol.BaseDataListItem;
import com.ilingtong.library.tongle.protocol.BaseDataResult;
import com.ilingtong.library.tongle.protocol.DataListItem;
import com.ilingtong.library.tongle.utils.ToastUtils;
import com.ilingtong.library.tongle.utils.Utils;
import com.ilingtong.library.tongle.widget.ClearEditText;

import java.util.ArrayList;
import java.util.List;

/**
 * User: lengjiqiang
 * Date: 2015/6/10
 * Time: 17:55
 * Email: jqleng@isoftstone.com
 * Desc: 新建收货地址页
 */
public class AddressNewActivity extends BaseActivity implements View.OnClickListener {
    private TextView top_name;
    private TextView top_btn_text;
    private ImageView left_arrow_btn;
    private ClearEditText to_name;
    private ClearEditText to_tel;
    private ClearEditText post_code;
    private EditText detail_address;
    private MySpinnerView provinceSpinner;
    private MySpinnerView citySpinner;
    private MySpinnerView districSpinner;

    ArrayList<BaseDataNode> proviceArray = new ArrayList<>();
    ArrayList<BaseDataNode> cityArray = new ArrayList<>();
    ArrayList<BaseDataNode> districArray = new ArrayList<>();
    ArrayList<DataListItem> provinceDataList = new ArrayList<>();
    ArrayList<DataListItem> cityDataList = new ArrayList<>();
    ArrayList<DataListItem> districDataList = new ArrayList<>();

    String mProvinceCode;   //省代码
    String mCityCode;       //城市代码
    String mDistricCode;    //区代码
    List list1 = new ArrayList();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.address_new_layout);
        initView();
        doRequest();
    }

    public void initView() {
        top_name = (TextView)findViewById(R.id.top_name);
        left_arrow_btn = (ImageView)findViewById(R.id.left_arrow_btn);
        top_btn_text = (TextView)findViewById(R.id.top_btn_text);
        to_name = (ClearEditText)findViewById(R.id.to_name);
        to_tel = (ClearEditText)findViewById(R.id.to_tel);
        post_code = (ClearEditText)findViewById(R.id.post_code);
        detail_address = (EditText)findViewById(R.id.detail_address);
        provinceSpinner = (MySpinnerView)findViewById(R.id.province_spinner);
        citySpinner = (MySpinnerView)findViewById(R.id.city_spinner);
        districSpinner = (MySpinnerView)findViewById(R.id.distric_spinner);
        top_btn_text.setOnClickListener(this);
        left_arrow_btn.setOnClickListener(this);

        left_arrow_btn.setVisibility(View.VISIBLE);
        left_arrow_btn.setOnClickListener(this);
        top_name.setText(R.string.select_address);
        top_name.setVisibility(View.VISIBLE);
        top_btn_text.setVisibility(View.VISIBLE);
        top_btn_text.setText(R.string.save);

        List provinceList = new ArrayList();
        provinceList.add("");
        ArrayAdapter<String> provinceAdapter = new ArrayAdapter<String>(AddressNewActivity.this,
                android.R.layout.simple_dropdown_item_1line, provinceList);
        provinceSpinner.setAdapter(provinceAdapter);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(AddressNewActivity.this,
                android.R.layout.simple_dropdown_item_1line, list1);
        list1.add(getString(R.string.address_new_select_province));
        citySpinner.setAdapter(adapter);
        List list2 = new ArrayList();
        list2.add(getString(R.string.address_new_select_city));
        ArrayAdapter<String> districAdapter = new ArrayAdapter<String>(AddressNewActivity.this,
                android.R.layout.simple_dropdown_item_1line, list2);
        districSpinner.setAdapter(districAdapter);

        provinceSpinner.setOnItemClickListener(new ProvinceSelectedListener());
        citySpinner.setOnItemClickListener(new citySelectedListener());
        districSpinner.setOnItemClickListener(new districSelectedListener());
    }

    public void doRequest() {
        if (ConfirmOrderModel.base_data_list == null || ConfirmOrderModel.base_data_list.size() < 0) {
            ToastUtils.toastShort(getString(R.string.common_get_base_data_error));
        } else {

            List list = new ArrayList<String>();
            for (int i = 0; i < ConfirmOrderModel.base_data_list.size(); i++) {
                BaseDataListItem item = new BaseDataListItem();
                item = ConfirmOrderModel.base_data_list.get(i);
                if (item.base_data_type.equals(TongleAppConst.ADMINISTRATIVE)) {
                    provinceDataList = item.data_list;
                    for (int j = 0; j < item.data_list.size(); j++) {
                        BaseDataNode areaItem = new BaseDataNode();
                        areaItem.code = item.data_list.get(j).code;
                        areaItem.name = item.data_list.get(j).name;
                        proviceArray.add(areaItem);

                        list.add(areaItem.name);
                    }
                }
            }

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(AddressNewActivity.this,
                    android.R.layout.simple_dropdown_item_1line, list);
            provinceSpinner.setAdapter(adapter);
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.top_btn_text) { //保存
            String consignee = to_name.getText().toString();
            String tel = to_tel.getText().toString();
            String address = detail_address.getText().toString();
            String postcode = post_code.getText().toString();
            AddInfo addInfo = new AddInfo();
            addInfo.consignee = consignee;
            addInfo.tel = tel;
            addInfo.post_code = postcode;
            addInfo.address = address;
            addInfo.province_id = mProvinceCode;
            addInfo.city_id = mCityCode;
            addInfo.area_id = mDistricCode;

            if (!Utils.isMobileNO(tel) || (tel.length() != TongleAppConst.PHONE_NUMBER_LENGTH)) {
                ToastUtils.toastShort(getString(R.string.regist_phone_error));
                return;
            }
//            if (!Utils.isPostcode(postcode)) {
//                ToastUtils.toastShort(getString(R.string.regist_post_error));
//                return;
//            }
            if (consignee.equals("")) {
                ToastUtils.toastShort(getString(R.string.common_username_null));
            } else if (tel.equals("")) {
                ToastUtils.toastShort(getString(R.string.common_phone_null));
            } else if (address.equals("")) {
                ToastUtils.toastShort(getString(R.string.common_detail_null));
            } else if (TextUtils.isEmpty(mProvinceCode)) {
                ToastUtils.toastShort(getString(R.string.address_new_select_province));
            } else if (TextUtils.isEmpty(mCityCode)) {
                ToastUtils.toastShort(getString(R.string.address_new_select_city));
            } else if (TextUtils.isEmpty(mDistricCode)) {
                ToastUtils.toastShort(getString(R.string.address_new_select_area));
            } else {
                //网络存储
                ServiceManager.doaddAddressRequest(TongleAppInstance.getInstance().getUserID(), addInfo, successListener(), errorListener());
            }
        } else if (v.getId() == R.id.left_arrow_btn) {
            finish();
        }
    }

    private class ProvinceSelectedListener implements AdapterView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            String provinceName = (String) ((TextView) view.findViewById(android.R.id.text1)).getText();
            if (proviceArray != null && proviceArray.size() > 0) {
                BaseDataNode provinceData = (BaseDataNode) proviceArray.get(position);
                mProvinceCode = provinceData.code;
                list1 = new ArrayList<String>();
                for (int i = 0; i < provinceDataList.size(); i++) {
                    if (provinceDataList.get(i).name.equals(provinceName)) {
                        cityDataList = provinceDataList.get(i).sub_list;
                        cityArray.clear();
                        for (int j = 0; j < cityDataList.size(); j++) {
                            BaseDataNode areaItem = new BaseDataNode();
                            areaItem.code = cityDataList.get(j).code;
                            areaItem.name = cityDataList.get(j).name;
                            cityArray.add(areaItem);
                            list1.add(areaItem.name);
                        }
                    }
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(AddressNewActivity.this,
                        android.R.layout.simple_dropdown_item_1line, list1);
                citySpinner.setAdapter(adapter);

            }
        }
    }

    private class citySelectedListener implements AdapterView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            if (TextUtils.isEmpty(mProvinceCode)) {
                ToastUtils.toastShort(getString(R.string.address_new_select_province));
            } else {
                String cityName = (String) ((TextView) view.findViewById(android.R.id.text1)).getText();
                BaseDataNode cityData = (BaseDataNode) cityArray.get(position);
                mCityCode = cityData.code;
                List list = new ArrayList<String>();
                for (int i = 0; i < cityDataList.size(); i++) {
                    if (cityDataList.get(i).name.equals(cityName)) {
                        districDataList = cityDataList.get(i).sub_list;
                        districArray.clear();
                        for (int j = 0; j < districDataList.size(); j++) {
                            BaseDataNode areaItem = new BaseDataNode();
                            areaItem.code = districDataList.get(j).code;
                            areaItem.name = districDataList.get(j).name;
                            districArray.add(areaItem);
                            list.add(areaItem.name);
                        }
                    }
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(AddressNewActivity.this,
                        android.R.layout.simple_dropdown_item_1line, list);
                districSpinner.setAdapter(adapter);
            }
        }
    }

    private class districSelectedListener implements AdapterView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            if (!TextUtils.isEmpty(mCityCode)) {
                BaseDataNode districData = (BaseDataNode) districArray.get(position);
                mDistricCode = districData.code;
            } else {
                ToastUtils.toastShort(getString(R.string.address_new_select_city));
            }
        }
    }

    /**
     * 功能：保存收货新建的地址响应成功，返回数据
     */
    private Response.Listener successListener() {
        return new Response.Listener<AddressResult>() {
            @Override
            public void onResponse(AddressResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    Toast.makeText(AddressNewActivity.this, getString(R.string.common_save_success), Toast.LENGTH_SHORT).show();
                    //更新内存中的地址列表
                    AddressModel.my_address_list = response.getBody().getMy_address_list();
                    finish();
                } else {
                    ToastUtils.toastShort(getString(R.string.para_exception) + response.getHead().getReturn_message());
                }
            }
        };
    }

    /**
     * 功能：获取基础数据网络响应成功，返回数据
     */
    private Response.Listener<BaseDataResult> baseDataListener() {
        return new Response.Listener<BaseDataResult>() {
            @Override
            public void onResponse(BaseDataResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    ConfirmOrderModel.base_data_list = response.getBody().getBase_data_list();
                    List list = new ArrayList<String>();

                    for (int i = 0; i < ConfirmOrderModel.base_data_list.size(); i++) {
                        BaseDataListItem item = new BaseDataListItem();
                        item = ConfirmOrderModel.base_data_list.get(i);
                        if (item.base_data_type.equals(TongleAppConst.ADMINISTRATIVE)) {
                            provinceDataList = item.data_list;
                            for (int j = 0; j < item.data_list.size(); j++) {
                                BaseDataNode areaItem = new BaseDataNode();
                                areaItem.code = item.data_list.get(j).code;
                                areaItem.name = item.data_list.get(j).name;
                                proviceArray.add(areaItem);

                                list.add(areaItem.name);
                            }
                        }
                    }

                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(AddressNewActivity.this,
                            android.R.layout.simple_dropdown_item_1line, list);
                    provinceSpinner.setAdapter(adapter);
                } else {
                    ToastUtils.toastShort(getString(R.string.para_exception) + response.getHead().getReturn_message());
                }
            }
        };
    }

    /**
     * 功能：网络响应失败
     */
    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ToastUtils.toastShort(getString(R.string.sys_exception));
            }
        };
    }
}
