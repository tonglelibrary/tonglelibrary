package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/5/25
 * Time: 17:30
 * Email: jqleng@isoftstone.com
 * Desc: 产品规格参数子列表属性类
 */
public class ProdParamsSubListItem implements Serializable {
    //规格名称
    public String item_name;
    //规则值
    public String item_value;
}
