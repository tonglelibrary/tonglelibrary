package com.ilingtong.library.tongle.activity;

import android.app.AlertDialog;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.adapter.CollectFragmentPagerAdapter;
import com.ilingtong.library.tongle.fragment.ExpertProductFragment;
import com.ilingtong.library.tongle.fragment.ExpertUpdateFragment;
import com.ilingtong.library.tongle.fragment.HomeFragment;
import com.ilingtong.library.tongle.fragment.MExpertFragment;
import com.ilingtong.library.tongle.protocol.ExpertBaseResult;
import com.ilingtong.library.tongle.protocol.FavoriteListItem;
import com.ilingtong.library.tongle.protocol.MStoreDetailCollectResult;
import com.ilingtong.library.tongle.protocol.ParametersJson;
import com.ilingtong.library.tongle.utils.ToastUtils;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

/**
 * User: shuailei
 * Date: 2015/5/20
 * Time: 15:21
 * Email: leishuai@isoftstone.com
 * Desc: 达人详情页
 */
public class CollectExpertDetailActivity extends BaseFragmentActivity implements View.OnClickListener {
    private ImageView imgButton, collect_btn, left_arrow_btn;
    public static TextView top_name;
    private boolean bIsFavorate = false;
    private String user_id, user_favorited_by_me;
    private Fragment[] fragments = new Fragment[2];

    private int intoType = 0; //表示从哪个页面进入
    private TextView txt_tab_update, txt_tab_product;
    private ImageView img_update_cursor, img_product_cursor;
    private int currIndex = 0;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expert_detail_layout);
        getData();
        initView();
        getExpertBaseInfo(user_id);
    }

    public void getData() {
        Bundle bundle = getIntent().getExtras();
        user_id = bundle.getString("user_id");
        user_favorited_by_me = bundle.getString("user_favorited_by_me");
        intoType = bundle.getInt(TongleAppConst.INTO_TYPE, 0);
    }

    public void doRequestQRCode() {
        ServiceManager.requestQRCode(TongleAppInstance.getInstance().getUserID(), user_id, successListener(), errorListener());
    }

    /**
     * 获取专家会员的基本信息，判断是否被我收藏。用来判断显示顶部title栏收藏图片状态
     *
     * @param user_id
     */
    private void getExpertBaseInfo(String user_id) {
        ServiceManager.doExpertBaseRequest(TongleAppInstance.getInstance().getUserID(), user_id, new Response.Listener<ExpertBaseResult>() {
            @Override
            public void onResponse(ExpertBaseResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    if (!TextUtils.isEmpty(response.getBody().user_favorited_by_me)) {
                        //0已收藏
                        if (response.getBody().user_favorited_by_me.equals("0")) {
                            collect_btn.setBackgroundResource(R.drawable.discollect_button_style);
                            bIsFavorate = true;
                        } else {
                            collect_btn.setBackgroundResource(R.drawable.collect_button_style);
                            bIsFavorate = false;
                        }
                    }
                    top_name.setText(response.getBody().user_nick_name+"");
                }
            }
        }, errorListener());
    }

    public void doRequestMStore() {
        //会员收藏
        ArrayList<FavoriteListItem> favList = new ArrayList<FavoriteListItem>();
        FavoriteListItem favListItem = new FavoriteListItem();
        favListItem.collection_type = TongleAppConst.COLLECT_TYPE_MEMBER;
        favListItem.key_value = user_id;
        favList.add(favListItem);
        ServiceManager.doMStoreCollectRequest(TongleAppInstance.getInstance().getUserID(), favList, "", collectListener(), errorListener());
    }

    public void initView() {
        top_name = (TextView) findViewById(R.id.top_name);
        left_arrow_btn = (ImageView) findViewById(R.id.left_arrow_btn);
        collect_btn = (ImageView) findViewById(R.id.collect_btn);
        imgButton = (ImageView) findViewById(R.id.QR_share_btn);
        left_arrow_btn.setOnClickListener(this);
        collect_btn.setOnClickListener(this);
        imgButton.setOnClickListener(this);
        imgButton.setVisibility(View.VISIBLE);
        left_arrow_btn.setVisibility(View.VISIBLE);
        collect_btn.setVisibility(View.VISIBLE);
        top_name.setVisibility(View.VISIBLE);
        txt_tab_update = (TextView) findViewById(R.id.update_text);
        txt_tab_product = (TextView) findViewById(R.id.product_text);
        img_product_cursor = (ImageView) findViewById(R.id.product_cursor2);
        img_update_cursor = (ImageView) findViewById(R.id.update_cursor1);

        if (user_favorited_by_me != null) {
            //0已收藏
            if (user_favorited_by_me.equals("0")) {
                collect_btn.setBackgroundResource(R.drawable.discollect_button_style);
                bIsFavorate = true;
            } else {
                collect_btn.setBackgroundResource(R.drawable.collect_button_style);
                bIsFavorate = false;
            }
        }

        txt_tab_update.setOnClickListener(new TextOnClickListener(0));
        txt_tab_product.setOnClickListener(new TextOnClickListener(1));

        viewPager = (ViewPager) findViewById(R.id.expert_detail_viewpager);
        fragments[0] = ExpertUpdateFragment.newInstance(user_id);
        fragments[1] = ExpertProductFragment.newInstance(user_id);
        CollectFragmentPagerAdapter adapter = new CollectFragmentPagerAdapter(getSupportFragmentManager(), fragments);
        viewPager.setOnPageChangeListener(new ViewPageOnPageChangeListener());
        viewPager.setOffscreenPageLimit(2);
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(0);
    }

    /**
     * TextView监听
     */
    private class TextOnClickListener implements View.OnClickListener {
        private int index = 0;

        public TextOnClickListener(int i) {
            index = i;
        }

        public void onClick(View v) {
            viewPager.setCurrentItem(index);
        }
    }

    /**
     * viewpager监听
     */

    public class ViewPageOnPageChangeListener implements ViewPager.OnPageChangeListener {
        public void onPageScrollStateChanged(int arg0) {
        }

        public void onPageScrolled(int arg0, float arg1, int arg2) {

            Toast.makeText(CollectExpertDetailActivity.this, getString(R.string.common_search_now), Toast.LENGTH_LONG);

        }

        public void onPageSelected(int arg0) {
            currIndex = arg0;
            int i = currIndex + 1;
            setTabTitle(i);
        }
    }

    /**
     * 设置选项卡title的字体颜色
     *
     * @param tabId
     */
    public void setTabTitle(int tabId) {
        //viewpager顶部导航游标，并设置游标显示位置
        if (tabId == 1) {
            img_update_cursor.setVisibility(View.VISIBLE);
            img_product_cursor.setVisibility(View.INVISIBLE);
        } else if (tabId == 2) {
            img_update_cursor.setVisibility(View.INVISIBLE);
            img_product_cursor.setVisibility(View.VISIBLE);
        }
        //viewpager顶部文字导航，并设置文字颜色
        Resources resource = (Resources) getResources();
        ColorStateList selectedTextColor = (ColorStateList) resource.getColorStateList(R.color.subcollect_fragment_selecttext_color);
        ColorStateList unselectedTextColor = (ColorStateList) resource.getColorStateList(R.color.subcollect_fragment_text_color);
        if (tabId == 1) {
            txt_tab_update.setTextColor(selectedTextColor);
            txt_tab_product.setTextColor(unselectedTextColor);
        } else if (tabId == 2) {
            txt_tab_update.setTextColor(unselectedTextColor);
            txt_tab_product.setTextColor(selectedTextColor);
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.QR_share_btn) {
            doRequestQRCode();
        } else if (v.getId() == R.id.left_arrow_btn) {
            finish();
        } else if (v.getId() == R.id.collect_btn) {
            if (ToastUtils.isFastClick()) {
                return;
            } else {
                if (bIsFavorate == false) {
                    doRequestMStore();
                } else {
                    ServiceManager.doMStoreCancelCollectRequest(TongleAppInstance.getInstance().getUserID(), user_id, TongleAppConst.COLLECT_TYPE_MEMBER, collectListener(), errorListener());
                }
            }
        }
    }

    /**
     * 功能：网络响应成功，返回数据
     */
    private Response.Listener<ParametersJson> successListener() {
        return new Response.Listener<ParametersJson>() {
            @Override
            public void onResponse(ParametersJson response) {
                if (TongleAppConst.SUCCESS.equals(response.head.return_flag)) {
                    TongleAppInstance.getInstance().setUser_qr_code_url(response.body.user_qr_code_url);
                    AlertDialog.Builder builder = new AlertDialog.Builder(CollectExpertDetailActivity.this, R.style.Dialog_FS);
                    View view = LayoutInflater.from(CollectExpertDetailActivity.this).inflate(R.layout.expert_popwindow_layout, null);
                    builder.setView(view);
                    final AlertDialog alert = builder.create();
                    alert.setCanceledOnTouchOutside(true);
                    ImageView coverImage = (ImageView) view.findViewById(R.id.qr_user);

                    view.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alert.dismiss();
                        }
                    });
                    ImageLoader.getInstance().displayImage(TongleAppInstance.getInstance().getUser_qr_code_url(), coverImage, TongleAppInstance.options);
                    alert.show();
                } else {
                    ToastUtils.toastShort(getString(R.string.para_exception) + response.head.return_message);
                }
            }
        };
    }

    /**
     * 功能：网络响应失败
     */
    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ToastUtils.toastShort(getString(R.string.sys_exception));
            }
        };
    }

    /**
     * 功能：添加收藏请求网络响应成功，返回数据
     */
    private Response.Listener collectListener() {
        return new Response.Listener<MStoreDetailCollectResult>() {
            @Override
            public void onResponse(MStoreDetailCollectResult response) {
                if (TongleAppConst.ADD_FAVORITE.equals(response.getHead().getFunction_id())) {
                    if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                        collect_btn.setBackgroundResource(R.drawable.discollect_button_style);
                        ToastUtils.toastShort(getString(R.string.favorite_success));
                        bIsFavorate = true;
                        if (intoType == TongleAppConst.HOME_INTO) {
                            HomeFragment.POST_UPDATE_FLAG = false; //收藏成功，表示又重新收藏了该M客。回MExpertFragment时不需要刷新
                            MExpertFragment.MEXPERTFRAGMENT_UPDATE_FLAG = true; //收藏成功，表示显示MExpertFragment时需要刷新
                        } else if (intoType == TongleAppConst.COLLECTFRAGMENT_INTO) {
                            MExpertFragment.UPDATE_LIST_FLAG = false;//收藏成功，表示又重新收藏了该M客。回MExpertFragment时不需要刷新
                            HomeFragment.POST_UPDATE_FLAG = false;
                        } else {
                            MExpertFragment.MEXPERTFRAGMENT_UPDATE_FLAG = true; //收藏成功，表示显示MExpertFragment时需要刷新
                            HomeFragment.POST_UPDATE_FLAG_OTHER = true;  //收藏的会员有变化，首页的帖子列表要刷新。
                        }

                    } else {
                        ToastUtils.toastShort(getString(R.string.para_exception) + response.getHead().getReturn_message());
                    }
                } else if (TongleAppConst.CANCEL_FAVORITE.equals(response.getHead().getFunction_id())) {
                    if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                        collect_btn.setBackgroundResource(R.drawable.collect_button_style);
                        ToastUtils.toastShort(getString(R.string.favorite_cancel));
                        bIsFavorate = false;
                        if (intoType == TongleAppConst.HOME_INTO) {
                            HomeFragment.POST_UPDATE_FLAG = false;
                            MExpertFragment.MEXPERTFRAGMENT_UPDATE_FLAG = true; //收藏成功，表示显示MExpertFragment时需要刷新
                        } else if (intoType == TongleAppConst.COLLECTFRAGMENT_INTO) {
                            MExpertFragment.UPDATE_LIST_FLAG = true; //取消收藏，MExpertFragment中删除选中的人，需要更新。
                            HomeFragment.POST_UPDATE_FLAG = true;
                        } else {
                            MExpertFragment.MEXPERTFRAGMENT_UPDATE_FLAG = true; //取消收藏，表示显示MExpertFragment时需要刷新
                            HomeFragment.POST_UPDATE_FLAG_OTHER = true;  //收藏的会员有变化，首页的帖子列表要刷新。
                        }
                    } else {
                        ToastUtils.toastShort(getString(R.string.para_exception) + response.getHead().getReturn_message());
                    }
                }
            }
        };
    }
}
