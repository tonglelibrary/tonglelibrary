package com.ilingtong.library.tongle.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.protocol.MStoreListItem;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

/**
 * User: lengjiqiang
 * Date: 2015/5/15
 * Time: 17:05
 * Email: jqleng@isoftstone.com
 * Dest:
 */
public class MStoreListAdapter extends BaseAdapter {
    private LayoutInflater inflater;
    private List<MStoreListItem> list;
    private MStoreListItem dataItem = new MStoreListItem();
    private Context mContext;

    public MStoreListAdapter(Context context, ArrayList list) {
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mContext = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder holder;
        if (view == null) {
            view = inflater.inflate(R.layout.list_item_mstore, null);
             holder = new ViewHolder();
            holder.mstoreImage = (ImageView) view.findViewById(R.id.mstore_image);
            holder.mstoreName = (TextView) view.findViewById(R.id.mstore_name);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        dataItem = list.get(position);
        ImageLoader.getInstance().displayImage(dataItem.mstore_pic_url, holder.mstoreImage, TongleAppInstance.options);
        holder.mstoreName.setText((CharSequence) dataItem.mstore_name);

        return view;
    }
    static class ViewHolder {
        TextView mstoreName;
        ImageView mstoreImage;
    }
}
