package com.ilingtong.library.tongle.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;

import com.ilingtong.library.tongle.AppManager;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.fragment.LoginFragment;


/**
 * User: lengjiqiang
 * Date: 2015/4/29
 * Time: 17:16
 * Email: jqleng@isoftstone.com
 * Desc: 用户登录页
 */
public class LoginActivity extends BaseFragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_layout);
        setDefaultFragment();
    }

    private void setDefaultFragment() {
        //String type = getIntent().getExtras().getString(TongleAppConst.MAINACTIVITY_NEWINTENT_FLAG_NAME, "");
        String type = getIntent().getStringExtra(TongleAppConst.MAINACTIVITY_NEWINTENT_FLAG_NAME);
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        LoginFragment fragment = LoginFragment.newInstance(type);

        transaction.add(R.id.login_framelayout, fragment, "");
        transaction.commit();

    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            AppManager.getAppManager().AppExit(this);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
