package com.ilingtong.library.tongle.listener;

import android.os.Bundle;

/**
 * 通知Fragment界面数据变更
 * 
 * @author GaiQS E-mail:gaiqs@sina.com
 * @Date 2015年5月15日
 * @Time 下午5:58:46
 */
public interface OperatListener {
	/** 系统通知 */
	final int MSG_SYS = 1;
	/** 切换首页 */
	final int CHANGE = 2;
	/** 数据刷新 */
	final int REFRESH = 3;
	/** 跳转 */
	final int SKIP = 4;

	/** 开始请求数据 */
	public static final int START_LOAD = 11;
	/** 结束请求 */
	public static final int STOP_LOAD = 10;

	/**
	 * 当fragment界面发生操作时回调
	 * 
	 * @param optionId
	 *            : 操作id
	 */
	void onOperate(int optionId, Bundle bundle);
}
