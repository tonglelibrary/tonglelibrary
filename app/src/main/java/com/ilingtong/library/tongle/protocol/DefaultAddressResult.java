package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/6/13
 * Time: 18:18
 * Email: jqleng@isoftstone.com
 * Desc:
 */
public class DefaultAddressResult implements Serializable {
    private BaseInfo head;
    private DefaultAddressInfo body;

    public BaseInfo getHead() {
        return head;
    }

    public void setHead(BaseInfo head) {
        this.head = head;
    }

    public DefaultAddressInfo getBody() {
        return body;
    }

    public void setBody(DefaultAddressInfo body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return body.toString();
    }
}
