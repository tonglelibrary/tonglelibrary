package com.ilingtong.library.tongle.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.activity.CollectForumDetailActivity;
import com.ilingtong.library.tongle.activity.SearchActivity;
import com.ilingtong.library.tongle.adapter.ForumListAdapter;
import com.ilingtong.library.tongle.external.maxwin.view.XListView;
import com.ilingtong.library.tongle.protocol.CollectForumResult;
import com.ilingtong.library.tongle.utils.ToastUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by wuqian on 2015/10/29.
 * mail: wuqian@ilingtong.com
 * Description: 收藏 -- M贴
 */
public class MForumFragment extends LazyFragment implements XListView.IXListViewListener,SearchActivity.FlushDataListener {
    private XListView forumListView;
    private ForumListAdapter forumListAdaper;
    private boolean flag = true;
    private List<com.ilingtong.library.tongle.protocol.UserFollowPostList> UserFollowPostList = new ArrayList<>();

    public static boolean UPDATE_LIST_FLAG = false; //是否刷新列表的标准
    public static boolean MFORUMFRAGMENT_UPDATE_FLAG = false; //进入页面刷新标志。为ture时表示刷新，反之不刷新
    private int listIndex = -1; //表示是从 position位置跳转到M客详情的
    private int intoType = 0;
    // 标志位，标志已经初始化完成。
    private boolean isPrepared;
    Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    forumListView.setRefreshTime();
                    forumListAdaper.notifyDataSetChanged();
                    break;
            }
        }
    };

    /**
     * 静态工厂方法需要一个int型的值来初始化fragment的参数，然后返回新的fragment到调用者
     *
     * @param intoType 在哪个页面创建
     * @return
     */
    public static MForumFragment newInstance(int intoType,List<com.ilingtong.library.tongle.protocol.UserFollowPostList> userFollowPostList) {
        MForumFragment fragment = new MForumFragment();
        Bundle args = new Bundle();
        args.putInt(TongleAppConst.INTO_TYPE, intoType);
        args.putSerializable("list", (Serializable) userFollowPostList);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.collect_forum_layout, null);
        initView(view);
        isPrepared = true;
        doRequest();
        return view;
    }

    /**
     * 初始化view
     *
     * @param view
     */
    private void initView(View view) {
        intoType = getArguments().getInt(TongleAppConst.INTO_TYPE);
        UserFollowPostList.clear();
        if (getArguments().get("list")!=null){
            UserFollowPostList = (List<com.ilingtong.library.tongle.protocol.UserFollowPostList>) getArguments().get("list");
        }

        forumListView = (XListView) view.findViewById(R.id.product_listview);
        forumListAdaper = new ForumListAdapter(getActivity(), UserFollowPostList);
        forumListView.setXListViewListener(this, 0);
        forumListView.setPullLoadEnable(false);
        forumListView.setAdapter(forumListAdaper);

        //点击列表Item，进入帖子详情
        forumListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent detailIntent = new Intent(getActivity(), CollectForumDetailActivity.class);
                detailIntent.putExtra("post_id", UserFollowPostList.get(position-1).post_id);
                detailIntent.putExtra(TongleAppConst.INTO_TYPE,intoType);
                listIndex = position;
                startActivityForResult(detailIntent, 10001);
            }
        });
    }

    /**
     * 点击M帖列表进入详情后，返回的回调。如果在详情页面有取消关注，回到该页面时刷新列表。没有则不刷新
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (intoType == TongleAppConst.COLLECTFRAGMENT_INTO && requestCode == 10001 && UPDATE_LIST_FLAG) {
            if (listIndex > 0) {
                UserFollowPostList.remove(listIndex - 1);
                UPDATE_LIST_FLAG = false;
                mHandler.sendEmptyMessage(0);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * 调用接口请求数据
     */
    private void doRequest() {
        if (intoType == TongleAppConst.COLLECTFRAGMENT_INTO) { //收藏
            ServiceManager.doCollectForumRequest(TongleAppInstance.getInstance().getUserID(), "", "1", TongleAppConst.FETCH_COUNT, successListener(), errorListener());
        } else if (intoType == TongleAppConst.FINDFRAGMENT_INTO) { //发现热门
            //发现进入的M帖，不可以有刷新和加载更多
            forumListView.setPullRefreshEnable(false);
            forumListView.setPullLoadEnable(false);
        }else if (intoType == TongleAppConst.SEARCH_INTO){ //关键字搜索进入
            forumListView.setPullRefreshEnable(false);
            forumListView.setPullLoadEnable(false);
        }
    }

    /**
     * 刷新
     *
     * @param id
     */
    @Override
    public void onRefresh(int id) {
        //刷新之前，清空list
        if (UserFollowPostList != null) {
            UserFollowPostList.clear();
        }
        MFORUMFRAGMENT_UPDATE_FLAG = false;
        UPDATE_LIST_FLAG = false;
        ServiceManager.doCollectForumRequest(TongleAppInstance.getInstance().getUserID(), "", "1", TongleAppConst.FETCH_COUNT, successListener(), errorListener());
    }

    /**
     * 加载更多
     *
     * @param id
     */
    @Override
    public void onLoadMore(int id) {
        //加载更多
        if (flag) {
            ServiceManager.doCollectForumRequest(TongleAppInstance.getInstance().getUserID(), UserFollowPostList.get(UserFollowPostList.size() - 1).post_id, "1", TongleAppConst.FETCH_COUNT, successListener(), errorListener());
        } else {
            ToastUtils.toastShort(getString(R.string.common_list_end));
            forumListView.setPullLoadEnable(false);
        }
    }
    /**
     * 功能：网络响应成功，返回数据
     */
    private Response.Listener successListener() {
        return new Response.Listener<CollectForumResult>() {
            @Override
            public void onResponse(CollectForumResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    UserFollowPostList.addAll(response.getBody().getPost_list());
                    //调用handler，发送消息
                    mHandler.sendEmptyMessage(0);
                    if (Integer.parseInt(response.getBody().getData_total_count()) > UserFollowPostList.size()) {
                        flag = true;
                        forumListView.setPullLoadEnable(true);
                    } else {
                        flag = false;
                        forumListView.setPullLoadEnable(false);
                    }
                } else {
                    ToastUtils.toastShort(getString(R.string.para_exception) + response.getHead().getReturn_message());
                }
            }
        };
    }

    /**
     * 功能：网络响应失败
     */
    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ToastUtils.toastShort(getString(R.string.sys_exception));
            }
        };
    }

    @Override
    public void flush(Object data) {
        if(forumListAdaper != null) {
            UserFollowPostList = (ArrayList<com.ilingtong.library.tongle.protocol.UserFollowPostList>) data;
            forumListAdaper.notifyDataSetChanged();
        }
    }
    @Override
    protected void lazyLoad() {
        if (!isPrepared || !isVisible) {
            return;
        }
        if (intoType == TongleAppConst.COLLECTFRAGMENT_INTO){
            if (MFORUMFRAGMENT_UPDATE_FLAG || UPDATE_LIST_FLAG){
                onRefresh(0);
            }
        }
    }
}
