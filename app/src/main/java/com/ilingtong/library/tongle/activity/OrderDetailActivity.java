package com.ilingtong.library.tongle.activity;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.adapter.OrderDetailListAdaper;
import com.ilingtong.library.tongle.external.maxwin.view.XListView;
import com.ilingtong.library.tongle.protocol.CodeResult;
import com.ilingtong.library.tongle.protocol.OrderDetailOrderDetailInfo;
import com.ilingtong.library.tongle.protocol.OrderDetailOrderInfo;
import com.ilingtong.library.tongle.protocol.OrderDetailResult;
import com.ilingtong.library.tongle.utils.ToastUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by wuqian on 2016/2/29.
 * mail: wuqian@ilingtong.com
 * Description:
 */
public class OrderDetailActivity extends BaseActivity implements View.OnClickListener, XListView.IXListViewListener {
    private ImageView left_arrow_btn;
    private TextView top_name;
    private TextView top_btn_text;
    private XListView listview;
    private View headView, footView;
    private TextView amount, fee_amount, consignee, address, order_no, order_time, statu, shipping_message, shipping_time;
    private TextView txt_coupon;  //优惠抵扣
    private TextView txt_product_total;  //商品总额
    private String order;
    private LinearLayout tariff_message_linear;
    private TextView orderdetail_tariff;
    private TextView pass_msg;
    private Button tariff_subBtn;
    private Button order_operation;  //订单右边按钮。  删除订单/立即付款/立即评价/确认收货
    private Button btn_cancel_order;  //顶部取消订单按钮
    private AlertDialog alert;
    private Button cancle, makesure;
    private String pay_type_id;  //支付方式
    String myurl;
    String myTitle;
    private List<OrderDetailOrderDetailInfo> order_detail_lsit = new ArrayList<>();  //订单详情子订单列表。（一个商品一个子订单）
    private boolean flag = false; //为true时表示订单状态有变化。回到订单列表页面需刷新列表
    public static final int REQUESTCODE_TO_EVALUATE = 10001;  //activityForResult 评价订单
    public static final int REQUESTCODE_TO_PAY = 10002;  //activityForResult 评价订单
    public static OrderDetailActivity instance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.xlistview_comm_layout);
        instance = this;
        getData();
        initView();
        doRequest();
    }

    public void getData() {
        order = getIntent().getExtras().getString("order_no");
    }

    public void initView() {
        top_name = (TextView) findViewById(R.id.top_name);
        top_btn_text = (TextView) findViewById(R.id.top_btn_text);
        left_arrow_btn = (ImageView) findViewById(R.id.left_arrow_btn);
        listview = (XListView) findViewById(R.id.xlistview);
        left_arrow_btn.setOnClickListener(this);
        left_arrow_btn.setVisibility(View.VISIBLE);
        top_name.setVisibility(View.VISIBLE);
        top_name.setText(getString(R.string.order_detail_delete_top_name));

        headView = LayoutInflater.from(this).inflate(R.layout.orderdetail_headlayout, null);
        footView = LayoutInflater.from(this).inflate(R.layout.orderdetail_footlayout, null);

        //商品总额
        txt_product_total = (TextView) headView.findViewById(R.id.orderdetail_product_total);
        //订单金额（实付金额）
        amount = (TextView) headView.findViewById(R.id.orderdetail_amount);
        //优惠抵扣
        txt_coupon = (TextView) headView.findViewById(R.id.orderdetail_coupon);
        //运费
        fee_amount = (TextView) headView.findViewById(R.id.orderdetail_fee_amount);
        //收件人
        consignee = (TextView) headView.findViewById(R.id.orderdetail_consignee);
        //地址
        address = (TextView) headView.findViewById(R.id.orderdetail_address);
        //订单号
        order_no = (TextView) footView.findViewById(R.id.orderdetail_order_no);
        //下单时间
        order_time = (TextView) footView.findViewById(R.id.orderdetail_order_time);
        //交易状态
        statu = (TextView) headView.findViewById(R.id.orderdetail_statu);
        statu.setText(getString(R.string.order_detail_evaluate_status));

        order_operation = (Button) headView.findViewById(R.id.orderdetail_deleteorder);
        order_operation.setOnClickListener(this);
        //快递信息
        shipping_message = (TextView) headView.findViewById(R.id.shipping_message);
        //快递时间
        shipping_time = (TextView) headView.findViewById(R.id.shipping_time);
        //关税信息linear
        tariff_message_linear = (LinearLayout) footView.findViewById(R.id.tariff_message_linear);
        //通关信息
        pass_msg = (TextView) footView.findViewById(R.id.pass_msg);
        //再次提交按钮
        tariff_subBtn = (Button) footView.findViewById(R.id.tariff_subBtn);
        tariff_subBtn.setOnClickListener(this);
        //关税
        orderdetail_tariff = (TextView) headView.findViewById(R.id.orderdetail_tariff);
        //取消订单
        btn_cancel_order = (Button) footView.findViewById(R.id.orderdetail_paynow_cancleorder);
        btn_cancel_order.setOnClickListener(this);

        listview.addHeaderView(headView);
        listview.addFooterView(footView);

    }

    /**
     * 从接口获取数据后根据不同的订单状态显示view
     *
     * @param order_info 订单详情
     */
    private void setViewData(OrderDetailOrderInfo order_info) {
        amount.setText(getString(R.string.common_fee_sum) + order_info.head_info.amount); //总金额
        txt_coupon.setText(String.format(getResources().getString(R.string.layout_orderdetail_head_coupon_txt), order_info.money));  //优惠抵扣
        txt_product_total.setText(String.format(getResources().getString(R.string.layout_orderdetail_head_product_total_txt), order_info.head_info.goods_amount));  //商品总额
        order_no.setText(getString(R.string.common_order_number) + order_info.head_info.order_no);  //订单号
        order_time.setText(getString(R.string.common_order_time) + order_info.head_info.order_time);  //下单时间
        fee_amount.setText(getString(R.string.common_fee_transportation) + order_info.head_info.fee_amount);  //运费
        consignee.setText(getString(R.string.common_order_consignee) + order_info.add_info.consignee);  //收件人
        String a = order_info.add_info.province + order_info.add_info.area + order_info.add_info.address;
        address.setText(getString(R.string.common_order_address) + a);  //收货地址
        orderdetail_tariff.setText(getString(R.string.common_fee_tariff) + order_info.head_info.tariff); //关税
        shipping_message.setText(order_info.logistics_info.shipping_company + "," + order_info.logistics_info.shipping_no);  //承运公司 + 运单号
        shipping_time.setText(order_info.logistics_info.shipping_time);   //发货时间
        pass_msg.setText(order_info.head_info.customs_fail_reason);  //通关失败原因

        //**********************************根据订单状态显示不同view************//
        if (TongleAppConst.ORDER_NEW.equals(order_info.head_info.status)) {//	新单 1,隐藏右边按钮；2，显示底部取消订单按钮
            statu.setText(getResources().getString(R.string.order_detail_delete_not_audited_txt));
            order_operation.setVisibility(View.GONE);
            btn_cancel_order.setVisibility(View.VISIBLE);
        } else if (TongleAppConst.ORDER_REVIEWED.equals(order_info.head_info.status)) { //	已审核 1,隐藏右边按钮；2，隐藏底部取消订单按钮
            statu.setText(getResources().getString(R.string.order_detail_delete_audited_txt));
            order_operation.setVisibility(View.GONE);
            btn_cancel_order.setVisibility(View.GONE);

        } else if (TongleAppConst.ORDER_PAID.equals(order_info.head_info.status)) { //	已付款 1,隐藏右边按钮；2,隐藏底部取消订单按钮
            statu.setText(getResources().getString(R.string.order_detail_delete_not_shipped_txt));
            order_operation.setVisibility(View.GONE);
            btn_cancel_order.setVisibility(View.GONE);

        } else if (TongleAppConst.ORDER_SENDING.equals(order_info.head_info.status)) {//	发货中sssssssssss 1,显示右边按钮为“确认收货";2,隐藏底部取消订单按钮
            statu.setText(getResources().getString(R.string.order_detail_delete_not_received_txt));
            order_operation.setVisibility(View.VISIBLE);
            order_operation.setText(getResources().getString(R.string.common_order_confirm_receive));
            btn_cancel_order.setVisibility(View.GONE);

        } else if (TongleAppConst.ORDER_NO_COMMENTS.equals(order_info.head_info.status)) {//完成未评价ssssssssssss 1,显示右边按钮为 立即评价 ；2,隐藏底部取消订单按钮
            statu.setText(getResources().getString(R.string.order_detail_delete_not_evaluated_txt));
            order_operation.setVisibility(View.VISIBLE);
            order_operation.setText(getResources().getString(R.string.order_detail_evaluate_btn_evaluate));
            btn_cancel_order.setVisibility(View.GONE);
            //返修/退返货按钮
            top_btn_text.setVisibility(View.VISIBLE);
            top_btn_text.setText(getString(R.string.order_detail_evaluate_top_txt));
            top_btn_text.setTextSize(12);
            top_btn_text.setOnClickListener(this);

        } else if (TongleAppConst.ORDER_HAVE_COMMENTS.equals(order_info.head_info.status)) { //	已评价 1,显示右边按钮为 删除订单；2,隐藏底部取消订单按钮
            statu.setText(getResources().getString(R.string.order_detail_delete_evaluated_txt));
            order_operation.setVisibility(View.VISIBLE);
            order_operation.setText(getResources().getString(R.string.order_detail_delete_txt_delete));
            btn_cancel_order.setVisibility(View.GONE);
            //返修/退返货按钮
            top_btn_text.setVisibility(View.VISIBLE);
            top_btn_text.setText(getString(R.string.order_detail_evaluate_top_txt));
            top_btn_text.setTextSize(12);
            top_btn_text.setOnClickListener(this);

        } else if (TongleAppConst.ORDER_REJECT.equals(order_info.head_info.status)) {  //	拒收 1，显示右边按钮为 删除订单 ；2,隐藏底部取消订单按钮
            statu.setText(getResources().getString(R.string.order_detail_delete_rejected_txt));
            order_operation.setVisibility(View.VISIBLE);
            order_operation.setText(getResources().getString(R.string.order_detail_delete_txt_delete));
            btn_cancel_order.setVisibility(View.GONE);

        } else if (TongleAppConst.ORDER_NO_PAY.equals(order_info.head_info.status)) { //	待付款ssssssssss 1，显示右边按钮为 立即支付 ；2,显示底部取消订单按钮
            statu.setText(getResources().getString(R.string.order_detail_no_pay_txt));
            order_operation.setVisibility(View.VISIBLE);
            order_operation.setText(getResources().getString(R.string.common_order_pay_now));
            btn_cancel_order.setVisibility(View.VISIBLE);

        } else if (TongleAppConst.ORDER_PAY_CONFIRM.equals(order_info.head_info.status)) {//付款确认中(微信支付使用) 1，隐藏右边按钮；2，隐藏底部取消订单按钮
            statu.setText(getResources().getString(R.string.order_detail_pay_confirming_txt));
            order_operation.setVisibility(View.GONE);
            btn_cancel_order.setVisibility(View.GONE);
        }
        //**********************************根据订单状态显示不同view   end************//

        //*******************根据商品类别（海淘/非海淘）判断是否显示关税几通关信息************/
        if (TongleAppConst.YES.equals(order_info.order_detail.get(0).import_goods_flag)) {  //表示是海淘商品：1,显示关税；2,判断是否通关，显示通关信息
            findViewById(R.id.tariff_message_linear).setVisibility(View.VISIBLE);
            orderdetail_tariff.setVisibility(View.VISIBLE);
            if (TongleAppConst.CUSTOMS_FLAG_SUCCESS.equals(order_info.head_info.customs_flag)) {
                pass_msg.setText(getResources().getString(R.string.order_detail_audited_txt));

            } else if (TongleAppConst.CUSTOMS_FLAG_FAILURE.equals(order_info.head_info.customs_flag)) {
                statu.setText(getString(R.string.common_tariff_error));
                pass_msg.setText(order_info.head_info.customs_fail_reason + "");
                tariff_subBtn.setVisibility(View.VISIBLE);
                pass_msg.setTextColor(getResources().getColor(R.color.order_payway_press));
            } else {
                statu.setText(getResources().getString(R.string.order_detail_auditing_txt));
                pass_msg.setText(getResources().getString(R.string.order_detail_auditing_msg));
            }
        } else {  //非海淘商品：1.隐藏关税；2，隐藏底部通关信息一览
            findViewById(R.id.tariff_message_linear).setVisibility(View.GONE);
            orderdetail_tariff.setVisibility(View.GONE);
        }
        //*******************根据商品类别（海淘/非海淘）判断是否显示关税几通关信息 end************/
    }

    public void doRequest() {
        ServiceManager.OrderDetailRequest(TongleAppInstance.getInstance().getUserID(), order, successListener(), errorListener());
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.left_arrow_btn) {
            finish();
        } else if (v.getId() == R.id.orderdetail_deleteorder) {
            if (getResources().getString(R.string.order_detail_evaluate_btn_evaluate).equals(order_operation.getText().toString())) {
                if (!TextUtils.isEmpty(order)) {
                    Intent i = new Intent(this, OrderDetailProdEvaluateActivity.class);
                    i.putExtra("order_no", order);
                    i.putExtra("list", (Serializable) order_detail_lsit);
                    startActivityForResult(i, REQUESTCODE_TO_EVALUATE);
                }
            } else if (getResources().getString(R.string.order_detail_delete_txt_delete).equals(order_operation.getText().toString())) {
                cancelOrder();
            } else if (getResources().getString(R.string.common_order_pay_now).equals(order_operation.getText().toString())) {
                toPayNow();
            } else if (getResources().getString(R.string.common_order_confirm_receive).equals(order_operation.getText().toString())) {
                ServiceManager.OrderConfirmRequest(TongleAppInstance.getInstance().getUserID(), order, confirmsuccessListener(), errorListener());
            }
        } else if (v.getId() == R.id.tariff_subBtn) {  //再次提交通关信息审核
            ServiceManager.doReCustomers(TongleAppInstance.getInstance().getUserID(), order, comminttsuccessListener(), errorListener());
        } else if (v.getId() == R.id.top_btn_text) {
            //拼接url
            String finalUrl = myurl + "?user_id=" + TongleAppInstance.getInstance().getUserID() + "&user_token=" + TongleAppInstance.getInstance().getToken() + "&order_no=" + order;

            Intent expertIntent = new Intent(this, ProdIntegralActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString("prod_url", finalUrl);
            bundle.putString("myTitle", myTitle);
            expertIntent.putExtras(bundle);
            startActivity(expertIntent);
        } else if (v.getId() == R.id.deleteorder_cancle) {
            alert.dismiss();
        } else if (v.getId() == R.id.deleteorder_sure) {
            String reason = "";//模拟删除订单原因
            ServiceManager.OrderDetailDeleteRequest(TongleAppInstance.getInstance().getUserID(), order, reason, successDeleteListener(), errorListener());
        } else if (v.getId() == R.id.orderdetail_paynow_cancleorder) {  //取消订单
            cancelOrder();
        }
    }

    /**
     * 功能：确认收货 网络响应成功，返回数据
     */
    private Response.Listener confirmsuccessListener() {
        return new Response.Listener<OrderDetailResult>() {
            @Override
            public void onResponse(OrderDetailResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    ToastUtils.toastShort(getString(R.string.common_order_confirm_receive_success));
                    onRefresh(0);
                    flag = true;
                } else {
                    ToastUtils.toastShort(getString(R.string.para_exception) + response.getHead().getReturn_message());
                }
            }
        };
    }

    /**
     * 取消订单
     */
    private void cancelOrder() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View view = LayoutInflater.from(this).inflate(R.layout.orderdetail_delete_layout, null);
        builder.setView(view);
        alert = builder.create();
        alert.setCanceledOnTouchOutside(true);
        alert.show();
        cancle = (Button) view.findViewById(R.id.deleteorder_cancle);
        makesure = (Button) view.findViewById(R.id.deleteorder_sure);
        cancle.setOnClickListener(this);
        makesure.setOnClickListener(this);
    }

    /**
     * 立即支付
     */
    private void toPayNow() {
        SelectPayTypeActivity.launcherForResult(OrderDetailActivity.this, order, TongleAppConst.PAY_MODE, REQUESTCODE_TO_PAY);
    }

    /**
     * 功能：删除订单网络响应成功，返回数据
     */
    private Response.Listener successDeleteListener() {
        return new Response.Listener<OrderDetailResult>() {
            @Override
            public void onResponse(OrderDetailResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    ToastUtils.toastShort(getString(R.string.common_order_cancel_success));
                    flag = true;
                    finish();
                }else {
                    ToastUtils.toastShort(getString(R.string.para_exception) + response.getHead().getReturn_message());
                }
            }
        };
    }

    /**
     * 功能：订单详情网络响应成功，返回数据
     */
    private Response.Listener successListener() {
        return new Response.Listener<OrderDetailResult>() {
            @Override
            public void onResponse(OrderDetailResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    setViewData(response.getBody().order_info);
                    myurl = response.getBody().order_info.goods_return_url;  //退货申请URL
                    myTitle = response.getBody().order_info.goods_return_title;  //退货申请title

                    order_detail_lsit.addAll(response.getBody().order_info.order_detail);
                    pay_type_id = response.getBody().order_info.pay_type_id;  //保存支付方式
                    updateListView();
                } else {
                    ToastUtils.toastShort(getString(R.string.para_exception) + response.getHead().getFunction_id() + response.getHead().getReturn_message());
                }
            }
        };
    }

    /**
     * 显示商品列表
     */
    public void updateListView() {
        listview.setPullLoadEnable(false);
        listview.setPullRefreshEnable(true);
        listview.setRefreshTime();
        listview.setXListViewListener(this, 0);
        listview.setAdapter(new OrderDetailListAdaper(this, order_detail_lsit));
    }

    /**
     * 功能：订单详情请求网络响应失败
     */
    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ToastUtils.toastShort(getString(R.string.sys_exception));
            }
        };
    }

    /**
     * 功能：再次提交网络响应成功，返回数据
     */
    private Response.Listener comminttsuccessListener() {
        return new Response.Listener<CodeResult>() {
            @Override
            public void onResponse(CodeResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    tariff_subBtn.setVisibility(View.GONE);
                    ToastUtils.toastShort(getString(R.string.common_tariff_success));
                } else {
                    ToastUtils.toastShort(getString(R.string.para_exception) + response.getHead().getFunction_id() + response.getHead().getReturn_message());
                }
            }
        };
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && (requestCode == REQUESTCODE_TO_EVALUATE || requestCode ==REQUESTCODE_TO_PAY )){
            flag = true;
            onRefresh(0);
        }
    }

    @Override
    public void onRefresh(int id) {
        order_detail_lsit.clear();
        doRequest();
    }

    @Override
    public void onLoadMore(int id) {
    }

    @Override
    public void finish() {
        if (flag) {
            setResult(RESULT_OK);
        }
        super.finish();
    }
}
