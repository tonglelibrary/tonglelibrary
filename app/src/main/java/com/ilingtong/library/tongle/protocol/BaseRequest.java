package com.ilingtong.library.tongle.protocol;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * User: lengjiqiang
 * Date: 2015/5/6
 * Time: 11:42
 * Email: jqleng@isoftstone.com
 * Dest:
 */
public class BaseRequest implements Serializable {
    public JSONObject toParam(Map<String, String> map) {
        JSONObject jsonObject = new JSONObject(map);
        String jsString = jsonObject.toString();
        Map<String, String> param = new HashMap<>();
        param.put("parameters_json", jsString);
        JSONObject jsonParam = new JSONObject(param);
        return jsonParam;
    }
}
