package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: shuailei
 * Date: 2015/6/24
 * Time: 22:05
 * Email: leishuai@isoftstone.com
 * Desc: 发票信息类型
 */
public class InvoiceInfo implements Serializable {
    //发票类型编号
    public String invoice_type_no;
    //发票内容区分
    public String invoice_content_flag;
    //发票抬头备注
    public String invoice_title;
}
