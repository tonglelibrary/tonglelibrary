package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: syc
 * Date: 2015/6/26
 * Time: 14:18
 * Email: ycshi@isoftstone.com
 * Desc: 【2015】
 */
public class APPpromotionResult  implements Serializable {


    private BaseInfo head;
    private APPpromotionInfo body;

    public BaseInfo getHead() {
        return head;
    }

    public void setHead(BaseInfo head) {
        this.head = head;
    }

    public APPpromotionInfo getBody() {
        return body;
    }

    public void setBody(APPpromotionInfo body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return body.toString();
    }
}
