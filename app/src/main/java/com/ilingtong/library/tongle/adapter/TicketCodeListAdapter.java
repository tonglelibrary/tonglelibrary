package com.ilingtong.library.tongle.adapter;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.activity.GroupTicketDetailActivity;
import com.ilingtong.library.tongle.activity.MyGroupTicketActivity;
import com.ilingtong.library.tongle.protocol.CouponCode;
import com.ilingtong.library.tongle.utils.Utils;

import java.util.List;

/**
 * author: liuting
 * Date: 2016/3/17
 * Time: 9:09
 * Email: liuting@ilingtong.com
 * Desc:团购券券号列表Adapter
 */
public class TicketCodeListAdapter extends BaseAdapter {
    private LayoutInflater mInflater;
    private List<CouponCode> mLvCode;
    private Context mContext;
    private String mOrderNo;//订单号
    private String mOutOfDate;//过期/未消费标志

    public TicketCodeListAdapter(Context context, List<CouponCode> codeList, String order_no, String out_of_date) {
        mContext = context;
        this.mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mLvCode = codeList;
        this.mOrderNo = order_no;
        this.mOutOfDate = out_of_date;
    }

    @Override
    public int getCount() {
        return mLvCode.size();
    }

    @Override
    public Object getItem(int position) {
        return mLvCode.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        final CouponCode mCodeItem = (CouponCode) getItem(position);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.my_ticket_list_item, null);
            viewHolder = new ViewHolder();
            viewHolder.ticketNumber = (TextView) convertView.findViewById(R.id.my_ticket_list_item_tv_number);
            viewHolder.llyMain = (LinearLayout) convertView.findViewById(R.id.my_ticket_list_item_lly_main);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        if (mOutOfDate.equals(TongleAppConst.GROUP_TICKET_NOT_USED)) {
            viewHolder.ticketNumber.setTextColor(mContext.getResources().getColor(R.color.my_ticket_list_item_num_txt_color));
        } else {
            viewHolder.ticketNumber.setTextColor(mContext.getResources().getColor(R.color.cart_list_second_line_color));
        }
        viewHolder.ticketNumber.setText(Utils.formatStringSequence(mCodeItem.getCoupon_code(),TongleAppConst.COUPON_SEQUENCE_LENGHT));
        final ViewHolder finalViewHolder = viewHolder;
        viewHolder.llyMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(finalViewHolder.ticketNumber.getText().toString())) {
                    GroupTicketDetailActivity.launcher((Activity) mContext, mOrderNo, mCodeItem.getCoupon_code(), mOutOfDate, MyGroupTicketActivity.REQUESTCODE_TO_GROUPTICKETDETAIL);
                }
            }
        });

        return convertView;
    }

    static class ViewHolder {
        TextView ticketNumber;//券号
        LinearLayout llyMain;//主布局
    }
}

