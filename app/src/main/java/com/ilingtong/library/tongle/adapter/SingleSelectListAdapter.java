package com.ilingtong.library.tongle.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.protocol.DataListItem;

import java.util.ArrayList;
import java.util.List;

/**
 * User: lengjiqiang
 * Date: 2015/6/11
 * Time: 16:27
 * Email: jqleng@isoftstone.com
 * Desc:
 */
public class SingleSelectListAdapter extends BaseAdapter {
    private LayoutInflater inflater;
    private List<DataListItem> list;
    DataListItem item = new DataListItem();
    private String tag;

    public SingleSelectListAdapter(Context context, ArrayList singleLineList,String tag) {
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.list = singleLineList;
        this.tag = tag;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder holder;
        if (view == null) {
            view = inflater.inflate(R.layout.list_item_single_select, null);
            holder = new ViewHolder();
            holder.text = (TextView) view.findViewById(R.id.single_text);
            holder.image = (ImageView) view.findViewById(R.id.single_select_icon);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        item = (DataListItem) getItem(position);
        holder.text.setText(item.name);
        if(item.name.equals(tag)){
            holder.image.setVisibility(View.VISIBLE);
        }else{
            holder.image.setVisibility(View.GONE);
        }
        return view;
    }

    static class ViewHolder {
        TextView text;
        ImageView image;
    }
}
