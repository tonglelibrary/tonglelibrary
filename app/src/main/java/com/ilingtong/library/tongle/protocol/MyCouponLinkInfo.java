package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/3/14.
 * mail: wuqian@ilingtong.com
 * Description: 优惠券链接信息
 * use by 6014
 */
public class MyCouponLinkInfo implements Serializable {
    private String title;   //优惠券固定文字
    private String pic;   //图片
    private String link_type;    //链接类型 0-商品 1-帖子 2-魔店 3-会员 4-首页 5-H5
    private String link_id;      //链接ID

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getLink_type() {
        return link_type;
    }

    public void setLink_type(String link_type) {
        this.link_type = link_type;
    }

    public String getLink_id() {
        return link_id;
    }

    public void setLink_id(String link_id) {
        this.link_id = link_id;
    }
}
