package com.ilingtong.library.tongle.activity;

import android.app.Activity;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.DisplayMetrics;

import com.ilingtong.library.tongle.AppManager;
import com.umeng.analytics.MobclickAgent;

import java.util.Locale;

/**
 * Created by wuqian on 2016/1/25.
 * mail: wuqian@ilingtong.com
 * Description: Activity统一父类   友盟统计
 */
public class BaseActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setLanguage();
        super.onCreate(savedInstanceState);
        AppManager.getAppManager().addActivity(this);
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
    }

    /**
     * 设置语言类型切换
     */
    private void setLanguage() {
        Resources resources = getResources();// 获得res资源对象
        Configuration config = resources.getConfiguration();// 获得设置对象
        DisplayMetrics dm = resources.getDisplayMetrics();// 获得屏幕参数：主要是分辨率，像素等。
        config.locale = Locale.CHINA; // 简体中文
        resources.updateConfiguration(config, dm);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        AppManager.getAppManager().removeActivity(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        //友盟统计
        MobclickAgent.onPageStart(this.getApplicationInfo().className);
        MobclickAgent.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        //友盟统计
        MobclickAgent.onPageEnd(this.getApplicationInfo().className);
        MobclickAgent.onPause(this);
    }

    @Override
    public void finish() {
        super.finish();
    }
}
