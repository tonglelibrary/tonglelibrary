package com.ilingtong.library.tongle.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.model.ConfirmOrderModel;
import com.ilingtong.library.tongle.protocol.BaseDataResult;
import com.ilingtong.library.tongle.utils.ToastUtils;

public class BaseDataService extends Service {

	public void onCreate() {
        ServiceManager.doBaseDataRequest("", baseListener(), errorListener());

	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}
    /**
     * 功能：获取基础数据网络响应成功，返回数据
     */
    private Response.Listener<BaseDataResult> baseListener() {
        return new Response.Listener<BaseDataResult>() {
            @Override
            public void onResponse(BaseDataResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    ConfirmOrderModel.base_data_list = response.getBody().getBase_data_list();
                    Log.e("啊哈哈","     终于加载完了！");
                    Intent intent = new Intent(BaseDataService.this, BaseDataService.class);
                    stopService(intent);
                } else {
                    ToastUtils.toastShort(getString(R.string.para_exception) + response.getHead().getReturn_message());
                }
            }
        };
    }
    /**
     * 功能：网络响应失败
     */
    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ToastUtils.toastShort(getString(R.string.sys_exception));
            }
        };
    }

}
