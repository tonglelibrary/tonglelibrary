package com.ilingtong.library.tongle.model;

import com.ilingtong.library.tongle.protocol.BaseDataListItem;
import com.ilingtong.library.tongle.protocol.DataListItem;

import java.util.ArrayList;

/**
 * User: lengjiqiang
 * Date: 2015/6/9
 * Time: 11:19
 * Email: jqleng@isoftstone.com
 * Desc:
 */
public class ConfirmOrderModel {
    public static ArrayList<BaseDataListItem> base_data_list;
    public static ArrayList<DataListItem> deliveryModeList;
    public static ArrayList<DataListItem> deliveryTimeList;
    public static DataListItem checkDeliveryMode = new DataListItem();
    public static DataListItem checkDeliveryTime = new DataListItem();
}
