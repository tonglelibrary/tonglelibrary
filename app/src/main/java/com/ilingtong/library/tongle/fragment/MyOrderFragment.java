//package com.ilingtong.app.tongle.fragment;
//
//import android.app.Activity;
//import android.app.Fragment;
//import android.app.LocalActivityManager;
//import android.content.Intent;
//import android.content.res.ColorStateList;
//import android.content.res.Resources;
//import android.os.Bundle;
//import android.support.v4.view.ViewPager;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ImageView;
//import android.widget.TextView;
//import com.ilingtong.app.tongle.R;
//import com.ilingtong.app.tongle.activity.MainActivity;
//import com.ilingtong.app.tongle.activity.OrderMineActivity;
//import com.ilingtong.app.tongle.activity.OrderTotalActivity;
//import com.ilingtong.app.tongle.activity.OrderWaitGetActivity;
//import com.ilingtong.app.tongle.activity.OrderWaitPayActivity;
//import com.ilingtong.app.tongle.activity.OrderWaiteValuateActivity;
//import com.ilingtong.app.tongle.adapter.TLPagerAdapter;
//
//import java.util.ArrayList;
//
///**
// * User: syc
// * Date: 2015/6/11
// * Time: 16:23
// * Email: ycshi@isoftstone.com
// * Desc: 我的订单
// */
//public class MyOrderFragment extends Fragment implements View.OnClickListener{
//    private ImageView left_arrow_btn;
//    private TextView top_name;
//    private TextView totalorder;
//    private TextView waitevaluate;
//    private TextView waitpay;
//    private TextView waitget;
//    private ViewPager viewPager;
//
//    private ImageView imageView1;
//    private ImageView imageView2;
//    private ImageView imageView3;
//    private ImageView imageView4;
//
//    private View totalorderView;
//    private View waitPayView;
//    private View waiteValuateView;
//    private View waitGetView;
//
//    ArrayList<View> viewlist;
//    LocalActivityManager manager;
//    private TLPagerAdapter pageAdapter;
//    private int currIndex = 0;
//
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        manager = new LocalActivityManager(getActivity(), true);
//        manager.dispatchCreate(savedInstanceState);
//        View rootView = inflater.inflate(R.layout.fragment_orderform_layout, null, false);
//        initView(rootView);
//        return rootView;
//    }
//
//    @Override
//    public void onActivityCreated(Bundle savedInstanceState) {
//        super.onActivityCreated(savedInstanceState);
//        select();
//    }
//    public void initView(View rootView){
//        top_name = (TextView) rootView.findViewById(R.id.top_name);
//        left_arrow_btn = (ImageView) rootView.findViewById(R.id.left_arrow_btn);
//        totalorder = (TextView) rootView.findViewById(R.id.total_order);
//        waitevaluate = (TextView) rootView.findViewById(R.id.wait_evaluate);
//        waitpay = (TextView) rootView.findViewById(R.id.wait_pay);
//        waitget = (TextView) rootView.findViewById(R.id.wait_get);
//        viewPager = (ViewPager) rootView.findViewById(R.id.order_pager);
//        imageView1 = (ImageView) rootView.findViewById(R.id.order_cursor1);
//        imageView2 = (ImageView) rootView.findViewById(R.id.order_cursor2);
//        imageView3 = (ImageView) rootView.findViewById(R.id.order_cursor3);
//        imageView4 = (ImageView) rootView.findViewById(R.id.order_cursor4);
//
//        top_name.setText(getString(R.string.my_order));
//        top_name.setGravity(View.TEXT_ALIGNMENT_CENTER);
//        top_name.setVisibility(View.VISIBLE);
//        left_arrow_btn.setVisibility(View.VISIBLE);
//        left_arrow_btn.setOnClickListener(this);
//    }
//    public void select(){
//        totalorder.setOnClickListener(new TextOnClickListener(0));
//        waitpay.setOnClickListener(new TextOnClickListener(1));
//        waitget.setOnClickListener(new TextOnClickListener(2));
//        waitevaluate.setOnClickListener(new TextOnClickListener(3));
//        viewlist = new ArrayList<View>();
//
//        Intent totalorderIntent = new Intent(getActivity(), OrderTotalActivity.class);
//        totalorderView = manager.startActivity("TotalOrderActivity", totalorderIntent).getDecorView();
//
//        Intent waitPayIntent = new Intent(getActivity(), OrderWaitPayActivity.class);
//        waitPayView = manager.startActivity("WaitPayActivity", waitPayIntent).getDecorView();
//
//        Intent waiteValuate = new Intent(getActivity(), OrderWaiteValuateActivity.class);
//        waiteValuateView = manager.startActivity("WaiteValuateActivity", waiteValuate).getDecorView();
//
//        Intent waitGet = new Intent(getActivity(), OrderWaitGetActivity.class);
//        waitGetView = manager.startActivity("WaitGetActivity", waitGet).getDecorView();
//
//        viewlist.add(totalorderView);
//        viewlist.add(waitPayView);
//        viewlist.add(waitGetView);
//        viewlist.add(waiteValuateView);
//
//        pageAdapter = new TLPagerAdapter(viewlist);
//        viewPager.setAdapter(pageAdapter);
//        viewPager.setCurrentItem(0);
//        viewPager.setOnPageChangeListener(new ViewPageOnPageChangeListener());
//    }
//
//
//    private class TextOnClickListener implements View.OnClickListener {
//        private int index = 0;
//
//        public TextOnClickListener(int i) {
//            index = i;
//        }
//
//        public void onClick(View v) {
//            viewPager.setCurrentItem(index);
//        }
//    }
//
//
//    public class ViewPageOnPageChangeListener implements ViewPager.OnPageChangeListener {
//        public void onPageScrollStateChanged(int arg0) {
//        }
//
//        public void onPageScrolled(int arg0, float arg1, int arg2) {
//        }
//
//        public void onPageSelected(int arg0) {
//            currIndex = arg0;
//            int i = currIndex + 1;
//            setTabTitle(i);
//        }
//    }
//
//    /**
//     * 设置选项卡title的字体颜色
//     *
//     * @param tabId
//     */
//    public void setTabTitle(int tabId) {
//        //viewpager顶部导航游标，并设置游标显示位置
//        if (tabId == 1) {
//            imageView1.setVisibility(View.VISIBLE);
//            imageView2.setVisibility(View.INVISIBLE);
//            imageView3.setVisibility(View.INVISIBLE);
//            imageView4.setVisibility(View.INVISIBLE);
//        } else if (tabId == 2) {
//            imageView1.setVisibility(View.INVISIBLE);
//            imageView2.setVisibility(View.VISIBLE);
//            imageView3.setVisibility(View.INVISIBLE);
//            imageView4.setVisibility(View.INVISIBLE);
//        } else if (tabId == 3) {
//            imageView1.setVisibility(View.INVISIBLE);
//            imageView2.setVisibility(View.INVISIBLE);
//            imageView3.setVisibility(View.VISIBLE);
//            imageView4.setVisibility(View.INVISIBLE);
//        } else if (tabId == 4) {
//
//            imageView1.setVisibility(View.INVISIBLE);
//            imageView2.setVisibility(View.INVISIBLE);
//            imageView3.setVisibility(View.INVISIBLE);
//            imageView4.setVisibility(View.VISIBLE);
//        }
//        //viewpager顶部文字导航，并设置文字颜色
//        Resources resource = (Resources) getResources();
//        ColorStateList selectedTextColor = (ColorStateList) resource.getColorStateList(R.color.subcollect_fragment_selecttext_color);
//        ColorStateList unselectedTextColor = (ColorStateList) resource.getColorStateList(R.color.subcollect_fragment_text_color);
//
//        if (tabId == 1) {
//            totalorder.setTextColor(selectedTextColor);
//            waitpay.setTextColor(unselectedTextColor);
//            waitget.setTextColor(unselectedTextColor);
//            waitevaluate.setTextColor(unselectedTextColor);
//
//        } else if (tabId == 2) {
//            totalorder.setTextColor(unselectedTextColor);
//            waitpay.setTextColor(selectedTextColor);
//            waitget.setTextColor(unselectedTextColor);
//            waitevaluate.setTextColor(unselectedTextColor);
//        } else if (tabId == 3) {
//            totalorder.setTextColor(unselectedTextColor);
//            waitpay.setTextColor(unselectedTextColor);
//            waitget.setTextColor(selectedTextColor);
//            waitevaluate.setTextColor(unselectedTextColor);
//        } else if (tabId == 4) {
//            totalorder.setTextColor(unselectedTextColor);
//            waitpay.setTextColor(unselectedTextColor);
//            waitget.setTextColor(unselectedTextColor);
//            waitevaluate.setTextColor(selectedTextColor);
//        }
//    }
//    @Override
//    public void onClick(View view) {
//        if (view.getId() == R.id.left_arrow_btn){
//            Activity myorder = (OrderMineActivity) getActivity();
//            Bundle bundle =myorder.getIntent().getBundleExtra("ConfirmOrder");
//            if (bundle != null) {
//                myorder.finish();
//                Intent intent = new Intent(myorder.getApplicationContext(),MainActivity.class);
//                Bundle bundle1 = new Bundle();
//                bundle1.putString("myorderfragment","myorderfragment");
//                intent.putExtras(bundle);
//                startActivity(intent);
//            } else {
//                myorder.finish();
//            }
//        }
//    }
//}
