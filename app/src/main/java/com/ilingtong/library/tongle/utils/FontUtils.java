package com.ilingtong.library.tongle.utils;

import android.text.Html;
import android.widget.EditText;
import android.widget.TextView;

import java.text.DecimalFormat;

/**
 * 字体工具类
 * 
 * @author GaiQS E-mail:gaiqs@sina.com
 * @Date: 2015年1月28日
 * @Time: 下午9:32:15
 */
public class FontUtils {

	/**
	 * 设置字体颜色
	 * 
	 * @param textView
	 *            传入需要变色的view
	 * @param string
	 *            防止adpter重载的问题，传入未变色的字
	 * @param colorStr
	 *            传入变色的字
	 */
	public static void setFontColorRED(final TextView textView, String string, String colorStr) {
		String str = string;
		Object[] arrayOfObject = new Object[1];
		arrayOfObject[0] = ("<font color=#f13641>" + colorStr + "</font>");
		textView.setText(Html.fromHtml(String.format(str, arrayOfObject)));
	}

	public static void setFontColorRED(final EditText editText, String string, String colorStr) {
		String str = string;
		Object[] arrayOfObject = new Object[1];
		arrayOfObject[0] = ("<font color=#f13641>" + colorStr + "</font>");
		editText.setText(Html.fromHtml(String.format(str, arrayOfObject)));
	}

	/**
	 * 设置字体颜色
	 * 
	 * @param textView
	 *            传入需要变色的View
	 * @param colorStr
	 *            传入变色的字体
	 */
	public static void setFontColorRED(TextView textView, String colorStr) {
		String str = textView.getText().toString();
		Object[] arrayOfObject = new Object[1];
		arrayOfObject[0] = ("<font color=#f13641>" + colorStr + "</font>");
		textView.setText(Html.fromHtml(String.format(str, arrayOfObject)));
	}

	/**
	 * 价格显示保留两位小数
	 */
	public static String setTwoDecimal(double price) {
		DecimalFormat decimalFormat = new DecimalFormat("0.00");
		return decimalFormat.format(price);
	}
}
