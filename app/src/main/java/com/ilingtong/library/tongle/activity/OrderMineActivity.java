//package com.ilingtong.app.tongle.activity;
//
//import android.app.Activity;
//import android.app.FragmentManager;
//import android.app.FragmentTransaction;
//import android.os.Bundle;
//import com.ilingtong.app.tongle.R;
//import com.ilingtong.app.tongle.fragment.MyOrderFragment;
//
///**
// * User: lengjiqiang
// * Date: 2015/6/7
// * Time: 22:41
// * Email: jqleng@isoftstone.com
// * Desc: 我的订单页
// */
//public class OrderMineActivity extends BaseActivity {
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.order_mine_layout);
//        MyOrderFragment fragement = new MyOrderFragment();
//        FragmentManager manager = getFragmentManager();
//        FragmentTransaction transaction = manager.beginTransaction();
//        transaction.replace(R.id.myorder_fram, fragement);
//        transaction.commit();
//    }
//}
