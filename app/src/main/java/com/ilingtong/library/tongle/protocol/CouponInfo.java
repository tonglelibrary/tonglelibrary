package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/3/11.
 * mail: wuqian@ilingtong.com
 * Description:团购券信息
 * use by 6013
 */
public class CouponInfo implements Serializable {
    private String operator_date;    //  订单日期/退款日期/使用日期/过期日期（不同状态下对应日期代表的意义不同）
    private double amount;   //金额
    private String coupon_id;        // 券号

    public String getOperator_date() {
        return operator_date;
    }

    public void setOperator_date(String operator_date) {
        this.operator_date = operator_date;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getCoupon_id() {
        return coupon_id;
    }

    public void setCoupon_id(String coupon_id) {
        this.coupon_id = coupon_id;
    }
}
