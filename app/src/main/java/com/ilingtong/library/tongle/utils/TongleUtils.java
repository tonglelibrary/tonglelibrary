package com.ilingtong.library.tongle.utils;

import android.os.Handler;

public class TongleUtils {
	private static final boolean adsTest = true;// 是否开启广告测试

//	/**
//	 * 首次获取列表数据
//	 *
//	 * @param type
//	 * @param subType
//	 */
//	public static void initJokeList(final int type, final LoadJokeListener listener) {
//		if (-1 != PreferenceManager.getJokeId("maxid", type)) {
//			getDownData(PreferenceManager.getJokeId("minid", type), type, listener);
//		} else {
//			NetworkManager.getJokeList(type, new SimpleRequestCallback<String>() {
//				@Override
//				public void onSuccess(ResponseInfo<String> responseInfo) {
//					super.onSuccess(responseInfo);
//					JokeBean list = JsonUtils.getObject(responseInfo.result, JokeBean.class);
//					if (null != list && null != list.data) {
//						PreferenceManager.setJokeId("maxid", type, list.data.get(0).id);
//						PreferenceManager.setJokeId("minid", type, list.data.get(list.data.size() - 1).id);
//					}
//					getJokeList(list, listener);
//				}
//
//				@Override
//				public void onFailure(HttpException error, String msg) {
//					super.onFailure(error, msg);
//					if (null != listener) {
//						listener.noItem(-1, null);
//					}
//				}
//			});
//		}
//
//	}
//
//	/**
//	 * 下拉加载数据
//	 *
//	 * @param arguments
//	 * @param loadJokeListener
//	 */
//	public static void getUpData(final int id, final int type, final LoadJokeListener listener) {
//		NetworkManager.getJokeListUp(id, type, new SimpleRequestCallback<String>() {
//			@Override
//			public void onSuccess(ResponseInfo<String> responseInfo) {
//				super.onSuccess(responseInfo);
//				JokeBean list = JsonUtils.getObject(responseInfo.result, JokeBean.class);
//				if (null != list && null != list.data) {
//					PreferenceManager.setJokeId("maxid", type, list.data.get(0).id);
//				}
//				getJokeList(list, listener);
//			}
//
//			@Override
//			public void onFailure(HttpException error, String msg) {
//				super.onFailure(error, msg);
//				if (null != listener) {
//					listener.noItem(-1, null);
//				}
//			}
//		});
//	}

//	/**
//	 * 上拉加载数据
//	 *
//	 * @param arguments
//	 * @param loadJokeListener
//	 */
//	public static void getDownData(final int id, final int type, final LoadJokeListener listener) {
//		NetworkManager.getJokeListDown(id, type, new SimpleRequestCallback<String>() {
//			@Override
//			public void onSuccess(ResponseInfo<String> responseInfo) {
//				super.onSuccess(responseInfo);
//				JokeBean list = JsonUtils.getObject(responseInfo.result, JokeBean.class);
//				if (null != list && null != list.data) {
//					PreferenceManager.setJokeId("minid", type, list.data.get(list.data.size() - 1).id);
//				}
//				getJokeList(list, listener);
//			}
//
//			@Override
//			public void onFailure(HttpException error, String msg) {
//				super.onFailure(error, msg);
//				if (null != listener) {
//					listener.noItem(-1, null);
//				}
//			}
//		});
//	}
//
//	private static void getJokeList(final JokeBean list, final LoadJokeListener listener) {
//		if (null != list && list.status == 200) {
//			if (adsTest) {
//				if (list.data.size() > App.adsInsert) {
//					int adsNumber = list.data.size() / App.adsInsert;
//					if (adsNumber > 0 && adsNumber < App.adsMax) {
//						for (int i = 1; i <= adsNumber; i++) {
//							AdsInfo info = App.getAds();
//							if (null != info) {
//								list.data.get(i == 1 ? i * App.adsInsert : (i * App.adsInsert) + 1).adsInfo = info;
//							}
//						}
//					}
//				}
//			}
//			if (null != listener && null != list.data) {
//				listener.loadJoke(list.data, true);
//			}
//		} else {
//			if (null != list && list.status == 100) {
//				if (null != listener) {
//					listener.noItem(100, null);
//				}
//				ToastUtils.toastShort(R.string.data_not);
//			} else {
//				ToastUtils.toastShort(R.string.data_failure);
//			}
//		}
//	}

	/**
	 * 延迟执行任务,自定义时间
	 * 
	 * @param runnable
	 *            任务
	 * @param duration
	 *            任务等待时长
	 */
	public static void postDelayed(Runnable runnable, long duration) {
		new Handler().postDelayed(runnable, duration);
	}

	// /**
	// * 插入数据到数据库
	// *
	// * @param type
	// * 分类id
	// * @param subType
	// * 子分类id
	// * @param info
	// */
	// public static void buldInsertData(final String a, final String data,
	// final LoadJokeListener listener) {
	// MyAsyncTask.executor(new OperateResultListener<List<Data>>() {
	// @Override
	// public List<Data> operate() {
	// List<Data> jokes = JsonUtils.getJokeList(data);
	// if (null != jokes && !jokes.isEmpty()) {
	// // 插入数据库操作
	// ContentResolver resolver = App.getAppContext().getContentResolver();
	// List<ContentValues> valueList = new ArrayList<ContentValues>();
	// int length = jokes.size();
	// Cursor cursor = null;
	// ContentValues values = null;
	// for (int i = 0; i < length; i++) {
	// Data textJoke = jokes.get(i);
	// // textJoke.a = a;
	// values = jokes.get(i).getContentValues();
	// // 添加到收藏列表
	// values.put("ut", System.currentTimeMillis());
	// cursor = resolver.query(JokeTable.JOKE_URI, null, "id=?  and a=?", new
	// String[] { String.valueOf(textJoke.id), a, }, null);
	// if (null != cursor && cursor.moveToFirst()) {
	// resolver.update(JokeTable.JOKE_URI, values, "id=? and a=?", new String[]
	// { String.valueOf(textJoke.id), a });
	// } else {
	// // 当前创建时间
	// values.put("ct", System.currentTimeMillis());
	// valueList.add(values);
	// }
	// if (null != cursor) {
	// cursor.close();
	// }
	// }
	// if (!valueList.isEmpty()) {
	// // Logger.appendInfo("action:" + a + " 缓存:" +
	// // valueList.size() + "条数据");
	// resolver.bulkInsert(JokeTable.JOKE_URI, valueList.toArray(new
	// ContentValues[valueList.size()]));
	// }
	// }
	// return jokes;
	// }
	//
	// @Override
	// public void result(List<Data> jokes) {
	// if (null != listener) {
	// Log.i("jokeUtils.class", "action:" + a + " 缓存数据结束.开始加载列表");
	// listener.loadJoke(jokes, null != jokes && !jokes.isEmpty());
	// // 设置初始加载完毕
	// // PreferenceManager.setIsFirstLoad(a, true);
	// }
	// }
	// });
	// }
}
