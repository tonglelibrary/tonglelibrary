package com.ilingtong.library.tongle.activity;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;
import com.ilingtong.library.tongle.R;

/**
 * User: shuailei
 * Date: 2015/11/11
 * Time: 11:32
 * Email: leishuai@isoftstone.com
 * Desc: 商品积分详情
 */
public class MyAppointsActivity extends BaseActivity implements View.OnClickListener{
    private TextView top_name;
    private ImageView left_arrow_btn;
    private WebView webView;
    private String point_url,point_title;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.webview_comm);
        initView();
    }
    public void initView(){
        Bundle bundle = getIntent().getExtras();
        point_url = bundle.getString("point_url");
        point_title = bundle.getString("point_title");

        top_name = (TextView)findViewById(R.id.top_name);
        left_arrow_btn = (ImageView)findViewById(R.id.left_arrow_btn);
        webView = (WebView)findViewById(R.id.webview);
        left_arrow_btn.setOnClickListener(this);
        left_arrow_btn.setVisibility(View.VISIBLE);
        top_name.setVisibility(View.VISIBLE);
        top_name.setText(point_title);

        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(point_url);
    }
    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.left_arrow_btn){
            finish();
        }
    }

}