package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/6/30
 * Time: 17:15
 * Email: jqleng@isoftstone.com
 * Desc:
 */
public class ExpertBaseInfo implements Serializable {
    public String user_id;
    public String user_nick_name ;
    public String user_signature ;
    public String user_head_photo_url ;
    public String user_favorited_by_me ;
}
