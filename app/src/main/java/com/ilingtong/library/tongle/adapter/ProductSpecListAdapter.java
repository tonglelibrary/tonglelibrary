package com.ilingtong.library.tongle.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.protocol.ProdParamsSubListItem;

import java.util.ArrayList;
import java.util.List;

/**
 * User: lengjiqiang
 * Date: 2015/5/25
 * Time: 17:38
 * Email: jqleng@isoftstone.com
 * Dest:
 */
public class ProductSpecListAdapter extends BaseAdapter {
    private LayoutInflater inflater;
    private List<ProdParamsSubListItem> list;
    private ProdParamsSubListItem dataItem = new ProdParamsSubListItem();

    public ProductSpecListAdapter(Context context, ArrayList list) {
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder holder;
        if (view == null) {
            view = inflater.inflate(R.layout.list_item_prod_spec, null);
            holder = new ViewHolder();
            holder.item_name = (TextView) view.findViewById(R.id.item_name);
            holder.item_value = (TextView) view.findViewById(R.id.item_value);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        dataItem = list.get(position);
        holder.item_name.setText((CharSequence) dataItem.item_name);
        holder.item_value.setText((CharSequence) dataItem.item_value);
        return view;
    }

    static class ViewHolder {
        TextView item_name;
        TextView item_value;
    }
}
