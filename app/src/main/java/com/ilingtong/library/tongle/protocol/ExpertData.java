package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * User: lengjiqiang
 * Date: 2015/5/9
 * Time: 16:42
 * Email: jqleng@isoftstone.com
 * Dest:
 */
public class ExpertData implements Serializable {
    public String data_total_count;
    public ArrayList<FriendListItem> user_follow_friend_list;
}
