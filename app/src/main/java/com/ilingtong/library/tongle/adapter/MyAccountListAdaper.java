package com.ilingtong.library.tongle.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.activity.ProdIntegralActivity;
import com.ilingtong.library.tongle.protocol.MyAccountInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * User: lengjiqiang
 * Date: 2015/5/8
 * Time: 16:13
 * Email: jqleng@isoftstone.com
 * Desc: 达人列表适配器
 */
public class MyAccountListAdaper extends BaseAdapter {
    private LayoutInflater inflater;
    private List<MyAccountInfo> list;
    MyAccountInfo DataItem = new MyAccountInfo();
    private Context adapterContext;
    View line_view;

    public MyAccountListAdaper(Context context, ArrayList expertList) {
        adapterContext = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.list = expertList;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder holder;
        if (view == null) {
            view = inflater.inflate(R.layout.account_item, null);
            holder = new ViewHolder();

            holder.account_name = (TextView) view.findViewById(R.id.account_name);
            holder.account_value = (TextView) view.findViewById(R.id.account_value);

            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        DataItem = list.get(position);
        holder.account_name.setText(DataItem.account_type_name);
        holder.account_value.setText(DataItem.account_qty);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View convertView) {

                Intent expertIntent = new Intent(adapterContext, ProdIntegralActivity.class);
                Bundle bundle = new Bundle();
                String url = list.get(position).account_detail_url + "?user_id=" + TongleAppInstance.getInstance().getUserID()
                        + "&user_token=" + TongleAppInstance.getInstance().getToken()
                        + "&account_id=" + list.get(position).account_type_id
                        + "&account_name=" + list.get(position).account_type_name;
                bundle.putString("prod_url", url);
                bundle.putString("myTitle", list.get(position).account_detail_title);
                expertIntent.putExtras(bundle);
                adapterContext.startActivity(expertIntent);
            }
        });

        return view;
    }

    static class ViewHolder {
        TextView account_name, account_value;
    }
}
