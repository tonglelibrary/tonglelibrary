package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/6/13
 * Time: 18:20
 * Email: jqleng@isoftstone.com
 * Desc:
 */
public class DefaultAddressListItem implements Serializable {
    public String address_no;
    public String consignee;
    public String tel;
    public String province_id;
    public String province_name;
    public String city_id;
    public String city_name;
    public String area_id;
    public String area_name;
    public String address;
    public String post_code;
    public String default_flag;
}
