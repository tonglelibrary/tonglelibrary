package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/6/4
 * Time: 11:50
 * Email: jqleng@isoftstone.com
 * Desc:
 */
public class CollectForumResult implements Serializable {
    private BaseInfo head;
    private CollectForumInfo body;

    public BaseInfo getHead() {
        return head;
    }

    public void setHead(BaseInfo head) {
        this.head = head;
    }

    public CollectForumInfo getBody() {
        return body;
    }

    public void setBody(CollectForumInfo body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return body.toString();
    }
}
