package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/1/16.
 * mail: wuqian@ilingtong.com
 * Description:user by 2040接口
 */
public class CodeUserData implements Serializable {
    CodeBaseInfo user_data;

    public CodeBaseInfo getUser_data() {
        return user_data;
    }

    public void setUser_data(CodeBaseInfo user_data) {
        this.user_data = user_data;
    }
}
