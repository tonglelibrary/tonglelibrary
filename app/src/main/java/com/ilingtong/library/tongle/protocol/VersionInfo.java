package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/6/24
 * Time: 16:00
 * Email: jqleng@isoftstone.com
 * Desc: 返回的版本信息类，该类包含通过网络返回的版本相关数据
 */
public class VersionInfo implements Serializable{
    private String has_update;
    private String force_update;
    private String recently_version_no;
    private String recently_version_tips;
    private String recently_version_link;

    public String getHas_update() {
        return has_update;
    }

    public String getForce_update() {
        return force_update;
    }

    public String getRecently_version_no() {
        return recently_version_no;
    }

    public String getRecently_version_tips() {
        return recently_version_tips;
    }

    public String getRecently_version_link() {
        return recently_version_link;
    }

    @Override
    public String toString() {
        return "has_update:" + has_update + "\r\n" +
                "force_update:" + force_update + "\r\n" +
                "recently_version_no:" + recently_version_no + "\r\n" +
                "recently_version_tips:" + recently_version_tips + "\r\n" +
                "recently_version_link:" + recently_version_link;
    }
}
