package com.ilingtong.library.tongle.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.library.tongle.DBOperator;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.adapter.AreaSpinnerAdapter;
import com.ilingtong.library.tongle.model.AddressModel;
import com.ilingtong.library.tongle.model.ConfirmOrderModel;
import com.ilingtong.library.tongle.protocol.AddInfo;
import com.ilingtong.library.tongle.protocol.AddressListItem;
import com.ilingtong.library.tongle.protocol.AddressNo;
import com.ilingtong.library.tongle.protocol.AddressResult;
import com.ilingtong.library.tongle.protocol.BaseDataNode;
import com.ilingtong.library.tongle.protocol.BaseDataListItem;
import com.ilingtong.library.tongle.protocol.BaseDataResult;
import com.ilingtong.library.tongle.protocol.DataListItem;
import com.ilingtong.library.tongle.protocol.DelAddrResult;
import com.ilingtong.library.tongle.utils.ToastUtils;
import com.ilingtong.library.tongle.utils.Utils;

import java.util.ArrayList;

/**
 * User: lengjiqiang
 * Date: 2015/6/14
 * Time: 21:49
 * Email: jqleng@isoftstone.com
 * Desc: 修改默认地址列表页
 */
public class AddressModifyActivity extends BaseActivity implements View.OnClickListener {
    private TextView top_name;
    private TextView top_btn_text;
    private ImageView left_arrow_btn;
    private TextView delAddress;
    private EditText to_name;
    private EditText to_tel;
    private EditText post_code;
    private EditText detail_address;

    private Spinner provinceSpinner;
    private Spinner citySpinner;
    private Spinner districSpinner;

    AreaSpinnerAdapter provinceAdapter, cityAdapter, districAdapter;
    ArrayList<DataListItem> provinceDataList = new ArrayList<>();
    ArrayList<DataListItem> cityDataList = new ArrayList<>();
    ArrayList<DataListItem> districDataList = new ArrayList<>();
    ArrayList<BaseDataNode> proviceArray = new ArrayList<>();
    ArrayList<BaseDataNode> cityArray = new ArrayList<>();
    ArrayList<BaseDataNode> districArray = new ArrayList<>();

    String mProvinceCode;
    String mCityCode;
    String mDistricCode;
    String mAddressNo;
    String mProvinceName, mCityName, mDistricName;

    private DBOperator dbOper;
    int mAddressID;
    AddressListItem addressItem;//地址属性类

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.address_modify_layout);
        getData();
        initView();

    }

    public void getData() {
        mAddressID = getIntent().getExtras().getInt("address_id");
    }

    public void initView() {
        top_name = (TextView) findViewById(R.id.top_name);
        top_btn_text = (TextView) findViewById(R.id.top_btn_text);
        left_arrow_btn = (ImageView) findViewById(R.id.left_arrow_btn);
        delAddress = (TextView) findViewById(R.id.del_address_text);
        to_name = (EditText) findViewById(R.id.to_name_edit);
        to_tel = (EditText) findViewById(R.id.to_tel_edit);
        post_code = (EditText) findViewById(R.id.post_code_edit);
        detail_address = (EditText) findViewById(R.id.detail_address_edit);
        provinceSpinner = (Spinner) findViewById(R.id.province_spinner);
        citySpinner = (Spinner) findViewById(R.id.city_spinner);
        districSpinner = (Spinner) findViewById(R.id.distric_spinner);
        top_btn_text.setOnClickListener(this);
        left_arrow_btn.setOnClickListener(this);
        delAddress.setOnClickListener(this);

        addressItem = AddressModel.my_address_list.get(mAddressID);
        left_arrow_btn.setVisibility(View.VISIBLE);
        top_name.setVisibility(View.VISIBLE);
        top_name.setText(R.string.default_address);
        top_btn_text.setText(getString(R.string.address_modify_btn_top_txt));
        top_btn_text.setVisibility(View.VISIBLE);
        to_name.setText(addressItem.consignee);
        to_tel.setText(addressItem.tel);
        post_code.setText(addressItem.post_code + "");
        detail_address.setText(addressItem.address);
        mAddressNo = addressItem.address_no;

        if (ConfirmOrderModel.base_data_list == null)   //没有数据从网络获取数据
            doRequest();
        else {
            for (int i = 0; i < ConfirmOrderModel.base_data_list.size(); i++) {
                BaseDataListItem item = new BaseDataListItem();
                item = ConfirmOrderModel.base_data_list.get(i);
                if (item.base_data_type.equals(TongleAppConst.ADMINISTRATIVE)) {
                    provinceDataList = item.data_list;
                    for (int j = 0; j < item.data_list.size(); j++) {
                        BaseDataNode areaItem = new BaseDataNode();
                        areaItem.code = item.data_list.get(j).code;
                        areaItem.name = item.data_list.get(j).name;
                        proviceArray.add(areaItem);
                    }
                }
            }
            provinceAdapter = new AreaSpinnerAdapter(this, proviceArray);

            provinceSpinner.setAdapter(provinceAdapter);
            for (int i = 0; i < proviceArray.size(); i++) {
                if (AddressModel.my_address_list.get(mAddressID).province_id.equals(proviceArray.get(i).code)) {
                    provinceSpinner.setSelection(i);
                }
            }
        }
        provinceSpinner.setOnItemSelectedListener(new ProvinceSelectedListener());
        citySpinner.setOnItemSelectedListener(new citySelectedListener());
        districSpinner.setOnItemSelectedListener(new districSelectedListener());

    }

    public void doRequest() {
        ServiceManager.doBaseDataRequest("", baseDataListener(), errorListener());
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.left_arrow_btn) {
            finish();
        } else if (v.getId() == R.id.top_btn_text) {
            saveMessage();
        } else if (v.getId() == R.id.del_address_text) {
            delAddress();
        }
    }

    /**
     * 保存信息
     */
    public void saveMessage() {
        String mname = to_name.getText().toString();
        String mtel = to_tel.getText().toString();
        String mpostcode = post_code.getText().toString();
        String mdetailaddress = detail_address.getText().toString();

        AddInfo addInfo = new AddInfo();
        addInfo.consignee = mname;
        addInfo.tel = mtel;
        addInfo.address = mdetailaddress;
        addInfo.province_id = mProvinceCode;
        addInfo.city_id = mCityCode;
        addInfo.area_id = mDistricCode;
        addInfo.post_code = mpostcode;

        if (!Utils.isMobileNO(addInfo.tel) || (addInfo.tel.length() != TongleAppConst.PHONE_NUMBER_LENGTH)) {
            ToastUtils.toastShort(getString(R.string.regist_phone_error));
            return;
        }
//        if (!Utils.isPostcode(addInfo.post_code)) {
//            ToastUtils.toastShort(getString(R.string.regist_post_error));
//            return;
//        }
        if (mname.equals("")) {
            ToastUtils.toastShort(getString(R.string.common_username_null));
        } else if (mtel.equals("")) {
            ToastUtils.toastShort(getString(R.string.common_phone_null));
        } else if (mdetailaddress.equals("")) {
            ToastUtils.toastShort(getString(R.string.common_detail_null));
        } else {
            //更新网络端数据
            ServiceManager.doModifyAddressRequest(TongleAppInstance.getInstance().getUserID(), mAddressNo, addInfo, successListener(), errorListener());
            //更新数据库端数据
            if (dbOper == null) {
                dbOper = new DBOperator(AddressModifyActivity.this);
                String strSQL = "update Address_table set " +
                        "name='" + addInfo.consignee + "'," +
                        "phone='" + addInfo.tel + "'," +
                        "address='" + addInfo.address + "'," +
                        "province_name='" + mProvinceName + "'," +
                        "city_name='" + mCityName + "'," +
                        "area_name='" + mDistricName + "'" +
                        "province_id='" + addInfo.province_id + "'," +
                        "city_id='" + addInfo.city_id + "'," +
                        "area_id='" + addInfo.area_id + "'" +
                        " where address_no='" + mAddressNo + "'";
                dbOper.execWriteDB(strSQL);
            }
            //更新内存
            AddressModel.my_address_list.get(mAddressID).consignee = addInfo.consignee;
            AddressModel.my_address_list.get(mAddressID).tel = addInfo.tel;
            AddressModel.my_address_list.get(mAddressID).address = addInfo.address;
            AddressModel.my_address_list.get(mAddressID).province_id = addInfo.province_id;
            AddressModel.my_address_list.get(mAddressID).province_name = mProvinceName;
            AddressModel.my_address_list.get(mAddressID).city_id = addInfo.city_id;
            AddressModel.my_address_list.get(mAddressID).city_name = mCityName;
            AddressModel.my_address_list.get(mAddressID).area_id = addInfo.area_id;
            AddressModel.my_address_list.get(mAddressID).area_name = mDistricName;
            AddressModel.my_address_list.get(mAddressID).post_code = addInfo.post_code;
        }
    }

    /**
     * 功能：删除收货地址
     */
    public void delAddress() {
        ArrayList<AddressNo> addrNoList = new ArrayList<AddressNo>();
        AddressNo item = new AddressNo();
        item.address_no = mAddressNo;
        addrNoList.add(item);
        ServiceManager.doDeleteAddrRequest(TongleAppInstance.getInstance().getUserID(), addrNoList, deleteListener(), errorListener());

        AddressModel.my_address_list.remove(item);
        if (AddressModel.my_address_list.size() != 0) {
            for (int i = 0; i < AddressModel.my_address_list.size(); i++) {
                if (AddressModel.my_address_list.get(i).address_no.equals(mAddressNo)) {
                    AddressModel.my_address_list.remove(AddressModel.my_address_list.get(i));
                }
            }
            AddressModel.my_address_list.size();
        }
        if (dbOper != null) {
            String strSQL = "delete Address_table where address_no =" + mAddressNo;
            dbOper.execWriteDB(strSQL);
        }

    }

    /**
     * 功能：保存到网络
     */
    private Response.Listener successListener() {
        return new Response.Listener<AddressResult>() {
            @Override
            public void onResponse(AddressResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    ToastUtils.toastShort(getString(R.string.common_save_success));
                } else {
                    ToastUtils.toastShort(getString(R.string.para_exception) + response.getHead().getReturn_message());
                }
            }
        };
    }

    /**
     * 功能：请求网络响应失败
     */
    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ToastUtils.toastShort(getString(R.string.sys_exception));
            }
        };
    }

    /**
     * 功能：删除收货地址网络响应成功，返回数据
     */
    private Response.Listener<DelAddrResult> deleteListener() {
        return new Response.Listener<DelAddrResult>() {
            @Override
            public void onResponse(DelAddrResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    ToastUtils.toastShort(getString(R.string.common_delete_success));
                    AddressModifyActivity.this.finish();
                } else {
                    ToastUtils.toastShort(getString(R.string.para_exception) + response.getHead().getReturn_message());
                }
            }
        };
    }

    private class ProvinceSelectedListener implements AdapterView.OnItemSelectedListener {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            BaseDataNode provinceData = (BaseDataNode) provinceAdapter.getItem(position);
            mProvinceCode = provinceData.code;
            mProvinceName = provinceData.name;

            for (int i = 0; i < provinceDataList.size(); i++) {
                if (provinceDataList.get(i).name.equals(mProvinceName)) {
                    cityDataList = provinceDataList.get(i).sub_list;
                    cityArray.clear();
                    for (int j = 0; j < cityDataList.size(); j++) {
                        BaseDataNode areaItem = new BaseDataNode();
                        areaItem.code = cityDataList.get(j).code;
                        areaItem.name = cityDataList.get(j).name;
                        cityArray.add(areaItem);
                    }
                }
            }
            cityAdapter = new AreaSpinnerAdapter(getApplicationContext(), cityArray);
            citySpinner.setAdapter(cityAdapter);

            for (int i = 0; i < cityArray.size(); i++) {
                if (AddressModel.my_address_list.get(mAddressID).city_id.equals(cityArray.get(i).code)) {
                    citySpinner.setSelection(i, true);
                }
            }

        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }

    private class citySelectedListener implements AdapterView.OnItemSelectedListener {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            BaseDataNode cityData = (BaseDataNode) cityAdapter.getItem(position);
            mCityCode = cityData.code;
            mCityName = cityData.name;

            for (int i = 0; i < cityDataList.size(); i++) {
                if (cityDataList.get(i).name.equals(mCityName)) {
                    districDataList = cityDataList.get(i).sub_list;
                    districArray.clear();
                    for (int j = 0; j < districDataList.size(); j++) {
                        BaseDataNode areaItem = new BaseDataNode();
                        areaItem.code = districDataList.get(j).code;
                        areaItem.name = districDataList.get(j).name;
                        districArray.add(areaItem);
                    }
                }
            }
            districAdapter = new AreaSpinnerAdapter(getApplicationContext(), districArray);
            districSpinner.setAdapter(districAdapter);

            for (int i = 0; i < districArray.size(); i++) {
                if (AddressModel.my_address_list.get(mAddressID).area_id.equals(districArray.get(i).code)) {
                    districSpinner.setSelection(i, true);
                }
            }

        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }

    private class districSelectedListener implements AdapterView.OnItemSelectedListener {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            BaseDataNode districData = (BaseDataNode) districAdapter.getItem(position);
            mDistricCode = districData.code;
            mDistricName = districData.name;
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }

    /**
     * 功能：网络响应成功，返回数据
     */
    private Response.Listener<BaseDataResult> baseDataListener() {
        return new Response.Listener<BaseDataResult>() {
            @Override
            public void onResponse(BaseDataResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    ConfirmOrderModel.base_data_list = response.getBody().getBase_data_list();
                    for (int i = 0; i < ConfirmOrderModel.base_data_list.size(); i++) {
                        BaseDataListItem item = new BaseDataListItem();
                        item = ConfirmOrderModel.base_data_list.get(i);
                        if (item.base_data_type.equals(TongleAppConst.ADMINISTRATIVE)) {
                            provinceDataList = item.data_list;
                            for (int j = 0; j < item.data_list.size(); j++) {
                                BaseDataNode areaItem = new BaseDataNode();
                                areaItem.code = item.data_list.get(j).code;
                                areaItem.name = item.data_list.get(j).name;
                                proviceArray.add(areaItem);
                            }
                        }
                    }
                    provinceAdapter = new AreaSpinnerAdapter(AddressModifyActivity.this, proviceArray);
                    provinceSpinner.setAdapter(provinceAdapter);
                    for (int i = 0; i < proviceArray.size(); i++) {
                        if (AddressModel.my_address_list.get(mAddressID).province_id.equals(proviceArray.get(i).code)) {
                            provinceSpinner.setSelection(i);
                        }
                    }

                } else {
                    ToastUtils.toastShort(getString(R.string.para_exception) + response.getHead().getReturn_message());
                }
            }
        };
    }

}
