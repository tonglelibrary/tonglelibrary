package com.ilingtong.library.tongle.external.mychartview;

import android.content.Context;
import android.graphics.Color;

import com.ilingtong.library.tongle.protocol.IncomeListData;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.TimeSeries;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * User: syc
 * Date: 2015/6/18
 * Time: 17:28
 * Email: ycshi@isoftstone.com
 * Desc:  折线统计图统计图
 */
public class LineChart implements MyChartAbstract   {


    //收益数据列表
    private ArrayList<IncomeListData> incomelist;
    //颜色
    private int color;


   /* public LineChart(ArrayList<MyProceeds> list) {
        this.list = list;


    }*/

     public LineChart( ArrayList<IncomeListData> list,int i) {
         this.incomelist = list;
         this.color = i;


    }

    /**
     * 返回需要的视图
     */

    @Override
    public GraphicalView getView(Context context) {

        GraphicalView view = ChartFactory.getLineChartView(context, getDataSet(), getRenderer());
        return view;


    }


    /**
     * 取得数据集
     */

    private XYMultipleSeriesDataset getDataSet() {

        XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
        TimeSeries series = new TimeSeries("变化趋势");
        for (IncomeListData data : incomelist) {
                    series.add(strToDate(data.date),Double.parseDouble(data.amount));
        }
        dataset.addSeries(series);
        return dataset;
    }

    /**
     * 字符串转换日期
     */
    private Date strToDate(String s) {
        Date date = null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            date = sdf.parse(s);

        } catch (ParseException e) {

            e.printStackTrace();
        }
        return date;
    }


    /**
     * 设置图像渲染
     */
    private XYMultipleSeriesRenderer getRenderer() {


        XYMultipleSeriesRenderer renderer = new XYMultipleSeriesRenderer();




        renderer.isShowAxes();
        //允许缩放
        renderer.setZoomEnabled(true);
        // 不允许拖动
        renderer.setPanEnabled(false, false);
        renderer.setXLabels(0);
        renderer.setYLabels(0);
        //设置背景颜色
        renderer.setApplyBackgroundColor(true);
        renderer.setBackgroundColor(Color.TRANSPARENT);
        //设置边距
        renderer.setMargins(new int[]{20,20,20,20});
        renderer.setMarginsColor(Color.argb(0x00, 0x01, 0x01, 0x01));
        //显示图例
        renderer.setShowLegend(false);
        //不显示x,y轴
        renderer.setShowAxes(false);
        // 消除锯齿
        renderer.setAntialiasing(true);


        //设置线与点的显示样式
            XYSeriesRenderer r = new XYSeriesRenderer();

            r.setPointStyle(PointStyle.CIRCLE);
            r.isFillPoints();
            r.setColor(color);
            r.setLineWidth(5f); //折线宽度

            renderer.addSeriesRenderer(r);

        return renderer;

    }


}
