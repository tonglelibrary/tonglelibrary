package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/6/4
 * Time: 15:20
 * Email: jqleng@isoftstone.com
 * Desc:
 */
public class ADListItem implements Serializable {
    public String ad_no;
    public String ad_pic_url;
    public String object_id;
    public String promotion_mode;
}
