package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/6/30
 * Time: 17:15
 * Email: jqleng@isoftstone.com
 * Desc:
 */
public class CodeBaseInfo implements Serializable {
    public String user_id;
    public String phone ;
}
