package com.ilingtong.library.tongle.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.protocol.MStoreListItem;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wuqian on 2015/10/28.
 * mail: wuqian@ilingtong.com
 * Description: 魔店adapter
 *
 * 修改记录：3/25 去掉getview（）描边方法，修改成给item根布局设置selector背景的方式
 */
public class HomeMstoreGridViewAdapter extends BaseAdapter {
    private Context context;
    private List<MStoreListItem> mstore_list = new ArrayList<>();
    public static final int TEXTCOLOR = 0xFF3d4245;//mstore字体颜色

    public HomeMstoreGridViewAdapter(Context context, List<MStoreListItem> mstore_list) {
        this.context = context;
        this.mstore_list = mstore_list;
    }

    @Override
    public int getCount() {
        return mstore_list.size() > 4 ? 4 : mstore_list.size();
    }

    @Override
    public Object getItem(int position) {
        return mstore_list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        String txt = mstore_list.get(position).mstore_name;
        String url = mstore_list.get(position).mstore_pic_url;
        ViewHolder holder = new ViewHolder();
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.mstore_item_layout, null);
            holder.img_store_logo = (ImageView) convertView.findViewById(R.id.mstore_item_img_mstore_logo);
            holder.txt_store_name = (TextView) convertView.findViewById(R.id.mstore_item_txt_mstore_name);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.txt_store_name.setText(txt);
        ImageLoader.getInstance().displayImage(url, holder.img_store_logo, TongleAppInstance.options);
        return convertView;
    }

    class ViewHolder {
        ImageView img_store_logo;  //魔店logo；
        TextView txt_store_name;  //魔店名称
    }
}
