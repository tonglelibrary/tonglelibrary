package com.ilingtong.library.tongle.model;

import android.app.Activity;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.protocol.CollectProductData;
import com.ilingtong.library.tongle.protocol.GsonRequest;
import com.ilingtong.library.tongle.protocol.MStorePicList;
import com.ilingtong.library.tongle.protocol.ParametersJson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;

/**
 * User: shuailei
 * Date: 2015/5/122
 * Time: 15:54
 * Email:leishuai@isoftstone.com
 * Dest:
 */
public class MStoreDetailUnCollectModel extends Observable {
    final private String productUrl = TongleAppConst.SERVER_ADDRESS + ":" + TongleAppInstance.getInstance().getToken() + "/favorites/remove";
    private String TAG = "MSTOREDETAILUNCOLLECTMODEL";
    Activity appActivity;
    public ArrayList<MStorePicList> playersList = new ArrayList<MStorePicList>();
    public CollectProductData productData = new CollectProductData();

    public MStoreDetailUnCollectModel(Activity activity) {
        appActivity = activity;
    }

    public void requestProductModel(String user_id, String collection_type, String key_value) {
        RequestQueue queue = Volley.newRequestQueue(appActivity);
        Map<String, String> requestParam = new HashMap<String, String>();
        requestParam.put("user_id", user_id);
        requestParam.put("collection_type", collection_type);
        requestParam.put("key_value", key_value);
        Gson mGson = new Gson();
        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);

        GsonRequest<ParametersJson> gsonRequest = new GsonRequest<ParametersJson>(Request.Method.POST, productUrl, ParametersJson.class, params_gson, new Response.Listener<ParametersJson>() {
            @Override
            public void onResponse(ParametersJson parameters_json) {

                productData.function_id = parameters_json.head.function_id;
                setChanged();
                notifyObservers(TAG);
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG + " - ERROR", error.getMessage(), error);
            }
        });
        queue.add(gsonRequest);
    }
}
