//package com.ilingtong.app.tongle.fragment;
//
//import android.app.Fragment;
//import android.app.FragmentTransaction;
//import android.content.res.ColorStateList;
//import android.content.res.Resources;
//import android.os.Bundle;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ImageView;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//
//import com.ilingtong.app.tongle.R;
//
///**
// * User: lengjiqiang
// * Date: 2015/5/25
// * Time: 10:24
// * Email: jqleng@isoftstone.com
// * Dest:  产品更多
// */
//public class ProductMoreFragment extends Fragment {
//    private TextView detailText;
//    private TextView specText;
//    private TextView commentText;
//    private RelativeLayout detailFrame;
//    private RelativeLayout specFrame;
//    private RelativeLayout commentFrame;
//    private ImageView prod_detail_cursor1;
//    private ImageView prod_detail_cursor2;
//    private ImageView prod_detail_cursor3;
//    ProductDetailFragment detailFragment;
//    ProductSpecFragment specFragment;
//    ProductCommentFragment commentFragment;
//
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        View fragmentView = inflater.inflate(R.layout.fragment_product_more_layout, container, false);
//        initView(fragmentView);
//        return fragmentView;
//    }
//    public void initView(View fragmentView){
//        detailText = (TextView) fragmentView.findViewById(R.id.detail_text);
//        specText = (TextView) fragmentView.findViewById(R.id.spec_text);
//        commentText = (TextView) fragmentView.findViewById(R.id.comment_text);
//        detailFrame = (RelativeLayout) fragmentView.findViewById(R.id.detail_frame);
//        specFrame = (RelativeLayout) fragmentView.findViewById(R.id.spec_frame);
//        commentFrame = (RelativeLayout) fragmentView.findViewById(R.id.comment_frame);
//        prod_detail_cursor1 = (ImageView) fragmentView.findViewById(R.id.prod_detail_cursor1);
//        prod_detail_cursor2 = (ImageView) fragmentView.findViewById(R.id.prod_detail_cursor2);
//        prod_detail_cursor3 = (ImageView) fragmentView.findViewById(R.id.prod_detail_cursor3);
//
//        detailFrame.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onTabSelected("detail_tab");
//                prod_detail_cursor1.setVisibility(View.VISIBLE);
//                prod_detail_cursor2.setVisibility(View.INVISIBLE);
//                prod_detail_cursor3.setVisibility(View.INVISIBLE);
//            }
//        });
//        specFrame.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onTabSelected("spec_tab");
//                prod_detail_cursor1.setVisibility(View.INVISIBLE);
//                prod_detail_cursor2.setVisibility(View.VISIBLE);
//                prod_detail_cursor3.setVisibility(View.INVISIBLE);
//            }
//        });
//        commentFrame.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onTabSelected("comment_tab");
//                prod_detail_cursor1.setVisibility(View.INVISIBLE);
//                prod_detail_cursor2.setVisibility(View.INVISIBLE);
//                prod_detail_cursor3.setVisibility(View.VISIBLE);
//            }
//        });
//        onTabSelected("detail_tab");
//    }
//
//    public void onTabSelected(String tabName) {
//        Resources resource = (Resources) getResources();
//        final ColorStateList selectedTextColor = (ColorStateList) resource.getColorStateList(R.color.expert_select_color);
//        final ColorStateList unselectedTextColor = (ColorStateList) resource.getColorStateList(R.color.expert_unselect_color);
//        if (tabName == "detail_tab") {
//            if (null == detailFragment) {
//                detailFragment = new ProductDetailFragment();
//            }
//            FragmentTransaction localFragmentTransaction = getFragmentManager().beginTransaction();
//            localFragmentTransaction.replace(R.id.product_more_fragment_container, detailFragment);
//            localFragmentTransaction.commit();
//            detailText.setTextColor(selectedTextColor);
//            specText.setTextColor(unselectedTextColor);
//            commentText.setTextColor(unselectedTextColor);
//        } else if (tabName == "spec_tab") {
//            if (null == specFragment) {
//                specFragment = new ProductSpecFragment();
//            }
//            FragmentTransaction localFragmentTransaction = getFragmentManager().beginTransaction();
//            localFragmentTransaction.replace(R.id.product_more_fragment_container, specFragment);
//            localFragmentTransaction.commit();
//            detailText.setTextColor(unselectedTextColor);
//            specText.setTextColor(selectedTextColor);
//            commentText.setTextColor(unselectedTextColor);
//        } else if (tabName == "comment_tab") {
//            if (null == commentFragment) {
//                commentFragment = new ProductCommentFragment();
//            }
//            FragmentTransaction localFragmentTransaction = getFragmentManager().beginTransaction();
//            localFragmentTransaction.replace(R.id.product_more_fragment_container, commentFragment);
//            localFragmentTransaction.commit();
//            detailText.setTextColor(unselectedTextColor);
//            specText.setTextColor(unselectedTextColor);
//            commentText.setTextColor(selectedTextColor);
//        }
//    }
//}
