package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by wuqian on 2016/3/11.
 * mail: wuqian@ilingtong.com
 * Description:团购商品详情的商品基础信息
 * use by 6007
 */
public class GroupPprodBaseInfo implements Serializable {
    private String prod_id;    // 商品ID
    private String prod_name;   //商品名称
    private String prod_resume;  //商品简述
    private String prod_property;   //商品属性
    private List<ProdPicUrlListItem> prod_pic_url_list;   //商品图片URL列表
    private String prod_status;        // 商品状态
    private String mstore_id;   //M店ID
    private String mstore_name;  //M店名称
    private double price;   // 现售价
    private double show_price;  //原售价
    private int stock_qty;  //库存数量
    private String good_rating_percent;   //商品好评度
    private String good_rating_qty;   //商品评价数
    private String prod_favorited_by_me;  //是否被当前用户收藏
    private String trade_prod_favorited_by_me;  //是否被当前用户收藏为宝贝
    private String relation_id;//二维码关联ID
    private String post_id;   //M贴ID
    private String sender;  //发货方
    private String point_rule_url;   //积分规则URL
    private String point_rule_title;   //积分规则Title
    private String sold_qty;   //已售数量
    private int buy_limit_qty;   // 限购数量
    private ArrayList<ProductSpecListItem> spec_detail_list;   //规格列表

    public String getProd_id() {
        return prod_id;
    }

    public void setProd_id(String prod_id) {
        this.prod_id = prod_id;
    }

    public String getProd_name() {
        return prod_name;
    }

    public void setProd_name(String prod_name) {
        this.prod_name = prod_name;
    }

    public String getProd_resume() {
        return prod_resume;
    }

    public void setProd_resume(String prod_resume) {
        this.prod_resume = prod_resume;
    }

    public String getProd_property() {
        return prod_property;
    }

    public void setProd_property(String prod_property) {
        this.prod_property = prod_property;
    }

    public List<ProdPicUrlListItem> getProd_pic_url_list() {
        return prod_pic_url_list;
    }

    public void setProd_pic_url_list(List<ProdPicUrlListItem> prod_pic_url_list) {
        this.prod_pic_url_list = prod_pic_url_list;
    }

    public String getProd_status() {
        return prod_status;
    }

    public void setProd_status(String prod_status) {
        this.prod_status = prod_status;
    }

    public String getMstore_id() {
        return mstore_id;
    }

    public void setMstore_id(String mstore_id) {
        this.mstore_id = mstore_id;
    }

    public String getMstore_name() {
        return mstore_name;
    }

    public void setMstore_name(String mstore_name) {
        this.mstore_name = mstore_name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getShow_price() {
        return show_price;
    }

    public void setShow_price(double show_price) {
        this.show_price = show_price;
    }

    public int getStock_qty() {
        return stock_qty;
    }

    public void setStock_qty(int stock_qty) {
        this.stock_qty = stock_qty;
    }

    public String getGood_rating_percent() {
        return good_rating_percent;
    }

    public void setGood_rating_percent(String good_rating_percent) {
        this.good_rating_percent = good_rating_percent;
    }

    public String getGood_rating_qty() {
        return good_rating_qty;
    }

    public void setGood_rating_qty(String good_rating_qty) {
        this.good_rating_qty = good_rating_qty;
    }

    public String getProd_favorited_by_me() {
        return prod_favorited_by_me;
    }

    public void setProd_favorited_by_me(String prod_favorited_by_me) {
        this.prod_favorited_by_me = prod_favorited_by_me;
    }

    public String getTrade_prod_favorited_by_me() {
        return trade_prod_favorited_by_me;
    }

    public void setTrade_prod_favorited_by_me(String trade_prod_favorited_by_me) {
        this.trade_prod_favorited_by_me = trade_prod_favorited_by_me;
    }

    public String getRelation_id() {
        return relation_id;
    }

    public void setRelation_id(String relation_id) {
        this.relation_id = relation_id;
    }

    public String getPost_id() {
        return post_id;
    }

    public void setPost_id(String post_id) {
        this.post_id = post_id;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getPoint_rule_url() {
        return point_rule_url;
    }

    public void setPoint_rule_url(String point_rule_url) {
        this.point_rule_url = point_rule_url;
    }

    public String getPoint_rule_title() {
        return point_rule_title;
    }

    public void setPoint_rule_title(String point_rule_title) {
        this.point_rule_title = point_rule_title;
    }

    public String getSold_qty() {
        return sold_qty;
    }

    public void setSold_qty(String sold_qty) {
        this.sold_qty = sold_qty;
    }

    public int getBuy_limit_qty() {
        return buy_limit_qty;
    }

    public void setBuy_limit_qty(int buy_limit_qty) {
        this.buy_limit_qty = buy_limit_qty;
    }

    public ArrayList<ProductSpecListItem> getSpec_detail_list() {
        return spec_detail_list;
    }

    public void setSpec_detail_list(ArrayList<ProductSpecListItem> spec_detail_list) {
        this.spec_detail_list = spec_detail_list;
    }
}
