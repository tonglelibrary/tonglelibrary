package com.ilingtong.library.tongle.model;

import com.ilingtong.library.tongle.protocol.OrderListItemData;

import java.util.ArrayList;

/**
 * User: shiyuchong
 * Date: 2015/6/10
 * Time: 14:27
 * Email: ycshi@isoftstone.com
 * Desc:[1040]
 */
public class UserOrdersModel {
    //数据总条数
    public String data_total_count;
    //我的订单列表
    public ArrayList<OrderListItemData> order_list;

}
