package com.ilingtong.library.tongle.fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.activity.MyCouponActivity;
import com.ilingtong.library.tongle.activity.MyGroupTicketActivity;
import com.ilingtong.library.tongle.activity.MyOrderMainActivity;
import com.ilingtong.library.tongle.activity.SettingActivity;
import com.ilingtong.library.tongle.activity.SettingBoxContactsActivity;
import com.ilingtong.library.tongle.activity.SettingFeedbackActivity;
import com.ilingtong.library.tongle.activity.SettingMyAccountActivity;
import com.ilingtong.library.tongle.activity.SettingMyBoxAPPActivity;
import com.ilingtong.library.tongle.activity.SettingMyBoxBabyActivity;
import com.ilingtong.library.tongle.activity.SettingMyInfoActivity;
import com.ilingtong.library.tongle.activity.SettingServiceAdminActivity;
import com.ilingtong.library.tongle.protocol.MineResult;
import com.ilingtong.library.tongle.utils.ImageUtil;
import com.ilingtong.library.tongle.utils.ToastUtils;
import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * User: lengjiqiang
 * Date: 2015/4/24
 * Time: 16:38
 * Email: jqleng@isoftstone.com
 * Desc: "我的"属性页
 */
public class MineFragment extends BaseFragment implements View.OnClickListener {
    private TextView tongle_name;
    private TextView tongle_no;
    private TextView integral;
    private TextView account_balance;
    private TextView coupon;
    private LinearLayout itemMyCountLiner;
    private LinearLayout itemMyOrderLiner;
    //    private LinearLayout itemMyGalleryLiner;
    private LinearLayout itemSettingLiner;
    private LinearLayout itemAppServiceLiner;
    private LinearLayout itemFeedbackLiner;
    private RelativeLayout ll_myuser;
    private ImageView headIconImg;
    private RelativeLayout to_information;
    public static MineFragment instance = null;
    private LinearLayout itemMyBoxLiner;//我的宝贝
    private LinearLayout itemMyAppLiner;//app下载推广
    private LinearLayout itemMyContactsLiner;//我的人脉圈
    private LinearLayout itemMyQrCodeLiner;//我的二维码
    private LinearLayout itemMyGroupBuy;  //我的团购券
    private LinearLayout itemMyCoupon;   //我的优惠券

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_mine_layout, null);
        initView(rootView);
        doRequest();
        return rootView;
    }

    public void initView(View rootView) {
        tongle_name = (TextView) rootView.findViewById(R.id.tongle_name);
        tongle_no = (TextView) rootView.findViewById(R.id.tongle_no);
        integral = (TextView) rootView.findViewById(R.id.integral);
        account_balance = (TextView) rootView.findViewById(R.id.account_balance);
        coupon = (TextView) rootView.findViewById(R.id.coupon);
        itemMyCountLiner = (LinearLayout) rootView.findViewById(R.id.item_my_count);
        itemMyOrderLiner = (LinearLayout) rootView.findViewById(R.id.item_my_order);
//        itemMyGalleryLiner = (LinearLayout) rootView.findViewById(R.id.item_my_gallery);
        itemSettingLiner = (LinearLayout) rootView.findViewById(R.id.item_my_setting);
        itemAppServiceLiner = (LinearLayout) rootView.findViewById(R.id.item_app_service);
        itemFeedbackLiner = (LinearLayout) rootView.findViewById(R.id.item_feedback);
        ll_myuser = (RelativeLayout) rootView.findViewById(R.id.ll_myuser);
        to_information = (RelativeLayout) rootView.findViewById(R.id.to_information);
        headIconImg = (ImageView) rootView.findViewById(R.id.head_icon_img);
        itemMyGroupBuy = (LinearLayout)rootView.findViewById(R.id.item_my_group_buy);
        itemMyCoupon = (LinearLayout)rootView.findViewById(R.id.item_my_coupon);

        itemMyGroupBuy.setOnClickListener(this);
        itemMyCoupon.setOnClickListener(this);
        itemMyCountLiner.setOnClickListener(this);
        itemMyOrderLiner.setOnClickListener(this);
//        itemMyGalleryLiner.setOnClickListener(this);
        itemSettingLiner.setOnClickListener(this);
        itemAppServiceLiner.setOnClickListener(this);
        itemFeedbackLiner.setOnClickListener(this);
        ll_myuser.setOnClickListener(this);
        to_information.setOnClickListener(this);
        headIconImg.setOnClickListener(this);

        //我的宝贝
        itemMyBoxLiner = (LinearLayout) rootView.findViewById(R.id.item_my_box);
        itemMyBoxLiner.setOnClickListener(this);
        //app下载推广
        itemMyAppLiner = (LinearLayout) rootView.findViewById(R.id.item_my_app);
        itemMyAppLiner.setOnClickListener(this);
        //我的人脉圈
        itemMyContactsLiner = (LinearLayout) rootView.findViewById(R.id.item_my_contacts);
        itemMyContactsLiner.setOnClickListener(this);
        //我的二维码
        itemMyQrCodeLiner = (LinearLayout) rootView.findViewById(R.id.item_my_qrCode);
        itemMyQrCodeLiner.setOnClickListener(this);

        setHead();
    }

    public void setHead() {
        if (TextUtils.isEmpty(TongleAppInstance.getInstance().getUser_photo_url())) {
            Drawable drawable = getResources().getDrawable(R.drawable.avatar_normal);
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            Bitmap bitmap = bitmapDrawable.getBitmap();
            BitmapDrawable bbb = new BitmapDrawable(ImageUtil.toRoundCorner(bitmap, 250));
            headIconImg.setBackgroundDrawable(bbb);
        } else {
            ImageLoader.getInstance().displayImage(TongleAppInstance.getInstance().getUser_photo_url(), headIconImg, TongleAppInstance.options_round);
        }
    }

    public void doRequest() {
        ServiceManager.doMineRequest(TongleAppInstance.getInstance().getUserID(), successListener(), errorListener());
    }


    /**
     * 该页中的所有点击事件处理方法
     */
    public void onClick(View v) {
        Intent intent;
        if (v.getId() == R.id.item_my_count) { //我的账户
            Intent account = new Intent(getActivity(), SettingMyAccountActivity.class);
            startActivity(account);
//        } else if (v.getId() == R.id.item_my_gallery) {//我的百宝箱
//            Intent box = new Intent(getActivity(), SettingBoxActivity.class);
//            startActivity(box);
        } else if (v.getId() == R.id.item_my_order) {//我的订单
            //intent = new Intent(getActivity().getApplicationContext(), OrderMineActivity.class);
            intent = new Intent(getActivity().getApplicationContext(), MyOrderMainActivity.class);
            startActivity(intent);
        } else if (v.getId() == R.id.item_my_setting) {//我的设置
            intent = new Intent(getActivity(), SettingActivity.class);
            startActivity(intent);
        } else if (v.getId() == R.id.item_app_service) {//服务管家
            intent = new Intent(getActivity(), SettingServiceAdminActivity.class);
            startActivity(intent);
        } else if (v.getId() == R.id.item_feedback) {//意见反馈
            intent = new Intent(getActivity(), SettingFeedbackActivity.class);
            startActivity(intent);
        } else if (v.getId() == R.id.ll_myuser) {//我的信息
            intent = new Intent(getActivity(), SettingMyInfoActivity.class);
            startActivity(intent);
        } else if (v.getId() == R.id.item_my_box) {//我的宝贝 点击事件
            intent = new Intent(getActivity(), SettingMyBoxBabyActivity.class);
            startActivity(intent);
        } else if (v.getId() == R.id.item_my_app) {//app下载推广 点击事件
            intent = new Intent(getActivity(), SettingMyBoxAPPActivity.class);
            startActivity(intent);
        } else if (v.getId() == R.id.item_my_contacts) {//我的人脉圈 点击事件
            intent = new Intent(getActivity(), SettingBoxContactsActivity.class);
            startActivity(intent);
        } else if (v.getId() == R.id.item_my_qrCode) { //我的二维码 点击事件
            if (ToastUtils.isFastClick()) {
                return;
            } else {
                View view = LayoutInflater.from(getActivity()).inflate(R.layout.expert_popwindow_layout, null);
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setView(view);
                AlertDialog alert = builder.create();
                alert.setCanceledOnTouchOutside(true);
                ImageView coverImage = (ImageView) view.findViewById(R.id.qr_user);
                if (TongleAppInstance.getInstance().getUser_qr_code_url() == null || TongleAppInstance.getInstance().getUser_qr_code_url().equals("")) {
                    ToastUtils.toastShort(getString(R.string.setting_box_qrcode_null));
                } else {
                    ImageLoader.getInstance().displayImage(TongleAppInstance.getInstance().getUser_qr_code_url(), coverImage, TongleAppInstance.options);
                    alert.show();
                }
            }
        } else if (v.getId() == R.id.item_my_group_buy) {   //我的团购券
            startActivity(new Intent(getActivity(), MyGroupTicketActivity.class));

        } else if (v.getId() == R.id.item_my_coupon){    //我的优惠券
            startActivity(new Intent(getActivity(), MyCouponActivity.class));
        }
    }

    private void updateListView() {
        tongle_name.setText(TongleAppInstance.getInstance().getUser_nick_name());
        tongle_no.setText(getString(R.string.tongle_id) + TongleAppInstance.getInstance().getUserID());
        integral.setText(getString(R.string.point) + TongleAppInstance.getInstance().getUser_sales_point());
        account_balance.setText(getString(R.string.account_balance) + TongleAppInstance.getInstance().getUser_account_balance());
        coupon.setText(getString(R.string.coupon) + TongleAppInstance.getInstance().getUser_coupons());
        setHead();
    }

    public void onResume() {
        super.onResume();
        if (!TextUtils.isEmpty(TongleAppInstance.getInstance().getUser_nick_name()) && !TextUtils.isEmpty(TongleAppInstance.getInstance().getUser_phone())) {
            ServiceManager.doMineRequest(TongleAppInstance.getInstance().getUserID(), successListener(), errorListener());
            ImageLoader.getInstance().displayImage(TongleAppInstance.getInstance().getUser_phone(), headIconImg, TongleAppInstance.options_round);
        }
    }

    /**
     * 功能：网络响应成功，返回数据
     */
    private Response.Listener<MineResult> successListener() {
        return new Response.Listener<MineResult>() {
            @Override
            public void onResponse(MineResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    TongleAppInstance.getInstance().setUserID(response.getBody().getUser_id());    //用户ID
                    TongleAppInstance.getInstance().setUser_phone(response.getBody().getUser_phone());//手机号
                    TongleAppInstance.getInstance().setUser_sales_point(response.getBody().getUser_sales_point()); //用户消费积分余额
                    TongleAppInstance.getInstance().setUser_rebate_point(response.getBody().getUser_rebate_point());//用户返利积分余额
                    TongleAppInstance.getInstance().setUser_account_balance(response.getBody().getUser_account_balance());//用户账户余额
                    TongleAppInstance.getInstance().setUser_coupons(response.getBody().getUser_coupons());//用户优惠券
                    TongleAppInstance.getInstance().setUser_fans(response.getBody().getUser_fans());//用户粉丝数
                    TongleAppInstance.getInstance().setUser_follows(response.getBody().getUser_follows());//用户关注数
                    TongleAppInstance.getInstance().setUser_qr_code_url(response.getBody().getUser_qr_code_url()); //用户二维码
                    TongleAppInstance.getInstance().setUser_photo_url(response.getBody().getUser_photo_url());//头像
                    TongleAppInstance.getInstance().setUser_nick_name(response.getBody().getUser_nick_name());//用户昵称
                    TongleAppInstance.getInstance().setUser_sex(response.getBody().getUser_sex());//性别
                    TongleAppInstance.getInstance().setUser_name(response.getBody().getUser_name());//用户名称
                    TongleAppInstance.getInstance().setUser_customs_flag(response.getBody().getUser_customs_flag());//积分开关
                    TongleAppInstance.getInstance().setUser_id_no(response.getBody().getUser_id_no());//身份证
                    //缓存重要数据
                    SharedPreferences sp = getActivity().getApplicationContext().getSharedPreferences("userInfo", Context.MODE_PRIVATE);
                    sp.edit().putString("user_name", response.getBody().getUser_name()).commit();  //用户名称
                    sp.edit().putString("user_nick_name", response.getBody().getUser_nick_name()).commit();  // 用户昵称
                    sp.edit().putString("user_phone", response.getBody().getUser_phone()).commit();   // 手机号
                    sp.edit().putString("user_photo_url", response.getBody().getUser_phone()).commit();   // 头像
                    sp.edit().putString("user_sex", response.getBody().getUser_sex()).commit(); // 性别
                    sp.edit().putString("user_id_no", response.getBody().getUser_id_no()).commit(); // 身份证号
                    sp.edit().putString("user_sales_point", response.getBody().getUser_sales_point()).commit(); // 用户消费积分余额
                    sp.edit().putString("user_rebate_point", response.getBody().getUser_rebate_point()).commit(); // 用户返利积分余额
                    sp.edit().putString("user_account_balance", response.getBody().getUser_account_balance()).commit(); //用户账户余额
                    sp.edit().putString("user_coupons", response.getBody().getUser_coupons()).commit(); // 用户优惠券
                    sp.edit().putString("user_fans", response.getBody().getUser_fans()).commit(); // 用户粉丝数
                    sp.edit().putString("user_follows", response.getBody().getUser_follows()).commit(); // 用户关注数
                    sp.edit().putString("user_qr_code_url", response.getBody().getUser_qr_code_url()).commit(); // 用户二维码URL
                    sp.edit().putString("user_customs_flag", response.getBody().getUser_customs_flag()).commit(); // 个人积分开关
                    updateListView();
                } else {
                    ToastUtils.toastShort(getString(R.string.para_exception) + response.getHead().getReturn_message());
                }
            }
        };
    }

    /**
     * 功能：网络响应失败
     */
    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ToastUtils.toastShort(getString(R.string.sys_exception));
            }
        };
    }
}
