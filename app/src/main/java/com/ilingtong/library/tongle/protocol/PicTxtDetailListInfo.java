package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * User: shuailei
 * Date: 2015/6/26
 * Time: 14:18
 * Email: leishuai@isoftstone.com
 * Desc:
 */
public class PicTxtDetailListInfo implements Serializable {

    public ArrayList<ProductPicTextListItem> prod_pic_text_list;

    @Override
    public String toString() {
        return  "prod_pic_text_list:" + prod_pic_text_list;
    }


}
