package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/3/11.
 * mail: wuqian@ilingtong.com
 * Description:团购商品按钮处理信息  例如：立即购买、卖光了、 已结束
 * use by 6007
 */
public class UIButtonInfo implements Serializable {
    private String btn_func_key;     //功能名
    private String btn_name;    //按钮名称
    private String btn_status;   //按钮状态

    public String getBtn_func_key() {
        return btn_func_key;
    }

    public void setBtn_func_key(String btn_func_key) {
        this.btn_func_key = btn_func_key;
    }

    public String getBtn_name() {
        return btn_name;
    }

    public void setBtn_name(String btn_name) {
        this.btn_name = btn_name;
    }

    public String getBtn_status() {
        return btn_status;
    }

    public void setBtn_status(String btn_status) {
        this.btn_status = btn_status;
    }
}
