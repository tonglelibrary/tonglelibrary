package com.ilingtong.library.tongle.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.activity.CollectForumDetailActivity;
import com.ilingtong.library.tongle.activity.CollectMStoreActivity;
import com.ilingtong.library.tongle.activity.CollectMStoreDetailActivity;
import com.ilingtong.library.tongle.adapter.ForumListAdapter;
import com.ilingtong.library.tongle.adapter.HomeMstoreGridViewAdapter;
import com.ilingtong.library.tongle.adapter.HomePagerAdapter;
import com.ilingtong.library.tongle.external.maxwin.view.XListView;
import com.ilingtong.library.tongle.model.HomeModel;
import com.ilingtong.library.tongle.protocol.BannerResult;
import com.ilingtong.library.tongle.protocol.ForumResult;
import com.ilingtong.library.tongle.protocol.HomeForumParam;
import com.ilingtong.library.tongle.protocol.MStoreListItem;
import com.ilingtong.library.tongle.protocol.MStoreRequestParam;
import com.ilingtong.library.tongle.protocol.MStoreResult;
import com.ilingtong.library.tongle.protocol.UserFollowPostList;
import com.ilingtong.library.tongle.service.BaseDataService;
import com.ilingtong.library.tongle.utils.DipUtils;
import com.ilingtong.library.tongle.utils.ToastUtils;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static android.view.ViewGroup.LayoutParams;

/**
 * User:lengjiqiang
 * Date:2015/4/23
 * Time:PM 14:16
 * Email:jqleng@isoftstone.com
 * Desc:首界面
 */
public class HomeFragment extends BaseFragment implements XListView.IXListViewListener, View.OnClickListener, ViewPager.OnPageChangeListener, AdapterView.OnItemClickListener {
    private ViewPager bannerViewPager;
    private XListView mListView;
    private ArrayList<View> bannerListView;
    private View mTouchTarget;
    private FrameLayout bannerView;
    private ForumListAdapter forumListAdapter;
    private View headView;
    private HomeModel homeModel;
    private TextView mPreSelectedBt;
    private LinearLayout banner_bottom;
    private ScheduledExecutorService scheduledExecutorService;
    private int currentItem = 0;//当前页面
    private HomeForumParam param;
    private ImageView image;
    private ProgressDialog pDlg;
    private HomePagerAdapter myAdapter;
    private GridView mGridView; //魔店的gradView
    private HomeMstoreGridViewAdapter homeMstoreGridViewAdapter;
    private ArrayList<MStoreListItem> mstore_list = new ArrayList<>();
    private ArrayList<UserFollowPostList> mlist = new ArrayList<UserFollowPostList>();

    public static final int UPDATE_MSTORE = 1;
    public static final int UPDATE_FORUMlIST = 0;
    public static boolean MSTORE_UPDATE_FLAG = false;    //魔店刷新标志，为true时表示要刷新魔店列表。该flag表示从收藏页跳转。取消收藏设值为true，马上收藏后设值为false
    public static boolean MSTORE_UPDATE_FLAG_OTHER = false; //魔店刷新标志，为true时表示要刷新魔店列表。与上面的flag区分。取消收藏设值为true，取消收藏还是设值为true
    public static boolean POST_UPDATE_FLAG = false;    //帖子刷新标志，为true时表示要刷新帖子列表。该flag表示从收藏页跳转。取消收藏设值为true，马上收藏后设值为false
    public static boolean POST_UPDATE_FLAG_OTHER = false;    //帖子刷新标志，为true时表示要刷新帖子列表.与上面的flag区分。取消收藏设值为true，取消收藏还是设值为true
    public static HomeFragment instance = null;
    HomeFragmentHandler homeFragmentHandler = new HomeFragmentHandler(this);

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        instance = this;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mainView = inflater.inflate(R.layout.fragment_home_layout, null);
        initView(mainView);
        return mainView;
    }

    public void initView(View mainView) {
        //创建并弹出“加载中Dialog”
        pDlg = new ProgressDialog(getActivity());
        pDlg.setCancelable(false);
        pDlg.setMessage(getString(R.string.loading));
        pDlg.show();

        headView = LayoutInflater.from(getActivity()).inflate(R.layout.banner_view_layout, null);
        image = (ImageView) headView.findViewById(R.id.imageView);
        image.setOnClickListener(this);

        mGridView = (GridView) headView.findViewById(R.id.home_mstroe_gridvew);
        mGridView.setSelector(new ColorDrawable(Color.TRANSPARENT)); //去掉点击背景色
        mGridView.setOnItemClickListener(this);
        mGridView.setHorizontalSpacing(5);
        homeMstoreGridViewAdapter = new HomeMstoreGridViewAdapter(getActivity(), mstore_list);
        mGridView.setAdapter(homeMstoreGridViewAdapter);
        bannerView = (FrameLayout) headView.findViewById(R.id.banner_view);
        if (homeModel == null)
            homeModel = new HomeModel();

        bannerViewPager = (ViewPager) bannerView.findViewById(R.id.banner_viewpager);
        setPageSize();
        bannerListView = new ArrayList<View>();
        myAdapter = new HomePagerAdapter(getActivity(), bannerListView, homeModel);
        bannerViewPager.setAdapter(myAdapter);
        bannerViewPager.setCurrentItem(0);
        bannerViewPager.setOnPageChangeListener(this);

        banner_bottom = (LinearLayout) bannerView.findViewById(R.id.banner_bottom);
        mListView = (XListView) mainView.findViewById(R.id.home_listview);
        mListView.addHeaderView(headView);

        mListView.setPullRefreshEnable(true);
        mListView.setPullLoadEnable(false);
        mListView.setXListViewListener(this, 0);
        mListView.setRefreshTime();

        forumListAdapter = new ForumListAdapter(getActivity(), mlist);
        mListView.setAdapter(forumListAdapter);
        //点击帖子列表Item，进入帖子详情
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent detailIntent = new Intent(getActivity(), CollectForumDetailActivity.class);
                detailIntent.putExtra("post_id", mlist.get(position - 2).post_id);  //因为有headview。所有这里position要减2
                detailIntent.putExtra(TongleAppConst.INTO_TYPE, TongleAppConst.HOME_INTO);
                startActivityForResult(detailIntent, 10001);
            }
        });

        //请求数据
        doBannerRequest();
        doMStoreRequest();
        doForumRequest();
        doBaseDataRequest();

        scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
        scheduledExecutorService.scheduleAtFixedRate(new ViewPagerTask(), 1, 6, TimeUnit.SECONDS);
    }

    //轮播图请求
    public void doBannerRequest() {
        String limit_size = "";
        ServiceManager.doBannerRequest(TongleAppInstance.getInstance().getUserID(), limit_size, bannerSuccessListener(), errorListener());
    }

    //魔店请求
    public void doMStoreRequest() {
        mstore_list.clear();
        MStoreRequestParam mStoreRequestParam = new MStoreRequestParam();
        mStoreRequestParam.user_id = TongleAppInstance.getInstance().getUserID();
        mStoreRequestParam.fetch_count = "";
        mStoreRequestParam.forward = "";
        mStoreRequestParam.mstore_id = "";
        ServiceManager.doMStoreRequest(mStoreRequestParam, mstoreSuccessListener(), errorListener());
    }

    //帖子请求
    public void doForumRequest() {
        mlist.clear();
        mListView.setPullLoadEnable(false); //刷新时设置加载更多功能为false。避免刷新时帖子列表头端出现“加载更多"
        param = new HomeForumParam();
        param.user_id = TongleAppInstance.getInstance().getUserID();
        param.forward = "1";
        param.post_id = "";
        param.fetch_count = TongleAppConst.FETCH_COUNT;
        ServiceManager.doForumRequest(param, forumSuccessListener(), errorListener());
    }

    //基础数据请求
    private void doBaseDataRequest() {
        Intent intent = new Intent(getActivity(), BaseDataService.class);
        getActivity().startService(intent);
    }

    //设置page尺寸
    public void setPageSize() {
        LayoutParams params1 = bannerViewPager.getLayoutParams();
        params1.height = (DipUtils.getScreenWidth(getActivity()) * 9 / 16);
        bannerViewPager.setLayoutParams(params1);
    }

    /**
     * 轮播图
     */
    public void updateBannerView() {
        if ((homeModel == null) && (homeModel.bannerList == null))
            return;
        if (bannerListView != null) {
            bannerListView.clear();
        }
        try {
            for (int i = 0; i < homeModel.bannerList.size(); i++) {
                String url = homeModel.bannerList.get(i).mobile_pic_url;
                ImageView viewOne = (ImageView) LayoutInflater.from(getActivity()).inflate(R.layout.banner_cell_layout, null);
                ImageLoader.getInstance().displayImage(url, viewOne, TongleAppInstance.options);
                bannerListView.add(viewOne);
            }
            banner_bottom.removeAllViews();
            //定义viewpager下的圆点
            for (int i = 0; i < bannerListView.size(); i++) {
                TextView bt = new TextView(getActivity());
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(12, 12);
                params.setMargins(10, 0, 0, 0);
                bt.setLayoutParams(params);
                bt.setBackgroundResource(R.color.main_bgcolor);
                banner_bottom.addView(bt);
                myAdapter.notifyDataSetChanged();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        bannerViewPager.setAdapter(myAdapter);
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        if (!hidden) {
            if (MSTORE_UPDATE_FLAG || MSTORE_UPDATE_FLAG_OTHER) {//刷新魔店
                doMStoreRequest();
            }
            if (POST_UPDATE_FLAG || POST_UPDATE_FLAG_OTHER) { //刷新帖子
                doForumRequest();
            }
        }
        super.onHiddenChanged(hidden);
    }

    @Override
    public void onRefresh(int id) {
        //轮播图请求
        doBannerRequest();
        //魔店请求
        doMStoreRequest();
        //帖子请求
        doForumRequest();
    }

    @Override
    public void onLoadMore(int id) {
        int i = Integer.parseInt(homeModel.forum_data_total_count);
        //根据list的数量，判断是否还有更多数据
        if (mlist.size() < i) {
            //第二个参数prod_id是list里最后一个参数的id
            HomeForumParam param = new HomeForumParam();
            param.user_id = TongleAppInstance.getInstance().getUserID();
            param.forward = "1";
            param.post_id = mlist.get(mlist.size() - 1).post_id;
            param.fetch_count = TongleAppConst.FETCH_COUNT;
            ServiceManager.doForumRequest(param, forumSuccessListener(), errorListener());
        } else {
            ToastUtils.toastShort(getString(R.string.common_list_end));
            mListView.setPullLoadEnable(false);
        }
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        int id = v.getId();
        if (id == R.id.imageView) {
            intent = new Intent(getActivity(), CollectMStoreActivity.class);
            intent.putExtra(TongleAppConst.INTO_TYPE, TongleAppConst.HOME_INTO);
            intent.putExtra("mstore_list", mstore_list);
            startActivityForResult(intent, 10001);
        }
    }

    /**
     * 功能：广告位列表网络响应成功，返回数据
     */
    private Response.Listener<BannerResult> bannerSuccessListener() {
        return new Response.Listener<BannerResult>() {
            @Override
            public void onResponse(BannerResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    homeModel.bannerList = response.getBody().getPromotionList();
                    updateBannerView();
                } else {
                    ToastUtils.toastShort(getString(R.string.para_exception) + response.getHead().getReturn_message());
                }
            }
        };
    }

    /**
     * 功能：请求魔店网络响应成功，返回数据
     */
    private Response.Listener<MStoreResult> mstoreSuccessListener() {
        return new Response.Listener<MStoreResult>() {
            @Override
            public void onResponse(MStoreResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    homeModel.mstore_data_total_count = response.getBody().getData_total_count();
                    homeModel.mstore_list = response.getBody().getMStoreList();
                    mstore_list.addAll(response.getBody().getMStoreList());
                    homeFragmentHandler.sendEmptyMessage(UPDATE_MSTORE);
                    MSTORE_UPDATE_FLAG = false;
                    MSTORE_UPDATE_FLAG_OTHER = false;
                } else {
                    ToastUtils.toastShort(getString(R.string.para_exception) + response.getHead().getReturn_message());
                }
            }
        };
    }

    /**
     * 功能：请求帖子网络响应成功，返回数据
     */
    private Response.Listener<ForumResult> forumSuccessListener() {
        return new Response.Listener<ForumResult>() {
            @Override
            public void onResponse(ForumResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    homeModel.forum_data_total_count = response.getBody().getData_total_count();
                    homeModel.user_follow_post_list = response.getBody().getUser_follow_post_list();
                    int total_count = Integer.parseInt(homeModel.forum_data_total_count);
                    //第一次请求或刷新页面时候判断是否隐藏底部加载更多按钮
                    if (total_count <= 10) {
                        mListView.setPullLoadEnable(false);
                    } else {
                        mListView.setPullLoadEnable(true);
                    }
                    //把请求到的返回数据添加到list里面
                    for (int i = 0; i < response.getBody().getUser_follow_post_list().size(); i++) {
                        mlist.add(response.getBody().getUser_follow_post_list().get(i));
                    }
                    //调用handler，发送消息
                    homeFragmentHandler.sendEmptyMessage(UPDATE_FORUMlIST);
                    POST_UPDATE_FLAG = false;
                    POST_UPDATE_FLAG_OTHER = false;

                } else {
                    ToastUtils.toastShort(getString(R.string.para_exception) + response.getHead().getReturn_message());
                }
                pDlg.dismiss();
            }
        };
    }

    /**
     * 功能：请求魔店信息列表网络响应失败
     */
    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                if (pDlg.isShowing()) {
                    pDlg.dismiss();
                }
                ToastUtils.toastShort(getString(R.string.sys_exception));
            }
        };
    }

    //监听
    private int mPreviousState = ViewPager.SCROLL_STATE_IDLE;

    @Override
    public void onPageScrolled(int i, float v, int i2) {

    }

    @Override
    public void onPageSelected(int i) {
        if (mPreSelectedBt != null) {
            mPreSelectedBt.setBackgroundResource(R.color.main_bgcolor);
        }

        TextView currentBt = (TextView) banner_bottom.getChildAt(i);
        currentBt.setBackgroundResource(R.color.topview_bgcolor);
        mPreSelectedBt = currentBt;
    }

    @Override
    public void onPageScrollStateChanged(int state) {
        if (mPreviousState == ViewPager.SCROLL_STATE_IDLE) {
            if (state == ViewPager.SCROLL_STATE_DRAGGING) {
                mTouchTarget = bannerViewPager;
            }
        } else {
            if (state == ViewPager.SCROLL_STATE_IDLE || state == ViewPager.SCROLL_STATE_SETTLING) {
                mTouchTarget = null;
            }
        }
        mPreviousState = state;
    }

    /**
     * 点击某个魔店，跳转到魔店详情页
     *
     * @param parent
     * @param view
     * @param position
     * @param id
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intent = new Intent(getActivity(), CollectMStoreDetailActivity.class);
        intent.putExtra(TongleAppConst.INTO_TYPE, TongleAppConst.HOME_INTO);    //入口type。表示从首页进入
        intent.putExtra("mstore_id", mstore_list.get(position).mstore_id);
        startActivityForResult(intent, 10001);
    }

    /**
     * startActivityForResult 回调
     *
     * @param requestCode 10001 表示点击魔店进入详情或者魔店列表。10002表示点击首页banner跳转
     * @param resultCode
     * @param data
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 10001 && MSTORE_UPDATE_FLAG) {
            doMStoreRequest();
        } else if (requestCode == 10002 && MSTORE_UPDATE_FLAG_OTHER) {
            doMStoreRequest();
        } else if (requestCode == 10002 && POST_UPDATE_FLAG_OTHER) {
            doForumRequest();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * 用来完成图片切换的任务
     */
    private class ViewPagerTask implements Runnable {

        public void run() {
            //实现我们的操作
            //改变当前页面
            currentItem = (currentItem + 1) % bannerListView.size();
            //Handler来实现图片切换
            handler.obtainMessage().sendToTarget();
        }
    }

    private Handler handler = new Handler() {

        public void handleMessage(Message msg) {
            //设定viewPager当前页面
            bannerViewPager.setCurrentItem(currentItem);
        }
    };

    @Override
    public void onPause() {
        super.onPause();
        scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
        scheduledExecutorService.scheduleAtFixedRate(new ViewPagerTask(), 1, 3, TimeUnit.SECONDS);
    }

    @Override
    public void onStop() {
        //停止图片切换
        scheduledExecutorService.shutdown();
        super.onStop();
    }

    static class HomeFragmentHandler extends Handler {
        // 弱引用
        WeakReference<HomeFragment> mFragment;

        HomeFragmentHandler(HomeFragment fragment) {
            mFragment = new WeakReference<HomeFragment>(fragment);
        }

        @Override
        public void handleMessage(Message msg) {
            HomeFragment theFragment = mFragment.get();
            switch (msg.what) {
                case UPDATE_MSTORE: // 刷新魔店列表
                    theFragment.mstore_list.size();
                    theFragment.homeMstoreGridViewAdapter.notifyDataSetInvalidated();
                    theFragment.homeMstoreGridViewAdapter.notifyDataSetChanged();
                    break;
                case UPDATE_FORUMlIST: //刷新帖子列表
                    theFragment.forumListAdapter.notifyDataSetChanged();
                    break;
            }
        }
    }
}
