package com.ilingtong.library.tongle.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.adapter.SingleSelectListAdapter;
import com.ilingtong.library.tongle.model.ConfirmOrderModel;
import com.ilingtong.library.tongle.protocol.DataListItem;

/**
 * User: lengjiqiang
 * Date: 2015/6/11
 * Time: 17:31
 * Email: jqleng@isoftstone.com
 * Desc: 配送时间选择页
 */
public class OrderDeliveryTimeActivity extends BaseActivity implements View.OnClickListener {
    private ImageView returnBtn;
    private TextView top_name;
    private ListView listview;
    private SingleSelectListAdapter listAdapter;
    private String tag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listview_comm_layout);
        getData();
        initView();
    }
    public void getData(){
        tag = getIntent().getExtras().getString("checkDeliveryTime");
    }
    public void initView(){
        top_name = (TextView) findViewById(R.id.top_name);
        returnBtn = (ImageView) findViewById(R.id.left_arrow_btn);
        listview = (ListView) findViewById(R.id.listview_comm);
        returnBtn.setOnClickListener(this);
        returnBtn.setVisibility(View.VISIBLE);
        top_name.setText(R.string.delivery_time);
        top_name.setVisibility(View.VISIBLE);
        listAdapter = new SingleSelectListAdapter(this, ConfirmOrderModel.deliveryTimeList,tag);
        listview.setAdapter(listAdapter);
        listview.setOnItemClickListener(new DeliveryTimeOnItemClickListener());
    }
    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.left_arrow_btn){
            finish();
        }
    }

    private class DeliveryTimeOnItemClickListener implements AdapterView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            for (int i = 0; i < listview.getCount(); i++) {
                View itemview = parent.getChildAt(i);
                ImageView selectIcon = (ImageView) itemview.findViewById(R.id.single_select_icon);
                selectIcon.setVisibility(View.INVISIBLE);
            }
            View selectView = parent.getChildAt(position);
            ImageView selectIcon = (ImageView) selectView.findViewById(R.id.single_select_icon);
            selectIcon.setVisibility(View.VISIBLE);
            ConfirmOrderModel.checkDeliveryTime = (DataListItem) parent.getItemAtPosition(position);
        }
    }
}