package com.ilingtong.library.tongle.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;

import com.ilingtong.library.tongle.AppManager;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.utils.ToastUtils;

/**
 * Created by wuqian on 2015/10/28.
 * mail: wuqian@ilingtong.com
 * Description: 最外层MainActivity
 */
public class MainActivity extends BaseFragmentActivity {

    Context mContext = null;
    public RadioGroup mRadioGroup;
    private Fragment[] fragments;
    private FragmentManager manager;
    private FragmentTransaction transaction;

    private final static int MAIN_INDEX = 0;//首頁
    private final static int FOLLOW_INDEX = 1;//收藏
    private final static int SCAN_INDEX = 2;//發現
    private final static int SHOPCART_INDEX = 3;//購物車
    private final static int MYINFO_INDEX = 4;//我的

    private long mLastClickTime = 0;
    private ProgressDialog progressDialog;
    public static MainActivity instance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mContext = this;
        instance = this;
        mRadioGroup = (RadioGroup) findViewById(R.id.main_host_rg);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.main_dialog_message));

        fragments = new Fragment[5];
        manager = getSupportFragmentManager();
        fragments[MAIN_INDEX] = manager
                .findFragmentById(R.id.main_fragment_main);
        fragments[FOLLOW_INDEX] = manager
                .findFragmentById(R.id.main_fragment_follow);
        fragments[SCAN_INDEX] = manager
                .findFragmentById(R.id.main_fragment_scan);
        fragments[SHOPCART_INDEX] = manager
                .findFragmentById(R.id.main_fragment_shopcart);
        fragments[MYINFO_INDEX] =manager.findFragmentById(R.id.main_fragment_myinfo);

        transaction = manager.beginTransaction();
        transaction.hide(fragments[FOLLOW_INDEX]).hide(fragments[SCAN_INDEX])
                .hide(fragments[SHOPCART_INDEX]).hide(fragments[MYINFO_INDEX])
                .hide(fragments[MAIN_INDEX]);
        transaction.show(fragments[MAIN_INDEX]);
        transaction.commit();
        setFragmentIndicator();
        processExtraData();    //防止Activity被回收而不调用onNewIntent方法。
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        processExtraData();
    }

    /**
     * 单例模式下启动MainActivity 根据intent传递的type判断显示首页还是购物车页面
     */
    private void processExtraData(){

        Intent intent = getIntent();
        String type = intent.getStringExtra(TongleAppConst.MAINACTIVITY_NEWINTENT_FLAG_NAME);

        if (TongleAppConst.MAINACTIVITY_NEWINTENT_FLAG_CART.equals(type)){
            mRadioGroup.check(R.id.main_rbt_host_shopcart);
        }else if (TongleAppConst.MAINACTIVITY_NEWINTENT_FLAG_MAIN.equals(type)){
            mRadioGroup.check(R.id.main_rbt_host_main);
        }
        Log.e("tag","type"+type);
    }

    private void setFragmentIndicator() {

        mRadioGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                transaction = manager.beginTransaction()
                        .hide(fragments[MAIN_INDEX])
                        .hide(fragments[MYINFO_INDEX])
                        .hide(fragments[FOLLOW_INDEX])
                        .hide(fragments[SHOPCART_INDEX])
                        .hide(fragments[SCAN_INDEX]);
                if (checkedId == R.id.main_rbt_host_main) {
                    transaction.show(fragments[MAIN_INDEX]).commit();

                } else if (checkedId == R.id.main_rbt_host_follow) {
                    transaction.show(fragments[FOLLOW_INDEX]).commit();

                } else if (checkedId == R.id.main_rbt_host_scan) {
                    transaction.show(fragments[SCAN_INDEX]).commit();

                } else if (checkedId == R.id.main_rbt_host_shopcart) {
                    transaction.show(fragments[SHOPCART_INDEX]).commit();

                } else if (checkedId == R.id.main_rbt_host_my) {
                    transaction.show(fragments[MYINFO_INDEX]).commit();
                }
            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if ((System.currentTimeMillis() - mLastClickTime) > 2000) {
                ToastUtils.toastShort( getString(R.string.main_exit_app_tip));
                mLastClickTime = System.currentTimeMillis();
            } else {
                AppManager.getAppManager().AppExit(this);
                //退出应用时清除所有缓存数据
                SharedPreferences sp = getApplicationContext().getSharedPreferences("userInfo",MODE_PRIVATE);
                sp.edit().clear().commit();
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
