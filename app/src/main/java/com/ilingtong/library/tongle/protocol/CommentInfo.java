package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/7/15
 * Time: 16:26
 * Email: jqleng@isoftstone.com
 * Desc: 商品评价类，包含网络返回的数据
 */
public class CommentInfo implements Serializable {
    private String str = "";

    @Override
    public String toString() {
        return str;
    }
}
