package com.ilingtong.library.tongle.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.activity.CollectProductDetailActivity;
import com.ilingtong.library.tongle.fragment.CartFragment;
import com.ilingtong.library.tongle.model.CartModel;
import com.ilingtong.library.tongle.protocol.DefualtResult;
import com.ilingtong.library.tongle.protocol.ShopCartListItem;
import com.ilingtong.library.tongle.utils.ToastUtils;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * User: lengjiqiang
 * Date: 2015/5/10
 * Time: 13:53
 * Email: jqleng@isoftstone.com
 * Desc:
 */
public class CartListAdaper extends BaseAdapter {
    // private LayoutInflater inflater;
    private List<ShopCartListItem> list;
    private Context mContext;
    private Fragment fragment;
    // private View footview;
    //float totalPrice = 0;
    android.os.Handler mHandler;
    ShopCartListItem cartDataItem = new ShopCartListItem();
    // 用来控制CheckBox的选中状况
    private static HashMap<Integer, Boolean> isSelected;
    //用来判断显示结算模式还是编辑模式
    private int type;
    public static int TYPE_EDTT = 1;
    public static int TYPE_CHECKOUT = 2;

//    private float totalPrice = 0;//价格
//    private int checkNum; // 记录选中的条目数量

    public void setType(int type) {
        this.type = type;
    }

    public CartListAdaper(Context context, android.os.Handler oshandler, ArrayList list,Fragment fragment) {
        this.list = list;
        this.fragment = fragment;
        mContext = context;
        CartModel.checkList.clear();
        mHandler = oshandler;
        isSelected = new HashMap<Integer, Boolean>();
        this.type = TYPE_CHECKOUT;
        // 初始化数据
        initDate();
    }

    // 初始化isSelected的数据
    private void initDate() {
        for (int i = 0; i < list.size(); i++) {
            getIsSelected().put(i, false);
        }
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        if (convertView == null) {
            // 获得ViewHolder对象
            holder = new ViewHolder();
            // 导入布局并赋值给convertview
            convertView = LayoutInflater.from(mContext).inflate(R.layout.cart_list_item, null);
            holder.cb = (CheckBox) convertView.findViewById(R.id.cart_checkbox);
            holder.listItemImageView = (ImageView) convertView.findViewById(R.id.cart_list_item_image);
            holder.listItemlinar = (LinearLayout) convertView.findViewById(R.id.cart_list_prodlinear);

            holder.desc = (TextView) convertView.findViewById(R.id.cart_list_item_desc);
            holder.cart_product_country = (TextView) convertView.findViewById(R.id.cart_product_country);
            holder.price = (TextView) convertView.findViewById(R.id.cart_list_item_price);
            holder.cart_num = (TextView) convertView.findViewById(R.id.cart_num);
            holder.cart_product_spec = (TextView) convertView.findViewById(R.id.cart_product_spec);
            holder.cart_list_item_lr_edit = (LinearLayout) convertView.findViewById(R.id.cart_list_item_lr_edit);
            holder.btn_add = (ImageView) convertView.findViewById(R.id.cart_list_item_product_numAdd);
            holder.btn_miuns = (ImageView) convertView.findViewById(R.id.cart_list_item_product_numMiuns);
            holder.edit_number = (EditText) convertView.findViewById(R.id.cart_list_item_product_number);
            // 为view设置标签
            convertView.setTag(holder);
        } else {
            // 取出holder
            holder = (ViewHolder) convertView.getTag();
        }
        //判断显示结算模式还是编辑模式
        if (type == TYPE_EDTT) {
            holder.cart_list_item_lr_edit.setVisibility(View.VISIBLE);
            holder.desc.setVisibility(View.GONE);
        } else if (type == TYPE_CHECKOUT) {
            holder.cart_list_item_lr_edit.setVisibility(View.GONE);
            holder.desc.setVisibility(View.VISIBLE);
        }

        //购物车图片标题栏点击事件。如果为查看模式，点击跳转到商品详情，如果为编辑模式，不可点击
        holder.listItemlinar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (type == TYPE_CHECKOUT) {
                    //点击进行跳转，并把产品id传到产品详情界面
                    if (!TextUtils.isEmpty(list.get(position).prod_id)) {
                        Intent detailIntent = new Intent(mContext, CollectProductDetailActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("product_id", list.get(position).prod_id);
                        bundle.putString("cartlist", "true");
                        bundle.putString("tag", "cart");
                        bundle.putString("relationId", list.get(position).relation_id);
                        detailIntent.putExtras(bundle);
                        fragment.startActivityForResult(detailIntent, 10010);
                    }
                }
            }
        });
        // 商品数量加
        holder.btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String number = holder.edit_number.getText().toString();
                holder.edit_number.setText((Integer.parseInt(number) + 1) + "");
                updateProductNumber(list.get(position).seq_no,list.get(position).prod_id,holder.edit_number.getText().toString(),position);
            }
        });
        //商品数量减
        holder.btn_miuns.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String edit2 = holder.edit_number.getText().toString();
                int j = Integer.valueOf(edit2).intValue();
                if (j > 1) {
                    j -= 1;
                    holder.edit_number.setText(j + "");
                    updateProductNumber(list.get(position).seq_no, list.get(position).prod_id, holder.edit_number.getText().toString(), position);
                } else {
                    ToastUtils.toastShort(mContext.getString(R.string.common_number_min));
                }
            }
        });
        //checkbox点击事件。
        holder.cb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getIsSelected().put(position, holder.cb.isChecked());
                if (holder.cb.isChecked() == true) {
                    CartModel.checkList.add(list.get(position));
                    BigDecimal b1 = new BigDecimal(Float.toString(CartFragment.totalPrice));
                    BigDecimal b2 = new BigDecimal(Float.toString(Float.parseFloat(list.get(position).price)));
                    BigDecimal b3 = new BigDecimal(Float.toString(Float.parseFloat(list.get(position).order_qty)));
                    CartFragment.totalPrice = b1.add(b2.multiply(b3)).floatValue();
                    CartFragment.checkNum = CartFragment.checkNum + Integer.parseInt(list.get(position).order_qty);
                } else {
                    CartModel.checkList.remove(list.get(position));
                    BigDecimal b1 = new BigDecimal(Float.toString(CartFragment.totalPrice));
                    BigDecimal b2 = new BigDecimal(Float.toString(Float.parseFloat(list.get(position).price)));
                    BigDecimal b3 = new BigDecimal(Float.toString(Float.parseFloat(list.get(position).order_qty)));
                    CartFragment.totalPrice = b1.subtract(b2.multiply(b3)).floatValue();
                    CartFragment.checkNum = CartFragment.checkNum - Integer.parseInt(list.get(position).order_qty);
                }
                //发送消息，刷新结算view一栏
                Message message = mHandler.obtainMessage();
                message.what = TongleAppConst.UPDATE_TOTALPRICE;
                message.obj = CartFragment.totalPrice;
                mHandler.sendMessage(message);
            }
        });
        // 根据isSelected来设置checkbox的选中状况
        holder.cb.setChecked(getIsSelected().get(position));
        cartDataItem = (ShopCartListItem) getItem(position);//list.get(position);
        ImageLoader.getInstance().displayImage(cartDataItem.prod_pic_url, holder.listItemImageView, TongleAppInstance.options);
        holder.desc.setText(cartDataItem.prod_name);
        holder.price.setText(cartDataItem.price);
        holder.cart_num.setText(mContext.getString(R.string.multiply) + cartDataItem.order_qty);
        holder.edit_number.setText(cartDataItem.order_qty+"");
        StringBuffer spec = new StringBuffer();
        if (cartDataItem.prod_spec_list != null && cartDataItem.prod_spec_list.size() > 0) {
            for (int i = 0; i < cartDataItem.prod_spec_list.size(); i++) {
                spec.append(cartDataItem.prod_spec_list.get(i).prod_spec_name + ":" + cartDataItem.prod_spec_list.get(i).spec_detail_name + ";  ");
            }
        }
        holder.cart_product_spec.setText(spec.toString());
        holder.cart_product_country.setText(cartDataItem.import_info_desc);
        return convertView;
    }

    private void updateProductNumber(final String seq_no,String product_id, final String order_qty, final int position){
        ServiceManager.doCartNumberModify(TongleAppInstance.getInstance().getUserID(), seq_no, product_id, order_qty, new Response.Listener<DefualtResult>() {
            @Override
            public void onResponse(DefualtResult defualtResult) {
                if (TongleAppConst.SUCCESS.equals(defualtResult.getHead().getReturn_flag())){
                    list.get(position).order_qty = order_qty;  //如果修改成功，数量显示修改后数量
                }else {
                    ToastUtils.toastShort(mContext.getResources().getString(R.string.para_exception) + defualtResult.getHead().getReturn_message());
                    list.get(position).order_qty = list.get(position).order_qty;   //如果修改失败，数量显示修改前数量
                }
                notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                list.get(position).order_qty = list.get(position).order_qty;   //如果修改失败，数量显示修改前数量
                notifyDataSetChanged();
                ToastUtils.toastShort(mContext.getString(R.string.sys_exception));
            }
        });
    }

    public class ViewHolder {
        public CheckBox cb;
        public TextView price, cart_num;
        public TextView desc;
        public ImageView listItemImageView;
        public LinearLayout listItemlinar;
        public TextView cart_product_spec; //商品规格
        public TextView cart_product_country; //商品进出口信息（如:美国直供）
        private LinearLayout cart_list_item_lr_edit; //编辑模式linearLayout。
        public ImageView btn_add;  //数量加
        public ImageView btn_miuns; //数量减
        public EditText edit_number; //商品数量

    }

    public static HashMap<Integer, Boolean> getIsSelected() {
        return isSelected;
    }
//    public static void setIsSelected(HashMap<Integer, Boolean> isSelected) {
//        CartListAdaper.isSelected = isSelected;
//    }
}
