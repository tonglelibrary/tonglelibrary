package com.ilingtong.library.tongle.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.adapter.MyFieldListAdapter;

import java.util.ArrayList;

/**
 * User: shuailei
 * Date: 2015/6/19
 * Time: 13:01
 * Email: leishuai@isoftstone.com
 * Desc:  领域子界面
 */
public class SettingMyFieldSubActivity extends BaseActivity implements View.OnClickListener {
    private ListView mfieldList;
    private ImageView left_arrow_btn;
    private TextView top_name;
    private MyFieldListAdapter mstoreListAdaper;
    private ArrayList arrlist;
    private String name;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.setting_myfield_layout);
        getData();
        initView();
        updateList();
    }
    //获取上级界面
    private void getData(){
        Bundle bundle = getIntent().getExtras();
        arrlist = bundle.getCharSequenceArrayList("arrlist");
        name = bundle.getString("name");
    }
    private void initView() {
        top_name = (TextView) findViewById(R.id.top_name);
        left_arrow_btn = (ImageView) findViewById(R.id.left_arrow_btn);
        mfieldList = (ListView) findViewById(R.id.mfield_listview);
        left_arrow_btn.setOnClickListener(this);
        left_arrow_btn.setVisibility(View.VISIBLE);
        top_name.setVisibility(View.VISIBLE);
        top_name.setText(name);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.left_arrow_btn ){
            finish();
        }
    }
    //更新listview
    void updateList() {
        if(mstoreListAdaper==null){
            mstoreListAdaper = new MyFieldListAdapter(this, arrlist);
        }
        mfieldList.setAdapter(mstoreListAdaper);
    }
}