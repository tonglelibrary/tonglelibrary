package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: shuailei
 * Date: 2015/8/7
 * Time: 9:30
 * Email: leishuai@isoftstone.com
 * Desc: 微信信息类
 */
public class WeChatPayInfo implements Serializable {
    private String app_id;
    private String partner_id;
    private String prepay_id;
//    private String package;
    private String nonce_str;
    private String timestamp;
    private String sign;

    public String getApp_id() {
        return app_id;
    }

    public String getPartner_id() {
        return partner_id;
    }

    public String getPrepay_id() {
        return prepay_id;
    }

    public String getNonce_str() {
        return nonce_str;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public String getSign() {
        return sign;
    }

    @Override
    public String toString() {
        return "app_id:" + app_id + "\r\n" +
                "partner_id:" + partner_id + "\r\n" +
                "prepay_id:" + prepay_id + "\r\n" +
//                "package:" + package + "\r\n" +
                "nonce_str:" + nonce_str + "\r\n" +
                "timestamp:" + timestamp + "\r\n" +
                "sign:" + sign;
    }
}
