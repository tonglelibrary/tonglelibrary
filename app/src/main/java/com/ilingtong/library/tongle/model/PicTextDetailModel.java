package com.ilingtong.library.tongle.model;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.protocol.GsonRequest;
import com.ilingtong.library.tongle.protocol.ParametersJson;
import com.ilingtong.library.tongle.protocol.ProductPicTextListItem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;

/**
 * User: lengjiqiang
 * Date: 2015/5/25
 * Time: 15:06
 * Email: jqleng@isoftstone.com
 * Dest:
 */
public class PicTextDetailModel extends Observable {
    Context contextModel;
    String TAG = "PIC_TEXT_DETAIL";
    // input parameters
    public String user_id;
    public String product_id;

    // output parameters
    public ArrayList<ProductPicTextListItem> picTextList;

    final private String detailUrl = TongleAppConst.SERVER_ADDRESS + ":" + TongleAppInstance.getInstance().getToken() + "/products/image_text_show";

    public PicTextDetailModel(Context context) {
        contextModel = context;
    }

    public void HttpRequest() {
        RequestQueue queue = Volley.newRequestQueue(contextModel);
        Map<String, String> requestParam = new HashMap<String, String>();
        requestParam.put("user_id", user_id);
        requestParam.put("product_id", product_id);
        Gson mGson = new Gson();
        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);

        GsonRequest<ParametersJson> gsonRequest = new GsonRequest<ParametersJson>(Request.Method.POST, detailUrl, ParametersJson.class, params_gson, new Response.Listener<ParametersJson>() {
            @Override
            public void onResponse(ParametersJson parameters_json) {
                picTextList = parameters_json.body.prod_pic_text_list;

                setChanged();
                notifyObservers(TAG);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG + " - ERROR", error.getMessage(), error);
            }
        });
        queue.add(gsonRequest);
    }

}
