package com.ilingtong.library.tongle.model;

import com.ilingtong.library.tongle.protocol.MyProceeds;

import java.util.ArrayList;

/**
 * User: shiyuchong
 * Date: 2015/6/19
 * Time: 14:27
 * Email: ycshi@isoftstone.com
 * Desc:[2014]
 */
public class MyProceedsModel {

    //收益数据列表
    public ArrayList<MyProceeds> my_income_list;
}
