package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;
import java.util.List;

/**
 * Created by wuqian on 2016/3/11.
 * mail: wuqian@ilingtong.com
 * Description:6007接口body entity
 */
public class GroupProductBodyInfo implements Serializable{
    private GroupPprodBaseInfo prod_base_info;   //商品基础信息
    private CouponFlagInfo coupon_flag;    //商品退货信息
    private CouponStoreListInfo coupon_store_info;    //适用商户信息
    private CouponPurchaseInfo coupon_purchase_info;   //团购信息详情信息
    private List<CouponPurchaseNoticeInfo> coupon_purchase_notice_list;   //购买须知列表
    private List<UIButtonInfo> ui_btn_control_list;  //商品按钮处理信息

    public GroupPprodBaseInfo getProd_base_info() {
        return prod_base_info;
    }

    public void setProd_base_info(GroupPprodBaseInfo prod_base_info) {
        this.prod_base_info = prod_base_info;
    }

    public CouponFlagInfo getCoupon_flag() {
        return coupon_flag;
    }

    public void setCoupon_flag(CouponFlagInfo coupon_flag) {
        this.coupon_flag = coupon_flag;
    }

    public CouponStoreListInfo getCoupon_store_info() {
        return coupon_store_info;
    }

    public void setCoupon_store_info(CouponStoreListInfo coupon_store_info) {
        this.coupon_store_info = coupon_store_info;
    }

    public CouponPurchaseInfo getCoupon_purchase_info() {
        return coupon_purchase_info;
    }

    public void setCoupon_purchase_info(CouponPurchaseInfo coupon_purchase_info) {
        this.coupon_purchase_info = coupon_purchase_info;
    }

    public List<CouponPurchaseNoticeInfo> getCoupon_purchase_notice_list() {
        return coupon_purchase_notice_list;
    }

    public void setCoupon_purchase_notice_list(List<CouponPurchaseNoticeInfo> coupon_purchase_notice_list) {
        this.coupon_purchase_notice_list = coupon_purchase_notice_list;
    }

    public List<UIButtonInfo> getUi_btn_control_list() {
        return ui_btn_control_list;
    }

    public void setUi_btn_control_list(List<UIButtonInfo> ui_btn_control_list) {
        this.ui_btn_control_list = ui_btn_control_list;
    }
}
