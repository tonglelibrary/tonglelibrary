package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;
import java.util.List;

/**
 * Created by wuqian on 2016/3/10.
 * mail: wuqian@ilingtong.com
 * Description:团购商品商品详情信息
 * use by 6007 6011
 */
public class CouponPurchaseInfo implements Serializable {
    private String head;   //信息标题
    private List<CouponPurchaseContentInfo> content_list;   //信息明细列表
    private double content_total_price;  //明细项目总金额
    private double content_actually_price; // 明细项目团购总额
    private String footer;  //信息脚注

    public String getHead() {
        return head;
    }

    public void setHead(String head) {
        this.head = head;
    }

    public List<CouponPurchaseContentInfo> getContent_list() {
        return content_list;
    }

    public void setContent_list(List<CouponPurchaseContentInfo> content_list) {
        this.content_list = content_list;
    }

    public double getContent_total_price() {
        return content_total_price;
    }

    public void setContent_total_price(double content_total_price) {
        this.content_total_price = content_total_price;
    }

    public double getContent_actually_price() {
        return content_actually_price;
    }

    public void setContent_actually_price(double content_actually_price) {
        this.content_actually_price = content_actually_price;
    }

    public void setContent_actually_price(int content_actually_price) {
        this.content_actually_price = content_actually_price;
    }

    public String getFooter() {
        return footer;
    }

    public void setFooter(String footer) {
        this.footer = footer;
    }
}
