//package com.ilingtong.app.tongle.activity;
//
//import android.os.Bundle;
//import android.view.View;
//import android.widget.LinearLayout;
//import android.widget.RelativeLayout;
//
//import com.android.volley.Response;
//import com.android.volley.VolleyError;
//import com.ilingtong.app.tongle.R;
//import com.ilingtong.app.tongle.ServiceManager;
//import com.ilingtong.app.tongle.TongleAppConst;
//import com.ilingtong.app.tongle.TongleAppInstance;
//import com.ilingtong.app.tongle.adapter.TotalOrderListAdapter;
//import com.ilingtong.app.tongle.external.NewActivity;
//import com.ilingtong.app.tongle.external.maxwin.view.XListView;
//import com.ilingtong.app.tongle.model.UserOrdersModel;
//import com.ilingtong.app.tongle.protocol.OrderRequestParam;
//import com.ilingtong.app.tongle.protocol.UserOrdersResult;
//import com.ilingtong.app.tongle.utils.ToastUtils;
//
///**
// * User: syc
// * Date: 2015/6/11
// * Time: 10:45
// * Email: ycshi@isoftstone.com
// * Dest: 全部订单 页
// */
//public class OrderTotalActivity extends NewActivity {
//    private XListView listview;
//    private RelativeLayout rl_replace;
//    private LinearLayout topview_layout;
//    private TotalOrderListAdapter totalOrderListAdapter;
//    private UserOrdersModel userOrdersModel;
//    private OrderRequestParam param;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.xlistview_comm_layout);
//        initView();
//        doRequest();
//    }
//
//    public void initView() {
//        listview = (XListView) findViewById(R.id.xlistview);
//        rl_replace = (RelativeLayout) findViewById(R.id.rl_replace);
//        topview_layout = (LinearLayout) findViewById(R.id.topview_layout);
//        topview_layout.setVisibility(View.GONE);
//        if (userOrdersModel == null)
//            userOrdersModel = new UserOrdersModel();
//    }
//
//    public void doRequest() {
//        param = new OrderRequestParam();
//        param.user_id = TongleAppInstance.getInstance().getUserID();
//        param.order_status = TongleAppConst.ORDER_FILTER_TOTAL;
//        param.order_date_from = "";
//        param.order_date_to = "";
//        param.order_no = "";
//        param.forward = "";
//        param.fetch_count = TongleAppConst.FETCH_COUNT;
//        ServiceManager.getUserOrdersRequest(param, successListener(), errorListener());
//    }
//
//    @Override
//    public void onRefresh(int id) {
//        doRequest();
//    }
//
//    @Override
//    public void onLoadMore(int id) {
//
//    }
//
//    /**
//     * 功能：网络响应成功，返回数据
//     */
//    private Response.Listener successListener() {
//        return new Response.Listener<UserOrdersResult>() {
//            @Override
//            public void onResponse(UserOrdersResult userOrdersResult) {
//                if (TongleAppConst.SUCCESS.equals(userOrdersResult.getHead().getReturn_flag())) {
//                    userOrdersModel.data_total_count = userOrdersResult.getBody().getData_total_count();
//                    userOrdersModel.order_list = userOrdersResult.getBody().getOrder_list();
//                    updateListView();
//                } else {
//                    ToastUtils.toastShort(getResources().getString(R.string.para_exception) + userOrdersResult.getHead().getFunction_id() + userOrdersResult.getHead().getReturn_message());
//                }
//            }
//
//        };
//    }
//
//    /**
//     * 功能：网络响应失败
//     */
//    private Response.ErrorListener errorListener() {
//        return new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError volleyError) {
//                ToastUtils.toastShort(getResources().getString(R.string.sys_exception));
//            }
//        };
//    }
//
//    public void updateListView() {
//        if (userOrdersModel.order_list.size() == 0) {
//            listview.setVisibility(View.GONE);
//            rl_replace.setVisibility(View.VISIBLE);
//        }
//        totalOrderListAdapter = new TotalOrderListAdapter(this, userOrdersModel.order_list);
//        listview.setPullLoadEnable(false);
//        listview.setPullRefreshEnable(true);
//        listview.setXListViewListener(this, 0);
//        listview.setRefreshTime();
//        listview.setAdapter(totalOrderListAdapter);
//    }
//}
