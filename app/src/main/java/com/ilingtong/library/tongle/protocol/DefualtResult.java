package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/1/6.
 * mail: wuqian@ilingtong.com
 * Description:该类为默认返回结果类，应用于只有head有数据，body没数据返回的接口
 */
public class DefualtResult implements Serializable {
    private BaseInfo head;
    private Object body;

    public BaseInfo getHead() {
        return head;
    }

    public void setHead(BaseInfo head) {
        this.head = head;
    }

    public Object getBody() {
        return body;
    }

    public void setBody(Object body) {
        this.body = body;
    }
}
