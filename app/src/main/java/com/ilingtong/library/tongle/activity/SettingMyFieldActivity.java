package com.ilingtong.library.tongle.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.adapter.MyFieldListAdapter;
import com.ilingtong.library.tongle.model.MyFieldModel;
import com.ilingtong.library.tongle.protocol.MFieldResult;
import com.ilingtong.library.tongle.utils.ToastUtils;

import java.util.ArrayList;

/**
 * User: shuailei
 * Date: 2015/6/19
 * Time: 13:01
 * Email: leishuai@isoftstone.com
 * Desc:  领域界面
 */
public class SettingMyFieldActivity extends BaseActivity implements View.OnClickListener {
    private ListView mfieldList;
    private ImageView left_arrow_btn;
    private TextView mfield_keep;
    private TextView top_name;
    private MyFieldListAdapter mstoreListAdaper;
    private MyFieldModel myFieldModel;
    private String tag; //标识，判断从哪一个页面跳进来,
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.setting_myfield_layout);
        getData();
        initView();
    }
    public void getData(){
        tag = getIntent().getExtras().getString("tag");
    }
    void initView() {
        top_name = (TextView) findViewById(R.id.top_name);
        mfield_keep = (TextView)findViewById(R.id.top_btn_text);
        left_arrow_btn = (ImageView) findViewById(R.id.left_arrow_btn);
        mfieldList = (ListView)findViewById(R.id.mfield_listview);
        left_arrow_btn.setOnClickListener(this);
        mfield_keep.setOnClickListener(this);
        top_name.setText(getString(R.string.mfield_text));
        mfield_keep.setText(getString(R.string.save));
        left_arrow_btn.setVisibility(View.VISIBLE);
        top_name.setVisibility(View.VISIBLE);
        mfield_keep.setVisibility(View.VISIBLE);

        if(myFieldModel==null){
            myFieldModel=new MyFieldModel();
        }
        if (MyFieldModel.checkList == null)
            MyFieldModel.checkList = new ArrayList();
        //请求数据
        ServiceManager.doMFieldRequest(TongleAppInstance.getInstance().getUserID(), successListener(), errorListener());
    }

    @Override
    public void onClick(View v) {
        if (v.getId() ==  R.id.left_arrow_btn){ //返回按钮,根据传过来的标识判断返回的界面(login为登陆界面，set为设置界面)
            if (tag.equals("login")) {
                Intent intent = new Intent(SettingMyFieldActivity.this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            } else {
                finish();
            }
        }else if (v.getId() == R.id.top_btn_text){
            if (MyFieldModel.checkList != null && MyFieldModel.checkList.size() > 0) {
                ServiceManager.saveFieldRequest(TongleAppInstance.getInstance().getUserID(), MyFieldModel.checkList, saveSuccessListener(), errorListener());
            } else {
                ToastUtils.toastShort(getString(R.string.myfield_no_select_onsave));
                //  根据传过来的标识判断返回的界面(login为登陆界面，set为设置界面)
                if (tag.equals("login")) {
                    Intent i = new Intent(SettingMyFieldActivity.this, MainActivity.class);
                    startActivity(i);
                } else {
                    finish();
                }
            }
        }
    }
    //更新listview
    void updateList() {
        if(mstoreListAdaper==null) {
            mstoreListAdaper = new MyFieldListAdapter(this, myFieldModel.type_list);
        }
        mfieldList.setAdapter(mstoreListAdaper);

    }
    /**
     * 功能：我的领域网络响应成功，返回数据
     */
    private Response.Listener successListener() {
        return new Response.Listener<MFieldResult>() {
            @Override
            public void onResponse(MFieldResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    myFieldModel.type_list=response.getBody().getType_list();
                    updateList();
                } else {
                    ToastUtils.toastShort(getString(R.string.para_exception)+response.getHead().getFunction_id() + response.getHead().getReturn_message());
                }
            }
        };
    }
    /**
     * 功能：我的领域网络响应成功，返回数据
     */
    private Response.Listener saveSuccessListener() {
        return new Response.Listener<MFieldResult>() {
            @Override
            public void onResponse(MFieldResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    ToastUtils.toastShort(getString(R.string.common_save_success));
                    //  根据传过来的标识判断返回的界面(login为登陆界面，set为设置界面)
                    if(tag.equals("login")){
                        Intent intent = new Intent(SettingMyFieldActivity.this, MainActivity.class);
                        startActivity(intent);
                    }else {
                        finish();
                    }
                } else {
                    ToastUtils.toastShort(getString(R.string.para_exception)+ response.getHead().getReturn_message());
                }
            }
        };
    }

    /**
     * 功能：魔店详情请求网络响应失败
     */
    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ToastUtils.toastShort(getString(R.string.sys_exception));
            }
        };
    }
}