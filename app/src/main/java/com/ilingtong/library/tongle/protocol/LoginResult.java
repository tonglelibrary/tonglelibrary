package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/6/2
 * Time: 17:51
 * Email: jqleng@isoftstone.com
 * Dest:
 */
public class LoginResult implements Serializable {
    private BaseInfo head;
    private LoginInfo body;

    public BaseInfo getHead() {
        return head;
    }

    public void setHead(BaseInfo head) {
        this.head = head;
    }

    public LoginInfo getBody() {
        return body;
    }

    public void setBody(LoginInfo body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return body.toString();
    }
}
