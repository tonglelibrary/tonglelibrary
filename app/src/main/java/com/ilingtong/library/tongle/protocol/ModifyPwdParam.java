package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/6/25
 * Time: 14:10
 * Email: jqleng@isoftstone.com
 * Desc: 修改密码参数类
 */
public class ModifyPwdParam implements Serializable {
    public String user_token;
    public String phone_number;
    public String old_password;
    public String latest_password;
    public String confirm_password;
}
