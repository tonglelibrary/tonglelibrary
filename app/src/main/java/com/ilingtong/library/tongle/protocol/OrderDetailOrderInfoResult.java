package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: shuailei
 * Date: 2015/6/10
 * Time: 13:01
 * Email: leishuai@isoftstone.com
 * Desc: 订单详情信息类，包含从网络返回的数据
 */
public class OrderDetailOrderInfoResult implements Serializable {
    public OrderDetailOrderInfo order_info;

    @Override
    public String toString() {
        return "order_info:" + order_info;
    }
}
