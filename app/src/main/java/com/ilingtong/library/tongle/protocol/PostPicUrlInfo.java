package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: leishuai
 * Date: 2015/8/6
 * Time: 10:02
 * Email: leishuai@isoftstone.com
 * Dest:
 */
public class PostPicUrlInfo implements Serializable {
    //帖子详情post_thumbnail_pic_url中的pic_url
    public String pic_url;

    @Override
    public String toString() {
        return "pic_url:" + pic_url;
    }
}
