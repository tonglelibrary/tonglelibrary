package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/6/17
 * Time: 13:18
 * Email: jqleng@isoftstone.com
 * Desc: 作为对象参数，通过网络传回到应用的的注册信息类
 */
public class RegistResult implements Serializable {

    private BaseInfo head;
    private RegistInfo body;

    public BaseInfo getHead() {
        return head;
    }

    public void setHead(BaseInfo head) {
        this.head = head;
    }

    public RegistInfo getBody() {
        return body;
    }

    public void setBody(RegistInfo body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return body.toString();
    }

}
