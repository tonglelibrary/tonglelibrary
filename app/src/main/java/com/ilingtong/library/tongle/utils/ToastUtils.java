package com.ilingtong.library.tongle.utils;

import android.content.Context;
import android.widget.Toast;
import com.ilingtong.library.tongle.TongleAppInstance;

/**
 * Toast工具类
 * 
 * @author GaiQS E-mail:gaiqs@sina.com
 * @Date: 2015年1月28日
 * @Time: 下午2:54:05
 */
public class ToastUtils {
	
	public static long LAST_CLOCK_TIME;

	public static void toastShort(String text) {
		Toast.makeText(TongleAppInstance.getAppContext(), text, Toast.LENGTH_SHORT).show();
	}

	public static void toastShort(int resId) {
		Toast.makeText(TongleAppInstance.getAppContext(), resId, Toast.LENGTH_SHORT).show();
	}

	public static void toastShort(int resId, Object... params) {
		Context context = TongleAppInstance.getAppContext();
		if (null != params) {
			Toast.makeText(context, context.getString(resId, params), Toast.LENGTH_SHORT).show();
		} else {
			Toast.makeText(context, resId, Toast.LENGTH_SHORT).show();
		}
	}

	// 防误点
	public synchronized static boolean isFastClick() {
		long time = System.currentTimeMillis();
		if (time - LAST_CLOCK_TIME < 3000) {
			return true;
		}
		LAST_CLOCK_TIME = time;
		return false;
	}

}
