package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * User: shuailei
 * Date: 2015/6/24
 * Time: 22:05
 * Email: leishuai@isoftstone.com
 * Desc:
 */
public class MyShoppingCartInfo implements Serializable {
    public String seq_no;
    public String post_id;
    public String prod_pic_url;
    public String prod_id;
    public String prod_name;
    public String mstore_id;
    public String mstore_name;
    public String price;
    public String order_qty;
    public String amount;
    public String fee_amount;
    public String relation_id;
    public ArrayList<ProdSpecListInfo> prod_spec_list;
    public String country_pic_url;
    public String import_info_desc;
    public String transfer_fee_desc;
    public String tariff_desc;
    public String tariff;
    public String import_goods_flag;


    @Override
    public String toString() {
        return "seq_no:" + seq_no + "\r\n" +
                "post_id:" + post_id + "\r\n" +
                "prod_pic_url:" + prod_pic_url + "\r\n" +
                "prod_id:" + prod_id + "\r\n" +
                "prod_name:" + prod_name + "\r\n" +
                "mstore_id:" + mstore_id + "\r\n" +
                "mstore_name:" + mstore_name + "\r\n" +
                "price:" + price + "\r\n" +
                "order_qty:" + order_qty + "\r\n" +
                "amount:" + amount + "\r\n" +
                "fee_amount:" + fee_amount + "\r\n" +
                "relation_id:" + relation_id + "\r\n" +
                "prod_spec_list:" + prod_spec_list + "\r\n" +
                "country_pic_url:" + country_pic_url + "\r\n" +
                "import_info_desc:" + import_info_desc + "\r\n" +
                "transfer_fee_desc:" + transfer_fee_desc + "\r\n" +
                "tariff_desc:" + tariff_desc + "\r\n" +
                "tariff:" + tariff + "\r\n" +
                "import_goods_flag:" + import_goods_flag;
    }
}
