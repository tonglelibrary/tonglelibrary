package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * User: syc
 * Date: 2015/6/19
 * Time: 14:18
 * Email: ycshi@isoftstone.com
 * Desc: 【1040】
 */
public class MyProceedsInfo implements Serializable {


    private ArrayList<MyProceeds> my_income_list;

    public ArrayList<MyProceeds> getIncome_list() {
        return my_income_list;
    }

    @Override
    public String toString() {
        return "income_list:" + my_income_list;
    }




}
