package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: syc
 * Date: 2015/6/26
 * Time: 14:18
 * Email: ycshi@isoftstone.com
 * Desc: 【2015】
 */
public class APPpromotionInfo implements Serializable {

    private String app_qr_code_url;


    public String getApp_qr_code_url() {
        return app_qr_code_url;
    }



    @Override
    public String toString() {
        return "app_qr_code_url:" + app_qr_code_url ;
    }
}
