//package com.ilingtong.app.tongle.activity;
//
//import android.app.Activity;
//import android.app.AlertDialog;
//import android.content.Intent;
//import android.os.Bundle;
//import android.os.Handler;
//import android.os.Message;
//import android.text.TextUtils;
//import android.view.KeyEvent;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.widget.Button;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.TextView;
//
//import com.alipay.sdk.app.PayTask;
//import com.android.volley.Response;
//import com.android.volley.VolleyError;
//import com.ilingtong.app.tongle.R;
//import com.ilingtong.app.tongle.ServiceManager;
//import com.ilingtong.app.tongle.TongleAppConst;
//import com.ilingtong.app.tongle.TongleAppInstance;
//import com.ilingtong.app.tongle.adapter.OrderDetailListAdaper;
//import com.ilingtong.app.tongle.demo.PayResult;
//import com.ilingtong.app.tongle.external.maxwin.view.XListView;
//import com.ilingtong.app.tongle.model.OrderDetailModel;
//import com.ilingtong.app.tongle.protocol.AliPayResult;
//import com.ilingtong.app.tongle.protocol.CodeResult;
//import com.ilingtong.app.tongle.protocol.OrderDetailResult;
//import com.ilingtong.app.tongle.protocol.WeChatPayResult;
//import com.ilingtong.app.tongle.utils.ToastUtils;
//import com.tencent.mm.sdk.modelpay.PayReq;
//import com.tencent.mm.sdk.openapi.IWXAPI;
//import com.tencent.mm.sdk.openapi.WXAPIFactory;
//
///**
// * User: shuailei
// * Date: 2015/6/10
// * Time: 13:01
// * Email: leishuai@isoftstone.com
// * Desc: 支付
// */
//public class OrderDetailPayActivity extends BaseActivity implements XListView.IXListViewListener, View.OnClickListener {
//    private ImageView left_arrow_btn;
//    private TextView top_name;
//    private XListView listview;
//    private Button cancle_order;
//    private View headView, footView;
//
//    private OrderDetailModel OrderDetailModel;
//    private TextView amount, fee_amount, consignee, address, order_no, order_time, statu, shipping_message, shipping_time;
//    private Button payNow, cancle, makesure;
//    private AlertDialog alert;
//    private String order;
//    private LinearLayout tariff_message_linear;
//    private Button tariff_subBtn;
//    private TextView orderdetail_tariff;
//    private TextView pass_msg;
//    String pay_type_id;
//    IWXAPI api;
//
//    private static final int SDK_PAY_FLAG = 1;
//
//    private static final int SDK_CHECK_FLAG = 2;
//
//    private Handler mHandler = new Handler() {
//        public void handleMessage(Message msg) {
//            if (msg.what == SDK_PAY_FLAG){
//                PayResult payResult = new PayResult((String) msg.obj);
//                // 支付宝返回此次支付结果及加签，建议对支付宝签名信息拿签约时支付宝提供的公钥做验签
//                String resultInfo = payResult.getResult();
//
//                String resultStatus = payResult.getResultStatus();
//
//                // 判断resultStatus 为“9000”则代表支付成功，具体状态码代表含义可参考接口文档
//                if (TextUtils.equals(resultStatus, "9000")) {
//                    ToastUtils.toastShort(getString(R.string.common_pay_success));
//                } else {
//                    // 判断resultStatus 为非“9000”则代表可能支付失败
//                    // “8000”代表支付结果因为支付渠道原因或者系统原因还在等待支付结果确认，最终交易是否成功以服务端异步通知为准（小概率状态）
//                    if (TextUtils.equals(resultStatus, "8000")) {
//                        ToastUtils.toastShort(getString(R.string.common_pay_not_success));
//
//                    } else {
//                        // 其他值就可以判断为支付失败，包括用户主动取消支付，或者系统返回的错误
//                        ToastUtils.toastShort(getString(R.string.common_pay_error));
//                    }
//                }
//                return;
//            }else if (msg.what == SDK_CHECK_FLAG){
//                ToastUtils.toastShort(getString(R.string.common_pay_result));
//                return;
//            }
//        }
//
//        ;
//    };
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.xlistview_comm_layout);
//        getData();
//        initView();
//        doRequest();
//    }
//    public void getData(){
//        order = getIntent().getExtras().getString("order_no");
//    }
//    public void initView(){
//        top_name = (TextView) findViewById(R.id.top_name);
//        left_arrow_btn = (ImageView) findViewById(R.id.left_arrow_btn);
//        listview = (XListView) findViewById(R.id.xlistview);
//        left_arrow_btn.setOnClickListener(this);
//        left_arrow_btn.setVisibility(View.VISIBLE);
//        top_name.setVisibility(View.VISIBLE);
//        top_name.setText(getString(R.string.order_detail_delete_top_name));
//
//        headView = LayoutInflater.from(this).inflate(R.layout.orderdetail_headlayout, null);
//        footView = LayoutInflater.from(this).inflate(R.layout.orderdetail_footlayout, null);
//
//        //订单金额
//        amount = (TextView) headView.findViewById(R.id.orderdetail_amount);
//        //运费
//        fee_amount = (TextView) headView.findViewById(R.id.orderdetail_fee_amount);
//        //收件人
//        consignee = (TextView) headView.findViewById(R.id.orderdetail_consignee);
//        //地址
//        address = (TextView) headView.findViewById(R.id.orderdetail_address);
//        //订单号
//        order_no = (TextView) footView.findViewById(R.id.orderdetail_order_no);
//        //下单时间
//        order_time = (TextView) footView.findViewById(R.id.orderdetail_order_time);
//        //关税信息linear
//        tariff_message_linear = (LinearLayout) footView.findViewById(R.id.tariff_message_linear);
//        //通关信息
//        pass_msg = (TextView) footView.findViewById(R.id.pass_msg);
//        //关税
//        orderdetail_tariff = (TextView) headView.findViewById(R.id.orderdetail_tariff);
//        //再次提交按钮
//        tariff_subBtn = (Button) footView.findViewById(R.id.tariff_subBtn);
//        tariff_subBtn.setOnClickListener(this);
//        //订单状态
//        statu = (TextView) headView.findViewById(R.id.orderdetail_statu);
//        statu.setText(getString(R.string.order_detail_pay_status));
//        //立即支付按钮
//        payNow = (Button) headView.findViewById(R.id.orderdetail_deleteorder);
//        payNow.setOnClickListener(this);
//        payNow.setText(getString(R.string.order_detail_pay_now));
//        //快递信息
//        shipping_message = (TextView) headView.findViewById(R.id.shipping_message);
//        shipping_message.setText(getString(R.string.order_detail_pay_ship_message));
//        //快递时间
//        shipping_time = (TextView) headView.findViewById(R.id.shipping_time);
//        shipping_time.setVisibility(View.INVISIBLE);
//        //底部取消订单linearlayout可见
//        LinearLayout lieanr = (LinearLayout) footView.findViewById(R.id.orderdetail_canclelinear);
//        lieanr.setVisibility(View.VISIBLE);
//        //底部取消按钮
//        cancle_order = (Button) footView.findViewById(R.id.orderdetail_paynow_cancleorder);
//        cancle_order.setOnClickListener(this);
//
//        listview.addHeaderView(headView);
//        listview.addFooterView(footView);
//
//        //订单基本信息
//        if (OrderDetailModel == null)
//            OrderDetailModel = new OrderDetailModel();
//    }
//    public void doRequest(){
//        ServiceManager.OrderDetailRequest(TongleAppInstance.getInstance().getUserID(), order, successListener(), errorListener());
//    }
//    @Override
//    public void onClick(View v) {
//        if (v.getId() == R.id.left_arrow_btn){
////            Intent intent = new Intent( OrderDetailPayActivity.this,OrderMineActivity.class);
////            startActivity(intent);
//            finish();
//        }else if (v.getId() == R.id.orderdetail_paynow_cancleorder){//取消订单
//            AlertDialog.Builder builder = new AlertDialog.Builder(OrderDetailPayActivity.this);
//            View view = LayoutInflater.from(OrderDetailPayActivity.this).inflate(R.layout.orderdetail_delete_layout, null);
//            builder.setView(view);
//            alert = builder.create();
//            alert.setCanceledOnTouchOutside(true);
//            alert.show();
//            cancle = (Button) view.findViewById(R.id.deleteorder_cancle);
//            makesure = (Button) view.findViewById(R.id.deleteorder_sure);
//            cancle.setOnClickListener(this);
//            makesure.setOnClickListener(this);
//        }else if (v.getId() ==  R.id.deleteorder_cancle){
//            alert.dismiss();
//        }else if (v.getId() == R.id.deleteorder_sure){
//            String reason = "";//模拟删除订单原因
//            ServiceManager.OrderDetailDeleteRequest(TongleAppInstance.getInstance().getUserID(), order, reason, successDeleteListener(), errorListener());
//        }else if (v.getId() == R.id.orderdetail_deleteorder){
//            //调支付宝付款方式（模拟）
//            if (pay_type_id.equals("1")) {
//                ServiceManager.AliPayRequest(TongleAppInstance.getInstance().getUserID(),order, aliPaySuccessListener(), errorListener());
//            } else if (pay_type_id.equals("3")) {
//                //调微信付款方式（模拟）
//                ServiceManager.WeChatPayRequest(TongleAppInstance.getInstance().getUserID(), order, WeChatsuccessListener(), errorListener());
//            }
//        }else if(v.getId()==R.id.tariff_subBtn){
//            ServiceManager.doReCustomers(TongleAppInstance.getInstance().getUserID(), order, comminttsuccessListener(), errorListener());
//        }
//    }
//
//    @Override
//    public void onRefresh(int id) {
//
//    }
//
//    @Override
//    public void onLoadMore(int id) {
//
//    }
//
//    /**
//     * 功能：订单详情网络响应成功，返回数据
//     */
//    private Response.Listener successListener() {
//        return new Response.Listener<OrderDetailResult>() {
//            @Override
//            public void onResponse(OrderDetailResult response) {
//                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
//                    OrderDetailModel.order_detail = response.getBody().order_info.order_detail;
//                    amount.setText(getString(R.string.common_fee_sum) + response.getBody().order_info.head_info.amount);
//                    order_no.setText(getString(R.string.common_order_number) + response.getBody().order_info.head_info.order_no);
//                    order_time.setText(getString(R.string.common_order_time) + response.getBody().order_info.head_info.order_time);
//                    fee_amount.setText(getString(R.string.common_fee_transportation) + response.getBody().order_info.head_info.fee_amount);
//                    consignee.setText(getString(R.string.common_order_consignee) + response.getBody().order_info.add_info.consignee);
//                    pass_msg.setText(response.getBody().order_info.head_info.customs_fail_reason);
//                    orderdetail_tariff.setText(getString(R.string.common_fee_tariff)+response.getBody().order_info.head_info.tariff);
//                    String a = response.getBody().order_info.add_info.province + response.getBody().order_info.add_info.area + response.getBody().order_info.add_info.address;
//                    address.setText(getString(R.string.common_order_address) + a);
//                    if(response.getBody().order_info.logistics_info.shipping_company==null||response.getBody().order_info.logistics_info.shipping_company.equals("")){
//                        shipping_message.setText(getString(R.string.order_detail_delete_not_shipped));
//                    }
//                    else{
//                        shipping_message.setText(response.getBody().order_info.logistics_info.shipping_company + "," + response.getBody().order_info.logistics_info.shipping_no);
//                    }
//                    shipping_time.setText(response.getBody().order_info.logistics_info.shipping_time);
//                    pay_type_id=response.getBody().order_info.pay_type_id;
//
//
//                    //是否通关（0是，1否）
//                    String tag_flag=response.getBody().order_info.head_info.customs_flag;
//                    if(tag_flag.equals("0")){
//                        tariff_subBtn.setVisibility(View.GONE);
//                    }else if(tag_flag.equals("2")){
//                        tariff_message_linear.setVisibility(View.GONE);
//                    }else{
//                        tariff_subBtn.setVisibility(View.VISIBLE);
//                        statu.setText(getString(R.string.common_tariff_error));
//                        pass_msg.setTextColor(getResources().getColor(R.color.order_payway_press));
//                    }
//
//
//                    updateListView();
//                } else {
//                    ToastUtils.toastShort(getString(R.string.para_exception)+response.getHead().getReturn_message());
//                }
//            }
//        };
//    }
//
//    public void updateListView() {
//        OrderDetailListAdaper adapter = new OrderDetailListAdaper(this, OrderDetailModel.order_detail);
//        listview.setPullLoadEnable(false);
//        listview.setPullRefreshEnable(true);
//        listview.setXListViewListener(this, 0);
//        listview.setRefreshTime();
//        listview.setAdapter(adapter);
//    }
//
//    /**
//     * 功能：订单详情请求网络响应失败
//     */
//    private Response.ErrorListener errorListener() {
//        return new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError volleyError) {
//                ToastUtils.toastShort(getString(R.string.sys_exception));
//            }
//        };
//    }
//
//    /**
//     * 功能：删除订单网络响应成功，返回数据
//     */
//    private Response.Listener successDeleteListener() {
//        return new Response.Listener<OrderDetailResult>() {
//            @Override
//            public void onResponse(OrderDetailResult response) {
//                ToastUtils.toastShort(getString(R.string.common_order_cancel_success));
////                Intent intent = new Intent(OrderDetailPayActivity.this, OrderMineActivity.class);
////                startActivity(intent);
//                finish();
//            }
//        };
//    }
//
//    /**
//     * 功能：网络响应成功，返回数据
//     */
//    private Response.Listener aliPaySuccessListener() {
//        return new Response.Listener<AliPayResult>() {
//            @Override
//            public void onResponse(AliPayResult result) {
//                if (TongleAppConst.SUCCESS.equals(result.getHead().getReturn_flag())) {
//
//                    final String orderInfo = result.getBody().getAlipay_info().toString(); // 订单信息
//                    Runnable payRunnable = new Runnable() {
//                        @Override
//                        public void run() {
//                            PayTask alipay = new PayTask(OrderDetailPayActivity.this);
//                            String result = alipay.pay(orderInfo);
//                            Message msg = new Message();
//                            msg.what = SDK_PAY_FLAG;
//                            msg.obj = result;
//                            mHandler.sendMessage(msg);
//                        }
//                    };
//                    // 异步调用
//                    Thread payThread = new Thread(payRunnable);
//                    payThread.start();
//
//
//                } else {
//                    ToastUtils.toastShort(getString(R.string.common_no_data));
//                }
//            }
//
//        };
//    }
//
//    /**
//     * 功能：再次提交网络响应成功，返回数据
//     */
//    private Response.Listener comminttsuccessListener() {
//        return new Response.Listener<CodeResult>() {
//            @Override
//            public void onResponse(CodeResult response) {
//                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
//                    tariff_subBtn.setVisibility(View.GONE);
//                    ToastUtils.toastShort(getString(R.string.common_tariff_success));
//                } else {
//                    ToastUtils.toastShort(getString(R.string.para_exception)+ response.getHead().getFunction_id() + response.getHead().getReturn_message());
//                }
//            }
//        };
//    }
//    /**
//     * 功能：网络响应成功，返回数据
//     */
//    private Response.Listener WeChatsuccessListener() {
//        return new Response.Listener<WeChatPayResult>() {
//            @Override
//            public void onResponse(WeChatPayResult userOrdersResult) {
//                if (TongleAppConst.SUCCESS.equals(userOrdersResult.getHead().getReturn_flag())) {
//                    api= WXAPIFactory.createWXAPI(OrderDetailPayActivity.this, userOrdersResult.getBody().getWexin_pay_info().getApp_id());
//                    //微信请求支付加参数
//                    PayReq req = new PayReq();
//                    req.appId = userOrdersResult.getBody().getWexin_pay_info().getApp_id();
//                    req.partnerId = userOrdersResult.getBody().getWexin_pay_info().getPartner_id();
//                    req.prepayId = userOrdersResult.getBody().getWexin_pay_info().getPrepay_id();
//                    req.nonceStr = userOrdersResult.getBody().getWexin_pay_info().getNonce_str();
//                    req.timeStamp = userOrdersResult.getBody().getWexin_pay_info().getTimestamp();
//                    req.packageValue = "Sign=WXPay";//"Sign=" + packageValue;
//                    req.sign = userOrdersResult.getBody().getWexin_pay_info().getSign();
//                    // 在支付之前，如果应用没有注册到微信，应该先调用IWXMsg.registerApp将应用注册到微信
//                    api.sendReq(req);
//
//                } else {
//                    ToastUtils.toastShort(getString(R.string.common_no_data));
//                }
//            }
//
//        };
//    }
//
////    @Override
////    public boolean onKeyDown(int keyCode, KeyEvent event) {
////        if (keyCode == KeyEvent.KEYCODE_BACK) {
////            Intent intent = new Intent( OrderDetailPayActivity.this,OrderMineActivity.class);
////            startActivity(intent);
////            finish();
////
////        }
////        return super.onKeyDown(keyCode, event);
////    }
//
//}