package com.ilingtong.library.tongle.fragment;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.adapter.CollectFragmentPagerAdapter;

/**
 * User: lengjiqiang
 * Date: 2015/4/24
 * Time: 16:23
 * Email: jqleng@isoftstone.com
 * Desc: 收藏fragmen 改fragment中嵌套四个fragment M客，M帖，M店，M品
 */
public class CollectFragment extends BaseFragment {
    private TextView topname;
    private ImageView left_arrow_btn;
    private TextView expertText;
    private TextView forumText;
    private TextView storeText;
    private TextView productText;

    private ImageView imageView1;
    private ImageView imageView2;
    private ImageView imageView3;
    private ImageView imageView4;
    private ViewPager viewPager;

    private int currIndex = 0;
    private Fragment[] fragments = new Fragment[4];


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mainView = inflater.inflate(R.layout.fragment_collect_layout, null);
        initView(mainView);
        return mainView;
    }
    public void initView(View mainView ){
        topname = (TextView) mainView.findViewById(R.id.top_name);
        left_arrow_btn = (ImageView) mainView.findViewById(R.id.left_arrow_btn);
        expertText = (TextView) mainView.findViewById(R.id.expert_text);
        forumText = (TextView) mainView.findViewById(R.id.forum_text);
        storeText = (TextView) mainView.findViewById(R.id.store_text);
        productText = (TextView) mainView.findViewById(R.id.product_text);
        viewPager = (ViewPager) mainView.findViewById(R.id.collect_pager);

        imageView1 = (ImageView) mainView.findViewById(R.id.collect_cursor1);
        imageView2 = (ImageView) mainView.findViewById(R.id.collect_cursor2);
        imageView3 = (ImageView) mainView.findViewById(R.id.collect_cursor3);
        imageView4 = (ImageView) mainView.findViewById(R.id.collect_cursor4);

        topname.setText(getString(R.string.fragment_collect_top_name));
        topname.setGravity(View.TEXT_ALIGNMENT_CENTER);
        topname.setVisibility(View.VISIBLE);
        left_arrow_btn.setVisibility(View.GONE);

        expertText.setOnClickListener(new TextOnClickListener(0));
        forumText.setOnClickListener(new TextOnClickListener(1));
        storeText.setOnClickListener(new TextOnClickListener(2));
        productText.setOnClickListener(new TextOnClickListener(3));
        fragments[0] = MExpertFragment.newInstance(TongleAppConst.COLLECTFRAGMENT_INTO, null);
        fragments[1] = MForumFragment.newInstance(TongleAppConst.COLLECTFRAGMENT_INTO, null);
        fragments[2] = MStoreFragment.newInstance(TongleAppConst.COLLECTFRAGMENT_INTO, null);
        fragments[3] = MProductFragment.newInstance(TongleAppConst.COLLECTFRAGMENT_INTO, null);
        CollectFragmentPagerAdapter adapter = new CollectFragmentPagerAdapter(getFragmentManager(),fragments);
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(4);
        viewPager.setCurrentItem(0);
        viewPager.setOnPageChangeListener(new ViewPageOnPageChangeListener());
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
    }

    private class TextOnClickListener implements View.OnClickListener {
        private int index = 0;

        public TextOnClickListener(int i) {
            index = i;
        }

        public void onClick(View v) {
            viewPager.setCurrentItem(index);
        }
    }
    public class ViewPageOnPageChangeListener implements ViewPager.OnPageChangeListener {
        public void onPageScrollStateChanged(int arg0) {
        }

        public void onPageScrolled(int arg0, float arg1, int arg2) {
        }

        public void onPageSelected(int arg0) {
            currIndex = arg0;
            int i = currIndex + 1;
            setTabTitle(i);
        }
    }
    /**
     * 设置选项卡title的字体颜色
     *
     * @param tabId
     */
    public void setTabTitle(int tabId) {
        //viewpager顶部导航游标，并设置游标显示位置
        if (tabId == 1) {
            imageView1.setVisibility(View.VISIBLE);
            imageView2.setVisibility(View.INVISIBLE);
            imageView3.setVisibility(View.INVISIBLE);
            imageView4.setVisibility(View.INVISIBLE);
        } else if (tabId == 2) {
            imageView1.setVisibility(View.INVISIBLE);
            imageView2.setVisibility(View.VISIBLE);
            imageView3.setVisibility(View.INVISIBLE);
            imageView4.setVisibility(View.INVISIBLE);
        } else if (tabId == 3) {
            imageView1.setVisibility(View.INVISIBLE);
            imageView2.setVisibility(View.INVISIBLE);
            imageView3.setVisibility(View.VISIBLE);
            imageView4.setVisibility(View.INVISIBLE);
        } else if (tabId == 4) {
            imageView1.setVisibility(View.INVISIBLE);
            imageView2.setVisibility(View.INVISIBLE);
            imageView3.setVisibility(View.INVISIBLE);
            imageView4.setVisibility(View.VISIBLE);
        }
        //viewpager顶部文字导航，并设置文字颜色
        Resources resource = (Resources) getResources();
        ColorStateList selectedTextColor = (ColorStateList) resource.getColorStateList(R.color.subcollect_fragment_selecttext_color);
        ColorStateList unselectedTextColor = (ColorStateList) resource.getColorStateList(R.color.subcollect_fragment_text_color);
        if (tabId == 1) {
            expertText.setTextColor(selectedTextColor);
            forumText.setTextColor(unselectedTextColor);
            storeText.setTextColor(unselectedTextColor);
            productText.setTextColor(unselectedTextColor);
        } else if (tabId == 2) {
            expertText.setTextColor(unselectedTextColor);
            forumText.setTextColor(selectedTextColor);
            storeText.setTextColor(unselectedTextColor);
            productText.setTextColor(unselectedTextColor);
        } else if (tabId == 3) {
            expertText.setTextColor(unselectedTextColor);
            forumText.setTextColor(unselectedTextColor);
            storeText.setTextColor(selectedTextColor);
            productText.setTextColor(unselectedTextColor);
        } else if (tabId == 4) {
            expertText.setTextColor(unselectedTextColor);
            forumText.setTextColor(unselectedTextColor);
            storeText.setTextColor(unselectedTextColor);
            productText.setTextColor(selectedTextColor);
        }
    }
}
