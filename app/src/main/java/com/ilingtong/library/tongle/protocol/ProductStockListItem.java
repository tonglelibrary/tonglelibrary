package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * Created by liuting on 2015/12/21.
 */
public class ProductStockListItem implements Serializable {
    public String spec_detail_id1;//规格1明细ID
    public String spec_detail_id2;//规格2明细ID
    public String spec_stock_qty;//剩余库存
    public String spec_price;//规格价格
}
