package com.ilingtong.library.tongle.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.activity.CollectMStoreDetailActivity;
import com.ilingtong.library.tongle.activity.SearchActivity;
import com.ilingtong.library.tongle.adapter.MStoreListAdapter;
import com.ilingtong.library.tongle.external.maxwin.view.XListView;
import com.ilingtong.library.tongle.protocol.MStoreListItem;
import com.ilingtong.library.tongle.protocol.MStoreRequestParam;
import com.ilingtong.library.tongle.protocol.MStoreResult;
import com.ilingtong.library.tongle.utils.ToastUtils;

import java.util.ArrayList;

/**
 * Created by wuqian on 2015/10/29.
 * mail: wuqian@ilingtong.com
 * Description:收藏 -- M店
 */
public class MStoreFragment extends LazyFragment implements XListView.IXListViewListener, SearchActivity.FlushDataListener {
    private XListView mstoreListView;
    private MStoreListAdapter mstoreListAdaper;
    private MStoreRequestParam mStoreRequestParam;
    private ArrayList<MStoreListItem> mstore_list = new ArrayList<>();
    private boolean flag = true;
    private int intoType = 0;

    public static boolean UPDATE_LIST_FLAG = false; //是否刷新列表的标准
    public static boolean MSTOREFRAGMENT_UPDATE_FLAG = false; //进入页面刷新标志。为ture时表示刷新，反之不刷新
    private int listIndex = -1; //表示是从 position位置跳转到M客详情的
    // 标志位，标志已经初始化完成。
    private boolean isPrepared;
    Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    mstoreListView.setRefreshTime();
                    mstoreListAdaper.notifyDataSetChanged();
                    break;
            }
        }
    };

    /**
     * 静态工厂方法需要一个int型的值来初始化fragment的参数，然后返回新的fragment到调用者
     *
     * @param intoType 在哪个页面创建
     * @return
     */
    public static MStoreFragment newInstance(int intoType, ArrayList<MStoreListItem> mstore_list) {
        MStoreFragment fragment = new MStoreFragment();
        Bundle args = new Bundle();
        args.putInt(TongleAppConst.INTO_TYPE, intoType);
        args.putSerializable("list", mstore_list);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.collect_mstore_layout, null);
        initView(view);
        isPrepared = true;
        doRequest();
        return view;
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
    }

    /**
     * 初始化view
     *
     * @param view
     */
    private void initView(View view) {
        intoType = getArguments().getInt(TongleAppConst.INTO_TYPE);
        mstore_list.clear();
        if (getArguments().get("list") != null) {
            mstore_list = (ArrayList<MStoreListItem>) getArguments().get("list");
        }
        mstoreListView = (XListView) view.findViewById(R.id.mstore_listview);
        mstoreListAdaper = new MStoreListAdapter(getActivity(), mstore_list);
        mstoreListView.setAdapter(mstoreListAdaper);
        mstoreListView.setXListViewListener(this, 0);
        mstoreListView.setPullLoadEnable(false);
        //点击列表Item，进去魔店详情
        mstoreListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getActivity(), CollectMStoreDetailActivity.class);
                intent.putExtra("mstore_id", mstore_list.get(position - 1).mstore_id);
                intent.putExtra(TongleAppConst.INTO_TYPE, intoType);
                listIndex = position;
                startActivityForResult(intent, 10001);
            }
        });
    }

    /**
     * 点击M客列表进入详情后，返回的回调。如果在详情页面有取消关注，回到该页面时刷新列表。没有则不刷新
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (intoType == TongleAppConst.COLLECTFRAGMENT_INTO && requestCode == 10001 && UPDATE_LIST_FLAG) {
            if (listIndex > 0) {
                mstore_list.remove(listIndex - 1);
                UPDATE_LIST_FLAG = false;
                mHandler.sendEmptyMessage(0);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * 调用接口，请求数据
     */
    private void doRequest() {
        mStoreRequestParam = new MStoreRequestParam();
        mStoreRequestParam.user_id = TongleAppInstance.getInstance().getUserID();
        mStoreRequestParam.fetch_count = TongleAppConst.FETCH_COUNT;
        mStoreRequestParam.forward = "1";
        if (intoType == TongleAppConst.COLLECTFRAGMENT_INTO) { //收藏
            ServiceManager.doMStoreRequest(mStoreRequestParam, mstoreSuccessListener(), errorListener());
        } else if (intoType == TongleAppConst.FINDFRAGMENT_INTO) { //发现热门
            mstoreListView.setPullRefreshEnable(false);
            mstoreListView.setPullLoadEnable(false);
        } else if (intoType == TongleAppConst.SEARCH_INTO) {  //关键字搜索
            mstoreListView.setPullRefreshEnable(false);
            mstoreListView.setPullLoadEnable(false);
        }
    }

    /**
     * 刷新
     *
     * @param id
     */
    @Override
    public void onRefresh(int id) {
        //刷新之前，清空list
        if (mstore_list != null) {
            mstore_list.clear();
        }
        MSTOREFRAGMENT_UPDATE_FLAG = false;
        UPDATE_LIST_FLAG = false;
        ServiceManager.doMStoreRequest(mStoreRequestParam, mstoreSuccessListener(), errorListener());
    }

    /**
     * 加载更多
     *
     * @param id
     */
    @Override
    public void onLoadMore(int id) {
        //加载更多
        if (flag) {
            //第二个参数prod_id是list里最后一个参数的id
            mStoreRequestParam = new MStoreRequestParam();
            mStoreRequestParam.user_id = TongleAppInstance.getInstance().getUserID();
            mStoreRequestParam.mstore_id = mstore_list.get(mstore_list.size() - 1).mstore_id;
            mStoreRequestParam.fetch_count = TongleAppConst.FETCH_COUNT;
            mStoreRequestParam.forward = "1";
            ServiceManager.doMStoreRequest(mStoreRequestParam, mstoreSuccessListener(), errorListener());
        } else {
            ToastUtils.toastShort(getString(R.string.common_list_end));
            mstoreListView.setPullLoadEnable(false);
        }
    }
    /**
     * 功能：请求魔店网络响应成功，返回数据 收藏
     */
    private Response.Listener<MStoreResult> mstoreSuccessListener() {
        return new Response.Listener<MStoreResult>() {
            @Override
            public void onResponse(MStoreResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    mstore_list.addAll(response.getBody().getMStoreList());
                    //第一次请求或刷新页面时候判断是否隐藏底部加载更多按钮
                    if (Integer.parseInt(response.getBody().getData_total_count()) > mstore_list.size()) {
                        flag = true;
                        mstoreListView.setPullLoadEnable(true);
                    } else {
                        flag = false;
                        mstoreListView.setPullLoadEnable(false);
                    }
                } else {
                    ToastUtils.toastShort(getString(R.string.para_exception) + response.getHead().getReturn_message());
                }
                //调用handler，发送消息
                mHandler.sendEmptyMessage(0);
            }
        };
    }

    /**
     * 功能：网络响应失败
     */
    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ToastUtils.toastShort(getString(R.string.sys_exception));
            }
        };
    }

    @Override
    public void flush(Object data) {
        if (mstoreListAdaper != null) {
            mstore_list = (ArrayList<MStoreListItem>) data;
            mstoreListAdaper.notifyDataSetChanged();
        }
    }

    @Override
    protected void lazyLoad() {
        if (!isPrepared || !isVisible) {
            return;
        }
        if (intoType == TongleAppConst.COLLECTFRAGMENT_INTO){
            if (MSTOREFRAGMENT_UPDATE_FLAG || UPDATE_LIST_FLAG){
                onRefresh(0);
            }
        }
    }
}
