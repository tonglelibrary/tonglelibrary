package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: cuishuang
 * Date: 2015/7/11
 * Time: 13:30
 * Email: shuangcui@isoftstone.com
 * Desc: 商品转发类，通过该对象，被用作网络请求参数
 */
public class ProdForwardResult  implements Serializable {
    private BaseInfo head;
    private ForwordDataInfo body;

    public BaseInfo getHead() {
        return head;
    }

    public void setHead(BaseInfo head) {
        this.head = head;
    }

    public ForwordDataInfo getBody() {
        return body;
    }

    public void setBody(ForwordDataInfo body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return body.toString();
    }
}
