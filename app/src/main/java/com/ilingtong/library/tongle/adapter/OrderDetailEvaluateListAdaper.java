package com.ilingtong.library.tongle.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.activity.EvaluateActivity;
import com.ilingtong.library.tongle.activity.OrderDetailProdEvaluateActivity;
import com.ilingtong.library.tongle.protocol.OrderDetailOrderDetailInfo;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

/**
 * User: shuailei
 * Date: 2015/6/17
 * Time: 10:45
 * Email: leishuai@isoftstone.com
 * Dest:订单详情中评价订单的适配器
 */
public class OrderDetailEvaluateListAdaper extends BaseAdapter {
    private LayoutInflater inflater;
    private List<OrderDetailOrderDetailInfo> list;
    Activity mContext;
    public static String order_detail_no, prod_id;
    String myOrder;
    View inflate;

    public OrderDetailEvaluateListAdaper(Activity context, List list, String order) {
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mContext = context;
        this.myOrder = order;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        final ViewHolder viewholder;
        //对view进行判空，如果为空则创建新的view
        if (view == null) {
            view = inflater.inflate(R.layout.orderdetail_evaluate_item, null);
            viewholder = new ViewHolder();
            //初始化控件
            viewholder.listItemImageView = (ImageView) view.findViewById(R.id.order_list_item_image);
            viewholder.desc = (TextView) view.findViewById(R.id.order_list_item_desc);
            viewholder.color = (TextView) view.findViewById(R.id.order_list_item_color);
            viewholder.evaluateorder_button = (Button) view.findViewById(R.id.evaluateorder_button);
            view.setTag(viewholder);
        } else {
            //如果不为空，直接调用，不需创建
            viewholder = (ViewHolder) view.getTag();
        }
        final OrderDetailOrderDetailInfo dataItem = list.get(position);

        //给属性填充数据
        ImageLoader.getInstance().displayImage(dataItem.prod_pic_url, viewholder.listItemImageView, TongleAppInstance.options);
        viewholder.desc.setText(dataItem.prod_name);
        order_detail_no = dataItem.order_detail_no;
        prod_id = dataItem.prod_id;
        StringBuffer spec = new StringBuffer();
        if (dataItem.prod_spec_list != null && dataItem.prod_spec_list.size() > 0) {
            for (int i = 0; i < dataItem.prod_spec_list.size(); i++) {
                spec.append(dataItem.prod_spec_list.get(i).prod_spec_name + ":" + dataItem.prod_spec_list.get(i).spec_detail_name + ";  ");
            }
        }
        viewholder.color.setText(spec);
        viewholder.evaluateorder_button.setText(dataItem.goods_eva_flag.equals(TongleAppConst.YES) ? R.string.order_detail_prod_evaluate_top_name : R.string.order_detail_prod_evaluate_complete);
        viewholder.evaluateorder_button.setEnabled(dataItem.goods_eva_flag.equals(TongleAppConst.YES) ? true : false);

        //每一条listview的item的点击事件，并把product_id传递过去
        /************此处去商品详情逻辑流程有缺陷，先保留***********/
//        view.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View arg0) {
//                Intent detailIntent = new Intent(mContext, CollectProductDetailActivity.class);
//                Bundle bundle = new Bundle();
//                bundle.putString("product_id", dataItem.prod_id);
//                detailIntent.putExtras(bundle);
//                mContext.startActivity(detailIntent);
//            }
//        });

        //评价订单的按钮点击事件
        viewholder.evaluateorder_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EvaluateActivity.luanchForResult(mContext, myOrder, myOrder, list.get(position).prod_id, OrderDetailProdEvaluateActivity.EVALUATE_REQUESTCODE);
            }
        });
        return view;
    }

    //做一个有关缓存的ViewHolder
    static class ViewHolder {
        TextView desc, color;
        ImageView listItemImageView;
        Button evaluateorder_button;
    }
}
