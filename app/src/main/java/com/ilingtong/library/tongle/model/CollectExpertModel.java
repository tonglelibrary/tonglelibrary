package com.ilingtong.library.tongle.model;

import com.ilingtong.library.tongle.protocol.FriendListItem;

import java.util.ArrayList;

/**
 * User: lengjiqiang
 * Date: 2015/6/4
 * Time: 12:02
 * Email: jqleng@isoftstone.com
 * Desc:
 */
public class CollectExpertModel {
    public  String collect_expert_data_total_count;
    public  ArrayList<FriendListItem> friend_list;
}
