package com.ilingtong.library.tongle.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.activity.EvaluateActivity;
import com.ilingtong.library.tongle.activity.TicketOrderDetailActivity;
import com.ilingtong.library.tongle.activity.TicketOrderSubmitActivity;
import com.ilingtong.library.tongle.fragment.GroupOrderListFragment;
import com.ilingtong.library.tongle.protocol.CouponOrderBaseInfo;
import com.ilingtong.library.tongle.protocol.ProdSpecListInfo;
import com.ilingtong.library.tongle.protocol.TicketOrderSubmitParam;
import com.ilingtong.library.tongle.utils.FontUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * author: liuting
 * Date: 2016/3/22
 * Time: 9:22
 * Email: liuting@ilingtong.com
 * Desc:团购订单列表Adapter
 */
public class GroupOrderListAdapter extends BaseAdapter {
    private LayoutInflater mInflater;
    private List<CouponOrderBaseInfo> mLvOrder;
    private Context mContext;
    private Fragment fragment;

    public GroupOrderListAdapter(Context context, List<CouponOrderBaseInfo> order_list, Fragment fragment) {
        mContext = context;
        this.mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mLvOrder = order_list;
        this.fragment = fragment;
    }

    @Override
    public int getCount() {
        return mLvOrder.size();
    }

    @Override
    public Object getItem(int position) {
        return mLvOrder.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder holder;
        if (view == null) {
            view = mInflater.inflate(R.layout.group_order_list_item, null);
            holder = new ViewHolder();
            holder.mImgPic = (ImageView) view.findViewById(R.id.group_order_img_icon);
            holder.mProdName = (TextView) view.findViewById(R.id.group_order_tv_name);
            holder.mOrderQty = (TextView) view.findViewById(R.id.group_order_tv_order_qty);
            holder.mAmountPrice = (TextView) view.findViewById(R.id.group_order_tv_amount_price);
            holder.mUnusedQty = (TextView) view.findViewById(R.id.group_order_tv_unused_qty);
            holder.mBtnPay = (TextView) view.findViewById(R.id.group_order_btn_pay);
            holder.mLlyMain = (LinearLayout) view.findViewById(R.id.group_order_lly_main);
            holder.mImgNoPay = (ImageView) view.findViewById(R.id.group_order_img_no_pay);
            holder.expireQty = (TextView) view.findViewById(R.id.group_order_tv_expire_qty);
            holder.refundingQty = (TextView) view.findViewById(R.id.group_order_tv_refunding_qty);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        final CouponOrderBaseInfo mOrderItem = (CouponOrderBaseInfo) getItem(position);
        com.nostra13.universalimageloader.core.ImageLoader.getInstance().displayImage(mOrderItem.getProd_pic_url_list().get(0).pic_url, holder.mImgPic, TongleAppInstance.options);
        holder.mProdName.setText(mOrderItem.getProd_name());
        holder.mOrderQty.setText(String.format(mContext.getResources().getString(R.string.group_order_order_qty_txt), mOrderItem.getOrder_qty()));
        holder.mAmountPrice.setText(String.format(mContext.getResources().getString(R.string.group_order_amount_price_txt), FontUtils.setTwoDecimal(mOrderItem.getOrder_amount())));
        holder.mUnusedQty.setText(String.format(mContext.getResources().getString(R.string.group_order_unused_qty_txt), mOrderItem.getUnused_qty()));
        holder.expireQty.setText(String.format(mContext.getResources().getString(R.string.group_order_expire_qty_txt), mOrderItem.getExpire_qty()));
        holder.refundingQty.setText(String.format(mContext.getResources().getString(R.string.group_order_refunding_qty_txt), mOrderItem.getRefunding_qty()));

        holder.expireQty.setVisibility(mOrderItem.getExpire_qty() > 0 ? View.VISIBLE : View.GONE);
        holder.refundingQty.setVisibility(mOrderItem.getRefunding_qty() > 0 ? View.VISIBLE : View.GONE);

        if (mOrderItem.getOrder_pay_info().getPay_status().equals(TongleAppConst.FLAG_NO_PAY)) {
            holder.mBtnPay.setVisibility(View.VISIBLE);//未支付状态时显示立即支付按钮
            holder.mBtnPay.setText(mContext.getString(R.string.common_order_pay_now));
            holder.mImgNoPay.setVisibility(View.VISIBLE);//显示未支付图标
            /*********************去提交订单页*****************/
            holder.mBtnPay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    toPay(mOrderItem);
                }
            });
            /*********************去提交订单页*****************/
            holder.mLlyMain.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    toPay(mOrderItem);
                }
            });
        } else if (TongleAppConst.YES.equals(mOrderItem.getEvaluate_flag())) {
            holder.mImgNoPay.setVisibility(View.GONE);
            holder.mBtnPay.setText(mContext.getString(R.string.ticket_order_detail_top_evaluate));
            holder.mBtnPay.setVisibility(View.VISIBLE);
            /*********************弹出评价框*************/
            holder.mBtnPay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EvaluateActivity.luanchForResult(fragment, mOrderItem.getOrder_no(), mOrderItem.getOrder_no(), mOrderItem.getProd_id(), GroupOrderListFragment.REQUESTCODE_TO_EVALUATE);
                }
            });
            //******************************去订单详情***********************************/
            holder.mLlyMain.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    TicketOrderDetailActivity.launcher(fragment, mOrderItem.getOrder_no(), GroupOrderListFragment.REQUESTCODE_TO_ORDER);
                }
            });
        } else {//已经支付了，则不显示立即支付按钮
            holder.mBtnPay.setVisibility(View.GONE);
            holder.mImgNoPay.setVisibility(View.GONE);
            //******************************去订单详情***********************************/
            holder.mLlyMain.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    TicketOrderDetailActivity.launcher(fragment, mOrderItem.getOrder_no(), GroupOrderListFragment.REQUESTCODE_TO_ORDER);
                }
            });
        }
        return view;
    }

    /**
     * 去提交订单页面
     *
     * @param mOrderItem
     */
    private void toPay(CouponOrderBaseInfo mOrderItem) {
        TicketOrderSubmitParam param = new TicketOrderSubmitParam();
        param.mstore_id = "";
        param.post_id = "";
        param.product_id = mOrderItem.getProd_id();
        param.relation_id = mOrderItem.getRelation_id();//二维码关联ID
        param.price = mOrderItem.getOrder_price() + "";//商品价格
        param.product_name = mOrderItem.getProd_name();//商品名称
        param.order_qty = mOrderItem.getOrder_qty() + "";//购买数量
        param.order_no = mOrderItem.getOrder_no();//订单编号
        param.prod_spec_list = new ArrayList<ProdSpecListInfo>();
        param.prod_spec_list.addAll(mOrderItem.getProd_spec_list());//规格列表
        TicketOrderSubmitActivity.launcher(fragment, param, GroupOrderListFragment.REQUESTCODE_TO_PAY,mOrderItem.getVoucher_base());
    }

    static class ViewHolder {
        ImageView mImgPic;//商品图片
        TextView mProdName;//商品名称
        TextView mOrderQty;//购买件数
        TextView mAmountPrice;//总金额
        TextView mUnusedQty;//可用个数
        TextView expireQty;  //过期数量
        TextView refundingQty;  //退款中数量
        TextView mBtnPay;//立即支付
        LinearLayout mLlyMain;//主布局
        ImageView mImgNoPay;//未支付图标
    }
}
