package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: shuailei
 * Date: 2015/6/24
 * Time: 22:05
 * Email: leishuai@isoftstone.com
 * Desc: 配送信息类型
 */
public class ShippingMethodInfo implements Serializable {
    //配送方式编号
    public String shipping_method_no;
    //配送时间
    public String shipping_time_memo;
}
