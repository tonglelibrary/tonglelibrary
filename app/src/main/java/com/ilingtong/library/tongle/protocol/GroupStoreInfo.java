package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/3/11.
 * mail: wuqian@ilingtong.com
 * Description:6008接口 body
 */
public class GroupStoreInfo implements Serializable{

    private CouponStoreListInfo coupon_store_info;  //商户信息

    public CouponStoreListInfo getCoupon_store_info() {
        return coupon_store_info;
    }

    public void setCoupon_store_info(CouponStoreListInfo coupon_store_info) {
        this.coupon_store_info = coupon_store_info;
    }
}
