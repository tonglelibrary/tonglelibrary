package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * User: shuailei
 * Date: 2015/6/26
 * Time: 14:18
 * Email: leishuai@isoftstone.com
 * Desc:
 */
public class ProdCommentListInfo implements Serializable {
    //output parameters
    public ArrayList<ProductRatingListItem> product_rating_list;

    @Override
    public String toString() {
        return  "product_rating_list:" + product_rating_list;
    }


}
