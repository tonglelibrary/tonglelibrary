package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;
import java.util.List;

/**
 * Created by wuqian on 2016/3/11.
 * mail: wuqian@ilingtong.com
 * Description:6011接口body 团购券详情信息
 */
public class CouponDetailInfo implements Serializable {
    private CouponDetailProdInfo prod_base_info;  //商品信息
    private CouponCodeurlInfo coupon_codeurl_info;        //商品识别码信息
    private CouponStoreListInfo coupon_store_info;    //适用商户信息
    private CouponPurchaseInfo coupon_purchase_info;    //团购信息详情信息
    private List<CouponPurchaseNoticeInfo> coupon_purchase_notice_list;   //购买须知列表

    public CouponDetailProdInfo getProd_base_info() {
        return prod_base_info;
    }

    public void setProd_base_info(CouponDetailProdInfo prod_base_info) {
        this.prod_base_info = prod_base_info;
    }

    public CouponCodeurlInfo getCoupon_codeurl_info() {
        return coupon_codeurl_info;
    }

    public void setCoupon_codeurl_info(CouponCodeurlInfo coupon_codeurl_info) {
        this.coupon_codeurl_info = coupon_codeurl_info;
    }

    public CouponStoreListInfo getCoupon_store_info() {
        return coupon_store_info;
    }

    public void setCoupon_store_info(CouponStoreListInfo coupon_store_info) {
        this.coupon_store_info = coupon_store_info;
    }

    public CouponPurchaseInfo getCoupon_purchase_info() {
        return coupon_purchase_info;
    }

    public void setCoupon_purchase_info(CouponPurchaseInfo coupon_purchase_info) {
        this.coupon_purchase_info = coupon_purchase_info;
    }

    public List<CouponPurchaseNoticeInfo> getCoupon_purchase_notice_list() {
        return coupon_purchase_notice_list;
    }

    public void setCoupon_purchase_notice_list(List<CouponPurchaseNoticeInfo> coupon_purchase_notice_list) {
        this.coupon_purchase_notice_list = coupon_purchase_notice_list;
    }
}
