package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * author: liuting
 * Date: 2016/3/17
 * Time: 16:57
 * Email: liuting@ilingtong.com
 * Desc:从商品详情传递到提交订单的参数类
 */
public class TicketOrderSubmitParam implements Serializable {
    public String product_id;//商品ID
    public String price;//商品单价
    public String relation_id;//关联ID
    public String mstore_id;//魔店ID
    public String post_id;//帖子ID
    public String product_name;//商品名称
    public String order_qty;//购买数量
    public String order_no;//订单编号
    public ArrayList<ProdSpecListInfo> prod_spec_list;   //规格列表
}
