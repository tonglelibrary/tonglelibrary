package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/6/3
 * Time: 16:52
 * Email: jqleng@isoftstone.com
 * Dest:
 */
public class ForumResult implements Serializable {
    private BaseInfo head;
    private ForumInfo body;

    public BaseInfo getHead() {
        return head;
    }

    public void setHead(BaseInfo head) {
        this.head = head;
    }

    public ForumInfo getBody() {
        return body;
    }

    public void setBody(ForumInfo body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return body.toString();
    }
}
