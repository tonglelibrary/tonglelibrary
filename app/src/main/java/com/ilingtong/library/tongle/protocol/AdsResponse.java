package com.ilingtong.library.tongle.protocol;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * User: lengjiqiang
 * Date: 2015/5/7
 * Time: 13:42
 * Email: jqleng@isoftstone.com
 * Dest:
 */
public class AdsResponse implements Serializable {
    public ArrayList<String> strAdsUrl = new ArrayList<String>();
    public int retVal;

    public void fromJson(JSONObject jsonObject) throws JSONException {
        if (null == jsonObject) {
            return;
        }
        JSONObject headObj = jsonObject.getJSONObject("head");
        JSONObject bodyObj = jsonObject.getJSONObject("body");
        retVal = headObj.getInt("return_flag");
        JSONArray adsArray = bodyObj.getJSONArray("top_promotion_list");
        for (int i = 0; i < adsArray.length(); i++) {
            JSONObject jsObj = adsArray.getJSONObject(i);
            strAdsUrl.add(i, jsObj.getString("mobile_pic_url"));
        }
    }
}
