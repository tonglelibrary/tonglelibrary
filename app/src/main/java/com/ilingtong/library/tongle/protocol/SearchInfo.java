package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * User: syc
 * Date: 2015/6/24
 * Time: 14:18
 * Email: ycshi@isoftstone.com
 * Desc: 【1090】
 */
public class SearchInfo implements Serializable {


    private ArrayList<MStoreListItem> mstore_list;
    private ArrayList<FriendListItem> user_list;
    private ArrayList<UserFollowPostList> post_list;
    private ArrayList<ProductListItemData> prod_list;


    public ArrayList<MStoreListItem> getMstore_list() {
        return mstore_list;
    }

    public ArrayList<FriendListItem> getUser_list() {
        return user_list;
    }

    public ArrayList<UserFollowPostList> getPost_list() {
        return post_list;
    }

    public ArrayList<ProductListItemData> getProd_list() {
        return prod_list;
    }


    @Override
    public String toString() {
        return "mstore_list:" + mstore_list + "\r\n" +
                        "user_list:" + user_list + "\r\n" +
                        "post_list:" + post_list + "\r\n" +
                        "prod_list:" + prod_list;
    }


}
