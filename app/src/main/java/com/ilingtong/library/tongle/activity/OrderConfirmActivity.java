package com.ilingtong.library.tongle.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.adapter.ConfirmOrderCartListAdapter;
import com.ilingtong.library.tongle.fragment.CartFragment;
import com.ilingtong.library.tongle.model.AddressModel;
import com.ilingtong.library.tongle.model.CartModel;
import com.ilingtong.library.tongle.model.ConfirmOrderModel;
import com.ilingtong.library.tongle.protocol.AddressListItem;
import com.ilingtong.library.tongle.protocol.AddressResult;
import com.ilingtong.library.tongle.protocol.BaseDataListItem;
import com.ilingtong.library.tongle.protocol.BaseDataResult;
import com.ilingtong.library.tongle.protocol.CartSettlementResult;
import com.ilingtong.library.tongle.protocol.DataListItem;
import com.ilingtong.library.tongle.protocol.InvoiceInfo;
import com.ilingtong.library.tongle.protocol.MyCouponAvailableItem;
import com.ilingtong.library.tongle.protocol.MyCouponBaseInfo;
import com.ilingtong.library.tongle.protocol.OrderInfoResult;
import com.ilingtong.library.tongle.protocol.OrderJSONInfo;
import com.ilingtong.library.tongle.protocol.ProductListInfo;
import com.ilingtong.library.tongle.protocol.ShippingMethodInfo;
import com.ilingtong.library.tongle.utils.ToastUtils;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * User: lengjiqiang
 * Date: 2015/6/7
 * Time: 22:41
 * Email: jqleng@isoftstone.com
 * Desc: 确认订单 页
 */
public class OrderConfirmActivity extends BaseActivity implements View.OnClickListener {
    private TextView top_name;
    private ImageView left_arrow_btn;
    private ListView listview;
    ConfirmOrderCartListAdapter listAdapter;
    final private String DEFAULT_ADDRESS = "0";
    View headview;
    View footview;
    //  LinearLayout payModeLinear;
    // LinearLayout billTypeLinear, isneed_bill, bill_linear;
    // LinearLayout billContentLinear;
    LinearLayout headview_linear;
    LinearLayout deliveryModeLine;
    LinearLayout deliveryTimeLine;
    TextView totalPriceTxt;  //合计
    TextView tv_addaddress;
    TextView deliveryTimeTxt;
    TextView deliveryModeTxt, guanshui_pay, yunfei_pay, all_pay, pro_totle, yunfei_desc;
    TextView toNameTxt;
    TextView toNumberTxt;
    TextView toAddrTxt;
    Button commitBtn;
    RadioButton rbtn1, rbtn2;
    String shipping_method_no;
    String shipping_time_memo;
    private ArrayList<ProductListInfo> product_list;
    private String address_no;
    EditText order_memo;
    //  String pay_type;
    String invoice_content_flag, invoice_type_no;
    ArrayList<DataListItem> invoiceList;
    ArrayList<DataListItem> invoicecontentList;
    RadioButton btn;
    private ProgressDialog pDlg;
    private String isNeed;
    CartSettlementResult response;

    private TextView txt_coupon;  //抵用券
    private TextView txt_selected_coupon;  //选择的优惠券名称
    private LinearLayout delivery_coupon_linear;  //去选择优惠券

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listview_comm_layout);
        initView();
    }

    public void initView() {
        response = (CartSettlementResult) getIntent().getSerializableExtra("response");

        ConfirmOrderModel.checkDeliveryMode = new DataListItem();
        ConfirmOrderModel.checkDeliveryTime = new DataListItem();
        //创建并弹出“加载中Dialog”
        pDlg = new ProgressDialog(this);
        pDlg.setCancelable(false);
        pDlg.setMessage(getString(R.string.loading));
        pDlg.show();
        top_name = (TextView) findViewById(R.id.top_name);
        left_arrow_btn = (ImageView) findViewById(R.id.left_arrow_btn);
        listview = (ListView) findViewById(R.id.listview_comm);
        left_arrow_btn.setOnClickListener(this);
        left_arrow_btn.setVisibility(View.VISIBLE);
        top_name.setVisibility(View.VISIBLE);
        top_name.setText(R.string.confirm_order);

        headview = LayoutInflater.from(this).inflate(R.layout.confirm_order_headview, null);
        listview.addHeaderView(headview);

        headview_linear = (LinearLayout) findViewById(R.id.headview_linear);
        tv_addaddress = (TextView) findViewById(R.id.tv_addaddress);
        headview_linear.setOnClickListener(this);
        tv_addaddress.setOnClickListener(this);

        footview = LayoutInflater.from(this).inflate(R.layout.confirm_order_footview, null);
        deliveryTimeTxt = (TextView) footview.findViewById(R.id.delivery_time);
        deliveryModeTxt = (TextView) footview.findViewById(R.id.delivery_mode);
        guanshui_pay = (TextView) footview.findViewById(R.id.guanshui_pay);
        yunfei_pay = (TextView) footview.findViewById(R.id.yunfei_pay);
        all_pay = (TextView) footview.findViewById(R.id.all_pay);
        pro_totle = (TextView) footview.findViewById(R.id.pro_totle);
        yunfei_desc = (TextView) footview.findViewById(R.id.yunfei_desc);
        txt_coupon = (TextView) footview.findViewById(R.id.confirm_order_footview_txt_coupon);  //结算区域 优惠券金额
        txt_selected_coupon = (TextView) footview.findViewById(R.id.delivery_coupon);  //选择的优惠券名称
        delivery_coupon_linear = (LinearLayout) footview.findViewById(R.id.delivery_coupon_linear);  //去选择优惠券
        delivery_coupon_linear.setOnClickListener(this);
        commitBtn = (Button) footview.findViewById(R.id.commit_order_btn);
        commitBtn.setOnClickListener(this);
        listview.addFooterView(footview);

//        payModeLinear = (LinearLayout) findViewById(R.id.pay_mode_linear);
//        billTypeLinear = (LinearLayout) findViewById(R.id.bill_type);
//        bill_linear = (LinearLayout) findViewById(R.id.bill_linear);
//        isneed_bill = (LinearLayout) findViewById(R.id.isneed_bill);
//        invoice_title = (EditText) findViewById(R.id.editText3);
        order_memo = (EditText) findViewById(R.id.editText2);
        //  billContentLinear = (LinearLayout) findViewById(R.id.bill_content);
        deliveryModeLine = (LinearLayout) footview.findViewById(R.id.delivery_mode_linear);
        deliveryModeLine.setOnClickListener(this);
        deliveryTimeLine = (LinearLayout) footview.findViewById(R.id.delivery_time_linear);
        deliveryTimeLine.setOnClickListener(this);

        totalPriceTxt = (TextView) findViewById(R.id.total_price_num);
        toNameTxt = (TextView) headview.findViewById(R.id.to_name);
        toNumberTxt = (TextView) headview.findViewById(R.id.to_number);
        toAddrTxt = (TextView) headview.findViewById(R.id.to_address);

        listAdapter = new ConfirmOrderCartListAdapter(this, CartModel.checkList);
        //确认订单时商品列表
        product_list = new ArrayList<ProductListInfo>();
        for (int i = 0; i < CartModel.checkList.size(); i++) {
            ProductListInfo listinfo = new ProductListInfo();
            listinfo.seq_no = CartModel.checkList.get(i).seq_no;
            listinfo.product_id = CartModel.checkList.get(i).prod_id;
            product_list.add(listinfo);
        }

        //选择的优惠券的名称
        if (!TextUtils.isEmpty(response.getBody().vouchers_name)) {
            txt_selected_coupon.setText(response.getBody().vouchers_name);
        }

        //运费
        yunfei_pay.setText(getString(R.string.RMB) + response.getBody().total_fee);
        float yunfei = Float.parseFloat(response.getBody().total_fee);
        if (yunfei > 0) {
            yunfei_desc.setVisibility(View.GONE);
        } else {
            yunfei_desc.setVisibility(View.VISIBLE);
        }
        //关税
        guanshui_pay.setText(getString(R.string.RMB) + response.getBody().total_tariff);
        //商品总额
        pro_totle.setText(getString(R.string.RMB) + response.getBody().total_goods_price);

        //应付金额 （商品总额加关税加运费）
        all_pay.setText(String.format(getResources().getString(R.string.confirm_order_footview_order_all_pay),response.getBody().pay_amount));

        //抵用券
        txt_coupon.setText(String.format(getResources().getString(R.string.confirm_order_footview_coupon_txt_money), response.getBody().money));
        //合计
        totalPriceTxt.setText(response.getBody().total_amount);

        ServiceManager.doAddressRequest(TongleAppInstance.getInstance().getUserID(), addrListener(), errorListener());
        if (ConfirmOrderModel.base_data_list == null) {
            ServiceManager.doBaseDataRequest("", baseListener(), errorListener());
        } else {
            updateControl();
        }
        listview.setAdapter(listAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onPageStart("orderComfirm");
        MobclickAgent.onResume(getApplicationContext());
        ServiceManager.doAddressRequest(TongleAppInstance.getInstance().getUserID(), addrListener(), errorListener());
        if ((ConfirmOrderModel.checkDeliveryTime != null) && (ConfirmOrderModel.checkDeliveryTime.name != null)) {
            deliveryTimeTxt.setText(ConfirmOrderModel.checkDeliveryTime.name);
            shipping_time_memo = ConfirmOrderModel.checkDeliveryTime.code;
        }
        if ((ConfirmOrderModel.checkDeliveryMode != null) && (ConfirmOrderModel.checkDeliveryMode.name != null)) {
            deliveryModeTxt.setText(ConfirmOrderModel.checkDeliveryMode.name);
            shipping_method_no = ConfirmOrderModel.checkDeliveryMode.code;
        }
        if (AddressModel.checkAddress != null) {
            if (AddressModel.checkAddress.consignee == null || AddressModel.checkAddress.consignee.equals("")) {
                //这个是判断针对从别的界面返回（例如配送方式界面返回）
                if (AddressModel.my_address_list != null && AddressModel.my_address_list.size() > 0) {
                    toNameTxt.setText(AddressModel.my_address_list.get(0).consignee);
                    toNumberTxt.setText(AddressModel.my_address_list.get(0).tel);
                    toAddrTxt.setText(AddressModel.my_address_list.get(0).province_name + AddressModel.my_address_list.get(0).city_name + AddressModel.my_address_list.get(0).area_name + AddressModel.my_address_list.get(0).address);
                    address_no = AddressModel.my_address_list.get(0).address_no;
                } else {
                    tv_addaddress.setVisibility(View.VISIBLE);
                    headview_linear.setVisibility(View.GONE);
                }
            } else {
                tv_addaddress.setVisibility(View.GONE);
                headview_linear.setVisibility(View.VISIBLE);

                toNameTxt.setText(AddressModel.checkAddress.consignee);
                toNumberTxt.setText(AddressModel.checkAddress.tel);
                toAddrTxt.setText(AddressModel.checkAddress.province_name + AddressModel.checkAddress.city_name + AddressModel.checkAddress.area_name + AddressModel.checkAddress.address);
                //确认订单时地址编码
                if (AddressModel.checkAddress.address_no != null)
                    address_no = AddressModel.checkAddress.address_no;
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPageStart("productDetail");
        MobclickAgent.onPause(getApplicationContext());
    }

    /**
     * 该页中的所有点击事件处理方法
     */
    @Override
    public void onClick(View v) {
        Intent intent;
        Bundle bundle = new Bundle();
        if (v.getId() == R.id.left_arrow_btn) {
            finish();
        } else if (v.getId() == R.id.headview_linear) {
            intent = new Intent(this, AddressSelectActivity.class);
            startActivity(intent);
        } else if (v.getId() == R.id.tv_addaddress) {
            intent = new Intent(this, AddressSelectActivity.class);
            startActivity(intent);
        } else if (v.getId() == R.id.delivery_mode_linear) {
            intent = new Intent(this, OrderDeliveryModeActivity.class);
            if (ConfirmOrderModel.checkDeliveryMode.name == null) {
                //第一次进入确认订单页，
                bundle.putString("checkDeliveryMode", ConfirmOrderModel.deliveryModeList.get(0).name);
            } else {
                bundle.putString("checkDeliveryMode", ConfirmOrderModel.checkDeliveryMode.name);
            }
            intent.putExtras(bundle);
            startActivity(intent);
        } else if (v.getId() == R.id.delivery_time_linear) {
            intent = new Intent(this, OrderDeliveryTimeActivity.class);
            if (ConfirmOrderModel.checkDeliveryTime.name == null) {
                //第一次进入确认订单页，
                bundle.putString("checkDeliveryTime", ConfirmOrderModel.deliveryTimeList.get(0).name);
            } else {
                bundle.putString("checkDeliveryTime", ConfirmOrderModel.checkDeliveryTime.name);
            }
            intent.putExtras(bundle);
            startActivity(intent);
        } else if (v.getId() == R.id.commit_order_btn) {
//            if (isNeed == null) {
//                ToastUtils.toastShort("是否需要发票");
//                return;
//            }
            String invoice_type = "";//是否需要发票
            //   if (isNeed.equals("否")) {
            invoice_type_no = "";
            invoice_content_flag = "";
            invoice_type = "0";
//            } else {
//                invoice_type = "1";
//            }

            //配送信息类实例化（配送方式，配送时间）
            ShippingMethodInfo shipping_method = new ShippingMethodInfo();
            shipping_method.shipping_method_no = shipping_method_no;
            shipping_method.shipping_time_memo = shipping_time_memo;

            //发票信息实例化（发票类型编号，发票内容区分，发票抬头备注）
            InvoiceInfo invoice_info = new InvoiceInfo();
            invoice_info.invoice_type_no = invoice_type_no;
            invoice_info.invoice_content_flag = invoice_content_flag;
            invoice_info.invoice_title = "";

            OrderJSONInfo order_info_json = new OrderJSONInfo();
            order_info_json.product_list = product_list;
            order_info_json.address_no = address_no;
            order_info_json.invoice_type = invoice_type;
            order_info_json.invoice_info = invoice_info;
            order_info_json.shipping_method = shipping_method;
            order_info_json.order_memo = order_memo.getText().toString();
            order_info_json.pay_type = "3";  //默认设置为微信支付
            order_info_json.tariff = response.getBody().total_tariff;
            order_info_json.vouchers_number_id = response.getBody().vouchers_number_id;  //优惠券号

            if (address_no == null) {
                Toast.makeText(OrderConfirmActivity.this, getString(R.string.order_confirm_address_null), Toast.LENGTH_SHORT).show();
                return;
            }
//            if (pay_type == null) {
//                Toast.makeText(OrderConfirmActivity.this, getString(R.string.order_confirm_pay_type_null), Toast.LENGTH_SHORT).show();
//                return;
//            }
            if (shipping_method_no == null) {
                Toast.makeText(OrderConfirmActivity.this, getString(R.string.order_confirm_method_null), Toast.LENGTH_SHORT).show();
                return;
            }
            if (shipping_time_memo == null) {
                Toast.makeText(OrderConfirmActivity.this, getString(R.string.order_confirm_time_null), Toast.LENGTH_SHORT).show();
                return;
            }
//            if (invoice_type_no == null) {
//                Toast.makeText(OrderConfirmActivity.this, "请选择发票类型", Toast.LENGTH_SHORT).show();
//                return;
//            }
//            if (invoice_content_flag == null) {
//                Toast.makeText(OrderConfirmActivity.this, "请选择发票内容", Toast.LENGTH_SHORT).show();
//                return;
//            }
            ServiceManager.addOrderRequest(TongleAppInstance.getInstance().getUserID(), order_info_json, addorderListener(), errorListener());
            Map<String, String> map_ekv = new HashMap<String, String>();
            map_ekv.put("product", order_info_json.product_list.toString());
            map_ekv.put("totle", pro_totle.getText().toString());
            MobclickAgent.onEvent(getApplicationContext(), "comfirmOrderClick", map_ekv);
        } else if (v.getId() == R.id.delivery_coupon_linear) {  //去选择优惠券

            List<MyCouponAvailableItem> goods_info = new ArrayList<>();
            for (int i = 0; i < response.getBody().my_shopping_cart.size(); i++) {
                MyCouponAvailableItem myCouponAvailableItem = new MyCouponAvailableItem();
                myCouponAvailableItem.goods_id = response.getBody().my_shopping_cart.get(i).prod_id;
                myCouponAvailableItem.spec_id1 = response.getBody().my_shopping_cart.get(i).prod_spec_list.get(0).spec_detail_id;
                myCouponAvailableItem.buy_count = response.getBody().my_shopping_cart.get(i).order_qty;
                if (response.getBody().my_shopping_cart.get(i).prod_spec_list.size() > 1) {
                    myCouponAvailableItem.spec_id2 = response.getBody().my_shopping_cart.get(i).prod_spec_list.get(1).spec_detail_id;
                } else {
                    myCouponAvailableItem.spec_id2 = "";
                }

                goods_info.add(myCouponAvailableItem);
            }
            SelectCouponActivity.launcher(OrderConfirmActivity.this, goods_info, response.getBody().vouchers_number_id);
        }
    }

    /**
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case TongleAppConst.REQUEST_CODE_SELECT_COUPON://选择优惠券的返回值
                if (resultCode == TongleAppConst.REQUEST_OK_SELECT_COUPON) {
                    MyCouponBaseInfo mMyCouponBaseInfo = (MyCouponBaseInfo) data.getSerializableExtra("my_coupon_base_info");
                    txt_selected_coupon.setText(mMyCouponBaseInfo.getVouchers_name());
                    txt_coupon.setText(String.format(getResources().getString(R.string.confirm_order_footview_coupon_txt_money), mMyCouponBaseInfo.getMoney() + ""));
                    totalPriceTxt.setText(Double.parseDouble(response.getBody().pay_amount) - mMyCouponBaseInfo.getMoney()+"");
                    response.getBody().vouchers_number_id = mMyCouponBaseInfo.getVouchers_number_id();    //保存新的优惠券id
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            this.finish();
        }
        return false;
    }

    /**
     * 功能：请求地址列表网络响应成功，返回数据
     */
    private Response.Listener<AddressResult> addrListener() {
        return new Response.Listener<AddressResult>() {
            @Override
            public void onResponse(AddressResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    //存储地址列表到内存中
                    AddressModel.my_address_list = response.getBody().getMy_address_list();
                    //存储地址列表到数据库中，并更新控件
                    if ((AddressModel.my_address_list != null) && (AddressModel.my_address_list.size() != 0)) {
                        for (int i = 0; i < AddressModel.my_address_list.size(); i++) {
                            //判断是否有默认地址
                            if (AddressModel.my_address_list.size() > 0) {
                                tv_addaddress.setVisibility(View.GONE);
                                headview_linear.setVisibility(View.VISIBLE);
                                break;
                            } else {
                                tv_addaddress.setVisibility(View.VISIBLE);
                                headview_linear.setVisibility(View.GONE);
                            }
                        }
                        Iterator iterator = AddressModel.my_address_list.iterator();

                        while (iterator.hasNext()) {
                            AddressListItem item = new AddressListItem();
                            item = (AddressListItem) iterator.next();
                            if (item.default_flag.equals(DEFAULT_ADDRESS)) {
                                toNameTxt.setText(item.consignee);
                                toNumberTxt.setText(item.tel);
                                toAddrTxt.setText(item.province_name + item.city_name + item.area_name + item.address);
                                address_no = item.address_no;
                            } else {
                                toNameTxt.setText(AddressModel.my_address_list.get(0).consignee);
                                toNumberTxt.setText(AddressModel.my_address_list.get(0).tel);
                                toAddrTxt.setText(AddressModel.my_address_list.get(0).province_name + AddressModel.my_address_list.get(0).city_name + AddressModel.my_address_list.get(0).area_name + AddressModel.my_address_list.get(0).address);
                                address_no = AddressModel.my_address_list.get(0).address_no;
                            }
                        }
                    } else {
                        tv_addaddress.setVisibility(View.VISIBLE);
                        headview_linear.setVisibility(View.GONE);
                    }
                } else {
                    ToastUtils.toastShort(getString(R.string.para_exception) + response.getHead().getReturn_message());
                }
            }
        };
    }

    /**
     * 功能：获取基础数据网络响应成功，返回数据
     */
    private Response.Listener<BaseDataResult> baseListener() {
        return new Response.Listener<BaseDataResult>() {
            @Override
            public void onResponse(BaseDataResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    ConfirmOrderModel.base_data_list = response.getBody().getBase_data_list();
                    updateControl();
                } else {
                    ToastUtils.toastShort(getString(R.string.para_exception) + response.getHead().getReturn_message());
                }
            }
        };
    }

    /**
     * 功能：我的添加订单网络响应成功，返回数据
     */
    private Response.Listener<OrderInfoResult> addorderListener() {
        return new Response.Listener<OrderInfoResult>() {
            @Override
            public void onResponse(OrderInfoResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    Toast.makeText(OrderConfirmActivity.this, getString(R.string.order_confirm_save_success), Toast.LENGTH_SHORT).show();
//                    Intent intent = new Intent(OrderConfirmActivity.this, MyOrderMainActivity.class);
//                    Bundle bundle = new Bundle();
//                    intent.putExtra("ConfirmOrder", bundle);
//                    startActivity(intent);
                    CartFragment.CARTFRAGMENT_UPDATE_FLAG = true; //下单成功，下次进入购物车页面时需刷新购物车页面
                    SelectPayTypeActivity.launcher(OrderConfirmActivity.this, response.getBody().order_info.order_no, TongleAppConst.PAY_MODE);
                    finish();
//                    if (CartFragment.instance != null) {
//                        CartFragment.instance.doRequest();
//                    }
                } else {
                    ToastUtils.toastShort(getString(R.string.para_exception) + response.getHead().getReturn_message());
                }
            }
        };
    }

    /**
     * 功能：网络响应失败
     */
    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ToastUtils.toastShort(getString(R.string.sys_exception));
                pDlg.dismiss();
            }
        };
    }

    /**
     * 功能：更新动态控件UI，如发票类型，发票内容，支付方式需要动态创建控件
     */
    public void updateControl() {
        pDlg.dismiss();
        //更新UI控件
        if ((AddressModel.my_address_list != null) && (AddressModel.my_address_list.size() != 0)) {
            Iterator iterator = AddressModel.my_address_list.iterator();
            while (iterator.hasNext()) {
                AddressListItem item = new AddressListItem();
                item = (AddressListItem) iterator.next();
                if (item.default_flag.equals(DEFAULT_ADDRESS)) {
                    toNameTxt.setText(item.consignee);
                    toNumberTxt.setText(item.tel);
                    toAddrTxt.setText(item.province_name + item.city_name + item.area_name + item.address);
                    address_no = item.address_no;
                }
            }
        }

        for (int i = 0; i < ConfirmOrderModel.base_data_list.size(); i++) {
            BaseDataListItem item = new BaseDataListItem();
            item = ConfirmOrderModel.base_data_list.get(i);
            //动态创建支付方式中的控件
//            if (item.base_data_type.equals(TongleAppConst.PAY_MODE)) {
//                FlowRadioGroup radioGroup = new FlowRadioGroup(this);
//                radioGroup.setItemPadding(10, 10);
//                for (int j = 0; j < item.data_list.size(); j++) {
//                    btn = (RadioButton) LayoutInflater.from(this).inflate(R.layout.single_button, null);
//                    RadioGroup.LayoutParams params = new RadioGroup.LayoutParams(200, 50);
//                    btn.setLayoutParams(params);
//                    btn.setText(item.data_list.get(j).name);
//                    btn.setGravity(Gravity.CENTER | Gravity.LEFT);
//                    radioGroup.addView(btn);
//                }

//                radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
//                    @Override
//                    public void onCheckedChanged(RadioGroup group, int checkedId) {
//                        int radioId = group.getCheckedRadioButtonId();
//                        btn = (RadioButton) findViewById(radioId);
//                        String ratingBa = btn.getText().toString();
//
//                        if (ratingBa.equals(getString(R.string.common_pay_type_01))) {
//                            pay_type = "1";
//                        } else if (ratingBa.equals(getString(R.string.common_pay_type_02))) {
//                            pay_type = "2";
//                        } else if (ratingBa.equals(getString(R.string.common_pay_type_03))) {
//                            pay_type = "0";
//                        } else if (ratingBa.equals(getString(R.string.common_pay_type_04))) {
//                            pay_type = "3";
//                        } else if (ratingBa.equals(getString(R.string.common_pay_type_05))) {
//                            pay_type = "4";
//                        } else if (ratingBa.equals(getString(R.string.common_pay_type_06))) {
//                            pay_type = "9";
//                        }
//                    }
//                });
            //payModeLinear.addView(radioGroup);
            //动态创建发票类型中的控件
//            } else if (item.base_data_type.equals(TongleAppConst.BILL_TYPE)) {
//                RadioGroup radioGroup1 = new RadioGroup(this);
//                radioGroup1.setOrientation(RadioGroup.HORIZONTAL);
//                invoiceList = item.data_list;
//                for (int j = 0; j < item.data_list.size(); j++) {
//                    rbtn1 = (RadioButton) LayoutInflater.from(this).inflate(R.layout.order_single_button, null);
//                    RadioGroup.LayoutParams params = new RadioGroup.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//                    params.setMargins(5, 10, 5, 10);
//                    rbtn1.setLayoutParams(params);
//                    rbtn1.setText(item.data_list.get(j).name);
//                    rbtn1.setTextColor(Color.GRAY);
//                    radioGroup1.addView(rbtn1);
//                }
//                //做radiogroup的单选事件
//                radioGroup1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
//                    @Override
//                    public void onCheckedChanged(RadioGroup group, int checkedId) {
//                        int radioId = group.getCheckedRadioButtonId();
//                        rbtn1 = (RadioButton) findViewById(radioId);
//                        String ratingBa = rbtn1.getText().toString();
//                        Iterator iter = invoiceList.iterator();
//                        while (iter.hasNext()) {
//                            DataListItem item = (DataListItem) iter.next();
//                            if (ratingBa.equals(item.name)) {
//                                invoice_type_no = item.code;
//                            }
//                        }
//                    }
//                });
//                //动态创建发票内容中的控件
//                //billTypeLinear.addView(radioGroup1);
//                //动态创建发票内容中的控件
//            }
//else if (item.base_data_type.equals(TongleAppConst.BILL_CONTENT)) {
//                RadioGroup radioGroup2 = new RadioGroup(this);
//                radioGroup2.setOrientation(RadioGroup.HORIZONTAL);
//
//                invoicecontentList = item.data_list;
//                for (int j = 0; j < item.data_list.size(); j++) {
//                    rbtn2 = (RadioButton) LayoutInflater.from(this).inflate(R.layout.order_single_button, null);
//                    RadioGroup.LayoutParams params = new RadioGroup.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//                    params.setMargins(5, 10, 5, 10);
//                    rbtn2.setLayoutParams(params);
//                    rbtn2.setText(item.data_list.get(j).name);
//                    rbtn2.setTextColor(Color.GRAY);
//                    radioGroup2.addView(rbtn2);
//                }
//                //做radiogroup的单选事件
//                radioGroup2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
//                    @Override
//                    public void onCheckedChanged(RadioGroup group, int checkedId) {
//                        int radioId = group.getCheckedRadioButtonId();
//                        rbtn2 = (RadioButton) findViewById(radioId);
//
//                        String ratingBa2 = rbtn2.getText().toString();
//                        Iterator iter = invoicecontentList.iterator();
//                        while (iter.hasNext()) {
//                            DataListItem item = (DataListItem) iter.next();
//                            if (ratingBa2.equals(item.name)) {
//                                invoice_content_flag = item.code;
//                            }
//                        }
//                    }
//                });
//                //配送方式
//                //billContentLinear.addView(radioGroup2);
//            } else
            if (item.base_data_type.equals(TongleAppConst.DELIVERY_MODE)) {
                ConfirmOrderModel.deliveryModeList = item.data_list;
                deliveryModeTxt.setText(ConfirmOrderModel.deliveryModeList.get(0).name);
                //确认地址时，默认的送货方式
                shipping_method_no = ConfirmOrderModel.deliveryModeList.get(0).code;
            }
            //配送时间
            else if (item.base_data_type.equals(TongleAppConst.DELIVERY_TIME)) {
                ConfirmOrderModel.deliveryTimeList = item.data_list;
                deliveryTimeTxt.setText(ConfirmOrderModel.deliveryTimeList.get(0).name);
                //确认地址时，默认的送货时间
                shipping_time_memo = ConfirmOrderModel.deliveryTimeList.get(0).code;
            }
        }
    }
}