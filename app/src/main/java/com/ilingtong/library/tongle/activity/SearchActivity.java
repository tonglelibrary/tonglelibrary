package com.ilingtong.library.tongle.activity;

import android.app.ProgressDialog;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.adapter.CollectFragmentPagerAdapter;
import com.ilingtong.library.tongle.adapter.TLPagerAdapter;
import com.ilingtong.library.tongle.fragment.MExpertFragment;
import com.ilingtong.library.tongle.fragment.MForumFragment;
import com.ilingtong.library.tongle.fragment.MProductFragment;
import com.ilingtong.library.tongle.fragment.MStoreFragment;
import com.ilingtong.library.tongle.protocol.FriendListItem;
import com.ilingtong.library.tongle.protocol.MStoreListItem;
import com.ilingtong.library.tongle.protocol.ProductListItemData;
import com.ilingtong.library.tongle.protocol.SearchResult;
import com.ilingtong.library.tongle.protocol.UserFollowPostList;
import com.ilingtong.library.tongle.utils.ToastUtils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * User: shiyuchong
 * Date: 2015/6/23
 * Time: 9:26
 * Email: ycshi@isoftstone.com
 * Desc:  搜索页面
 */
public class SearchActivity extends BaseFragmentActivity implements View.OnClickListener {
    private View topview;
    private ImageView leftArrowBtn;
    private TextView topname;
    private LinearLayout searchlayout;
    private SearchView search;
    //Tab控件
    private TextView expertText;
    private TextView forumText;
    private TextView storeText;
    private TextView productText;
    private ViewPager viewPager;
    //image
    private ImageView imageView1;
    private ImageView imageView2;
    private ImageView imageView3;
    private ImageView imageView4;
    //pager 与 适配器
    private TLPagerAdapter pageAdapter;
    //管理View的集合
    private List<View> listViews;
    private int currIndex = 0;
    private Fragment[] fragments = new Fragment[4];
    private ArrayList<MStoreListItem> mstore_list = new ArrayList<>();
    private ArrayList<FriendListItem> user_list = new ArrayList<>();
    private ArrayList<UserFollowPostList> post_list = new ArrayList<>();
    private ArrayList<ProductListItemData> prod_list = new ArrayList<>();
    private ArrayList<FlushDataListener> mFragmentSet = new ArrayList<FlushDataListener>();
    //搜索内容
    private String keywords;
    ProgressDialog pDlg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_collect_layout);
        initView();
    }

    private void initView() {
        topview = findViewById(R.id.proceeds_top_view);
        topname = (TextView) findViewById(R.id.top_name);
        search  =  (SearchView) findViewById(R.id.search_view);
        leftArrowBtn = (ImageView) findViewById(R.id.left_arrow_btn);
        searchlayout = (LinearLayout) findViewById(R.id.ll_search);
        expertText = (TextView) findViewById(R.id.expert_text);
        forumText = (TextView) findViewById(R.id.forum_text);
        storeText = (TextView) findViewById(R.id.store_text);
        productText = (TextView) findViewById(R.id.product_text);
        viewPager = (ViewPager) findViewById(R.id.collect_pager);
        imageView1 = (ImageView) findViewById(R.id.collect_cursor1);
        imageView2 = (ImageView) findViewById(R.id.collect_cursor2);
        imageView3 = (ImageView) findViewById(R.id.collect_cursor3);
        imageView4 = (ImageView) findViewById(R.id.collect_cursor4);
        leftArrowBtn.setOnClickListener(this);
        leftArrowBtn.setVisibility(View.VISIBLE);
        topname.setText(getString(R.string.search_top_name));
        topname.setGravity(View.TEXT_ALIGNMENT_CENTER);
        topname.setVisibility(View.VISIBLE);

        expertText.setOnClickListener(new TextOnClickListener(0));
        forumText.setOnClickListener(new TextOnClickListener(1));
        storeText.setOnClickListener(new TextOnClickListener(2));
        productText.setOnClickListener(new TextOnClickListener(3));

        viewPager.setOnPageChangeListener(new ViewPageOnPageChangeListener());

        pDlg = new ProgressDialog(this);
        pDlg.setTitle(getString(R.string.search_dialog_title));
        pDlg.setMessage(getString(R.string.search_dialog_message));

        //获取搜索模块
        searchlayout.setVisibility(View.VISIBLE);
        searchlayout.setBackgroundColor(Color.LTGRAY);
        keywords = (String) getIntent().getExtras().get("KeyWords");
        doRequest(keywords);

        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            public boolean onQueryTextSubmit(String query) {
                keywords = query;
                doRequest(query);
                return true;
            }

            public boolean onQueryTextChange(String newText) {
                return true;
            }
        });
        setSearchViewBackground(search);
        setMyAdapter();
    }

    /**
     *  android4.0 SearchView去掉（修改）搜索框的背景 修改光标
     */
    public void setSearchViewBackground(SearchView search) {
        try {
            Class<?> argClass = search.getClass();
            // 指定某个私有属性
            Field ownField = argClass.getDeclaredField("mSearchPlate"); // 注意mSearchPlate的背景是stateListDrawable(不同状态不同的图片)
            // 所以不能用BitmapDrawable
            // setAccessible 它是用来设置是否有权限访问反射类中的私有属性的，只有设置为true时才可以访问，默认为false
            ownField.setAccessible(true);
            View mView = (View) ownField.get(search);
            mView.setBackgroundDrawable(getResources().getDrawable(
                    R.drawable.input_bk));

            // 指定某个私有属性
            Field mQueryTextView = argClass.getDeclaredField("mQueryTextView");
            mQueryTextView.setAccessible(true);
            Class<?> mTextViewClass = mQueryTextView.get(search).getClass()
                    .getSuperclass().getSuperclass().getSuperclass();

            // mCursorDrawableRes光标图片Id的属性
            // 这个属性是TextView的属性，所以要用mQueryTextView（SearchAutoComplete）的父类（AutoCompleteTextView）的父
            // 类( EditText）的父类(TextView)
            Field mCursorDrawableRes = mTextViewClass
                    .getDeclaredField("mCursorDrawableRes");

            // setAccessible 它是用来设置是否有权限访问反射类中的私有属性的，只有设置为true时才可以访问，默认为false
            mCursorDrawableRes.setAccessible(true);
            mCursorDrawableRes.set(mQueryTextView.get(search),
                    R.drawable.input_line);// 注意第一个参数持有这个属性(mQueryTextView)的对象(mSearchView)
            // 光标必须是一张图片不能是颜色，因为光标有两张图片，一张是第一次获得焦点的时候的闪烁的图片，一张是后边有内容时候的图片，如果用颜色填充的话，就会失去闪烁的那张图片，颜色填充的会缩短文字和光标的距离（某些字母会背光标覆盖一部分）。
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * 显示内容
     */
    private void setMyAdapter() {
        fragments[0] = MExpertFragment.newInstance(TongleAppConst.SEARCH_INTO, user_list);
        fragments[1] = MForumFragment.newInstance(TongleAppConst.SEARCH_INTO, post_list);
        fragments[2] = MStoreFragment.newInstance(TongleAppConst.SEARCH_INTO, mstore_list);
        fragments[3] = MProductFragment.newInstance(TongleAppConst.SEARCH_INTO, prod_list);
        mFragmentSet.add((MExpertFragment)fragments[0]);
        mFragmentSet.add((MForumFragment) fragments[1]);
        mFragmentSet.add((MStoreFragment) fragments[2]);
        mFragmentSet.add((MProductFragment) fragments[3]);
        CollectFragmentPagerAdapter adapter = new CollectFragmentPagerAdapter(getSupportFragmentManager(), fragments);
        viewPager.setOffscreenPageLimit(4);
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(0);
    }

    /**
     * 调用接口，请求数据
     *
     * @param keywords
     */
    private void doRequest(String keywords) {
        pDlg.show();
        mstore_list.clear();
        user_list.clear();
        post_list.clear();
        prod_list.clear();

        ServiceManager.searchRequest(TongleAppInstance.getInstance().getUserID(), keywords, "", searchSuccessListener(), errorListener());
    }

    /**
     * 搜索功能
     */
    private Response.Listener searchSuccessListener() {
        return new Response.Listener<SearchResult>() {
            @Override
            public void onResponse(SearchResult searchResulte) {
                if (TongleAppConst.SUCCESS.equals(searchResulte.getHead().getReturn_flag())) {
                    if (searchResulte.getBody().getMstore_list().size() > 0) {
                        mstore_list.addAll(searchResulte.getBody().getMstore_list());
                    }
                    if (searchResulte.getBody().getUser_list().size() > 0) {
                        user_list.addAll(searchResulte.getBody().getUser_list());
                    }
                    if (searchResulte.getBody().getPost_list().size() > 0) {
                        post_list.addAll(searchResulte.getBody().getPost_list());
                    }
                    if (searchResulte.getBody().getProd_list().size() > 0) {
                        prod_list.addAll(searchResulte.getBody().getProd_list());
                    }
                    for (int i = 0; i < mFragmentSet.size(); i++) {
                        if (i == 0){
                            mFragmentSet.get(i).flush(user_list);
                        }else if (i == 1){
                            mFragmentSet.get(i).flush(post_list);
                        }else if (i == 2){
                            mFragmentSet.get(i).flush(mstore_list);
                        }else if (i == 3){
                            mFragmentSet.get(i).flush(prod_list);
                        }
                    }
                } else {
                    ToastUtils.toastShort(getString(R.string.para_exception) + searchResulte.getHead().getReturn_message());
                }
                pDlg.dismiss();
            }

        };
    }

    /**
     * 功能：网络响应失败
     */
    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ToastUtils.toastShort(getString(R.string.sys_exception));
                pDlg.dismiss();
            }
        };
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.left_arrow_btn) {
            finish();
        }
    }

    /**
     * TextView监听
     */
    private class TextOnClickListener implements View.OnClickListener {
        private int index = 0;

        public TextOnClickListener(int i) {
            index = i;
        }

        public void onClick(View v) {
            viewPager.setCurrentItem(index);
        }
    }

    /**
     * viewpager监听
     */

    public class ViewPageOnPageChangeListener implements ViewPager.OnPageChangeListener {
        public void onPageScrollStateChanged(int arg0) {
        }

        public void onPageScrolled(int arg0, float arg1, int arg2) {

            Toast.makeText(SearchActivity.this, getString(R.string.common_search_now), Toast.LENGTH_LONG);

        }

        public void onPageSelected(int arg0) {
            currIndex = arg0;
            int i = currIndex + 1;
            setTabTitle(i);
        }
    }

    /**
     * 设置选项卡title的字体颜色
     *
     * @param tabId
     */
    public void setTabTitle(int tabId) {
        //viewpager顶部导航游标，并设置游标显示位置
        if (tabId == 1) {
            imageView1.setVisibility(View.VISIBLE);
            imageView2.setVisibility(View.INVISIBLE);
            imageView3.setVisibility(View.INVISIBLE);
            imageView4.setVisibility(View.INVISIBLE);
        } else if (tabId == 2) {
            imageView1.setVisibility(View.INVISIBLE);
            imageView2.setVisibility(View.VISIBLE);
            imageView3.setVisibility(View.INVISIBLE);
            imageView4.setVisibility(View.INVISIBLE);
        } else if (tabId == 3) {
            imageView1.setVisibility(View.INVISIBLE);
            imageView2.setVisibility(View.INVISIBLE);
            imageView3.setVisibility(View.VISIBLE);
            imageView4.setVisibility(View.INVISIBLE);
        } else if (tabId == 4) {

            imageView1.setVisibility(View.INVISIBLE);
            imageView2.setVisibility(View.INVISIBLE);
            imageView3.setVisibility(View.INVISIBLE);
            imageView4.setVisibility(View.VISIBLE);
        }
        //viewpager顶部文字导航，并设置文字颜色
        Resources resource = (Resources) getResources();
        ColorStateList selectedTextColor = (ColorStateList) resource.getColorStateList(R.color.subcollect_fragment_selecttext_color);
        ColorStateList unselectedTextColor = (ColorStateList) resource.getColorStateList(R.color.subcollect_fragment_text_color);
        if (tabId == 1) {
            expertText.setTextColor(selectedTextColor);
            forumText.setTextColor(unselectedTextColor);
            storeText.setTextColor(unselectedTextColor);
            productText.setTextColor(unselectedTextColor);
        } else if (tabId == 2) {
            expertText.setTextColor(unselectedTextColor);
            forumText.setTextColor(selectedTextColor);
            storeText.setTextColor(unselectedTextColor);
            productText.setTextColor(unselectedTextColor);
        } else if (tabId == 3) {
            expertText.setTextColor(unselectedTextColor);
            forumText.setTextColor(unselectedTextColor);
            storeText.setTextColor(selectedTextColor);
            productText.setTextColor(unselectedTextColor);
        } else if (tabId == 4) {
            expertText.setTextColor(unselectedTextColor);
            forumText.setTextColor(unselectedTextColor);
            storeText.setTextColor(unselectedTextColor);
            productText.setTextColor(selectedTextColor);
        }
    }

    public interface FlushDataListener{
        public void flush(Object data);
    }
}
