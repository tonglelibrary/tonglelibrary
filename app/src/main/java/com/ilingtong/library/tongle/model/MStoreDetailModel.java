package com.ilingtong.library.tongle.model;

import com.ilingtong.library.tongle.protocol.ADListItem;

import com.ilingtong.library.tongle.protocol.ProductListItemData;

import java.util.ArrayList;

/**
 * User: shuailei
 * Date: 2015/5/122
 * Time: 15:54
 * Email:leishuai@isoftstone.com
 * Desc:
 */
public class MStoreDetailModel {
    //魔店详情
    public String mstore_id;
    public String mstore_name;
    public String mstore_favorited_by_me;
    public ArrayList<ADListItem> ad_list;
    public ArrayList<ProductListItemData> prod_list;
    public String vouchers_text;       // 优惠介绍文字  add at 2016/03/29
    public String vouchers_url;       //优惠LinkUrl add at 2016/03/29
    public String vouchers_title;       //优惠券标题  add at 2016/03/29
    //二维码url
    public String mstore_qr_code_url;
}
