package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: shuailei
 * Date: 2015/6/24
 * Time: 22:05
 * Email: leishuai@isoftstone.com
 * Desc: 产品列表中的元素属性类
 */
public class ProductListInfo implements Serializable {
    //明细序号
    public String seq_no;
    //商品ID
    public String product_id;
    @Override
    public String toString() {
        return "seq_no:" + seq_no + "\r\n" +
                "product_id:" + product_id;
    }

}
