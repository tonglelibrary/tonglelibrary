package com.ilingtong.library.tongle.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.adapter.ProInfoListAdapter;
import com.ilingtong.library.tongle.adapter.ProNoticeListAdapter;
import com.ilingtong.library.tongle.external.MyListView;
import com.ilingtong.library.tongle.protocol.CouponDetailResult;
import com.ilingtong.library.tongle.utils.DialogUtils;
import com.ilingtong.library.tongle.utils.FontUtils;
import com.ilingtong.library.tongle.utils.ToastUtils;
import com.ilingtong.library.tongle.utils.Utils;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.io.Serializable;

/**
 * author: liuting
 * Date: 2016/3/11
 * Time: 9:02
 * Email: liuting@ilingtong.com
 * Desc:团购券详情页
 */
public class GroupTicketDetailActivity extends BaseActivity implements View.OnClickListener {
    private TextView mTxtTitle;//标题
    private ImageView mImgBack;//返回图标
    private TextView mTxtAgain;//再来一单

    private RelativeLayout mRlyProduct;//商品详情一栏
    private ImageView mImgProductPic;//商品图片
    private TextView mTxtProductName;//商品名称
    private TextView mTxtCode;//券号
    private TextView mTXtDate;//有效期
    private Button mBtnRefund;//申请退款
    private ImageView mImgQr;//二维码图标
    private LinearLayout mLlyPhone;//发送到手机

    private RelativeLayout mRlyStoreCount;//适用商户块
    private TextView mTxtStoreCount;//适用商户数
    private TextView mTxtStoreName;//商户名称
    private TextView mTxtStoreAddress;//商户地址
    private ImageButton mImgStorePhone;//商户电话

    private LinearLayout mLlyDetail;//图文详情块
    private TextView mTxtInfoHead;//团购详情标题
    private MyListView mLvInfo;//团购详情明细列表
    private TextView mTxtInfoTotal;//团购详情总价
    private TextView mTxtInfoActual;//团购详情团购价
    private TextView mTxtFooter;//脚注

    private MyListView mLvNotice;//购买须知列表

    private CouponDetailResult mCouponDetailResult;//团购券详情

    private String mOrderNo;//订单编号
    private String mCode;//券号
    private String mOutOfDate;//过期/未消费标志

    private Dialog mDialog;//加载对话框
    private AlertDialog mAlert;//显示一维码和二维码的dialog
    private ProInfoListAdapter mProInfoListAdapter;//团购信息明细Adapter
    private ProNoticeListAdapter mProNoticeListAdapter;//购买须知Adapter

    private ScrollView mSvMain;//有信息时显示
    private RelativeLayout mRlyReplace;//无相关信息时显示
    private TextView mTxtReplace;//显示无相关信息或者错误信息

    private boolean isRefund = false;  //是否申请了退款标志

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_ticket_detail);
        initView();
        getData();
    }

    public void initView() {
        mTxtTitle = (TextView) findViewById(R.id.top_name);
        mImgBack = (ImageView) findViewById(R.id.left_arrow_btn);
        mTxtAgain = (TextView) findViewById(R.id.top_btn_text);
        mTxtTitle.setText(getResources().getString(R.string.ticket_detail_top_name));
        mTxtAgain.setText(getString(R.string.ticket_detail_top_btn_text));
        mTxtTitle.setVisibility(View.VISIBLE);
        mImgBack.setVisibility(View.VISIBLE);
//        mTxtAgain.setVisibility(View.VISIBLE);
        mTxtAgain.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14); //14SP
        mImgBack.setOnClickListener(this);
        mTxtAgain.setOnClickListener(this);
        mTxtAgain.setBackgroundResource(R.color.topview_bgcolor);

        mRlyProduct = (RelativeLayout) findViewById(R.id.ticket_detail_rly_product);
        mImgProductPic = (ImageView) findViewById(R.id.ticket_detail_img_product_pic);
        mTxtProductName = (TextView) findViewById(R.id.ticket_detail_tv_product_name);
        mTxtCode = (TextView) findViewById(R.id.ticket_detail_tv_code);
        mTXtDate = (TextView) findViewById(R.id.ticket_detail_tv_date);
        mBtnRefund = (Button) findViewById(R.id.ticket_detail_btn_refund);
        mImgQr = (ImageView) findViewById(R.id.ticket_detail_img_qr);
        mLlyPhone = (LinearLayout) findViewById(R.id.ticket_detail_lly_phone);
        mImgQr.setOnClickListener(this);
        mLlyPhone.setOnClickListener(this);
        mRlyProduct.setOnClickListener(this);
        mBtnRefund.setOnClickListener(this);

        mRlyStoreCount = (RelativeLayout) findViewById(R.id.ticket_detail_rly_store_count);
        mTxtStoreCount = (TextView) findViewById(R.id.ticket_detail_tv_store_count);
        mTxtStoreName = (TextView) findViewById(R.id.ticket_detail_tv_store_name);
        mTxtStoreAddress = (TextView) findViewById(R.id.ticket_detail_tv_store_address);
        mImgStorePhone = (ImageButton) findViewById(R.id.ticket_detail_btn_phone);
        mRlyStoreCount.setOnClickListener(this);
        mImgStorePhone.setOnClickListener(this);

        mLlyDetail = (LinearLayout) findViewById(R.id.ticket_detail_lly_detail);
        mTxtInfoHead = (TextView) findViewById(R.id.ticket_detail_tv_head);
        mLvInfo = (MyListView) findViewById(R.id.ticket_detail_lv_info);
        mTxtInfoTotal = (TextView) findViewById(R.id.ticket_detail_tv_price_total);
        mTxtInfoActual = (TextView) findViewById(R.id.ticket_detail_tv_price_actual);
        mTxtFooter = (TextView) findViewById(R.id.ticket_detail_tv_footer);
        mLlyDetail.setVisibility(View.GONE);

        mLvNotice = (MyListView) findViewById(R.id.ticket_detail_lv_notice);

        mSvMain = (ScrollView) findViewById(R.id.group_ticket_detail_sv_main);
        mRlyReplace = (RelativeLayout) findViewById(R.id.rl_replace);
        mTxtReplace = (TextView) findViewById(R.id.tv_replace);

        mDialog = DialogUtils.createLoadingDialog(GroupTicketDetailActivity.this);
        mDialog.setCancelable(false);
        mDialog.show();

        mOrderNo = getIntent().getExtras().getString("order_no");
        mCode = getIntent().getExtras().getString("coupon_code");
        mOutOfDate = getIntent().getExtras().getString("out_of_date");
    }

    public void initData() {
        ImageLoader.getInstance().displayImage(mCouponDetailResult.getBody().getProd_base_info().getProd_pic_url_list().get(0).pic_url, mImgProductPic, TongleAppInstance.options);
        mTxtProductName.setText(mCouponDetailResult.getBody().getProd_base_info().getProd_name());
        mTxtCode.setText(Utils.formatStringSequence(mCode, TongleAppConst.COUPON_SEQUENCE_LENGHT));
        mTXtDate.setText(mCouponDetailResult.getBody().getProd_base_info().getExpired_date());
//        if (Integer.parseInt(mCouponDetailResult.getBody().getProd_base_info().getRefund_qty()) < 1||mOutOfDate.equals(TongleAppConst.GROUP_TICKET_OUT_OF)) {
//            //可退数量小于1或者过期/未消费标志为过期，则不可申请退款
//            mBtnRefund.setEnabled(false);
//        } else {
//            //否则可以申请退款
//            mBtnRefund.setEnabled(true);
//        }

        mTxtStoreCount.setText(String.format(getResources().getString(R.string.ticket_detail_store_count_txt), mCouponDetailResult.getBody().getCoupon_store_info().getStore_total_count()));
        mTxtStoreName.setText(mCouponDetailResult.getBody().getCoupon_store_info().getStore_list().get(0).getName());
        mTxtStoreAddress.setText(mCouponDetailResult.getBody().getCoupon_store_info().getStore_list().get(0).getAddress());

        mTxtInfoHead.setText(mCouponDetailResult.getBody().getCoupon_purchase_info().getHead());
        mProInfoListAdapter = new ProInfoListAdapter(GroupTicketDetailActivity.this, mCouponDetailResult.getBody().getCoupon_purchase_info().getContent_list());
        mLvInfo.setAdapter(mProInfoListAdapter);
        mTxtInfoTotal.setText(String.format(getResources().getString(R.string.ticket_detail_total_txt), FontUtils.setTwoDecimal(mCouponDetailResult.getBody().getCoupon_purchase_info().getContent_total_price())));
        mTxtInfoActual.setText(String.format(getResources().getString(R.string.ticket_detail_total_txt), FontUtils.setTwoDecimal(mCouponDetailResult.getBody().getCoupon_purchase_info().getContent_actually_price())));
        mTxtFooter.setText(mCouponDetailResult.getBody().getCoupon_purchase_info().getFooter());

        mProNoticeListAdapter = new ProNoticeListAdapter(GroupTicketDetailActivity.this, mCouponDetailResult.getBody().getCoupon_purchase_notice_list());
        mLvNotice.setAdapter(mProNoticeListAdapter);

    }

    public void getData() {
        mCouponDetailResult = new CouponDetailResult();
//        mCouponDetailResult = (CouponDetailResult) TestInterface.parseJson(CouponDetailResult.class, "6011.txt");
        ServiceManager.doGroupTicketDetailRequest(TongleAppInstance.getInstance().getUserID(), mOrderNo, mCode, mOutOfDate, successListener(), errorListener());
    }

    /**
     * 功能：网络响应成功，返回数据
     *
     * @return
     */
    private Response.Listener successListener() {
        return new Response.Listener<CouponDetailResult>() {
            @Override
            public void onResponse(CouponDetailResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    mCouponDetailResult = response;
                    mRlyReplace.setVisibility(View.GONE);
                    mSvMain.setVisibility(View.VISIBLE);
                    mTxtAgain.setVisibility(View.VISIBLE);
                    initData();
                } else {
                    mRlyReplace.setVisibility(View.VISIBLE);
                    mTxtReplace.setVisibility(View.VISIBLE);
                    mSvMain.setVisibility(View.GONE);
                    mTxtAgain.setVisibility(View.GONE);
                    mTxtReplace.setText(getString(R.string.para_exception) + response.getHead().getReturn_message());
                }
                mDialog.dismiss();
            }
        };
    }

    /**
     * 功能：请求网络响应失败
     *
     * @return
     */
    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ToastUtils.toastShort(getString(R.string.sys_exception));
                mSvMain.setVisibility(View.GONE);
                mTxtAgain.setVisibility(View.GONE);
                mDialog.dismiss();
            }
        };
    }


    /**
     * @param context
     * @param order_no    订单号
     * @param coupon_code 券号
     * @param out_of_date 过期/未消费标志
     */
    public static void launcher(Activity context, String order_no, String coupon_code, String out_of_date, int requestCode) {
        Intent intent = new Intent(context, GroupTicketDetailActivity.class);
        intent.putExtra("order_no", (Serializable) order_no);
        intent.putExtra("coupon_code", (Serializable) coupon_code);
        intent.putExtra("out_of_date", (Serializable) out_of_date);
        context.startActivityForResult(intent, requestCode);
    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.left_arrow_btn) {
            //返回
            finish();
        } else if (v.getId() == R.id.top_btn_text) {
            //再来一单
            //toDetailRequest();
            ProductTicketDetailActivity.launch(GroupTicketDetailActivity.this, mCouponDetailResult.getBody().getProd_base_info().getProd_id(), TongleAppConst.ACTIONID_ORDER, mCouponDetailResult.getBody().getProd_base_info().getRelation_id(), "", "");
        } else if (v.getId() == R.id.ticket_detail_rly_store_count) {
            //查看商户列表
            StoreInfoActivity.launcher(GroupTicketDetailActivity.this, mCouponDetailResult.getBody().getProd_base_info().getProd_id());
        } else if (v.getId() == R.id.ticket_detail_btn_phone) {
            //拨打电话
            Utils.callPhone(GroupTicketDetailActivity.this, mCouponDetailResult.getBody().getCoupon_store_info().getStore_list().get(0).getPhone());
        } else if (v.getId() == R.id.ticket_detail_rly_product) {
            //前往商品详情页
            ProductTicketDetailActivity.launch(GroupTicketDetailActivity.this, mCouponDetailResult.getBody().getProd_base_info().getProd_id(), TongleAppConst.ACTIONID_ORDER, mCouponDetailResult.getBody().getProd_base_info().getRelation_id(), "", "");
        } else if (v.getId() == R.id.ticket_detail_img_qr) {
            //显示一维码和二维码dialog
            mAlert = DialogUtils.showQrDialog(GroupTicketDetailActivity.this, mCouponDetailResult.getBody().getCoupon_codeurl_info().getCoupon_1d_qrcode_url(), mCouponDetailResult.getBody().getCoupon_codeurl_info().getCoupon_2d_qrcode_url());
        } else if (v.getId() == R.id.ticket_detail_btn_refund) {
            //申请退款
            isRefund = true;  //表示点击了申请退款标志。返回“我的团购券”页面时刷新列表
            //拼接url
            String finalUrl = mCouponDetailResult.getBody().getProd_base_info().getGoods_return_url() + "?user_id=" + TongleAppInstance.getInstance().getUserID() + "&user_token=" + TongleAppInstance.getInstance().getToken() + "&order_no=" + mOrderNo;
            Intent expertIntent = new Intent(this, ProdIntegralActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString("prod_url", finalUrl);
            bundle.putString("myTitle", mCouponDetailResult.getBody().getProd_base_info().getGoods_return_title());
            expertIntent.putExtras(bundle);
            startActivity(expertIntent);
        } else if (v.getId() == R.id.ticket_detail_lly_phone) {
            //发送到手机
            String message = String.format(getResources().getString(R.string.group_ticket_detail_send_msg), mCouponDetailResult.getBody().getProd_base_info().getProd_name(), mCode);
            sendMessage(message);
        }

    }

//    /**
//     * 前往商品详情界面
//     */
//    public void toDetailRequest() {
//        Intent detailIntent = new Intent(GroupTicketDetailActivity.this, ProductTicketDetailActivity.class);
//        Bundle bundle = new Bundle();
//        bundle.putString("product_id", mCouponDetailResult.getBody().getProd_base_info().getProd_id());
//        bundle.putString("tag", "order");
//        bundle.putString("relationId", mCouponDetailResult.getBody().getProd_base_info().getRelation_id());
//        detailIntent.putExtras(bundle);
//        startActivity(detailIntent);
//    }

    /**
     * 发送短信
     *
     * @param message 发送短信的内容
     */
    public void sendMessage(String message) {
        Uri smsToUri = Uri.parse("smsto:");
        Intent intent = new Intent(Intent.ACTION_SENDTO, smsToUri);
        intent.putExtra("sms_body", message);
        startActivity(intent);
    }

    @Override
    public void finish() {
        if (isRefund) {
            setResult(RESULT_OK);
        }
        super.finish();
    }
}
