package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/6/9
 * Time: 22:03
 * Email: jqleng@isoftstone.com
 * Desc: 地址类，通过该对象，被用作网络请求参数
 */
public class AddressResult implements Serializable {
    private BaseInfo head;
    private AddressInfo body;

    public BaseInfo getHead() {
        return head;
    }

    public void setHead(BaseInfo head) {
        this.head = head;
    }

    public AddressInfo getBody() {
        return body;
    }

    public void setBody(AddressInfo body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return body.toString();
    }
}
