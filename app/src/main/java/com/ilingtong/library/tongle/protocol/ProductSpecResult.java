package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/6/24
 * Time: 11:36
 * Email: jqleng@isoftstone.com
 * Desc: 用户协议类，通过该对象，被用作网络请求参数
 */
public class ProductSpecResult implements Serializable {
    private BaseInfo head;
    private ProdParameterListInfo body;

    public BaseInfo getHead() {
        return head;
    }

    public void setHead(BaseInfo head) {
        this.head = head;
    }

    public ProdParameterListInfo getBody() {
        return body;
    }

    public void setBody(ProdParameterListInfo body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return body.toString();
    }
}
