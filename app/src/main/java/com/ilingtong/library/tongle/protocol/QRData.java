package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/7/2
 * Time: 23:41
 * Email: jqleng@isoftstone.com
 * Desc: QR 数据类型
 */
public class QRData implements Serializable {
    public String code_type;
    public String prod_id;
    public String post_id;
    public String user_id;
    public String store_id;
    public String relation_id;

    @Override
    public String toString() {
        return "QRData{" +
                "code_type='" + code_type + '\'' +
                ", prod_id='" + prod_id + '\'' +
                ", post_id='" + post_id + '\'' +
                ", user_id='" + user_id + '\'' +
                ", store_id='" + store_id + '\'' +
                ", relation_id='" + relation_id + '\'' +
                '}';
    }

}
