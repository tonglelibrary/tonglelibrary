package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * User: lengjiqiang
 * Date: 2015/5/15
 * Time: 16:46
 * Email: jqleng@isoftstone.com
 * Dest:
 */
public class MStoreResponseData implements Serializable {
    public String data_total_count;
    public ArrayList<MStoreListItem> mstore_list;
}
