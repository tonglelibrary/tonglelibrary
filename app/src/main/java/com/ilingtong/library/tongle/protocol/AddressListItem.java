package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/6/9
 * Time: 22:10
 * Email: jqleng@isoftstone.com
 * Desc: 地址属性类，地址列表中的各项均为该属性，因为要通过参数传递给其他activity,所以设置为Serializable
 */
public class AddressListItem implements Serializable{
    public String address_no;
    public String user_tag;
    public String consignee;
    public String tel;
    public String province_id;
    public String province_name;
    public String area_id;
    public String city_id;
    public String city_name;
    public String area_name;
    public String address;
    public String post_code;
    public String default_flag;

    public String getAddress_no() {
        return address_no;
    }

    public void setAddress_no(String address_no) {
        this.address_no = address_no;
    }

    public String getUser_tag() {
        return user_tag;
    }

    public void setUser_tag(String user_tag) {
        this.user_tag = user_tag;
    }

    public String getConsignee() {
        return consignee;
    }

    public void setConsignee(String consignee) {
        this.consignee = consignee;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getProvince_id() {
        return province_id;
    }

    public void setProvince_id(String province_id) {
        this.province_id = province_id;
    }

    public String getProvince_name() {
        return province_name;
    }

    public void setProvince_name(String province_name) {
        this.province_name = province_name;
    }

    public String getArea_id() {
        return area_id;
    }

    public void setArea_id(String area_id) {
        this.area_id = area_id;
    }

    public String getCity_id() {
        return city_id;
    }

    public void setCity_id(String city_id) {
        this.city_id = city_id;
    }

    public String getCity_name() {
        return city_name;
    }

    public void setCity_name(String city_name) {
        this.city_name = city_name;
    }

    public String getArea_name() {
        return area_name;
    }

    public void setArea_name(String area_name) {
        this.area_name = area_name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPost_code() {
        return post_code;
    }

    public void setPost_code(String post_code) {
        this.post_code = post_code;
    }

    public String getDefault_flag() {
        return default_flag;
    }

    public void setDefault_flag(String default_flag) {
        this.default_flag = default_flag;
    }

    @Override
    public String toString() {
        return "address_no:" + address_no + "\r\n" +
                "user_tag:" + user_tag + "\r\n" +
                "consignee:" + consignee + "\r\n" +
                "tel:" + tel + "\r\n" +
                "province_id:" + province_id + "\r\n" +
                "province_name:" + province_name + "\r\n" +
                "area_id:" + area_id + "\r\n" +
                "city_name:" + city_name + "\r\n" +
                "area_name:" + area_name + "\r\n" +
                "address:" + address + "\r\n" +
                "post_code:" + post_code + "\r\n" +
                "default_flag:" + default_flag;
    }

}
