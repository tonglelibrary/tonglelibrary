package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * Created by Administrator on 2015/5/20.
 */
public class ExpertDetailData implements Serializable {
    public String user_qr_code_url;
    public String mstore_qr_code_url;
    public String prod_qr_code_url;
}
