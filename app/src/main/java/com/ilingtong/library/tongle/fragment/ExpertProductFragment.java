package com.ilingtong.library.tongle.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.activity.CollectProductDetailActivity;
import com.ilingtong.library.tongle.activity.ProductTicketDetailActivity;
import com.ilingtong.library.tongle.adapter.ExpertProductListAdaper;
import com.ilingtong.library.tongle.external.maxwin.view.XListView;
import com.ilingtong.library.tongle.protocol.ExpertProductResult;
import com.ilingtong.library.tongle.protocol.ExpertRequestParam;
import com.ilingtong.library.tongle.protocol.ProductListItemData;
import com.ilingtong.library.tongle.utils.ToastUtils;

import java.util.ArrayList;

/**
 * User: shuailei
 * Date: 2015/5/19
 * Time: 10:45
 * Email: leishaui@isoftstone.com
 * Dest:  达人产品
 */
public class ExpertProductFragment extends BaseFragment implements XListView.IXListViewListener {
    private XListView productList;
    private ExpertProductListAdaper productListAdaper;
    ArrayList<ProductListItemData> product_List = new ArrayList<>();
    private boolean flag = true;//上拉加载更多标识
    private ProgressDialog pDlg;
    private String user_id;
    Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    productListAdaper.notifyDataSetChanged();
                    break;
            }
        }
    };

    public static ExpertProductFragment newInstance(String userId) {

        Bundle args = new Bundle();
        args.putString("user_id", userId);
        ExpertProductFragment fragment = new ExpertProductFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.collect_forum_layout, container, false);
        getData();
        doRequest();
        initView(rootView);
        return rootView;
    }
    public void getData() {
        user_id = getArguments().getString("user_id");
    }

    public void initView(View rootView) {
        pDlg = new ProgressDialog(getActivity());
        pDlg.setCancelable(false);
        pDlg.setMessage(getString(R.string.loading));
        pDlg.show();

        productList = (XListView) rootView.findViewById(R.id.product_listview);
        productListAdaper = new ExpertProductListAdaper(getActivity(), product_List);
        productList.setAdapter(productListAdaper);
        productList.setXListViewListener(this, 0);
        productList.setRefreshTime();
        productList.setPullLoadEnable(false);
        productList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
               if (TongleAppConst.YES.equals(product_List.get(position-1).coupon_flag)){
                   ProductTicketDetailActivity.launch(getActivity(),product_List.get(position - 1).prod_id,TongleAppConst.ACTIONID_EXPERT_BABY,product_List.get(position - 1).relation_id,"","");
               }else {
                   CollectProductDetailActivity.launch(getActivity(),product_List.get(position - 1).prod_id,TongleAppConst.ACTIONID_EXPERT_BABY,product_List.get(position - 1).relation_id,"","");
               }
            }
        });

    }

    public void doRequest() {
        if (!TextUtils.isEmpty(user_id)) {
            ExpertRequestParam param = new ExpertRequestParam();
            param.user_id = user_id;
            param.fetch_count = TongleAppConst.FETCH_COUNT;
            product_List.clear();
            ServiceManager.doExpertProductRequest(param, successListener(), errorListener());
        }
    }

    /**
     * 功能：网络响应成功，返回数据
     */
    private Response.Listener successListener() {
        return new Response.Listener<ExpertProductResult>() {
            @Override
            public void onResponse(ExpertProductResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    if (response.getBody().getMy_prod_list().size() < Integer.parseInt(TongleAppConst.FETCH_COUNT)) {
                        flag = false;
                        productList.setPullLoadEnable(false);
                    } else {
                        productList.setPullLoadEnable(true);
                    }
                    product_List.addAll(response.getBody().getMy_prod_list());
                } else {
                    ToastUtils.toastShort(getString(R.string.para_exception) + response.getHead().getReturn_message());
                }
                //调用handler，发送消息
                mHandler.sendEmptyMessage(0);
                pDlg.dismiss();
            }
        };
    }

    /**
     * 功能：网络响应失败
     */
    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ToastUtils.toastShort(getString(R.string.sys_exception));
                pDlg.dismiss();
            }
        };
    }

    @Override
    public void onRefresh(int id) {
        doRequest();
    }

    @Override
    public void onLoadMore(int id) {
        if (flag) {
            ExpertRequestParam param = new ExpertRequestParam();
            param.user_id = user_id;
            param.fetch_count = TongleAppConst.FETCH_COUNT;
            param.forward = "1";
            param.expert_user_id = product_List.get(product_List.size() - 1).prod_id;
            ServiceManager.doExpertProductRequest(param, successListener(), errorListener());
        } else {
            ToastUtils.toastShort(getString(R.string.common_list_end));
            productList.setPullLoadEnable(false);
        }
    }
}
