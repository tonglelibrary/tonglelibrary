package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * User: syc
 * Date: 2015/6/10
 * Time: 17:01
 * Email: ycshi@isoftstone.com
 * Dest:  belong to [1040]   order form data
 */
public class OrderListItem implements Serializable {

    public String order_no;
    public String amount;
    public String fee_amount;
    public String order_time;
    public String status;
    public String pay_method;
    public String modify_flag;
    public String cancel_flag;
    public String evaluate_flag;

    public ArrayList<ProdDetailListItem> prod_detail;


}
