package com.ilingtong.library.tongle.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.protocol.ShopCartListItem;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

/**
 * User: lengjiqiang
 * Date: 2015/6/8
 * Time: 15:45
 * Email: jqleng@isoftstone.com
 * Desc:
 */
public class ConfirmOrderCartListAdapter extends BaseAdapter {
    private LayoutInflater inflater;
    private List<ShopCartListItem> list;
    private Context mContext;
    ShopCartListItem cartDataItem = new ShopCartListItem();

    public ConfirmOrderCartListAdapter(Context context, ArrayList list) {
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.list = new ArrayList<>();
        mContext = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final Hodler hodler;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.confirm_order_cart_list_item, null);
            hodler = new Hodler();
            hodler.listItemImageView = (ImageView) convertView.findViewById(R.id.product_icon);
            hodler.name = (TextView) convertView.findViewById(R.id.corfirm_order_product_name);
            hodler.price = (TextView) convertView.findViewById(R.id.product_price);
            hodler.amount = (TextView) convertView.findViewById(R.id.order_qty);
            hodler.spec = (TextView) convertView.findViewById(R.id.confirm_order_cart_list_item_txt_spec);
            convertView.setTag(hodler);
        } else {
            hodler = (Hodler) convertView.getTag();
        }
        cartDataItem = (ShopCartListItem) getItem(position);//list.get(position);
        ImageLoader.getInstance().displayImage(cartDataItem.prod_pic_url, hodler.listItemImageView, TongleAppInstance.options);
        hodler.name.setText(cartDataItem.prod_name);
        hodler.price.setText(mContext.getString(R.string.RMB) + cartDataItem.price);
        hodler.amount.setText(mContext.getString(R.string.multiply) + cartDataItem.order_qty);
        StringBuffer spec = new StringBuffer();
        if (cartDataItem.prod_spec_list!=null &&cartDataItem.prod_spec_list.size()>0){
            for (int i = 0; i < cartDataItem.prod_spec_list.size(); i++) {
                spec.append(cartDataItem.prod_spec_list.get(i).prod_spec_name+":"+cartDataItem.prod_spec_list.get(i).spec_detail_name+";  ");
            }
        }
        hodler.spec.setText(spec.toString());
        return convertView;
    }

    static class Hodler {
        TextView price;
        TextView name;
        TextView amount;
        TextView spec;  //商品规格
        ImageView listItemImageView;
    }
}
