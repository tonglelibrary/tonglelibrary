package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/5/11
 * Time: 14:16
 * Email: yphana@isoftstone.com
 * Desc: CheckVersion model
 */
public class HeadJson implements Serializable {
    public String function_id;
    public String return_flag;
    public String return_message;
}
