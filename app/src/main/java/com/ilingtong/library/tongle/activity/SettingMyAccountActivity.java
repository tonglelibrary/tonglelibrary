package com.ilingtong.library.tongle.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.adapter.MyAccountListAdaper;
import com.ilingtong.library.tongle.protocol.MyAccountInfo;
import com.ilingtong.library.tongle.protocol.MyPointsResult;
import com.ilingtong.library.tongle.utils.ToastUtils;

import java.util.ArrayList;

/**
 * User: lengjiqiang
 * Date: 2015/5/9
 * Time: 10:37
 * Email: jqleng@isoftstone.com
 * Dest: 我的账户
 */
public class SettingMyAccountActivity extends BaseActivity implements View.OnClickListener{
    private ImageView arrow;
    private TextView top_name;
    private TextView top_btn_text;
    private String url,title;
    private ListView mLitsView;
    private ArrayList<MyAccountInfo> account_list;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.setting_myaccount);
        initView();
        doRequest();

    }
    private void doRequest(){

        ServiceManager.doMyPointsRequest(TongleAppInstance.getInstance().getUserID(), MyPointsListener(), errorListener());

    }
    /**
     * 初始化
     */
    public void initView(){
        top_name = (TextView) findViewById(R.id.top_name);
        top_btn_text = (TextView) findViewById(R.id.top_btn_text);
        arrow = (ImageView) findViewById(R.id.left_arrow_btn);
        mLitsView = (ListView) findViewById(R.id.mLitsView);
        arrow.setOnClickListener(this);
        //初始化顶部title控件（返回按钮以及标题）
        arrow.setVisibility(View.VISIBLE);
        top_name.setVisibility(View.VISIBLE);
        top_name.setText(getString(R.string.my_count));
        top_btn_text.setVisibility(View.VISIBLE);
        top_btn_text.setText(getString(R.string.setting_my_account_top_txt));
        top_btn_text.setTextSize(12);
        top_btn_text.setOnClickListener(this);
    }
    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.left_arrow_btn){
            finish();
        }else if(v.getId()==R.id.top_btn_text){

            //拼接url
            String myurl=url+"?user_id="+TongleAppInstance.getInstance().getUserID()+"&user_token="+TongleAppInstance.getInstance().getToken();
            Intent expertIntent = new Intent(SettingMyAccountActivity.this, ProdIntegralActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString("prod_url", myurl);
            bundle.putString("myTitle", title);
            expertIntent.putExtras(bundle);
            startActivity(expertIntent);
        }
    }

    /**
     * 功能：我的积分
     */
    private Response.Listener MyPointsListener() {
        return new Response.Listener<MyPointsResult>() {
            @Override
            public void onResponse(MyPointsResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    url=response.getBody().user_account_info.withdraw_url;
                    title=response.getBody().user_account_info.withdraw_title;
                    account_list = response.getBody().user_account_info.account_list;
                    mLitsView.setAdapter(new MyAccountListAdaper(SettingMyAccountActivity.this,response.getBody().user_account_info.account_list));
                } else {
                    ToastUtils.toastShort(getString(R.string.para_exception) + response.getHead().getReturn_message());
                }
            }
        };
    }

    /**
     * 功能：请求网络响应失败
     */
    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ToastUtils.toastShort(getString(R.string.sys_exception));
            }
        };
    }
}