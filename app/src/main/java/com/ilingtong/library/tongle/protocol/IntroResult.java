package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/6/24
 * Time: 11:05
 * Email: jqleng@isoftstone.com
 * Desc: 功能介绍类，通过该对象，被用作网络请求参数
 */
public class IntroResult implements Serializable {
    private BaseInfo head;
    private IntroInfo body;

    public BaseInfo getHead() {
        return head;
    }

    public void setHead(BaseInfo head) {
        this.head = head;
    }

    public IntroInfo getBody() {
        return body;
    }

    public void setBody(IntroInfo body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return body.toString();
    }
}
