package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/5/15
 * Time: 16:47
 * Email: jqleng@isoftstone.com
 * Dest:
 */
public class MStoreListItem implements Serializable{
    public String mstore_id;
    public String mstore_name;
    public String mstore_pic_url;
    public String mstore_favorited_by_me;
}
