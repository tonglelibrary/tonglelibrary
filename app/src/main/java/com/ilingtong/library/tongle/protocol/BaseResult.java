package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * Created by wuqian on 2016/3/11.
 * mail: wuqian@ilingtong.com
 * Description:接口返回class的父类
 * 所有json都会返回head块。这里做成共通
 */
public class BaseResult implements Serializable{
    BaseInfo head;

    public BaseInfo getHead() {
        return head;
    }

    public void setHead(BaseInfo head) {
        this.head = head;
    }
}
