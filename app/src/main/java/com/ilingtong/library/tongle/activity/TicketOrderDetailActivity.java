package com.ilingtong.library.tongle.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.adapter.GroupTicketListAdapter;
import com.ilingtong.library.tongle.external.MyListView;
import com.ilingtong.library.tongle.protocol.CouponInfo;
import com.ilingtong.library.tongle.protocol.GroupOrderDetailResult;
import com.ilingtong.library.tongle.utils.DialogUtils;
import com.ilingtong.library.tongle.utils.FontUtils;
import com.ilingtong.library.tongle.utils.ToastUtils;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.io.Serializable;
import java.util.List;

/**
 * author: liuting
 * Date: 2016/3/9
 * Time: 10:00
 * Email: liuting@ilingtong.com
 * Desc:订单详情
 */
public class TicketOrderDetailActivity extends BaseActivity implements View.OnClickListener {
    private TextView mTxtTitle;//标题
    private ImageView mImgBack;//返回图标

    private ImageView mImgIcon;//商品图片
    private TextView mTxtName;//商品名称
    private TextView mTxtNum;//商品数量
    private TextView mTxtPrice;//商品价格
    private ImageView mImgMore;//更多图标
    private RelativeLayout mRlyProd;//商品详情一栏

    private MyListView mLvUnusedTicket;//未消费团购券列表
    private MyListView mLvRefundingTicket;//退款中团购券列表
    private MyListView mLvRefundTicket;//退款团购券列表
    private MyListView mLvUsedTicket;//消费团购券列表
    private MyListView mLvExpireTicket;//过期团购券列表

    private LinearLayout mLlyUnused;//可用列表块
    private LinearLayout mLlyRefunding;//退款中列表块
    private LinearLayout mLlyRefund;//退款列表块
    private LinearLayout mLlyUsed;//消费列表块
    private LinearLayout mLlyExpire;//过期列表块

    private RelativeLayout mRlyUnused;//可用列表块头部
    private RelativeLayout mRlyRefunding;//退款中列表块头部
    private RelativeLayout mRlyRefund;//退款列表块头部
    private RelativeLayout mRlyUsed;//消费列表块头部
    private RelativeLayout mRlyExpire;//过期列表块头部

    private TextView mTxtUnused;//可用列表头部信息
    private TextView mTxtRefunding;//退款中列表头部信息
    private TextView mTxtRefund;//退款列表头部信息
    private TextView mTxtUsed;//消费列表头部信息
    private TextView mTxtExpire;//过期列表头部信息

    private TextView mTxtDateUnused;//订单日期
    private TextView mTxtDateRefunding;//退款中日期
    private TextView mTxtDateRefund;//退款日期
    private TextView mTxtDateUsed;//使用日期
    private TextView mTxtDateExpire;//过期日期

    private RelativeLayout mRlyLine;//垂直虚线
    private ImageView mImgQr;//二维码图标

    private TextView mTxtNo;//订单编号
    private TextView mTxtTime;//下单时间

    private ScrollView mSvMain;//内容主控件

    private GroupOrderDetailResult mGroupOrderDetailResult;//订单详情返回结果
    private Dialog mDialog;//加载对话框
    private String mOrderNo;//订单编号
    private AlertDialog mAlert;//显示一维码和二维码dialog
    private GroupTicketListAdapter mGroupTicketListAdapter;//券号列表

    private TextView txt_top_evaluate;//订单可评价时显示
    private boolean isEvaluate = false;  //是否评价过标志


    /**
     * @param context
     * @param order_no 订单号
     */
    public static void launcher(Activity context, String order_no) {
        Intent intent = new Intent(context, TicketOrderDetailActivity.class);
        intent.putExtra("order_no", (Serializable) order_no);
        context.startActivityForResult(intent, TongleAppConst.REQUEST_CODE_SUBMIT);
    }

    /**
     * @param context
     * @param order_no 订单号
     */
    public static void launcher(Fragment context, String order_no, int requestCode) {
        Intent intent = new Intent(context.getActivity(), TicketOrderDetailActivity.class);
        intent.putExtra("order_no", (Serializable) order_no);
        context.startActivityForResult(intent, requestCode);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ticket_order_detail);
        initView();
        getData(mOrderNo);
    }

    public void initView() {
        mTxtTitle = (TextView) findViewById(R.id.top_name);
        mImgBack = (ImageView) findViewById(R.id.left_arrow_btn);
        findViewById(R.id.QR_share_btn).setVisibility(View.GONE);
        mTxtTitle.setText(getResources().getString(R.string.ticket_order_detail_top_name));
        mTxtTitle.setVisibility(View.VISIBLE);
        mImgBack.setVisibility(View.VISIBLE);
        mImgBack.setOnClickListener(this);

        mImgIcon = (ImageView) findViewById(R.id.ticket_order_detail_img_icon);
        mTxtName = (TextView) findViewById(R.id.ticket_order_detail_tv_name);
        mTxtPrice = (TextView) findViewById(R.id.ticket_order_detail_tv_price);
        mTxtNum = (TextView) findViewById(R.id.ticket_order_detail_tv_num);
        mImgMore = (ImageView) findViewById(R.id.ticket_order_detail_img_more);
        mRlyProd = (RelativeLayout) findViewById(R.id.ticket_order_detail_rly_prod);
        mRlyProd.setOnClickListener(this);

        txt_top_evaluate = (TextView) findViewById(R.id.top_btn_text);

        mTxtNo = (TextView) findViewById(R.id.ticket_order_detail_tv_no);
        mTxtTime = (TextView) findViewById(R.id.ticket_order_detail_tv_time);

        mSvMain = (ScrollView) findViewById(R.id.ticket_order_detail_sv_main);

        mDialog = DialogUtils.createLoadingDialog(TicketOrderDetailActivity.this);
        mDialog.setCancelable(false);

        mOrderNo = getIntent().getExtras().getString("order_no");
    }

    /**
     * 初始化可用列表块
     */
    public void initUnused() {
        mLlyUnused = (LinearLayout) findViewById(R.id.ticket_order_detail_lly_unused);
        mImgQr = (ImageView) mLlyUnused.findViewById(R.id.my_ticket_list_item_img_qr);
        mImgQr.setVisibility(View.VISIBLE);
        mImgQr.setOnClickListener(this);
        mRlyUnused = (RelativeLayout) mLlyUnused.findViewById(R.id.my_ticket_rly_top);
        mRlyUnused.setBackgroundResource(R.drawable.tuangou_coupon_top);
        mLvUnusedTicket = (MyListView) mLlyUnused.findViewById(R.id.my_ticket_lv_list);
        mTxtUnused = (TextView) mLlyUnused.findViewById(R.id.my_ticket_tv_product);
        mTxtDateUnused = (TextView) mLlyUnused.findViewById(R.id.my_ticket_tv_time);
        mRlyLine = (RelativeLayout) mLlyUnused.findViewById(R.id.my_ticket_list_item_rly_line);

        if (mGroupOrderDetailResult.getBody().getUnused_coupon_list().size() > 0) {//有可用的团购券显示列表块
            mLlyUnused.setVisibility(View.VISIBLE);
            mGroupTicketListAdapter = new GroupTicketListAdapter(TicketOrderDetailActivity.this, mGroupOrderDetailResult.getBody().getUnused_coupon_list(), TongleAppConst.GROUP_TICKET_NOT_USED);
            mLvUnusedTicket.setAdapter(mGroupTicketListAdapter);
            mTxtUnused.setText(String.format(getResources().getString(R.string.ticket_order_detail_unused), mGroupOrderDetailResult.getBody().getUnused_coupon_list().size()));
            mTxtDateUnused.setText(mGroupOrderDetailResult.getBody().getUnused_coupon_list().get(0).getOperator_date());
            DialogUtils.setLineHeight(mRlyLine, TongleAppConst.IMG_LINE_HEIGHT * mGroupOrderDetailResult.getBody().getUnused_coupon_list().size());
        } else {//没有则不显示
            mLlyUnused.setVisibility(View.GONE);
        }
    }

    /**
     * 初始化退款中列表块
     */
    public void initRefunding() {
        mLlyRefunding = (LinearLayout) findViewById(R.id.ticket_order_detail_lly_refunding);
        mRlyRefunding = (RelativeLayout) mLlyRefunding.findViewById(R.id.my_ticket_rly_top);
        mRlyRefunding.setBackgroundResource(R.drawable.used_coupon_top);
        mLvRefundingTicket = (MyListView) mLlyRefunding.findViewById(R.id.my_ticket_lv_list);
        mTxtRefunding = (TextView) mLlyRefunding.findViewById(R.id.my_ticket_tv_product);
        mTxtDateRefunding = (TextView) mLlyRefunding.findViewById(R.id.my_ticket_tv_time);

        if (mGroupOrderDetailResult.getBody().getRefunding_coupon_list() != null && mGroupOrderDetailResult.getBody().getRefunding_coupon_list().size() > 0) {
            //有退款中的团购券显示列表块
            mLlyRefunding.setVisibility(View.VISIBLE);
            mGroupTicketListAdapter = new GroupTicketListAdapter(TicketOrderDetailActivity.this, mGroupOrderDetailResult.getBody().getRefunding_coupon_list(), TongleAppConst.TICKET_STATUS_REFUNDING);
            mLvRefundingTicket.setAdapter(mGroupTicketListAdapter);
            mTxtRefunding.setText(getResources().getString(R.string.ticket_order_detail_refunding));
            mTxtDateRefunding.setText(mGroupOrderDetailResult.getBody().getRefunding_coupon_list().get(0).getOperator_date());
        } else {//没有则不显示
            mLlyRefunding.setVisibility(View.GONE);
        }
    }


    /**
     * 初始化退款列表块
     */
    public void initRefund() {
        mLlyRefund = (LinearLayout) findViewById(R.id.ticket_order_detail_lly_refund);
        mRlyRefund = (RelativeLayout) mLlyRefund.findViewById(R.id.my_ticket_rly_top);
        mRlyRefund.setBackgroundResource(R.drawable.used_coupon_top);
        mLvRefundTicket = (MyListView) mLlyRefund.findViewById(R.id.my_ticket_lv_list);
        mTxtRefund = (TextView) mLlyRefund.findViewById(R.id.my_ticket_tv_product);
        mTxtDateRefund = (TextView) mLlyRefund.findViewById(R.id.my_ticket_tv_time);

        if (mGroupOrderDetailResult.getBody().getRefund_coupon_list().size() > 0) {
            //有退款的团购券显示列表块
            mLlyRefund.setVisibility(View.VISIBLE);
            mGroupTicketListAdapter = new GroupTicketListAdapter(TicketOrderDetailActivity.this, mGroupOrderDetailResult.getBody().getRefund_coupon_list(), TongleAppConst.TICKET_STATUS_REFUND);
            mLvRefundTicket.setAdapter(mGroupTicketListAdapter);
            mTxtRefund.setText(String.format(getResources().getString(R.string.ticket_order_detail_refund), FontUtils.setTwoDecimal(sumAmount(mGroupOrderDetailResult.getBody().getRefund_coupon_list()))));
            mTxtDateRefund.setText(mGroupOrderDetailResult.getBody().getRefund_coupon_list().get(0).getOperator_date());
        } else {//没有则不显示
            mLlyRefund.setVisibility(View.GONE);
        }
    }

    /**
     * 初始化消费列表块
     */
    public void initUsed() {
        mLlyUsed = (LinearLayout) findViewById(R.id.ticket_order_detail_lly_used);
        mRlyUsed = (RelativeLayout) mLlyUsed.findViewById(R.id.my_ticket_rly_top);
        mRlyUsed.setBackgroundResource(R.drawable.used_coupon_top);
        mLvUsedTicket = (MyListView) mLlyUsed.findViewById(R.id.my_ticket_lv_list);
        mTxtUsed = (TextView) mLlyUsed.findViewById(R.id.my_ticket_tv_product);
        mTxtDateUsed = (TextView) mLlyUsed.findViewById(R.id.my_ticket_tv_time);

        if (mGroupOrderDetailResult.getBody().getUsed_coupon_list().size() > 0) {
            //有已消费的团购券显示列表块
            mLlyUsed.setVisibility(View.VISIBLE);
            mGroupTicketListAdapter = new GroupTicketListAdapter(TicketOrderDetailActivity.this, mGroupOrderDetailResult.getBody().getUsed_coupon_list(), TongleAppConst.TICKET_STATUS_USED);
            mLvUsedTicket.setAdapter(mGroupTicketListAdapter);
            mTxtUsed.setText(getResources().getString(R.string.ticket_order_detail_used));
            mTxtDateUsed.setText(mGroupOrderDetailResult.getBody().getUsed_coupon_list().get(0).getOperator_date());
        } else {//没有则不显示
            mLlyUsed.setVisibility(View.GONE);
        }
    }

    /**
     * 初始化过期列表块
     */
    public void initExpire() {
        mLlyExpire = (LinearLayout) findViewById(R.id.ticket_order_detail_lly_expire);
        mRlyExpire = (RelativeLayout) mLlyExpire.findViewById(R.id.my_ticket_rly_top);
        mRlyExpire.setBackgroundResource(R.drawable.used_coupon_top);
        mLvExpireTicket = (MyListView) mLlyExpire.findViewById(R.id.my_ticket_lv_list);
        mTxtExpire = (TextView) mLlyExpire.findViewById(R.id.my_ticket_tv_product);
        mTxtDateExpire = (TextView) mLlyExpire.findViewById(R.id.my_ticket_tv_time);

        if (mGroupOrderDetailResult.getBody().getExpire_coupon_list().size() > 0) {
            //有已过期的团购券显示列表块
            mLlyExpire.setVisibility(View.VISIBLE);
            mGroupTicketListAdapter = new GroupTicketListAdapter(TicketOrderDetailActivity.this, mGroupOrderDetailResult.getBody().getExpire_coupon_list(), TongleAppConst.GROUP_TICKET_OUT_OF);
            mLvExpireTicket.setAdapter(mGroupTicketListAdapter);
            mTxtExpire.setText(getResources().getString(R.string.ticket_order_detail_expire));
            mTxtDateExpire.setText(mGroupOrderDetailResult.getBody().getExpire_coupon_list().get(0).getOperator_date());
        } else {//没有则不显示
            mLlyExpire.setVisibility(View.GONE);
        }
    }

    /**
     * 计算总金额
     *
     * @param couponInfoList 团购券信息
     * @return double 金额总数
     */
    public double sumAmount(List<CouponInfo> couponInfoList) {
        double sum = 0;
        for (int i = 0; i < couponInfoList.size(); i++) {
            sum = sum + couponInfoList.get(i).getAmount();
        }
        return sum;
    }

    /**
     * 取得数据
     */
    public void getData(String orderNo) {
        mDialog.show();
        ServiceManager.doMyGroupOrderDetailRequest(TongleAppInstance.getInstance().getUserID(), orderNo, getSuccessListener(), new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                mDialog.dismiss();
                ToastUtils.toastShort(R.string.sys_exception);
                mSvMain.setVisibility(View.GONE);
            }
        });

    }

    /**
     * 请求接口成功回调
     *
     * @return
     */
    private Response.Listener<GroupOrderDetailResult> getSuccessListener() {
        return new Response.Listener<GroupOrderDetailResult>() {
            @Override
            public void onResponse(GroupOrderDetailResult groupOrderDetailResult) {
                mDialog.dismiss();
                if (TongleAppConst.SUCCESS.equals(groupOrderDetailResult.getHead().getReturn_flag())) {
                    mGroupOrderDetailResult = groupOrderDetailResult;
                    mSvMain.setVisibility(View.VISIBLE);
                    initData();
                } else {
                    ToastUtils.toastShort(R.string.para_exception + groupOrderDetailResult.getHead().getReturn_message());
                    mSvMain.setVisibility(View.GONE);
                }
            }
        };
    }

    /**
     * 初始化数据
     */
    public void initData() {
        if (TongleAppConst.YES.equals(mGroupOrderDetailResult.getBody().getOrder_base().getEvaluate_flag())) {
            txt_top_evaluate.setText(getString(R.string.ticket_order_detail_top_evaluate));
            txt_top_evaluate.setVisibility(View.VISIBLE);
            txt_top_evaluate.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
            txt_top_evaluate.setOnClickListener(this);
        }
        mTxtName.setText(mGroupOrderDetailResult.getBody().getOrder_base().getProd_name());
        mTxtPrice.setText(String.format(getResources().getString(R.string.group_order_amount_price_txt), FontUtils.setTwoDecimal(mGroupOrderDetailResult.getBody().getOrder_base().getOrder_amount())));
        mTxtNum.setText(String.format(getResources().getString(R.string.group_order_order_qty_txt), mGroupOrderDetailResult.getBody().getOrder_base().getOrder_qty()));

        mTxtNo.setText(mOrderNo);
        mTxtTime.setText(mGroupOrderDetailResult.getBody().getOrder_base().getOrder_create_time());

        ImageLoader.getInstance().displayImage(mGroupOrderDetailResult.getBody().getOrder_base().getProd_pic_url_list().get(0).pic_url, mImgIcon, TongleAppInstance.options);

        initUnused();
        initRefunding();
        initRefund();
        initUsed();
        initExpire();
    }

    @Override
    public void onClick(View v) {
//        switch (v.getId()) {
//            case R.id.left_arrow_btn://结束当前页
//                finish();
//                break;
//            case R.id.my_ticket_list_item_img_qr://显示一维码和二维码dialog
//                mAlert = DialogUtils.showQrDialog(TicketOrderDetailActivity.this, mGroupOrderDetailResult.getBody().getCoupon_codeurl_info().getCoupon_1d_qrcode_url(), mGroupOrderDetailResult.getBody().getCoupon_codeurl_info().getCoupon_2d_qrcode_url());
//                break;
//            case R.id.ticket_order_detail_rly_prod://前往商品详情
//                ProductTicketDetailActivity.launch(TicketOrderDetailActivity.this, mGroupOrderDetailResult.getBody().getOrder_base().getProd_id(), TongleAppConst.ACTIONID_ORDER, mGroupOrderDetailResult.getBody().getOrder_base().getRelation_id(), "", "");
//                break;
//            case R.id.top_btn_text:
//                EvaluateActivity.luanchForResult(this, mGroupOrderDetailResult.getBody().getOrder_base().getOrder_no(), mGroupOrderDetailResult.getBody().getOrder_base().getOrder_no(), mGroupOrderDetailResult.getBody().getOrder_base().getProd_id(), 10001);
//                break;
//
//        }

        if (v.getId()==R.id.left_arrow_btn) {//结束当前页
            finish();
        }else if(v.getId()==R.id.my_ticket_list_item_img_qr){//显示一维码和二维码dialog
            mAlert = DialogUtils.showQrDialog(TicketOrderDetailActivity.this, mGroupOrderDetailResult.getBody().getCoupon_codeurl_info().getCoupon_1d_qrcode_url(), mGroupOrderDetailResult.getBody().getCoupon_codeurl_info().getCoupon_2d_qrcode_url());
        }else if(v.getId()==R.id.ticket_order_detail_rly_prod){//前往商品详情
            ProductTicketDetailActivity.launch(TicketOrderDetailActivity.this, mGroupOrderDetailResult.getBody().getOrder_base().getProd_id(), TongleAppConst.ACTIONID_ORDER, mGroupOrderDetailResult.getBody().getOrder_base().getRelation_id(), "", "");
        }else if(v.getId()==R.id.top_btn_text){//评价
            EvaluateActivity.luanchForResult(this, mGroupOrderDetailResult.getBody().getOrder_base().getOrder_no(), mGroupOrderDetailResult.getBody().getOrder_base().getOrder_no(), mGroupOrderDetailResult.getBody().getOrder_base().getProd_id(), 10001);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && requestCode == 10001) {
            txt_top_evaluate.setVisibility(View.GONE);
            isEvaluate = true;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void finish() {
        if (isEvaluate) {
            setResult(RESULT_OK);
        }
        super.finish();
    }
}
