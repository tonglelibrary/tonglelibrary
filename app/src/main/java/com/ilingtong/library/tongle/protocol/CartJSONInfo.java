package com.ilingtong.library.tongle.protocol;

import java.util.ArrayList;

/**
 * User: shuailei
 * Date: 2015/6/24
 * Time: 22:05
 * Email: leishuai@isoftstone.com
 * Desc: 订单详细信息类型
 */
public class CartJSONInfo {
    //订单信息（商品明细列表（明细序号，商品ID)
    public ArrayList<CarttListInfo> product_info;
}
