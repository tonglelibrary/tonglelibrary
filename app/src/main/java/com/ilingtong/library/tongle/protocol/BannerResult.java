package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/6/2
 * Time: 17:54
 * Email: jqleng@isoftstone.com
 * Desc:
 */
public class BannerResult implements Serializable {
    private BaseInfo head;
    private BannerInfo body;

    public BaseInfo getHead() {
        return head;
    }

    public void setHead(BaseInfo head) {
        this.head = head;
    }

    public BannerInfo getBody() {
        return body;
    }

    public void setBody(BannerInfo body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return body.toString();
    }
}
