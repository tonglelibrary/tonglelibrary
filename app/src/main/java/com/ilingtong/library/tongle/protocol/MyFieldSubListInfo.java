package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * User: lengjiqiang
 * Date: 2015/6/9
 * Time: 22:05
 * Email: jqleng@isoftstone.com
 * Desc:
 */
public class MyFieldSubListInfo implements Serializable {
    private String type_id;
    private String type_name;
    private String desc;
    private String pic_url;
    private  ArrayList<MyFieldSubListInfo> sub_list;

    public ArrayList getSub_list() {
        return sub_list;
    }

    public String getType_id() {
        return type_id;
    }

    public String getType_name() {
        return type_name;
    }

    public String getDesc() {
        return desc;
    }

    public String getPic_url() {
        return pic_url;
    }

    @Override
    public String toString() {
        return "type_id:" + type_id + "\r\n" +
                "type_name:" + type_name + "\r\n" +
                "desc:" + desc + "\r\n" +
                "pic_url:" + pic_url + "\r\n" +
                "sub_list:" + sub_list;
    }
}
