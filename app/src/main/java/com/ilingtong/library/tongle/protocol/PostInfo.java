package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/5/21
 * Time: 17:16
 * Email: jqleng@isoftstone.com
 * Desc:  [1022]功能点返回参数类
 */
public class PostInfo implements Serializable {
    public String post_id;
    public String my_post_flag;
    public String post_comment;
    public String prod_link_url;
}
