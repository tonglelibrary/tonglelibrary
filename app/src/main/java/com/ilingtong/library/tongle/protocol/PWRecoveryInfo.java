package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/6/17
 * Time: 16:35
 * Email: jqleng@isoftstone.com
 * Desc: 密码找回信息类
 */
public class PWRecoveryInfo implements Serializable {
    private String token;

    public void setToken(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    @Override
    public String toString() {
        return "token:" + token;
    }
}
