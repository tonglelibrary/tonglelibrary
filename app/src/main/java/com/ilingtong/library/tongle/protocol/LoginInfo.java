package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/6/2
 * Time: 17:54
 * Email: jqleng@isoftstone.com
 * Desc: 登录信息类，包含服务器返回的数据
 */
public class LoginInfo implements Serializable {
    private String user_id;
    private String user_type;
    private String is_first_login;
    private String require_change_pwd;
    private String user_nick_name;
    private String user_head_photo_url;
    private String user_sex;
    private String token;

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void setUser_type(String user_type) {
        this.user_type = user_type;
    }

    public void setIs_first_login(String is_first_login) {
        this.is_first_login = is_first_login;
    }

    public void setRequire_change_pwd(String require_change_pwd) {
        this.require_change_pwd = require_change_pwd;
    }

    public void setUser_nick_name(String user_nick_name) {
        this.user_nick_name = user_nick_name;
    }

    public void setUser_head_photo_url(String user_head_photo_url) {
        this.user_head_photo_url = user_head_photo_url;
    }

    public String getUser_head_photo_url() {
        return user_head_photo_url;
    }
    public String getIs_first_login() {
        return is_first_login;
    }

    public void setUser_sex(String user_sex) {
        this.user_sex = user_sex;
    }

    public String getUserID() {
        return user_id;
    }

    public String getToken() {
        return token;
    }

    @Override
    public String toString() {
        return "user_id:" + user_id + "\r\n" +
                "user_type:" + user_type + "\r\n" +
                "is_first_login:" + is_first_login + "\r\n" +
                "require_change_pwd:" + require_change_pwd + "\r\n" +
                "user_nick_name:" + user_nick_name + "\r\n" +
                "user_head_photo_url:" + user_head_photo_url + "\r\n" +
                "user_sex:" + user_sex + "\r\n" +
                "token:" + token;
    }
}
