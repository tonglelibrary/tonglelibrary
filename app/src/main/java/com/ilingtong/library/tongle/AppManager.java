package com.ilingtong.library.tongle;

import java.util.List;
import java.util.Stack;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;

/**
 * 应用程序Activity管理类用于Activity管理和应用程序退出
 * @author wuqian
 * @created
 */
public class AppManager {
	private static Stack<Activity> activityStack;
	private static Stack<Activity> activityRecordStack;
	private static AppManager instance;
	
	private AppManager(){}
	/**
	 * 单一实例
	 */
	public static AppManager getAppManager(){
		if(instance==null){
			instance=new AppManager();
		}
		return instance;
	}
	/**
	 * 添加Activity到堆栈
	 */
	public void addActivity(Activity activity){
		if(activityStack==null){
			activityStack=new Stack<Activity>();
			activityRecordStack = new Stack<Activity>();
		}
		activityStack.add(activity);
		activityRecordStack.add(activity);
	}
	/**
	 * 获取当前Activity（堆栈中最后一个压入的）
	 */
	public Activity currentActivity(){
		if(activityStack != null){
			Activity activity=activityStack.lastElement();
			return activity;
		}
		return null;
	}
	/**
	 * 结束当前Activity（堆栈中最后一个压入的）
	 */
	public void finishActivity(){
		Activity activity=activityStack.lastElement();
		finishActivity(activity);
	}
	/**
	 * 结束指定的Activity
	 */
	public void finishActivity(Activity activity){
		if(activity!=null){
			activityStack.remove(activity);
			activity.finish();
			activity=null;
		}
	}
	
	public void removeActivity(Activity activity){
		if(activity!=null){
			activityStack.remove(activity);
			activity=null;
		}
	}
	public void finishActivity2(Class<?> cls){
		for (int i = 0; i < activityStack.size(); i++) {
			Activity activity = activityStack.get(i);
			if(activity.getClass().equals(cls) ){
				finishActivity(activity);
				break;
			}
		}
	}
	
	/**
	 * 结束指定类名的Activity
	 */
	public void finishActivity(Class<?> cls){
		for (Activity activity : activityStack) {
			if(activity.getClass().equals(cls) ){
				finishActivity(activity);
			}
		}
	}
	/**
	 * 结束所有Activity
	 */
	public void finishAllActivity(){
		for (int i = 0, size = activityStack.size(); i < size; i++){
            if (null != activityStack.get(i)){
            	activityStack.get(i).finish();
            }
	    }
		activityStack.clear();
	}
	/**
	 * 退出应用程序
	 */
	public void AppExit(Context context) {
		try {
			finishAllActivity();
			ActivityManager activityMgr= (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
			activityMgr.restartPackage(context.getPackageName());
			System.exit(0);
		} catch (Exception e) {	}
	}
	
	/**
	 * 结束当前以外的所有已记录的activity
	 */
	public void finishAllRecordActivityExpcetCurrent(Class<?> cls){
		Activity currentActivity = null;
		for (int i = 0; i < activityRecordStack.size(); i++) {
			Activity activity = activityRecordStack.get(i);
			if(activity.getClass().equals(cls) ){
				currentActivity = activity;
				continue;
			}
			finishActivity(activity);
		}
		activityRecordStack.clear();
		activityStack.clear();
		if(null != currentActivity){
			activityRecordStack.add(currentActivity);
			activityStack.add(currentActivity);
		}
	}
	
	/**
	 * 结束当前以外的所有activity
	 */
	public void finishAllActivityExpcetCurrent(Class<?> cls){
		Activity currentActivity = null;
		for (int i = 0; i < activityStack.size(); i++) {
			Activity activity = activityStack.get(i);
			if(activity.getClass().equals(cls) ){
				currentActivity = activity;
				continue;
			}
			finishActivity(activity);
		}
		activityRecordStack.clear();
		activityStack.clear();
		if(null != currentActivity){
			activityRecordStack.add(currentActivity);
			activityStack.add(currentActivity);
		}
	}
	
	/**
	 * 结束给定集合内的activity
	 * @param list
	 */
	public void finishActivity(List<Class<?>> list){
		if(null == list){
			return;
		}
		Class<?> cls = null;
		for (int i = 0; i < list.size(); i++) {
			cls = list.get(i);
			for (Activity activity : activityStack) {
				if(activity.getClass().equals(cls)){
					finishActivity(activity);
				}
			}
		}
	}
}