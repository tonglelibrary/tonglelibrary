package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: syc
 * Date: 2015/6/19
 * Time: 14:18
 * Email: ycshi@isoftstone.com
 * Desc: [2014]
 */
public class MyProceedsResult implements Serializable {

    private BaseInfo head;
    private MyProceedsInfo body;

    public BaseInfo getHead() {
        return head;
    }

    public void setHead(BaseInfo head) {
        this.head = head;
    }

    public MyProceedsInfo getBody() {
        return body;
    }

    public void setBody(MyProceedsInfo body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return body.toString();
    }
}
