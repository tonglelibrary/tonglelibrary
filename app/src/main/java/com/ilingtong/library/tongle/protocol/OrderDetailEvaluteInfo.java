package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: shuailei
 * Date: 2015/6/10
 * Time: 13:01
 * Email: leishuai@isoftstone.com
 * Desc: 订单评价需要的入口参数类
 */
public class OrderDetailEvaluteInfo implements Serializable {
    private String order_no;//订单编号
    private String order_detail_no;//订单详情编号
    private String product_id;//产品id
    private String level;//满意度
    private String memo;//评价内容

    public String getOrder_no() {
        return order_no;
    }

    public void setOrder_no(String order_no) {
        this.order_no = order_no;
    }

    public String getOrder_detail_no() {
        return order_detail_no;
    }

    public void setOrder_detail_no(String order_detail_no) {
        this.order_detail_no = order_detail_no;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }
}
