package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/6/18
 * Time: 12:06
 * Email: jqleng@isoftstone.com
 * Desc:
 */
public class ResetPwdResult implements Serializable {

    private BaseInfo head;
    private ResetPwdInfo body;

    public BaseInfo getHead() {
        return head;
    }

    public void setHead(BaseInfo head) {
        this.head = head;
    }

    public ResetPwdInfo getBody() {
        return body;
    }

    public void setBody(ResetPwdInfo body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return body.toString();
    }

}
