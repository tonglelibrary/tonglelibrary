package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/5/25
 * Time: 15:27
 * Email: jqleng@isoftstone.com
 * Dest:
 */
public class ProductPicTextListItem implements Serializable {
    public String pic_url;
    public String pic_memo;
}
