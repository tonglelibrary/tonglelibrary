package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/7/7
 * Time: 14:44
 * Email: jqleng@isoftstone.com
 * Desc: 添加新地址中需要的地址属性类
 */
public class AddInfo implements Serializable{
    public String consignee;
    public String tel;
    public String province_id;
    public String city_id;
    public String area_id;
    public String address;
    public String post_code;

}
