package com.ilingtong.library.tongle.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.protocol.DataListItem;

import java.util.List;

/**
 * Created by wuqian on 2016/3/29.
 * mail: wuqian@ilingtong.com
 * Description:支付方式选择列表的adapter
 */
public class PayTypeListAdapter extends BaseAdapter{
    private List<DataListItem> list;
    private Context context;

    public PayTypeListAdapter(List<DataListItem> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null){
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.select_pay_type_list_item,null);
            viewHolder.img_logo = (ImageView) convertView.findViewById(R.id.select_pay_type_img_type);
            viewHolder.txt_name = (TextView) convertView.findViewById(R.id.select_pay_type_txt_name);
            convertView.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.txt_name.setText(list.get(position).getName());
        switch (Integer.parseInt(list.get(position).getCode())){
            case TongleAppConst.PAY_TYPE_INT_WECHAT:  //微信
                viewHolder.img_logo.setImageResource(R.drawable.icon_wechat);
                break;
            case TongleAppConst.PAY_TYPE_INT_ALIPAY:  //支付宝
                viewHolder.img_logo.setImageResource(R.drawable.icon_alipay);
                break;
            case TongleAppConst.PAY_TYPE_INT_CASH:  //货到付款
                viewHolder.img_logo.setImageResource(R.drawable.icon_huodaofukuan);
                break;
            default:
                break;
        }
        return convertView;
    }

    class ViewHolder{
        ImageView img_logo;  //支付方式logo；
        TextView txt_name;   //支付方式名称
    }
}
