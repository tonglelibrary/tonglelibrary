package com.ilingtong.library.tongle.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.adapter.AttentionAdaper;
import com.ilingtong.library.tongle.external.maxwin.view.XListView;
import com.ilingtong.library.tongle.protocol.ExpertRequestParam;
import com.ilingtong.library.tongle.protocol.FriendListItem;
import com.ilingtong.library.tongle.protocol.MyAttentionInfo;
import com.ilingtong.library.tongle.protocol.ParametersJson;
import com.ilingtong.library.tongle.utils.ToastUtils;

import java.util.ArrayList;

/**
 * User: shuailei
 * Date: 2015/6/25
 * Time: 10:45
 * Email: leishuai@isoftstone.com
 * Dest:  我的关注
 */
public class MyAttentionFragment extends BaseFragment implements XListView.IXListViewListener {
    private XListView mExpertList;
    private AttentionAdaper mExpertListAdaper;
    private ProgressDialog pDlg;
    private MyAttentionInfo info;
    private boolean flag = true;
    private ArrayList<FriendListItem> user_follow_friend_list = new ArrayList<>();

    Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    mExpertListAdaper.notifyDataSetChanged();
                    break;
            }
        }
    };

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.collect_forum_layout, container, false);
        user_follow_friend_list.clear();
        initView(rootView);
        updateListView();
        flag = true;
        doRequest();
        return rootView;
    }

    public void initView(View rootView ) {
        //实例化，为空时创建
        if (info == null) {
            info = new MyAttentionInfo();
        }
        //创建并弹出“加载中Dialog”
        pDlg = new ProgressDialog(getActivity());
        pDlg.setCancelable(false);
        pDlg.setMessage(getString(R.string.loading));
        pDlg.show();
        mExpertList = (XListView) rootView.findViewById(R.id.product_listview);
    }

    public void doRequest() {
        //实例化，准备请求数据时参数
        ExpertRequestParam param = new ExpertRequestParam();
        param.user_id = TongleAppInstance.getInstance().getUserID();
        param.fetch_count = TongleAppConst.FETCH_COUNT;

        ServiceManager.requestCollectExpert(param, successListener(), errorListener());
    }

    @Override
    public void onRefresh(int id) {
        if (user_follow_friend_list!=null){
            user_follow_friend_list.clear();
            doRequest();
        }
    }

    @Override
    public void onLoadMore(int id) {
        //加载更多
        if (flag) {
            ExpertRequestParam param = new ExpertRequestParam();
            param.user_id = TongleAppInstance.getInstance().getUserID();
            param.fetch_count = TongleAppConst.FETCH_COUNT;
            param.forward = TongleAppConst.FORWORD_DONW;
            param.expert_user_id = user_follow_friend_list.get(user_follow_friend_list.size() - 1).user_id;
            ServiceManager.requestCollectExpert(param, successListener(), errorListener());
        } else {
            ToastUtils.toastShort(getString(R.string.common_list_end));
            mExpertList.setPullLoadEnable(false);
        }
    }
    /**
     * 功能：网络响应成功，返回数据
     */
    private Response.Listener<ParametersJson> successListener() {
        return new Response.Listener<ParametersJson>() {
            @Override
            public void onResponse(ParametersJson response) {
                //判断返回数据是否成功
                if (TongleAppConst.SUCCESS.equals(response.head.return_flag)) {
                    info.data_total_count = response.body.data_total_count;
                    info.user_follow_friend_list = response.body.user_follow_friend_list;
                    //第一次请求或刷新页面时候判断是否隐藏底部加载更多按钮
                    if (response.body.user_follow_friend_list.size()<Integer.parseInt(TongleAppConst.FETCH_COUNT)) {
                        flag = false;
                        mExpertList.setPullLoadEnable(false);
                    } else {
                        mExpertList.setPullLoadEnable(true);
                    }
                    user_follow_friend_list.addAll(response.body.user_follow_friend_list);
                    mHandler.sendEmptyMessage(0);
                } else {
                    ToastUtils.toastShort(getString(R.string.para_exception) + response.head.return_message);
                }
                pDlg.dismiss();
            }
        };
    }

    /**
     * 功能：网络响应失败
     */
    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ToastUtils.toastShort(getString(R.string.sys_exception));
                pDlg.dismiss();
            }
        };
    }

    private void updateListView() {
        //实例化适配器，并把listview传值过去
        mExpertListAdaper = new AttentionAdaper(getActivity(), user_follow_friend_list);
        mExpertList.setPullLoadEnable(false);
        mExpertList.setPullRefreshEnable(true);
        mExpertList.setXListViewListener(this, 0);
        mExpertList.setRefreshTime();
        mExpertList.setAdapter(mExpertListAdaper);
    }
}
