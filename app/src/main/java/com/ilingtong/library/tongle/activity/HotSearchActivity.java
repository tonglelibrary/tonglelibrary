package com.ilingtong.library.tongle.activity;

import android.app.ProgressDialog;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.adapter.CollectFragmentPagerAdapter;
import com.ilingtong.library.tongle.fragment.MExpertFragment;
import com.ilingtong.library.tongle.fragment.MForumFragment;
import com.ilingtong.library.tongle.fragment.MProductFragment;
import com.ilingtong.library.tongle.fragment.MStoreFragment;
import com.ilingtong.library.tongle.protocol.FriendListItem;
import com.ilingtong.library.tongle.protocol.MStoreListItem;
import com.ilingtong.library.tongle.protocol.ProductListItemData;
import com.ilingtong.library.tongle.protocol.SearchResult;
import com.ilingtong.library.tongle.protocol.UserFollowPostList;
import com.ilingtong.library.tongle.utils.ToastUtils;

import java.util.ArrayList;

/**
 * User: shiyuchong
 * Date: 2015/6/26
 * Time: 9:26
 * Email: ycshi@isoftstone.com
 * Desc: 热门搜索页面
 */
public class HotSearchActivity extends BaseFragmentActivity {
    private View topview;
    private ImageView leftArrowBtn;
    private TextView topname;
    private LinearLayout searchlayout;
    private SearchView search;
    //Tab控件
    private TextView expertText;
    private TextView forumText;
    private TextView storeText;
    private TextView productText;
    private ViewPager viewPager;
    //image
    private ImageView imageView1;
    private ImageView imageView2;
    private ImageView imageView3;
    private ImageView imageView4;

    private ProgressDialog mProgressDialog;
    private Fragment[] fragments = new Fragment[4];
    private int currIndex = 0;
    //目标对应页
    private int target = 0;
    private ArrayList<MStoreListItem> mstore_list = new ArrayList<>();
    private ArrayList<FriendListItem> user_list = new ArrayList<>();
    private ArrayList<UserFollowPostList> post_list = new ArrayList<>();
    private ArrayList<ProductListItemData> prod_list = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_collect_layout);
        initView();
        doRequest();
    }

    public void initView() {
        target = Integer.parseInt(String.valueOf(getIntent().getExtras().get("target")));

        topview = findViewById(R.id.proceeds_top_view);
        topname = (TextView) findViewById(R.id.top_name);
        leftArrowBtn = (ImageView) findViewById(R.id.left_arrow_btn);
        searchlayout = (LinearLayout) findViewById(R.id.ll_search);
        search = (SearchView) findViewById(R.id.search_view);
        expertText = (TextView) findViewById(R.id.expert_text);
        forumText = (TextView) findViewById(R.id.forum_text);
        storeText = (TextView) findViewById(R.id.store_text);
        productText = (TextView) findViewById(R.id.product_text);
        viewPager = (ViewPager) findViewById(R.id.collect_pager);
        imageView1 = (ImageView) findViewById(R.id.collect_cursor1);
        imageView2 = (ImageView) findViewById(R.id.collect_cursor2);
        imageView3 = (ImageView) findViewById(R.id.collect_cursor3);
        imageView4 = (ImageView) findViewById(R.id.collect_cursor4);

        leftArrowBtn.setVisibility(View.VISIBLE);
        leftArrowBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        //设置标题
        topname.setText(getString(R.string.hot_search_top_name));
        topname.setGravity(View.TEXT_ALIGNMENT_CENTER);
        topname.setVisibility(View.VISIBLE);

        expertText.setOnClickListener(new TextOnClickListener(0));
        forumText.setOnClickListener(new TextOnClickListener(1));
        storeText.setOnClickListener(new TextOnClickListener(2));
        productText.setOnClickListener(new TextOnClickListener(3));

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setTitle(getString(R.string.hot_search_dialog_title));
        mProgressDialog.setMessage(getString(R.string.hot_search_dialog_message));

    }

    private void initFragments() {
        fragments[0] = MExpertFragment.newInstance(TongleAppConst.FINDFRAGMENT_INTO, user_list);
        fragments[1] = MForumFragment.newInstance(TongleAppConst.FINDFRAGMENT_INTO, post_list);
        fragments[2] = MStoreFragment.newInstance(TongleAppConst.FINDFRAGMENT_INTO, mstore_list);
        fragments[3] = MProductFragment.newInstance(TongleAppConst.FINDFRAGMENT_INTO, prod_list);
        CollectFragmentPagerAdapter adapter = new CollectFragmentPagerAdapter(getSupportFragmentManager(), fragments);
        viewPager.setOffscreenPageLimit(4);
        viewPager.setAdapter(adapter);
        viewPager.setOnPageChangeListener(new ViewPageOnPageChangeListener());
        viewPager.setCurrentItem(target);
    }

    private class TextOnClickListener implements View.OnClickListener {
        private int index = 0;

        public TextOnClickListener(int i) {
            index = i;
        }

        public void onClick(View v) {
            viewPager.setCurrentItem(index);
        }
    }

    public class ViewPageOnPageChangeListener implements ViewPager.OnPageChangeListener {
        public void onPageScrollStateChanged(int arg0) {
        }

        public void onPageScrolled(int arg0, float arg1, int arg2) {
        }

        public void onPageSelected(int arg0) {
            currIndex = arg0;
            int i = currIndex + 1;
            setTabTitle(i);
        }
    }

    private void doRequest() {
        mProgressDialog.show();
        ServiceManager.hotSearchRequest(TongleAppInstance.getInstance().getUserID(), hotSearchSuccessListener(), errorListener());
    }

    /**
     * 功能：网络响应成功，返回数据 发现热门
     */
    private Response.Listener hotSearchSuccessListener() {
        return new Response.Listener<SearchResult>() {
            @Override
            public void onResponse(SearchResult searchResulte) {
                mProgressDialog.dismiss();
                if (TongleAppConst.SUCCESS.equals(searchResulte.getHead().getReturn_flag())) {
                    if (searchResulte.getBody().getMstore_list().size() > 0) {
                        mstore_list.addAll(searchResulte.getBody().getMstore_list());
                    }
                    if (searchResulte.getBody().getUser_list().size() > 0) {
                        user_list.addAll(searchResulte.getBody().getUser_list());
                    }
                    if (searchResulte.getBody().getPost_list().size() > 0) {
                        post_list.addAll(searchResulte.getBody().getPost_list());
                    }
                    if (searchResulte.getBody().getProd_list().size() > 0) {
                        prod_list.addAll(searchResulte.getBody().getProd_list());
                    }
                    initFragments();
                } else {
                    ToastUtils.toastShort(getString(R.string.para_exception) + searchResulte.getHead().getReturn_message());
                }
            }

        };
    }

    /**
     * 功能：网络响应失败
     */
    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                mProgressDialog.dismiss();
                ToastUtils.toastShort(getString(R.string.sys_exception));
            }
        };
    }

    /**
     * 设置选项卡title的字体颜色
     *
     * @param tabId
     */
    public void setTabTitle(int tabId) {
        //viewpager顶部导航游标，并设置游标显示位置
        if (tabId == 1) {
            imageView1.setVisibility(View.VISIBLE);
            imageView2.setVisibility(View.INVISIBLE);
            imageView3.setVisibility(View.INVISIBLE);
            imageView4.setVisibility(View.INVISIBLE);
        } else if (tabId == 2) {
            imageView1.setVisibility(View.INVISIBLE);
            imageView2.setVisibility(View.VISIBLE);
            imageView3.setVisibility(View.INVISIBLE);
            imageView4.setVisibility(View.INVISIBLE);
        } else if (tabId == 3) {
            imageView1.setVisibility(View.INVISIBLE);
            imageView2.setVisibility(View.INVISIBLE);
            imageView3.setVisibility(View.VISIBLE);
            imageView4.setVisibility(View.INVISIBLE);
        } else if (tabId == 4) {
            imageView1.setVisibility(View.INVISIBLE);
            imageView2.setVisibility(View.INVISIBLE);
            imageView3.setVisibility(View.INVISIBLE);
            imageView4.setVisibility(View.VISIBLE);
        }
        //viewpager顶部文字导航，并设置文字颜色
        Resources resource = (Resources) getResources();
        ColorStateList selectedTextColor = (ColorStateList) resource.getColorStateList(R.color.subcollect_fragment_selecttext_color);
        ColorStateList unselectedTextColor = (ColorStateList) resource.getColorStateList(R.color.subcollect_fragment_text_color);
        if (tabId == 1) {
            expertText.setTextColor(selectedTextColor);
            forumText.setTextColor(unselectedTextColor);
            storeText.setTextColor(unselectedTextColor);
            productText.setTextColor(unselectedTextColor);
        } else if (tabId == 2) {
            expertText.setTextColor(unselectedTextColor);
            forumText.setTextColor(selectedTextColor);
            storeText.setTextColor(unselectedTextColor);
            productText.setTextColor(unselectedTextColor);
        } else if (tabId == 3) {
            expertText.setTextColor(unselectedTextColor);
            forumText.setTextColor(unselectedTextColor);
            storeText.setTextColor(selectedTextColor);
            productText.setTextColor(unselectedTextColor);
        } else if (tabId == 4) {
            expertText.setTextColor(unselectedTextColor);
            forumText.setTextColor(unselectedTextColor);
            storeText.setTextColor(unselectedTextColor);
            productText.setTextColor(selectedTextColor);
        }
    }
}
