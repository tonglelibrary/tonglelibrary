package com.ilingtong.library.tongle;

import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ilingtong.library.tongle.protocol.APPpromotionResult;
import com.ilingtong.library.tongle.protocol.AddConnectionResult;
import com.ilingtong.library.tongle.protocol.AddInfo;
import com.ilingtong.library.tongle.protocol.AddressResult;
import com.ilingtong.library.tongle.protocol.AgreementResult;
import com.ilingtong.library.tongle.protocol.AliPayResult;
import com.ilingtong.library.tongle.protocol.AppServiceResult;
import com.ilingtong.library.tongle.protocol.BannerResult;
import com.ilingtong.library.tongle.protocol.BaseDataResult;
import com.ilingtong.library.tongle.protocol.BaseRequestParam;
import com.ilingtong.library.tongle.protocol.CartResult;
import com.ilingtong.library.tongle.protocol.CartSettlementResult;
import com.ilingtong.library.tongle.protocol.CodeBaseResult;
import com.ilingtong.library.tongle.protocol.CodeResult;
import com.ilingtong.library.tongle.protocol.CollectForumResult;
import com.ilingtong.library.tongle.protocol.CollectProductResult;
import com.ilingtong.library.tongle.protocol.CommentResult;
import com.ilingtong.library.tongle.protocol.CouponDetailResult;
import com.ilingtong.library.tongle.protocol.CouponListResult;
import com.ilingtong.library.tongle.protocol.DefaultAddressResult;
import com.ilingtong.library.tongle.protocol.DefualtResult;
import com.ilingtong.library.tongle.protocol.DelAddrResult;
import com.ilingtong.library.tongle.protocol.DeleteInCartResult;
import com.ilingtong.library.tongle.protocol.ExpertBaseResult;
import com.ilingtong.library.tongle.protocol.ExpertProductResult;
import com.ilingtong.library.tongle.protocol.ExpertRequestParam;
import com.ilingtong.library.tongle.protocol.ExpertUpdateRequestParam;
import com.ilingtong.library.tongle.protocol.ExpertUpdateResult;
import com.ilingtong.library.tongle.protocol.FeedbackResult;
import com.ilingtong.library.tongle.protocol.ForumResult;
import com.ilingtong.library.tongle.protocol.GroupOrderDetailResult;
import com.ilingtong.library.tongle.protocol.GroupOrderGreatResult;
import com.ilingtong.library.tongle.protocol.GroupOrderListResult;
import com.ilingtong.library.tongle.protocol.GroupProductResult;
import com.ilingtong.library.tongle.protocol.GroupStoreListResult;
import com.ilingtong.library.tongle.protocol.GsonRequest;
import com.ilingtong.library.tongle.protocol.HomeForumParam;
import com.ilingtong.library.tongle.protocol.IntroResult;
import com.ilingtong.library.tongle.protocol.LoginResult;
import com.ilingtong.library.tongle.protocol.MFieldResult;
import com.ilingtong.library.tongle.protocol.MStoreDetailCollectResult;
import com.ilingtong.library.tongle.protocol.MStoreDetailQRResult;
import com.ilingtong.library.tongle.protocol.MStoreDetailResult;
import com.ilingtong.library.tongle.protocol.MStoreRequestParam;
import com.ilingtong.library.tongle.protocol.MStoreResult;
import com.ilingtong.library.tongle.protocol.MineResult;
import com.ilingtong.library.tongle.protocol.ModifyMessageResult;
import com.ilingtong.library.tongle.protocol.ModifyPwdParam;
import com.ilingtong.library.tongle.protocol.ModifyPwdResult;
import com.ilingtong.library.tongle.protocol.MyCouponAvailableItem;
import com.ilingtong.library.tongle.protocol.MyCouponAvailableResult;
import com.ilingtong.library.tongle.protocol.MyCouponInfoResult;
import com.ilingtong.library.tongle.protocol.MyFansResult;
import com.ilingtong.library.tongle.protocol.MyPointsResult;
import com.ilingtong.library.tongle.protocol.MyProceedsResult;
import com.ilingtong.library.tongle.protocol.OrderDetailResult;
import com.ilingtong.library.tongle.protocol.OrderInfoResult;
import com.ilingtong.library.tongle.protocol.OrderJSONInfo;
import com.ilingtong.library.tongle.protocol.OrderRequestParam;
import com.ilingtong.library.tongle.protocol.PWRecoveryResult;
import com.ilingtong.library.tongle.protocol.ParametersJson;
import com.ilingtong.library.tongle.protocol.PicTxtDetailResult;
import com.ilingtong.library.tongle.protocol.PostDetailResult;
import com.ilingtong.library.tongle.protocol.PostForwardResult;
import com.ilingtong.library.tongle.protocol.ProdCartItem;
import com.ilingtong.library.tongle.protocol.ProdDetailQRResult;
import com.ilingtong.library.tongle.protocol.ProdDetailRequestParam;
import com.ilingtong.library.tongle.protocol.ProdForwardResult;
import com.ilingtong.library.tongle.protocol.ProductCommentResult;
import com.ilingtong.library.tongle.protocol.ProductDetailResult;
import com.ilingtong.library.tongle.protocol.ProductSpecResult;
import com.ilingtong.library.tongle.protocol.RegistInputParam;
import com.ilingtong.library.tongle.protocol.RegistResult;
import com.ilingtong.library.tongle.protocol.ResetPwdParam;
import com.ilingtong.library.tongle.protocol.ResetPwdResult;
import com.ilingtong.library.tongle.protocol.ScanDataResult;
import com.ilingtong.library.tongle.protocol.SearchResult;
import com.ilingtong.library.tongle.protocol.TicketOrderSubmitRequestParam;
import com.ilingtong.library.tongle.protocol.UserOrdersResult;
import com.ilingtong.library.tongle.protocol.VersionResult;
import com.ilingtong.library.tongle.protocol.WeChatPayResult;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * User: lengjiqiang
 * Date: 2015/5/31
 * Time: 9:59
 * Email: jqleng@isoftstone.com
 * Dest:
 */
public class ServiceManager {

    /**
     * system接口参数转换。userid和token都为空
     * @param requestParam
     * @return
     */
    public static Map<String, String> getParams_gsonForSystem(Map<String, String> requestParam) {
        Map<String, String> params_gson = new HashMap();
        Gson mGson = new Gson();
        BaseRequestParam head = new BaseRequestParam("", "");
        requestParam.put("head", mGson.toJson(head));
        params_gson.put("parameters_json", mGson.toJson(requestParam).toString());
        return params_gson;
    }

    /**
     * 带token接口参数转换。userid和token为必须项
     * @param requestParam
     * @return
     */
    public static Map<String, String> getParams_gson(Map<String, String> requestParam) {
        Map<String, String> params_gson = new HashMap();
        Gson mGson = new Gson();
        BaseRequestParam head = new BaseRequestParam(TongleAppInstance.getInstance().getUserID(),TongleAppInstance.getInstance().getToken());
        requestParam.put("head", mGson.toJson(head));
        params_gson.put("parameters_json", mGson.toJson(requestParam).toString());
        return params_gson;
    }
    /**
     * 功能：[1004] 登录 传送用户手机号及登录密码给服务器，服务器进行登录校验。
     *
     * @param phoneNumber 手机号码
     * @param password    登录密码
     */
    static public void doLogin(String phoneNumber, String password, Response.Listener successListener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + "system/login";
        Map<String, String> requestParam = new HashMap<String, String>();
        requestParam.put("phone_number", phoneNumber);
        requestParam.put("password", password);
//        Gson mGson = new Gson();
//        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> params_gson = new HashMap();
//        params_gson.put("parameters_json", gson);
        params_gson = getParams_gson(requestParam);
        GsonRequest<LoginResult> gsonRequest = new GsonRequest<LoginResult>(Request.Method.POST, url, LoginResult.class, params_gson, successListener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[1004] 登录 " + url + "?parameters_json=" + params_gson.get("parameters_json"));
    }

    /**
     * 功能：[1008] 取得广告位列表
     *
     * @param userID     用户ID（必须指定）
     * @param limit_size 活动取得件数（可为空）
     */
    static public void doBannerRequest(String userID, String limit_size, Response.Listener listener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/system/homepage_ads";
        Map<String, String> requestParam = new HashMap<String, String>();
        requestParam.put("user_token", TongleAppInstance.getInstance().getToken());
        requestParam.put("user_id", userID);
        requestParam.put("limit_size", limit_size);
        Gson mGson = new Gson();
        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);
        GsonRequest<BannerResult> gsonRequest = new GsonRequest<BannerResult>(Request.Method.POST, url, BannerResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[1008] 取得广告位列表 " + url + "?parameters_json=" + gson);
    }

    /**
     * 功能：[2010] 取得用户收藏的魔店列表
     *
     * @param param 向服务器请求魔店信息的参数
     */
    static public void doMStoreRequest(MStoreRequestParam param, Response.Listener listener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/favorites/user_mstores";
        Map<String, String> requestParam = new HashMap<String, String>();
        requestParam.put("user_id", param.user_id);
        requestParam.put("user_token", TongleAppInstance.getInstance().getToken());
        requestParam.put("mstore_id", param.mstore_id);
        requestParam.put("forward", param.forward);
        requestParam.put("fetch_count", param.fetch_count);
        Gson mGson = new Gson();
        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> params_gson = new HashMap();
        //params_gson.put("parameters_json", gson);
        params_gson = getParams_gson(requestParam);
        GsonRequest<MStoreResult> gsonRequest = new GsonRequest<MStoreResult>(Request.Method.POST, url, MStoreResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[2010] 取得用户收藏的魔店列表 " + url + "?parameters_json=" + params_gson.get("parameters_json"));
    }

    /**
     * 功能：[1011] 取得首页我的关注帖子列表
     *
     * @param param 向服务器请求关注帖子的参数
     */
    static public void doForumRequest(HomeForumParam param, Response.Listener listener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/my/friends_posts";
        Map<String, String> requestParam = new HashMap<String, String>();
        requestParam.put("user_id", param.user_id);
        requestParam.put("user_token", TongleAppInstance.getInstance().getToken());
        requestParam.put("post_id", param.post_id);
        requestParam.put("forward", param.forward);
        requestParam.put("fetch_count", param.fetch_count);
        Gson mGson = new Gson();
        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);
        GsonRequest<ForumResult> gsonRequest = new GsonRequest<ForumResult>(Request.Method.POST, url, ForumResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[1011] 取得首页我的关注帖子列表 " + url + "?parameters_json=" + gson);
    }

    /**
     * 功能：[1018] 取得收藏 -> 达人 -> 更新数据
     *
     * @param param 向服务器请求达人更新的参数
     */
    static public void doExpertUpdateRequest(ExpertUpdateRequestParam param, Response.Listener listener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/posts/user_posts";
        Map<String, String> requestParam = new HashMap<String, String>();
        requestParam.put("user_id", param.user_id);
        requestParam.put("user_token", TongleAppInstance.getInstance().getToken());
        requestParam.put("expert_user_id", param.expert_user_id);
        requestParam.put("post_id", param.post_id);
        requestParam.put("forward", param.forward);
        requestParam.put("fetch_count", param.fetch_count);
        requestParam.put("action", param.action);
        Gson mGson = new Gson();
        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);
        GsonRequest<ExpertUpdateResult> gsonRequest = new GsonRequest<ExpertUpdateResult>(Request.Method.POST, url, ExpertUpdateResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[1018] 取得收藏 -> 达人 -> 更新数据 " + url + "?parameters_json=" + gson);
    }

    /**
     * 功能：[2013] 取得收藏 -> 达人 -> 产品数据
     *
     * @param param 向服务器请求达人 -> 产品的参数
     */
    static public void doExpertProductRequest(ExpertRequestParam param, Response.Listener listener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/my/promote_products";
        Map<String, String> requestParam = new HashMap<String, String>();
        requestParam.put("user_id", param.user_id);
        requestParam.put("user_token", TongleAppInstance.getInstance().getToken());
        requestParam.put("prod_id", param.expert_user_id);
        requestParam.put("forward", param.forward);
        requestParam.put("fetch_count", param.fetch_count);
        Gson mGson = new Gson();
        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);
        GsonRequest<ExpertProductResult> gsonRequest = new GsonRequest<ExpertProductResult>(Request.Method.POST, url, ExpertProductResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[2013] 取得收藏 -> 达人 -> 产品数据 " + url + "?parameters_json=" + gson);
    }

    /**
     * 功能：[2020] 取得收藏 -> 帖子 属性页数据
     *
     * @param userID 用户ID
     */
    static public void doCollectForumRequest(String userID, String post_id, String forward, String fetch_count, Response.Listener listener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/favorites/user_posts";
        Map<String, String> requestParam = new HashMap<String, String>();
        requestParam.put("user_id", userID);
        requestParam.put("user_token", TongleAppInstance.getInstance().getToken());
        requestParam.put("post_id", post_id);
        requestParam.put("forward", forward);
        requestParam.put("fetch_count", fetch_count);
        Gson mGson = new Gson();
        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);
        GsonRequest<CollectForumResult> gsonRequest = new GsonRequest<CollectForumResult>(Request.Method.POST, url, CollectForumResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[2020] 取得收藏 -> 帖子 " + url + "?parameters_json=" + gson);
    }

    /**
     * 功能：[2019] 取得我收藏的商品列表
     *
     * @param userID 用户ID
     */
    static public void doCollectProductRequest(String userID, String prod_id, String forward, String fetch_count, Response.Listener listener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/favorites/user_products";
        Map<String, String> requestParam = new HashMap<String, String>();
        requestParam.put("user_id", userID);
        requestParam.put("prod_id", prod_id);
        requestParam.put("forward", forward);
        requestParam.put("fetch_count", fetch_count);
        requestParam.put("user_token", TongleAppInstance.getInstance().getToken());
        Gson mGson = new Gson();
        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);
        GsonRequest<CollectProductResult> gsonRequest = new GsonRequest<CollectProductResult>(Request.Method.POST, url, CollectProductResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[2019] 取得我收藏的商品列表 " + url + "?parameters_json=" + gson);
    }

    /**
     * 功能：[1026] 取得我的购物车商品列表
     *
     * @param userID 用户ID
     */
    static public void doCartRequest(String userID, Response.Listener listener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/shopping_carts/show";
        Map<String, String> requestParam = new HashMap<String, String>();
        requestParam.put("user_id", userID);
        requestParam.put("user_token", TongleAppInstance.getInstance().getToken());
        Gson mGson = new Gson();
        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);
        GsonRequest<CartResult> gsonRequest = new GsonRequest<CartResult>(Request.Method.POST, url, CartResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[1026] 取得我的购物车商品列表 " + url + "?parameters_json=" + gson);
    }

    /**
     * 功能：[2012] 取得用户基本信息
     *
     * @param userID 用户ID
     */
    static public void doMineRequest(String userID, Response.Listener listener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/my/personal_info_show";
        Map<String, String> requestParam = new HashMap<String, String>();
        requestParam.put("user_id", userID);
        requestParam.put("user_token", TongleAppInstance.getInstance().getToken());
        Gson mGson = new Gson();
        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);
        GsonRequest<MineResult> gsonRequest = new GsonRequest<MineResult>(Request.Method.POST, url, MineResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[2012] 取得用户基本信息 " + url + "?parameters_json=" + gson);
    }

    /**
     * 功能：[2011] 取得魔店基本信息
     *
     * @param userID   用户ID
     * @param mstoreID 魔店ID
     */
    static public void doMStoreDetailRequest(String userID, String mstoreID, Response.Listener listener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/mstores/show";
        Map<String, String> requestParam = new HashMap<String, String>();
        requestParam.put("user_id", userID);
        requestParam.put("mstore_id", mstoreID);
        requestParam.put("user_token", TongleAppInstance.getInstance().getToken());
        Gson mGson = new Gson();
        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);
        GsonRequest<MStoreDetailResult> gsonRequest = new GsonRequest<MStoreDetailResult>(Request.Method.POST, url, MStoreDetailResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[2011] 取得魔店基本信息 " + url + "?parameters_json=" + gson);
    }

    /**
     * 功能：[1046] 取得产品评价信息
     *
     * @param userID        用户ID
     * @param product_id    产品ID
     * @param level         评价等级
     * @param rating_doc_no 评价单号
     * @param forward       翻页方向
     * @param fetch_count   返回条数
     */
    static public void prdCommentRequest(String userID, String product_id, String level, String rating_doc_no, String forward, String fetch_count, Response.Listener listener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/products/evaluation_show";
        Map<String, String> requestParam = new HashMap<String, String>();
        requestParam.put("user_id", userID);
        requestParam.put("user_token", TongleAppInstance.getInstance().getToken());
        requestParam.put("product_id", product_id);
        requestParam.put("level", level);
        requestParam.put("rating_doc_no", rating_doc_no);
        requestParam.put("forward", forward);
        requestParam.put("fetch_count", fetch_count);
        Gson mGson = new Gson();
        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);
        GsonRequest<ProductCommentResult> gsonRequest = new GsonRequest<ProductCommentResult>(Request.Method.POST, url, ProductCommentResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[1046] 取得产品评价信息 " + url + "?parameters_json=" + gson);
    }

    /**
     * 功能： [1023] 取得产品规模信息
     *
     * @param userID     用户ID
     * @param product_id 产品ID
     */
    static public void productSpecRequest(String userID, String product_id, Response.Listener listener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/products/specification_show";
        Map<String, String> requestParam = new HashMap<String, String>();
        requestParam.put("user_id", userID);
        requestParam.put("user_token", TongleAppInstance.getInstance().getToken());
        requestParam.put("product_id", product_id);
        Gson mGson = new Gson();
        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);
        GsonRequest<ProductSpecResult> gsonRequest = new GsonRequest<ProductSpecResult>(Request.Method.POST, url, ProductSpecResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[1023] 取得产品规模信息 " + url + "?parameters_json=" + gson);
    }

    /**
     * 功能：[1024] 取得图文详情信息
     *
     * @param userID     用户ID
     * @param product_id 产品ID
     */
    static public void productDetailRequest(String userID, String product_id, Response.Listener listener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/products/image_text_show";
        Map<String, String> requestParam = new HashMap<String, String>();
        requestParam.put("user_id", userID);
        requestParam.put("user_token", TongleAppInstance.getInstance().getToken());
        requestParam.put("product_id", product_id);
        Gson mGson = new Gson();
        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);
        GsonRequest<PicTxtDetailResult> gsonRequest = new GsonRequest<PicTxtDetailResult>(Request.Method.POST, url, PicTxtDetailResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[1024] 取得图文详情信息 " + url + "?parameters_json=" + gson);
    }

    /**
     * 功能：[1084] 生成魔店动态二维码
     *
     * @param userID   用户ID
     * @param mstoreID 魔店ID
     */
    static public void doMStoreQRRequest(String userID, String mstoreID, Response.Listener listener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/qrcode/mstore_show";
        Map<String, String> requestParam = new HashMap<String, String>();
        requestParam.put("user_id", userID);
        requestParam.put("user_token", TongleAppInstance.getInstance().getToken());
        requestParam.put("mstore_id", mstoreID);
        Gson mGson = new Gson();
        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);
        GsonRequest<MStoreDetailQRResult> gsonRequest = new GsonRequest<MStoreDetailQRResult>(Request.Method.POST, url, MStoreDetailQRResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[1084] 生成魔店动态二维码 " + url + "?parameters_json=" + gson);
    }

    /**
     * 功能：[2021] 收藏商品/魔店/会员/帖子/宝贝商品
     *
     * @param userID         用户ID
     * @param favorites_list 收藏列表
     * @param relation_id    二维码关联ID（可为空）
     */
    static public void doMStoreCollectRequest(String userID, ArrayList favorites_list, String relation_id, Response.Listener listener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/favorites/add";
        Map<String, Object> requestParam = new HashMap<String, Object>();
        requestParam.put("user_id", userID);
        requestParam.put("user_token", TongleAppInstance.getInstance().getToken());
        requestParam.put("relation_id", relation_id);
        requestParam.put("favorites_list_json", favorites_list);
        Gson mGson = new Gson();
        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);
        GsonRequest<MStoreDetailCollectResult> gsonRequest = new GsonRequest<MStoreDetailCollectResult>(Request.Method.POST, url, MStoreDetailCollectResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[2021] 收藏商品/魔店/会员/帖子/宝贝商品 " + url + "?parameters_json=" + gson);
    }

    /**
     * 功能：[1088] 取消已收藏商品/魔店/会员/帖子/宝贝商品
     *
     * @param userID          用户ID
     * @param key_value       魔店ID
     * @param collection_type 收藏类型
     */
    static public void doMStoreCancelCollectRequest(String userID, String key_value, String collection_type, Response.Listener listener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/favorites/remove";
        Map<String, String> requestParam = new HashMap<String, String>();
        requestParam.put("user_id", userID);
        requestParam.put("user_token", TongleAppInstance.getInstance().getToken());
        requestParam.put("key_value", key_value);
        requestParam.put("collection_type", collection_type);
        Gson mGson = new Gson();
        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);
        GsonRequest<MStoreDetailCollectResult> gsonRequest = new GsonRequest<MStoreDetailCollectResult>(Request.Method.POST, url, MStoreDetailCollectResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[1088] 取消已收藏商品/魔店/会员/帖子/宝贝商品 " + url + "?parameters_json=" + gson);
    }

    /**
     * 功能：[1022] 取得商品信息
     *
     * @param param 取得商品详情的参数类
     */
    static public void doProductDetailRequest(ProdDetailRequestParam param, Response.Listener listener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/products/show";
        Map<String, String> requestParam = new HashMap<String, String>();
        requestParam.put("user_id", param.user_id);
        requestParam.put("user_token", TongleAppInstance.getInstance().getToken());
        requestParam.put("product_id", param.product_id);
        requestParam.put("post_id", param.post_id);
        requestParam.put("action", param.action);
        requestParam.put("mstore_id", param.mstore_id);
        requestParam.put("relation_id", param.relation_id);
        Gson mGson = new Gson();
        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);
        GsonRequest<ProductDetailResult> gsonRequest = new GsonRequest<ProductDetailResult>(Request.Method.POST, url, ProductDetailResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[1022] 取得商品信息 " + url + "?parameters_json=" + gson);
    }

    /**
     * 功能：[1085] 生成商品动态二维码
     *
     * @param userID      用户ID
     * @param prod_id     商品ID
     * @param relation_id 原帖子ID
     */
    static public void doProdDetailQRRequest(String userID, String prod_id, String relation_id, Response.Listener listener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/qrcode/product_show";
        Map<String, String> requestParam = new HashMap<String, String>();
        requestParam.put("user_id", userID);
        requestParam.put("user_token", TongleAppInstance.getInstance().getToken());
        requestParam.put("prod_id", prod_id);
        requestParam.put("relation_id", relation_id);
        Gson mGson = new Gson();
        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);
        GsonRequest<ProdDetailQRResult> gsonRequest = new GsonRequest<ProdDetailQRResult>(Request.Method.POST, url, ProdDetailQRResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[1085] 生成商品动态二维码 " + url + "?parameters_json=" + gson);
    }

    /**
     * 功能：[2018] 取得系统基础数据列表
     *
     * @param base_data_type_json 基础数据类型json
     */
    static public void doBaseDataRequest(String base_data_type_json, Response.Listener listener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + "system/base_data";
        Map<String, String> requestParam = new HashMap<String, String>();
        requestParam.put("base_data_type_json", base_data_type_json);
        Gson mGson = new Gson();
        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);
        GsonRequest<BaseDataResult> gsonRequest = new GsonRequest<BaseDataResult>(Request.Method.POST, url, BaseDataResult.class, params_gson, listener, errorListener);
        gsonRequest.setRetryPolicy(new DefaultRetryPolicy(5000 * 3,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[2018] 取得系统基础数据列表 " + url + "?parameters_json=" + gson);
    }

    /**
     * 功能：[1030] 取得我的收货地址列表
     *
     * @param userID 用户ID
     */
    static public void doAddressRequest(String userID, Response.Listener listener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/my/addresses";
        Map<String, String> requestParam = new HashMap<String, String>();
        requestParam.put("user_id", userID);
        requestParam.put("user_token", TongleAppInstance.getInstance().getToken());
        Gson mGson = new Gson();
        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);
        GsonRequest<AddressResult> gsonRequest = new GsonRequest<AddressResult>(Request.Method.POST, url, AddressResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[1030] 取得我的收货地址列表 " + url + "?parameters_json=" + gson);
    }

    /**
     * 功能：[2023] 将指定的地址设定为默认地址
     *
     * @param userID     用户ID
     * @param address_no 地址编号
     */
    static public void doSetDefaultAddrRequest(String userID, String address_no, Response.Listener listener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/my/address_default";
        Map<String, String> requestParam = new HashMap<String, String>();
        requestParam.put("user_id", userID);
        requestParam.put("user_token", TongleAppInstance.getInstance().getToken());
        requestParam.put("address_no", address_no);
        Gson mGson = new Gson();
        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);
        GsonRequest<DefaultAddressResult> gsonRequest = new GsonRequest<DefaultAddressResult>(Request.Method.POST, url, DefaultAddressResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[2023] 将指定的地址设定为默认地址 " + url + "?parameters_json=" + gson);
    }

    /**
     * 功能：[1033] 删除收货地址
     *
     * @param userID       用户ID
     * @param address_list 地址列表
     */
    static public void doDeleteAddrRequest(String userID, ArrayList address_list, Response.Listener listener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/my/address_remove";
        Map<String, String> requestParam = new HashMap<String, String>();
        requestParam.put("user_id", userID);
        requestParam.put("user_token", TongleAppInstance.getInstance().getToken());

        Map<String, ArrayList> param = new HashMap<>();
        param.put("address_list", address_list);
        Gson mGson = new Gson();
        String addrGson = mGson.toJson(param).toString();
        requestParam.put("address_json", addrGson);

        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);
        GsonRequest<DelAddrResult> gsonRequest = new GsonRequest<DelAddrResult>(Request.Method.POST, url, DelAddrResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[1033] 删除收货地址 " + url + "?parameters_json=" + gson);
    }

    /**
     * 功能：[1012] 取得我的关注人列表
     *
     * @param param 获取收藏- > 达人列表的参数
     */
    static public void requestCollectExpert(ExpertRequestParam param, Response.Listener<ParametersJson> listener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/my/friends";
        Map<String, String> requestParam = new HashMap<String, String>();
        requestParam.put("user_id", param.user_id);
        requestParam.put("user_token", TongleAppInstance.getInstance().getToken());
        requestParam.put("expert_user_id", param.expert_user_id);
        requestParam.put("forward", param.forward);
        requestParam.put("fetch_count", param.fetch_count);
        Gson mGson = new Gson();
        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);
        GsonRequest<ParametersJson> gsonRequest = new GsonRequest<ParametersJson>(Request.Method.POST, url, ParametersJson.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[1012] 取得我的关注人列表 " + url + "?parameters_json=" + gson);
    }

    /**
     * 功能：[1090] 搜索
     *
     * @param user_id   用户ID
     * @param find_key  搜索关键字
     * @param find_type 搜索类型
     */
    static public void searchRequest(String user_id, String find_key, String find_type, Response.Listener listener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/tools/search";
        Map<String, String> requestParam = new HashMap<String, String>();
        requestParam.put("user_id", user_id);
        requestParam.put("user_token", TongleAppInstance.getInstance().getToken());
        requestParam.put("find_key", find_key);
        requestParam.put("find_type", find_type);
        Gson mGson = new Gson();
        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);
        GsonRequest<SearchResult> gsonRequest = new GsonRequest<SearchResult>(Request.Method.POST, url, SearchResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[1090] 搜索 " + url + "?parameters_json=" + gson);
    }

    /**
     * 功能：[2029] 热门搜索
     *
     * @param user_id 用户ID
     */
    static public void hotSearchRequest(String user_id, Response.Listener listener, Response.ErrorListener errorListener) {

        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/tools/hot_search";
        Map<String, String> requestParam = new HashMap<String, String>();
        requestParam.put("user_id", user_id);
        requestParam.put("user_token", TongleAppInstance.getInstance().getToken());
        Gson mGson = new Gson();
        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);
        GsonRequest<SearchResult> gsonRequest = new GsonRequest<SearchResult>(Request.Method.POST, url, SearchResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[2029] 热门搜索 " + url + "?parameters_json=" + gson);

    }


    /**
     * 功能：[1086] 生成会员的动态二维码
     *
     * @param userID       用户ID
     * @param expertUserID 达人ID
     */
    static public void requestQRCode(String userID, String expertUserID, Response.Listener<ParametersJson> listener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/qrcode/user_show";
        Map<String, String> requestParam = new HashMap<String, String>();
        requestParam.put("user_id", userID);
        requestParam.put("user_token", TongleAppInstance.getInstance().getToken());
        requestParam.put("expert_user_id", expertUserID);

        Gson mGson = new Gson();
        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);
        GsonRequest<ParametersJson> gsonRequest = new GsonRequest<ParametersJson>(Request.Method.POST, url, ParametersJson.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[1086] 生成会员的动态二维码 " + url + "?parameters_json=" + gson);
    }

    /**
     * 功能：[1025] 加入购物车
     *
     * @param userID       用户ID
     * @param product_info 商品信息
     */
    static public void addToCart(String userID, String postID, ProdCartItem product_info, String mstoreID, String realtionId, Response.Listener listener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/shopping_carts/create";
        Map<String, String> requestParam = new HashMap<String, String>();
        requestParam.put("user_id", userID);
        requestParam.put("user_token", TongleAppInstance.getInstance().getToken());
        requestParam.put("post_id", postID);
        requestParam.put("wifi_id", "");
        requestParam.put("mstore_id", mstoreID);
        requestParam.put("realtion_id", realtionId);

        Map<String, ProdCartItem> param = new HashMap<String, ProdCartItem>();
        param.put("product_info", product_info);
        Gson mGson = new Gson();
        String prodInfoJson = mGson.toJson(param).toString();
        requestParam.put("product_info_json", prodInfoJson);

        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> pro_info_gson = new HashMap();
        pro_info_gson.put("parameters_json", gson);
        GsonRequest<CartResult> gsonRequest = new GsonRequest<CartResult>(Request.Method.POST, url, CartResult.class, pro_info_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[1025] 加入购物车 " + url + "?parameters_json=" + gson);
    }

    /**
     * 功能：[1040] 取得我的订单
     *
     * @param param 订单请求参数
     */
    static public void getUserOrdersRequest(OrderRequestParam param, Response.Listener listener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/orders/user_orders";

        Map<String, String> requestParam = new HashMap<String, String>();

        requestParam.put("user_id", param.user_id);
        requestParam.put("user_token", TongleAppInstance.getInstance().getToken());
        requestParam.put("order_status", param.order_status);
        requestParam.put("order_date_from", param.order_date_from);
        requestParam.put("order_date_to", param.order_date_to);
        requestParam.put("order_no", param.order_no);
        requestParam.put("forward", param.forward);
        requestParam.put("fetch_count", param.fetch_count);
        Gson mGson = new Gson();

        String gson = mGson.toJson(requestParam).toString();

        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);

        GsonRequest<UserOrdersResult> gsonRequest = new GsonRequest<UserOrdersResult>(Request.Method.POST, url, UserOrdersResult.class,
                params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[1040] 取得我的订单 " + url + "?parameters_json=" + gson);

    }


    /**
     * 功能：[2015] App推广二维码图片
     *
     * @param userID 用户ID
     */

    static public void getAppPromoteRequest(String userID, Response.Listener listener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/system/app_promote";
        Map<String, String> requestParam = new HashMap<String, String>();
        requestParam.put("user_id", userID);
        requestParam.put("user_token", TongleAppInstance.getInstance().getToken());

        Gson mGson = new Gson();
        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);
        GsonRequest<APPpromotionResult> gsonRequest = new GsonRequest<APPpromotionResult>(Request.Method.POST, url, APPpromotionResult.class,
                params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[2015] App推广二维码图片 " + url + "?parameters_json=" + gson);

    }


    /**
     * 功能：[2014] 取得我的收益
     *
     * @param userID 用户ID
     */


    static public void getMyIncomesRequest(String userID, String period_type, String income_type_list, Response.Listener listener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/my/incomes";


        Map<String, String> requestParam = new HashMap<String, String>();

        requestParam.put("user_token", TongleAppInstance.getInstance().getToken());
        requestParam.put("user_id", userID);
        requestParam.put("period_type", period_type);
        requestParam.put("income_type_list", income_type_list);

        Gson mGson = new Gson();

        String gson = mGson.toJson(requestParam).toString();

        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);

        GsonRequest<MyProceedsResult> gsonRequest = new GsonRequest<MyProceedsResult>(Request.Method.POST, url, MyProceedsResult.class,
                params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[2014] 取得我的收益 " + url + "?parameters_json=" + gson);

    }

    /**
     * 功能：[1041] 取得订单详情信息
     *
     * @param userID  用户ID
     * @param orderNo 订单编号
     */
    static public void OrderDetailRequest(String userID, String orderNo, Response.Listener listener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/orders/show";
        Map<String, String> requestParam = new HashMap<String, String>();
        requestParam.put("user_id", userID);
        requestParam.put("user_token", TongleAppInstance.getInstance().getToken());
        requestParam.put("order_no", orderNo);
        Gson mGson = new Gson();
        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);
        GsonRequest<OrderDetailResult> gsonRequest = new GsonRequest<OrderDetailResult>(Request.Method.POST, url, OrderDetailResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[1041] 取得订单详情信息 " + url + "?parameters_json=" + gson);
    }

    /**
     * 功能：[2032] 支付结果查询
     *
     * @param userID     用户ID
     * @param orderNo    订单编号
     * @param alipaySign 支付宝签名
     */
    static public void PayResultQueryRequest(String userID, String orderNo, String alipaySign, Response.Listener listener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/orders/pay_result_search";
        Map<String, String> requestParam = new HashMap<String, String>();
        requestParam.put("user_id", userID);
        requestParam.put("user_token", TongleAppInstance.getInstance().getToken());
        requestParam.put("order_no", orderNo);
        requestParam.put("alipay_sign", alipaySign);
        Gson mGson = new Gson();
        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);
        GsonRequest<OrderDetailResult> gsonRequest = new GsonRequest<OrderDetailResult>(Request.Method.POST, url, OrderDetailResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[2032] 支付结果查询 " + url + "?parameters_json=" + gson);
    }

    /**
     * 功能：[2034] 支付宝支付参数取得
     *
     * @param userID  用户ID
     * @param orderNo 订单编号
     */
    static public void AliPayRequest(String userID, String orderNo, Response.Listener listener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/orders/alipay_parameter";
        Map<String, String> requestParam = new HashMap<String, String>();
        requestParam.put("user_id", userID);
        requestParam.put("user_token", TongleAppInstance.getInstance().getToken());
        requestParam.put("order_no", orderNo);
        requestParam.put("app_inner_no", TongleAppInstance.getApp_inner_no());
        requestParam.put("pay_method", TongleAppConst.PAY_TYPE_ALIPAY);
        Gson mGson = new Gson();
        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);
        GsonRequest<AliPayResult> gsonRequest = new GsonRequest<AliPayResult>(Request.Method.POST, url, AliPayResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[2034] 支付宝支付参数取得 " + url + "?parameters_json=" + gson);
    }

    /**
     * 功能：[1042] 订单详情——删除订单
     *
     * @param userID  用户ID
     * @param orderNo 订单编号
     */
    static public void OrderDetailDeleteRequest(String userID, String orderNo, String reason, Response.Listener listener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/orders/remove";
        Map<String, String> requestParam = new HashMap<String, String>();
        requestParam.put("user_id", userID);
        requestParam.put("user_token", TongleAppInstance.getInstance().getToken());
        requestParam.put("order_no", orderNo);
        requestParam.put("reason", reason);
        Gson mGson = new Gson();
        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);
        GsonRequest<OrderDetailResult> gsonRequest = new GsonRequest<OrderDetailResult>(Request.Method.POST, url, OrderDetailResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[1042] 订单详情——删除订单 " + url + "?parameters_json=" + gson);
    }

    /**
     * 功能：[1045] 订单详情——立即评价
     *
     * @param userID        用户ID
     * @param rating_detail 评价的商品信息
     */
    static public void doOrderDetailEvaluateRequest(String userID, ArrayList rating_detail, Response.Listener listener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/products/evaluation_create";
        Map<String, String> requestParam = new HashMap<String, String>();
        requestParam.put("user_id", userID);
        requestParam.put("user_token", TongleAppInstance.getInstance().getToken());
        Map<String, ArrayList> param = new HashMap<String, ArrayList>();
        param.put("rating_detail", rating_detail);
        Gson mGson = new Gson();
        String rateGson = mGson.toJson(param).toString();
        requestParam.put("rating_detail_json", rateGson);

        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);
        GsonRequest<CommentResult> gsonRequest = new GsonRequest<CommentResult>(Request.Method.POST, url, CommentResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[1045] 订单详情——立即评价 " + url + "?parameters_json=" + gson);
    }

    /**
     * 功能：[1031] 新增收货地址
     *
     * @param userID   用户ID
     * @param add_info 收货地址信息
     */
    static public void doaddAddressRequest(String userID, AddInfo add_info, Response.Listener listener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/my/address_create";
        Map<String, String> requestParam = new HashMap<String, String>();
        requestParam.put("user_id", userID);
        requestParam.put("user_token", TongleAppInstance.getInstance().getToken());

        Map<String, AddInfo> param = new HashMap<String, AddInfo>();
        param.put("add_info", add_info);
        Gson mGson = new Gson();
        String addrGson = mGson.toJson(param).toString();
        requestParam.put("add_info_json", addrGson);

        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);
        GsonRequest<AddressResult> gsonRequest = new GsonRequest<AddressResult>(Request.Method.POST, url, AddressResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[1031] 新增收货地址 " + url + "?parameters_json=" + gson);
    }

    /**
     * 功能：[1032] 更改默认地址
     *
     * @param userID     用户ID
     * @param address_no 地址编号
     * @param add_info   收货地址信息记录
     */
    static public void doModifyAddressRequest(String userID, String address_no, AddInfo add_info, Response.Listener listener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/my/address_modify";
        Map<String, String> requestParam = new HashMap<String, String>();
        requestParam.put("user_id", userID);
        requestParam.put("user_token", TongleAppInstance.getInstance().getToken());
        requestParam.put("address_no", address_no);

        Map<String, AddInfo> param = new HashMap<String, AddInfo>();
        param.put("add_info", add_info);
        Gson mGson = new Gson();
        String addrGson = mGson.toJson(param).toString();
        requestParam.put("add_info_json", addrGson);

        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);
        GsonRequest<AddressResult> gsonRequest = new GsonRequest<AddressResult>(Request.Method.POST, url, AddressResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[1032] 更改默认地址 " + url + "?parameters_json=" + gson);
    }

    /**
     * 功能：[2019] 取得我收藏的商品列表
     *
     * @param userID      用户ID
     * @param prodID      商品ID
     * @param forward     翻页方向
     * @param fetch_count 返回条数
     */
    static public void mineBaby(String userID, String prodID, String forward, String fetch_count, Response.Listener listener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/my/promote_products";
        Map<String, String> requestParam = new HashMap<String, String>();
        requestParam.put("user_id", userID);
        requestParam.put("user_token", TongleAppInstance.getInstance().getToken());
        requestParam.put("prod_id", prodID);
        requestParam.put("forward", forward);
        requestParam.put("fetch_count", fetch_count);
        Gson mGson = new Gson();
        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);
        GsonRequest<CollectProductResult> gsonRequest = new GsonRequest<CollectProductResult>(Request.Method.POST, url, CollectProductResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[2019] 取得我收藏的商品列表 " + url + "?parameters_json=" + gson);
    }

    /**
     * 功能：[1003] 传送用户注册的基本信息给服务器，服务器完成用户注册，成功后返回用户ID。
     *
     * @param param 用户注册需要输入的参数类
     */
    static public void doRegistRequest(RegistInputParam param, Response.Listener listener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + "system/register";
        Map<String, String> requestParam = new HashMap<String, String>();
        requestParam.put("phone_number", param.phone_number);
        requestParam.put("password", param.password);
        requestParam.put("verification_code", param.verification_code);
        requestParam.put("user_nick_name", param.user_nick_name);
        requestParam.put("channel_no", param.channel_no);
        requestParam.put("mobile_os_version", param.mobile_os_version);
        requestParam.put("terminal_device", param.terminal_device);
        requestParam.put("recommend_code", param.recommend_code);
        Gson mGson = new Gson();
        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);
        GsonRequest<RegistResult> gsonRequest = new GsonRequest<RegistResult>(Request.Method.POST, url, RegistResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[1003] 注册 " + url + "?parameters_json=" + gson);
    }

    /**
     * 功能：[1002] 调用服务器方法向指定手机号码发送验证码。
     *
     * @param phoneNumber 手机号
     */
    static public void doCodeRequest(String phoneNumber, Response.Listener listener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + "system/verification_code_send";
        Map<String, String> requestParam = new HashMap<String, String>();
        requestParam.put("phone_number", phoneNumber);
        Gson mGson = new Gson();
        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);
        GsonRequest<CodeResult> gsonRequest = new GsonRequest<CodeResult>(Request.Method.POST, url, CodeResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[1002] 调用服务器方法向指定手机号码发送验证码 " + url + "?parameters_json=" + gson);
    }

    /**
     * 功能：[1078] 密码找回
     *
     * @param phoneNumber 手机号
     * @param vCode       验证码
     */
    static public void doPWRecoverRequest(String phoneNumber, String vCode, Response.Listener listener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + "system/password_retrieve";
        Map<String, String> requestParam = new HashMap<String, String>();
        requestParam.put("phone_number", phoneNumber);
        requestParam.put("verification_code", vCode);
        Gson mGson = new Gson();
        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);
        GsonRequest<PWRecoveryResult> gsonRequest = new GsonRequest<PWRecoveryResult>(Request.Method.POST, url, PWRecoveryResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[1078] 密码找回 " + url + "?parameters_json=" + gson);
    }

    /**
     * 功能：[1089] 帖子详情
     *
     * @param userID 用户ID
     * @param postID 帖子D
     */
    static public void doPostDetailRequest(String userID, String postID, Response.Listener listener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/posts/post_show";
        Map<String, String> requestParam = new HashMap<String, String>();
        requestParam.put("user_id", userID);
        requestParam.put("user_token", TongleAppInstance.getInstance().getToken());
        requestParam.put("post_id", postID);
        Gson mGson = new Gson();
        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);
        GsonRequest<PostDetailResult> gsonRequest = new GsonRequest<PostDetailResult>(Request.Method.POST, url, PostDetailResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[1089] 帖子详情 " + url + "?parameters_json=" + gson);
    }

    /**
     * 功能：[1010] 重置密码 发送新密码到服务器
     *
     * @param param 重置密码参数
     */
    static public void doRestPwdRquest(ResetPwdParam param, Response.Listener successListener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + "system/password_reset";
        Map<String, String> requestParam = new HashMap<String, String>();
        requestParam.put("user_token", TongleAppInstance.getInstance().getToken());
        requestParam.put("phone_number", param.phone_number);
        requestParam.put("password", param.password);
        requestParam.put("confirm_password", param.confirm_password);
        requestParam.put("verification_code", param.verification_code);
        Gson mGson = new Gson();
        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);
        GsonRequest<ResetPwdResult> gsonRequest = new GsonRequest<ResetPwdResult>(Request.Method.POST, url, ResetPwdResult.class, params_gson, successListener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[1010] 重置密码 发送新密码到服务器 " + url + "?parameters_json=" + gson);
    }

    /**
     * 功能：[2024] 订单详情---确认订单接口
     *
     * @param userID  用户ID
     * @param orderNo 订单编号
     */
    static public void OrderConfirmRequest(String userID, String orderNo, Response.Listener listener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/orders/confirm";
        Map<String, String> requestParam = new HashMap<String, String>();
        requestParam.put("user_id", userID);
        requestParam.put("user_token", TongleAppInstance.getInstance().getToken());
        requestParam.put("order_no", orderNo);
        Gson mGson = new Gson();
        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);
        GsonRequest<OrderDetailResult> gsonRequest = new GsonRequest<OrderDetailResult>(Request.Method.POST, url, OrderDetailResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[2024] 订单详情---确认订单接口 " + url + "?parameters_json=" + gson);
    }

    /**
     * 功能：[1081] 保存选择的领域
     *
     * @param userID    用户ID
     * @param type_list 保存的领域列表
     */
    static public void saveFieldRequest(String userID, ArrayList type_list, Response.Listener listener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/my/product_types_create";
        Map<String, String> requestParam = new HashMap<String, String>();
        requestParam.put("user_id", userID);
        requestParam.put("user_token", TongleAppInstance.getInstance().getToken());

        Gson mGson = new Gson();
        Map<String, ArrayList> typeListParam = new HashMap<>();
        typeListParam.put("type_list", type_list);
        String typeListString = mGson.toJson(typeListParam).toString();
        requestParam.put("type_info", typeListString);

        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);
        GsonRequest<MFieldResult> gsonRequest = new GsonRequest<MFieldResult>(Request.Method.POST, url, MFieldResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[1081] 保存选择的领域 " + url + "?parameters_json=" + gson);
    }

    /**
     * 功能：[1080] 取得我的领域信息
     *
     * @param userID 用户ID
     */
    static public void doMFieldRequest(String userID, Response.Listener listener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/system/product_types";
        Map<String, String> requestParam = new HashMap<String, String>();
        requestParam.put("user_id", userID);
        requestParam.put("user_token", TongleAppInstance.getInstance().getToken());
        Gson mGson = new Gson();
        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);
        GsonRequest<MFieldResult> gsonRequest = new GsonRequest<MFieldResult>(Request.Method.POST, url, MFieldResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[1080] 取得我的领域信息 " + url + "?parameters_json=" + gson);
    }

    /**
     * 功能：[2028] 意见反馈联系方式 URL取得
     */
    static public void doFeedbackRequest(Response.Listener successListener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + "system/contact";
        Map<String, String> requestParam = new HashMap<String, String>();
        Gson mGson = new Gson();
        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);
        GsonRequest<FeedbackResult> gsonRequest = new GsonRequest<FeedbackResult>(Request.Method.POST, url, FeedbackResult.class, params_gson, successListener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[2028] 意见反馈联系方式 URL取得 " + url + "?parameters_json=" + gson);
    }

    /**
     * 功能：[2027] 服务管家QA URL取得
     */
    static public void doAppServiceRequest(Response.Listener successListener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + "system/qa_service";
        Map<String, String> requestParam = new HashMap<String, String>();
        Gson mGson = new Gson();
        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);
        GsonRequest<AppServiceResult> gsonRequest = new GsonRequest<AppServiceResult>(Request.Method.POST, url, AppServiceResult.class, params_gson, successListener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[2027] 服务管家QA URL取得 " + url + "?parameters_json=" + gson);
    }

    /**
     * 功能：[1029] 发起我的添加订单请求
     *
     * @param userID     用户ID
     * @param order_info 订单信息
     */
    static public void addOrderRequest(String userID, OrderJSONInfo order_info, Response.Listener listener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/orders/create";
        Map<String, String> requestParam = new HashMap<String, String>();
        requestParam.put("user_id", userID);
        requestParam.put("user_token", TongleAppInstance.getInstance().getToken());
        requestParam.put("buy_user_id", userID);

        Map<String, OrderJSONInfo> param = new HashMap<String, OrderJSONInfo>();
        param.put("order_info", order_info);
        Gson mGson = new Gson();
        String order_info_json = mGson.toJson(param).toString();
        requestParam.put("order_info_json", order_info_json);

        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);
        GsonRequest<OrderInfoResult> gsonRequest = new GsonRequest<OrderInfoResult>(Request.Method.POST, url, OrderInfoResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[1029] 发起我的添加订单请求 " + url + "?parameters_json=" + gson);
    }

    /**
     * 功能：[2026] 功能介绍URL取得
     */
    static public void doIntroRequest(Response.Listener successListener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + "system/function_introduction";
        Map<String, String> requestParam = new HashMap<String, String>();
        Gson mGson = new Gson();
        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);
        GsonRequest<IntroResult> gsonRequest = new GsonRequest<IntroResult>(Request.Method.POST, url, IntroResult.class, params_gson, successListener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[2026] 功能介绍URL取得 " + url + "?parameters_json=" + gson);
    }

    /**
     * 功能：[2025] 用户协议及版权申明URL取得
     */
    static public void doAgreementRequest(Response.Listener successListener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + "system/user_agreement";
        Map<String, String> requestParam = new HashMap<String, String>();
        Gson mGson = new Gson();
        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);
        GsonRequest<AgreementResult> gsonRequest = new GsonRequest<AgreementResult>(Request.Method.POST, url, AgreementResult.class, params_gson, successListener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[2025] 用户协议及版权申明URL取得 " + url + "?parameters_json=" + gson);
    }

    /**
     * 功能：[1001] 检查是否需要更新版本
     */
    static public void doVersionRequest(String mobile_os_no, String local_app_version, String app_inner_no, Response.Listener successListener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + "system/version_check";
        Map<String, String> requestParam = new HashMap<String, String>();
        requestParam.put("mobile_os_no", mobile_os_no);
        requestParam.put("local_app_version", local_app_version);
        requestParam.put("app_inner_no", app_inner_no);
        Gson mGson = new Gson();
        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);
        GsonRequest<VersionResult> gsonRequest = new GsonRequest<VersionResult>(Request.Method.POST, url, VersionResult.class, params_gson, successListener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[1001] 检查是否需要更新版本 " + url + "?parameters_json=" + gson);
    }

    /**
     * 功能：[1079] 修改登录密码
     *
     * @param param 修改密码参数
     */
    static public void doModifyPasswordRequest(ModifyPwdParam param, Response.Listener successListener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/system/password_modify";
        Map<String, String> requestParam = new HashMap<String, String>();
        requestParam.put("phone_number", param.phone_number);
        requestParam.put("user_token", TongleAppInstance.getInstance().getToken());
        requestParam.put("old_password", param.old_password);
        requestParam.put("latest_password", param.latest_password);
        requestParam.put("confirm_password", param.confirm_password);
        Gson mGson = new Gson();
        String gson = mGson.toJson(requestParam).toString();

        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);
        GsonRequest<ModifyPwdResult> gsonRequest = new GsonRequest<ModifyPwdResult>(Request.Method.POST, url, ModifyPwdResult.class, params_gson, successListener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[1079] 修改登录密码 " + url + "?parameters_json=" + gson);
    }

    /**
     * 功能：[2016]取得我的粉丝列表
     *
     * @param user_id      用户ID
     * @param fans_user_id 粉丝用户ID（可为空）
     * @param forward      翻页方向（向前翻或向后翻）（可为空）
     * @param fetch_count  返回列表条数（可为空）
     */
    static public void mineFanstRequest(String user_id, String fans_user_id, String forward, String fetch_count, Response.Listener listener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/my/fans";
        Map<String, String> requestParam = new HashMap<String, String>();
        requestParam.put("user_id", user_id);
        requestParam.put("user_token", TongleAppInstance.getInstance().getToken());
        requestParam.put("fans_user_id", fans_user_id);
        requestParam.put("forward", forward);
        requestParam.put("fetch_count", fetch_count);
        Gson mGson = new Gson();
        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);
        GsonRequest<MyFansResult> gsonRequest = new GsonRequest<MyFansResult>(Request.Method.POST, url, MyFansResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[2016]取得我的粉丝列表 " + url + "?parameters_json=" + gson);
    }

    /**
     * 功能：[1016] 将指定会员加入到我的关注列表中
     *
     * @param userID         用户ID
     * @param expert_user_id 粉丝的userID
     */
    static public void AddAttentionRequest(String userID, String expert_user_id, Response.Listener listener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/friendships/create";
        Map<String, String> requestParam = new HashMap<String, String>();
        requestParam.put("user_id", userID);
        requestParam.put("user_token", TongleAppInstance.getInstance().getToken());
        requestParam.put("expert_user_id", expert_user_id);
        Gson mGson = new Gson();
        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);
        GsonRequest<AddConnectionResult> gsonRequest = new GsonRequest<AddConnectionResult>(Request.Method.POST, url, AddConnectionResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[1016] 将指定会员加入到我的关注列表中 " + url + "?parameters_json=" + gson);
    }


    /**
     * 功能：[1064] 扫描数据解析（二维码解析）
     *
     * @param userID   用户ID
     * @param scanData 扫描出来的数据
     */
    static public void doScanDataRequest(String userID, String scanData, Response.Listener listener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/tools/scan_decode";
        Map<String, String> requestParam = new HashMap<String, String>();
        requestParam.put("user_id", userID);
        requestParam.put("user_token", TongleAppInstance.getInstance().getToken());
        requestParam.put("scan_data", scanData);
        Gson mGson = new Gson();
        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);
        GsonRequest<ScanDataResult> gsonRequest = new GsonRequest<ScanDataResult>(Request.Method.POST, url, ScanDataResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[1064] 扫描数据解析（二维码解析） " + url + "?parameters_json=" + gson);
    }

    /**
     * 功能：[1077] 音频数据解析
     *
     * @param userID   用户ID
     * @param scanData 扫描出来的数据
     */
    static public void doListennDataRequest(String userID, String scanData, Response.Listener listener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/tools/voice_decode";
        Map<String, String> requestParam = new HashMap<String, String>();
        requestParam.put("user_id", userID);
        requestParam.put("user_token", TongleAppInstance.getInstance().getToken());
        requestParam.put("scan_data", scanData);
        Gson mGson = new Gson();
        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);
        GsonRequest<ScanDataResult> gsonRequest = new GsonRequest<ScanDataResult>(Request.Method.POST, url, ScanDataResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[1077] 音频数据解析 " + url + "?parameters_json=" + gson);
    }

    /**
     * 功能：[1027] 删除购物车中的指定商品
     *
     * @param userID     用户ID
     * @param deleteList 商品明细(明细序号,商品ID)（必须指定）
     */
    static public void doDeleteInCartRequest(String userID, ArrayList deleteList, Response.Listener listener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/shopping_carts/item_remove";
        Map<String, String> requestParam = new HashMap<String, String>();
        requestParam.put("user_id", userID);
        requestParam.put("user_token", TongleAppInstance.getInstance().getToken());

        Map<String, ArrayList> param = new HashMap<>();
        param.put("detail", deleteList);
        Gson mGson = new Gson();
        String delGson = mGson.toJson(param).toString();
        requestParam.put("detail_json", delGson);

        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);
        GsonRequest<DeleteInCartResult> gsonRequest = new GsonRequest<DeleteInCartResult>(Request.Method.POST, url, DeleteInCartResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[1027] 删除购物车中的指定商品 " + url + "?parameters_json=" + gson);
    }

    /**
     * 功能：[2022] 商品转发
     *
     * @param userID     用户ID
     * @param productId  商品ID
     * @param message    转发内容
     * @param repostType 转发类别
     */
    static public void doProdForwardRequest(String userID, String productId, String message, String repostType, String relationId, Response.Listener listener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/products/repost";
        Map<String, String> requestParam = new HashMap<String, String>();
        requestParam.put("user_id", userID);
        requestParam.put("user_token", TongleAppInstance.getInstance().getToken());
        requestParam.put("product_id", productId);
        requestParam.put("message", message);
        requestParam.put("repost_type", repostType);
        requestParam.put("relation_id", relationId);

        Gson mGson = new Gson();
        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);
        GsonRequest<ProdForwardResult> gsonRequest = new GsonRequest<ProdForwardResult>(Request.Method.POST, url, ProdForwardResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[2022] 商品转发 " + url + "?parameters_json=" + gson);
    }

    /**
     * 功能：[1020] 帖子转发
     *
     * @param userID       用户ID
     * @param parentPostID 原帖子ID
     * @param message      转发内容
     * @param repostType   转发类别
     */
    static public void doPostForwardRequest(String userID, String parentPostID, String message, String repostType, Response.Listener listener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/posts/repost";
        Map<String, String> requestParam = new HashMap<String, String>();
        requestParam.put("user_id", userID);
        requestParam.put("user_token", TongleAppInstance.getInstance().getToken());
        requestParam.put("parent_post_id", parentPostID);
        requestParam.put("message", message);
        requestParam.put("repost_type", repostType);

        Gson mGson = new Gson();
        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);
        GsonRequest<PostForwardResult> gsonRequest = new GsonRequest<PostForwardResult>(Request.Method.POST, url, PostForwardResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[1020] 帖子转发 " + url + "?parameters_json=" + gson);
    }

    /**
     * 功能：[2033] 我的订单---立即支付(微信)
     *
     * @param userID  用户ID
     * @param orderNo 订单编号
     */
    static public void WeChatPayRequest(String userID, String orderNo, Response.Listener listener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/orders/weixin_pay_parameter";
        Map<String, String> requestParam = new HashMap<String, String>();
        requestParam.put("user_id", userID);
        requestParam.put("user_token", TongleAppInstance.getInstance().getToken());
        requestParam.put("order_no", orderNo);
        requestParam.put("app_inner_no", TongleAppInstance.getApp_inner_no());
        requestParam.put("pay_method", TongleAppConst.PAY_TYPE_WECHAT);
        Gson mGson = new Gson();
        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);
        GsonRequest<WeChatPayResult> gsonRequest = new GsonRequest<WeChatPayResult>(Request.Method.POST, url, WeChatPayResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[2033] 我的订单---立即支付(微信) " + url + "?parameters_json=" + gson);
    }

    /**
     * 功能[1049]：修改用户的个人资料
     *
     * @param userID                 用户ID
     * @param head_photo_binary_data 头像二进制数据（可为空）
     * @param user_nick_name         昵称（可为空）
     * @param sex                    性别（可为空）
     * @param id_no                  性别（可为空）
     * @param customs_flag           性别（可为空）
     * @param user_name              性别（可为空）
     */
    static public void ModifyMessageRequest(String userID, String head_photo_binary_data, String user_nick_name, String sex, String id_no, String customs_flag, String user_name, Response.Listener listener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/my/personal_info_modify";
        Map<String, String> requestParam = new HashMap<String, String>();
        requestParam.put("user_id", userID);
        requestParam.put("user_token", TongleAppInstance.getInstance().getToken());
        requestParam.put("head_photo_binary_data", head_photo_binary_data);
        requestParam.put("user_nick_name", user_nick_name);
        requestParam.put("sex", sex);
        requestParam.put("id_no", id_no);
        requestParam.put("customs_flag", customs_flag);
        requestParam.put("user_name", user_name);
        Gson mGson = new Gson();
        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);
        GsonRequest<ModifyMessageResult> gsonRequest = new GsonRequest<ModifyMessageResult>(Request.Method.POST, url, ModifyMessageResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[1049]修改用户的个人资料 " + url + "?parameters_json=" + gson);
    }

    /**
     * 功能：[2041] 再次通关
     *
     * @param userID   用户ID
     * @param order_no 订单编号
     */
    static public void doReCustomers(String userID, String order_no, Response.Listener listener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/tools/re_customers";
        Map<String, String> requestParam = new HashMap<String, String>();
        requestParam.put("user_id", userID);
        requestParam.put("user_token", TongleAppInstance.getInstance().getToken());
        requestParam.put("order_no", order_no);
        Gson mGson = new Gson();
        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);
        GsonRequest<CodeResult> gsonRequest = new GsonRequest<CodeResult>(Request.Method.POST, url, CodeResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[2041] 再次通关 " + url + "?parameters_json=" + gson);
    }

    //    /**
//     * 功能：[2042] 购物车结算
//     *
//     * @param userID     用户ID
//     * @param product_info 订单信息
//     */
//    /**
    static public void addCartRequest(String userID, ArrayList product_info, Response.Listener listener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/shopping_carts/balance";
        Map<String, String> requestParam = new HashMap<String, String>();
        requestParam.put("user_id", userID);
        requestParam.put("user_token", TongleAppInstance.getInstance().getToken());

        Gson mGson = new Gson();
        Map<String, ArrayList> typeListParam = new HashMap<>();
        typeListParam.put("product_info", product_info);
        String typeListString = mGson.toJson(typeListParam).toString();
        requestParam.put("product_info_json", typeListString);

        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);
        GsonRequest<CartSettlementResult> gsonRequest = new GsonRequest<CartSettlementResult>(Request.Method.POST, url, CartSettlementResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[2042] 购物车结算 " + url + "?parameters_json=" + gson);
    }

    /**
     * 功能：[2040] 会员个人二维码解析
     *
     * @param scan_data 扫出的原始数据
     */
    static public void scanUserDecode(String scan_data, Response.Listener listener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + "/tools/scan_user_decode";
        Map<String, String> requestParam = new HashMap<String, String>();
        requestParam.put("scan_data", scan_data);
//        Gson mGson = new Gson();
//        String gson = mGson.toJson(requestParam).toString();
        GsonBuilder gb = new GsonBuilder();
        gb.disableHtmlEscaping();
        String gson = gb.create().toJson(requestParam);
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);
        GsonRequest<CodeBaseResult> gsonRequest = new GsonRequest<CodeBaseResult>(Request.Method.POST, url, CodeBaseResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[2040] 会员个人二维码解析 " + url + "?parameters_json=" + gson);
    }

    /**
     * 功能：[2035] 获取专家会员基本信息
     *
     * @param user_id        用户ID（必须指定）
     * @param expert_user_id 专家会员ID（必须指定）
     */
    static public void doExpertBaseRequest(String user_id, String expert_user_id, Response.Listener listener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + "/user/expert_user_info";
        Map<String, String> requestParam = new HashMap<String, String>();
        requestParam.put("user_id", user_id);
        requestParam.put("user_token", TongleAppInstance.getInstance().getToken());
        requestParam.put("expert_user_id", expert_user_id);
        Gson mGson = new Gson();
        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);
        GsonRequest<ExpertBaseResult> gsonRequest = new GsonRequest<ExpertBaseResult>(Request.Method.POST, url, ExpertBaseResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[2035] 获取专家会员基本信息 " + url + "?parameters_json=" + gson);
    }

    /**
     * 功能：[1048]取得相关会员的积分信息
     *
     * @param userID 用户ID
     */
    static public void doMyPointsRequest(String userID, Response.Listener listener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/my/points";
        Map<String, String> requestParam = new HashMap<String, String>();
        requestParam.put("user_id", userID);
        requestParam.put("user_token", TongleAppInstance.getInstance().getToken());
        Gson mGson = new Gson();
        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);
        GsonRequest<MyPointsResult> gsonRequest = new GsonRequest<MyPointsResult>(Request.Method.POST, url, MyPointsResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[1048]取得相关会员的积分信息" + url + "?parameters_json=" + gson);
    }

    /**
     * 功能 【1028】 修改购物车中指定商品的订购数量
     *
     * @param user_id
     * @param seq_no        商品明细ID
     * @param product_id    商品ID
     * @param order_qty     商品数量
     * @param listener
     * @param errorListener
     */
    static public void doCartNumberModify(String user_id, String seq_no, String product_id, String order_qty, Response.Listener<DefualtResult> listener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/shopping_carts/item_modify";
        Map<String, String> requestParam = new HashMap<>();
        requestParam.put("user_id", user_id);
        requestParam.put("user_token", TongleAppInstance.getInstance().getToken());
        requestParam.put("seq_no", seq_no);
        requestParam.put("product_id", product_id);
        requestParam.put("order_qty", order_qty);
        Gson mGson = new Gson();
        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);
        GsonRequest<DefualtResult> request = new GsonRequest<>(Request.Method.POST, url, DefualtResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(request);
        Log.e("tag", "[1028]修改购物车中指定商品的订购数量 " + url + "?parameters_json=" + gson);
    }

    /**
     * 功能：[6007] 取得团购商品信息
     *
     * @param param 取得团购商品详情的参数类
     */
    static public void doProductTicketDetailRequest(ProdDetailRequestParam param, Response.Listener listener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/gp_product/show";
        Map<String, String> requestParam = new HashMap<String, String>();
        requestParam.put("user_id", TongleAppInstance.getInstance().getUserID());
        requestParam.put("user_token", TongleAppInstance.getInstance().getToken());
        requestParam.put("product_id", param.product_id);
        requestParam.put("post_id", param.post_id);
        requestParam.put("action", param.action);
        requestParam.put("mstore_id", param.mstore_id);
        requestParam.put("relation_id", param.relation_id);
        requestParam.put("longitude", TongleAppInstance.getInstance().getLongitude());
        requestParam.put("latitude", TongleAppInstance.getInstance().getLatitude());
        Gson mGson = new Gson();
        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);
        GsonRequest<GroupProductResult> gsonRequest = new GsonRequest<GroupProductResult>(Request.Method.POST, url, GroupProductResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[6007] 取得团购商品信息 " + url + "?parameters_json=" + gson);
    }

    /**
     * 功能：[6008] 取得商户列表
     *
     * @param storeID     商户ID
     * @param prodID      商品ID
     * @param forward     翻页方向
     * @param fetch_count 返回条数
     */
    static public void doProdStoreListRequest(String storeID, String prodID, String forward, String fetch_count, Response.Listener listener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/system/shop_list_show";
        Map<String, String> requestParam = new HashMap<String, String>();
        requestParam.put("user_id", TongleAppInstance.getInstance().getUserID());
        requestParam.put("user_token", TongleAppInstance.getInstance().getToken());
        requestParam.put("id", storeID);
        requestParam.put("product_id", prodID);
        requestParam.put("forward", forward);
        requestParam.put("fetch_count", fetch_count);
        requestParam.put("longitude", TongleAppInstance.getInstance().getLongitude());
        requestParam.put("latitude", TongleAppInstance.getInstance().getLatitude());
        Gson mGson = new Gson();
        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);
        GsonRequest<GroupStoreListResult> gsonRequest = new GsonRequest<GroupStoreListResult>(Request.Method.POST, url, GroupStoreListResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[6008] 取得商户列表 " + url + "?parameters_json=" + gson);
    }

    /**
     * 功能：[6009] 团购券商品提交订单
     *
     * @param param 提交订单入口参数类
     */
    static public void doTicketOrderSubmitRequest(TicketOrderSubmitRequestParam param, Response.Listener listener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/gp_product/order_create";
        Gson mGson = new Gson();
        String gson = mGson.toJson(param);
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);
        GsonRequest<GroupOrderGreatResult> gsonRequest = new GsonRequest<GroupOrderGreatResult>(Request.Method.POST, url, GroupOrderGreatResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[6009] 提交订单 " + url + "?parameters_json=" + gson);
    }

    /**
     * 功能：[6015] 获取我的可用优惠券
     *
     * @param userID     用户ID
     * @param goods_info 商品信息
     */
    static public void doMyUsefulCouponRequest(String userID, List<MyCouponAvailableItem> goods_info, Response.Listener listener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/orders/useful_vouchers_info";
        Map<String, Object> requestParam = new HashMap<String, Object>();
        requestParam.put("user_id", userID);
        requestParam.put("user_token", TongleAppInstance.getInstance().getToken());
        requestParam.put("goods_info", goods_info);
        Gson mGson = new Gson();
        String gson = mGson.toJson(requestParam);
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);
        GsonRequest<MyCouponAvailableResult> gsonRequest = new GsonRequest<MyCouponAvailableResult>(Request.Method.POST, url, MyCouponAvailableResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[6015] 获取我的可用优惠券 " + url + "?parameters_json=" + gson);
    }

    /**
     * 功能：[6010] 取得我的团购券列表
     *
     * @param userID      用户ID
     * @param orderNo     订单编号
     * @param outOfDate   过期/未消费标志
     * @param forward     翻页方向
     * @param fetch_count 返回条数
     */
    static public void doMyGroupTicketListRequest(String userID, String orderNo, String outOfDate, String forward, String fetch_count, Response.Listener listener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/my/gp_coupons";
        Map<String, String> requestParam = new HashMap<String, String>();
        requestParam.put("user_id", userID);
        requestParam.put("user_token", TongleAppInstance.getInstance().getToken());
        requestParam.put("order_no", orderNo);
        requestParam.put("out_of_date", outOfDate);
        requestParam.put("forward", forward);
        requestParam.put("fetch_count", fetch_count);
        Gson mGson = new Gson();
        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);
        GsonRequest<CouponListResult> gsonRequest = new GsonRequest<CouponListResult>(Request.Method.POST, url, CouponListResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[6010] 取得我的团购券列表 " + url + "?parameters_json=" + gson);
    }

    /**
     * 功能：[6011] 取得我的团购券详情
     *
     * @param userID     用户ID
     * @param orderNo    订单编号
     * @param couponCode 券号
     * @param outOfDate  过期/未消费标志
     */
    static public void doGroupTicketDetailRequest(String userID, String orderNo, String couponCode, String outOfDate, Response.Listener listener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/gp_coupon/show";
        Map<String, String> requestParam = new HashMap<String, String>();
        requestParam.put("user_id", userID);
        requestParam.put("user_token", TongleAppInstance.getInstance().getToken());
        requestParam.put("order_no", orderNo);
        requestParam.put("coupon_code", couponCode);
        requestParam.put("out_of_date", outOfDate);
        requestParam.put("longitude", TongleAppInstance.getInstance().getLongitude());
        requestParam.put("latitude", TongleAppInstance.getInstance().getLatitude());
        Gson mGson = new Gson();
        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);
        GsonRequest<CouponDetailResult> gsonRequest = new GsonRequest<CouponDetailResult>(Request.Method.POST, url, CouponDetailResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[6011] 取得我的团购券详情 " + url + "?parameters_json=" + gson);
    }

    /**
     * 功能：[6012] 取得我的团购订单列表
     *
     * @param param 订单请求参数类
     */
    static public void doMyGroupOrderListRequest(OrderRequestParam param, Response.Listener listener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/orders/gp_orders";
        Map<String, String> requestParam = new HashMap<String, String>();
        requestParam.put("user_id", param.user_id);
        requestParam.put("user_token", TongleAppInstance.getInstance().getToken());
        requestParam.put("order_status", param.order_status);
        requestParam.put("order_date_from", param.order_date_from);
        requestParam.put("order_date_to", param.order_date_to);
        requestParam.put("order_no", param.order_no);
        requestParam.put("forward", param.forward);
        requestParam.put("fetch_count", param.fetch_count);
        Gson mGson = new Gson();
        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);
        GsonRequest<GroupOrderListResult> gsonRequest = new GsonRequest<GroupOrderListResult>(Request.Method.POST, url, GroupOrderListResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[6012] 取得我的团购订单列表 " + url + "?parameters_json=" + gson);
    }

    /**
     * 功能：[6013] 取得我的团购订单详情
     *
     * @param userID  用户ID
     * @param orderNo 订单编号
     */
    static public void doMyGroupOrderDetailRequest(String userID, String orderNo, Response.Listener listener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/order/gp_orders_detail";
        Map<String, String> requestParam = new HashMap<String, String>();
        requestParam.put("user_id", userID);
        requestParam.put("user_token", TongleAppInstance.getInstance().getToken());
        requestParam.put("order_no", orderNo);
        Gson mGson = new Gson();
        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);
        GsonRequest<GroupOrderDetailResult> gsonRequest = new GsonRequest<GroupOrderDetailResult>(Request.Method.POST, url, GroupOrderDetailResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[6013] 取得我的团购订单详情 " + url + "?parameters_json=" + gson);
    }

    /**
     * 功能：[6014] 取得我的优惠券列表
     *
     * @param userID 用户ID
     */
    static public void doMyCouponListRequest(String userID, Response.Listener listener, Response.ErrorListener errorListener) {
        String url = TongleAppConst.SERVER_ADDRESS + TongleAppInstance.getInstance().getToken() + "/orders/my_vouchers_info";
        Map<String, String> requestParam = new HashMap<String, String>();
        requestParam.put("user_id", userID);
        requestParam.put("user_token", TongleAppInstance.getInstance().getToken());
        Gson mGson = new Gson();
        String gson = mGson.toJson(requestParam).toString();
        Map<String, String> params_gson = new HashMap();
        params_gson.put("parameters_json", gson);
        GsonRequest<MyCouponInfoResult> gsonRequest = new GsonRequest<MyCouponInfoResult>(Request.Method.POST, url, MyCouponInfoResult.class, params_gson, listener, errorListener);
        TongleAppInstance.getInstance().getRequestQueue().add(gsonRequest);
        Log.e("tag", "[6014] 取得我的优惠券列表 " + url + "?parameters_json=" + gson);
    }
}
