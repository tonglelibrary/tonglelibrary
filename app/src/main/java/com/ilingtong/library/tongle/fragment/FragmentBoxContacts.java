//package com.ilingtong.app.tongle.fragment;
//
//import android.content.res.ColorStateList;
//import android.content.res.Resources;
//import android.os.Bundle;
//import android.support.v4.app.FragmentTransaction;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ImageView;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//
//import com.ilingtong.app.tongle.R;
//
///**
// * User: shuailei
// * Date: 2015/6/25
// * Time: 16:47
// * Email: leishuai@isoftstone.com
// * Dest: 我的人脉
// */
//public class FragmentBoxContacts extends BaseFragment {
//    private TextView updateText;
//    private TextView productText;
//    private RelativeLayout updateFrame;
//    private RelativeLayout productFrame;
//    private ImageView expert_detail_left_cusor;
//    private ImageView expert_detail_right_cusor;
//
//    private MyAttentionFragment updateFragment;
//    private MyFansFragment productFragment;
//
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        View rootView = inflater.inflate(R.layout.fragment_box_contacts_layout, container, false);
//        initView(rootView);
//        return rootView;
//    }
//
//    public void initView(View rootView) {
//        updateText = (TextView) rootView.findViewById(R.id.expert_update_text);
//        productText = (TextView) rootView.findViewById(R.id.expert_product_text);
//        updateFrame = (RelativeLayout) rootView.findViewById(R.id.update_frame);
//        productFrame = (RelativeLayout) rootView.findViewById(R.id.product_frame);
//        expert_detail_left_cusor = (ImageView) rootView.findViewById(R.id.expert_detail_left_cusor);
//        expert_detail_right_cusor = (ImageView) rootView.findViewById(R.id.expert_detail_right_cusor);
//        updateFrame.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onTabSelected("attention");
//                expert_detail_left_cusor.setVisibility(View.VISIBLE);
//                expert_detail_right_cusor.setVisibility(View.GONE);
//            }
//        });
//        productFrame.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onTabSelected("fans");
//                expert_detail_left_cusor.setVisibility(View.GONE);
//                expert_detail_right_cusor.setVisibility(View.VISIBLE);
//            }
//        });
//        onTabSelected("attention");
//    }
//
//    public void onTabSelected(String tabName) {
//        Resources resource = (Resources) getResources();
//        final ColorStateList selectedTextColor = (ColorStateList) resource.getColorStateList(R.color.expert_select_color);
//        final ColorStateList unselectedTextColor = (ColorStateList) resource.getColorStateList(R.color.expert_unselect_color);
//        if (tabName == "attention") {
//            if (null == updateFragment) {
//                updateFragment = new MyAttentionFragment();
//            }
//            FragmentTransaction localFragmentTransaction = getFragmentManager().beginTransaction();
//            localFragmentTransaction.replace(R.id.expert_detail_fragment_container, updateFragment);
//            localFragmentTransaction.commit();
//            updateText.setTextColor(selectedTextColor);
//            productText.setTextColor(unselectedTextColor);
//        } else if (tabName == "fans") {
//            if (null == productFragment) {
//                productFragment = new MyFansFragment();
//            }
//            FragmentTransaction localFragmentTransaction = getFragmentManager().beginTransaction();
//            localFragmentTransaction.replace(R.id.expert_detail_fragment_container, productFragment);
//            localFragmentTransaction.commit();
//            updateText.setTextColor(unselectedTextColor);
//            productText.setTextColor(selectedTextColor);
//        }
//    }
//}
