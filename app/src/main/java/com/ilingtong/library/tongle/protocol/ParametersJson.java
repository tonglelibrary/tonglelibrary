package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * Created by Administrator on 2015/5/11.
 */
public class ParametersJson implements Serializable {
    public HeadJson head;
    public BodyJson body;
}
