package com.ilingtong.library.tongle.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.adapter.MStoreListAdapter;
import com.ilingtong.library.tongle.external.maxwin.view.XListView;
import com.ilingtong.library.tongle.fragment.HomeFragment;
import com.ilingtong.library.tongle.model.HomeModel;
import com.ilingtong.library.tongle.protocol.MStoreListItem;
import com.ilingtong.library.tongle.protocol.MStoreRequestParam;
import com.ilingtong.library.tongle.protocol.MStoreResult;
import com.ilingtong.library.tongle.utils.ToastUtils;

import java.util.ArrayList;

/**
 * User: lengjiqiang
 * Date: 2015/5/10
 * Time: 13:01
 * Email: jqleng@isoftstone.com
 * Desc: 魔店列表
 * 修改记录： 2016/03/25 替换顶部title栏。统一用topview_layout
 */
public class CollectMStoreActivity extends BaseActivity implements XListView.IXListViewListener {
    private ImageView left_arrow_btn;
    private TextView top_name;
    private XListView mstoreList;
    private LinearLayout mostore_linear;
    private MStoreListAdapter mstoreListAdaper;
    private HomeModel homeModel;
    private MStoreRequestParam mStoreRequestParam;
    ArrayList<MStoreListItem> mstore_list = new ArrayList<>();
    private boolean flag = true;
    private int intoType = 0;
    private int listIndex = -1;
    static CollectMStoreActivity instance;
    Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    mstoreListAdaper.notifyDataSetChanged();
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.collect_mstore_layout);
        instance = this;
        getData();
        initData();
        initView();
    }

    public void getData() {
        intoType = getIntent().getIntExtra(TongleAppConst.INTO_TYPE, 0);
    }

    public void initView() {
        flag = true;
        if (homeModel == null)
            homeModel = new HomeModel();
        top_name = (TextView) findViewById(R.id.top_name);
        top_name.setText(getString(R.string.mstore_text));
        left_arrow_btn = (ImageView) findViewById(R.id.left_arrow_btn);

        top_name.setVisibility(View.VISIBLE);
        left_arrow_btn.setVisibility(View.VISIBLE);

        mstoreList = (XListView)findViewById(R.id.mstore_listview);
        mostore_linear = (LinearLayout)findViewById(R.id.mostore_linear);
        mstoreListAdaper = new MStoreListAdapter(this, mstore_list);
        mstoreList.setAdapter(mstoreListAdaper);
        mstoreList.setXListViewListener(this, 0);
        mstoreList.setRefreshTime();
        mstoreList.setPullLoadEnable(false);
        left_arrow_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CollectMStoreActivity.this.finish();
            }
        });
        mstoreList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(CollectMStoreActivity.this, CollectMStoreDetailActivity.class);
                Bundle bundle = new Bundle();
                bundle.putInt(TongleAppConst.INTO_TYPE, TongleAppConst.HOME_INTO);
                bundle.putString("mstore_id", HomeModel.mstore_list.get(position - 1).mstore_id);
                intent.putExtras(bundle);
                startActivityForResult(intent, 10001);
                listIndex = position - 1;
            }
        });
        mostore_linear.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 10001 && HomeFragment.MSTORE_UPDATE_FLAG) {
            mstore_list.remove(listIndex);
            mHandler.sendEmptyMessage(0);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void initData() {
        if (intoType == TongleAppConst.HOME_INTO) {
            mstore_list = (ArrayList<MStoreListItem>) getIntent().getSerializableExtra("mstore_list");
        } else {
            //doRequest();
        }
    }

    @Override
    public void onRefresh(int id) {
        //刷新之前，清空list
        if (mstore_list != null) {
            mstore_list.clear();
        }
        mStoreRequestParam = new MStoreRequestParam();
        mStoreRequestParam.user_id = TongleAppInstance.getInstance().getUserID();
        mStoreRequestParam.fetch_count = TongleAppConst.FETCH_COUNT;
        mStoreRequestParam.forward = "1";
        ServiceManager.doMStoreRequest(mStoreRequestParam, mstoreSuccessListener(), errorListener());
    }

    @Override
    public void onLoadMore(int id) {
        //加载更多
        if (flag) {
            //第二个参数prod_id是list里最后一个参数的id
            mStoreRequestParam = new MStoreRequestParam();
            mStoreRequestParam.user_id = TongleAppInstance.getInstance().getUserID();
            mStoreRequestParam.mstore_id = mstore_list.get(mstore_list.size() - 1).mstore_id;
            mStoreRequestParam.fetch_count = TongleAppConst.FETCH_COUNT;
            mStoreRequestParam.forward = "1";
            ServiceManager.doMStoreRequest(mStoreRequestParam, mstoreSuccessListener(), errorListener());
        } else {
            ToastUtils.toastShort(getString(R.string.common_list_end));
            mstoreList.setPullLoadEnable(false);
        }
    }

    /**
     * 功能：请求魔店网络响应成功，返回数据
     */
    private Response.Listener<MStoreResult> mstoreSuccessListener() {
        return new Response.Listener<MStoreResult>() {
            @Override
            public void onResponse(MStoreResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    //第一次请求或刷新页面时候判断是否隐藏底部加载更多按钮
                    if (response.getBody().getMStoreList().size() < Integer.parseInt(TongleAppConst.FETCH_COUNT)) {
                        flag = false;
                        mstoreList.setPullLoadEnable(false);
                    } else {
                        mstoreList.setPullLoadEnable(true);
                    }
                    mstore_list.addAll(response.getBody().getMStoreList());
                } else {
                    ToastUtils.toastShort(getString(R.string.para_exception) + response.getHead().getReturn_message());
                }
                //调用handler，发送消息
                mHandler.sendEmptyMessage(0);
            }
        };
    }

    /**
     * 功能：网络响应失败
     */
    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ToastUtils.toastShort(getString(R.string.sys_exception));
            }
        };
    }
}