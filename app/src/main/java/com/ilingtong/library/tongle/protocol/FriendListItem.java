package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/5/9
 * Time: 16:51
 * Email: jqleng@isoftstone.com
 * Desc:
 */
public class FriendListItem implements Serializable{
    public String user_id;
    public String user_nick_name;
    public String user_signature;
    public String user_head_photo_url;
    public String latest_post_info;
    public String latest_post_count;
    public String post_update_time;
    public String user_favorited_by_me;
}
