package com.ilingtong.library.tongle.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.model.APPpromotionModel;
import com.ilingtong.library.tongle.protocol.APPpromotionResult;
import com.ilingtong.library.tongle.utils.ToastUtils;
import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * User:shiyuchong
 * Date:2015/6/23
 * Time:AM 11:16
 * Email:ycshi@isoftstone.com
 * Desc:我的--》我的百宝箱--》APP下载
 */
public class SettingMyBoxAPPActivity extends BaseActivity implements View.OnClickListener{
    private TextView top_name;
    private ImageView left_arrow_btn;
    private ImageView app_promote_android_iv;
    private APPpromotionModel promotemodel = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.setting_mybox_app_layout);
        initView();
        doRequest();
    }
    private void initView() {
        if (promotemodel==null)
            promotemodel = new APPpromotionModel();
        top_name = (TextView) findViewById(R.id.top_name);
        left_arrow_btn = (ImageView) findViewById(R.id.left_arrow_btn);
        app_promote_android_iv = (ImageView) findViewById(R.id.app_promote_android_iv);

        left_arrow_btn.setOnClickListener(this);
        app_promote_android_iv.setOnClickListener(this);
        left_arrow_btn.setVisibility(View.VISIBLE);
        top_name.setText(getString(R.string.setting_my_box_app_top_name));
        top_name.setGravity(View.TEXT_ALIGNMENT_CENTER);
        top_name.setVisibility(View.VISIBLE);
    }

    public void doRequest(){
        ServiceManager.getAppPromoteRequest(TongleAppInstance.getInstance().getUserID(), successListener(), errorListener());
    }
    /**
     * 功能：网络响应成功，返回数据
     */
    private Response.Listener successListener() {
        return new Response.Listener<APPpromotionResult>() {
            @Override
            public void onResponse(APPpromotionResult  aPPpromotionResult ) {

                if (TongleAppConst.SUCCESS.equals(aPPpromotionResult.getHead().getReturn_flag())) {
                    promotemodel.app_qr_code_url = aPPpromotionResult.getBody().getApp_qr_code_url();
                    updateView();
                } else {
                    ToastUtils.toastShort(getResources().getString(R.string.para_exception)+aPPpromotionResult.getHead().getReturn_message());
                }
            }

        };
    }

    /**
     * 功能：网络响应失败
     */
    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ToastUtils.toastShort(getResources().getString(R.string.sys_exception));
            }
        };
    }
    /**
     * 填充图片
     */
    public void updateView() {
        String url = promotemodel.app_qr_code_url;
        if (!TextUtils.isEmpty(url)){
            ImageLoader.getInstance().displayImage(url,app_promote_android_iv, TongleAppInstance.options);
        }
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.left_arrow_btn){
            finish();
        }
    }
}
