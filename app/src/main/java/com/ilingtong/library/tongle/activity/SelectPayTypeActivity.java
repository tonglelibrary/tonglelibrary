package com.ilingtong.library.tongle.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.alipay.sdk.app.PayTask;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.adapter.PayTypeListAdapter;
import com.ilingtong.library.tongle.demo.PayResult;
import com.ilingtong.library.tongle.protocol.AliPayResult;
import com.ilingtong.library.tongle.protocol.BaseDataResult;
import com.ilingtong.library.tongle.protocol.DataListItem;
import com.ilingtong.library.tongle.protocol.DataTypeName;
import com.ilingtong.library.tongle.protocol.DataTypes;
import com.ilingtong.library.tongle.protocol.WeChatPayResult;
import com.ilingtong.library.tongle.utils.DialogUtils;
import com.ilingtong.library.tongle.utils.ToastUtils;
import com.tencent.mm.sdk.modelpay.PayReq;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * author: liuting
 * Date: 2016/3/11
 * Time: 14:01
 * Email: liuting@ilingtong.com
 * Desc:选择支付方式
 */
public class SelectPayTypeActivity extends BaseActivity implements View.OnClickListener {
    private TextView mTxtTitle;//标题
    private ImageView mImgBack;//返回图标
    private ListView mLvPay;//支付列表选项
    private Button mBtnPay;//支付按钮
    private AlertDialog mAlert;//对话框
    private Button mBtnCancel;//取消操作
    private Button mBtnSure;//确定取消支付
    private PayTypeListAdapter adapter;
    private List<DataListItem> payTypeList = new ArrayList<>();
    private String selectedPayType = "";  //选中的支付方式
    private String intoType;  //画面入口  如：传统商品：2、团购商品:9。

    private Dialog dialog;
    private CheckBox lastCheckedOption;
    private String orderNo;  //订单编号
    IWXAPI api;  //微信支付相关

    public static final int REFRESH_LIST = 0;
    private static final int SDK_PAY_FLAG = 1;
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case REFRESH_LIST:
                    adapter.notifyDataSetChanged();
                    break;
                case SDK_PAY_FLAG:
                    PayResult payResult = new PayResult((String) msg.obj);
                    // 支付宝返回此次支付结果及加签，建议对支付宝签名信息拿签约时支付宝提供的公钥做验签
                    String resultInfo = payResult.getResult();

                    String resultStatus = payResult.getResultStatus();

                    // 判断resultStatus 为“9000”则代表支付成功，具体状态码代表含义可参考接口文档
                    if (TextUtils.equals(resultStatus, "9000")) {
                        ToastUtils.toastShort(getString(R.string.common_pay_success));
                        paySuccess(intoType);
                    } else {
                        // 判断resultStatus 为非“9000”则代表可能支付失败
                        // “8000”代表支付结果因为支付渠道原因或者系统原因还在等待支付结果确认，最终交易是否成功以服务端异步通知为准（小概率状态）
                        if (TextUtils.equals(resultStatus, "8000")) {
                            ToastUtils.toastShort(getString(R.string.common_pay_not_success));
                            paySuccess(intoType);
                        } else {
                            // 其他值就可以判断为支付失败，包括用户主动取消支付，或者系统返回的错误
                            ToastUtils.toastShort(getString(R.string.common_pay_error));
                        }
                    }
                    break;
            }
        }
    };


    /**
     * @param context
     * @param orderNo 订单编号
     */
    public static void launcher(Activity context, String orderNo, String intoType) {
        Intent intent = new Intent(context, SelectPayTypeActivity.class);
        intent.putExtra("orderNo", orderNo);
        intent.putExtra("intoType", intoType);
        context.startActivity(intent);
    }

    /**
     * @param context
     * @param orderNo 订单编号
     */
    public static void launcherForResult(Activity context, String orderNo, String intoType, int requestCode) {
        Intent intent = new Intent(context, SelectPayTypeActivity.class);
        intent.putExtra("orderNo", orderNo);
        intent.putExtra("intoType", intoType);
        context.startActivityForResult(intent, requestCode);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_pay_type);

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(payBroadcastAction);
        registerReceiver(payFinishReceiver, intentFilter);

        initView();
        getData();
    }

    /**
     * 获取数据
     */
    private void getData() {
        orderNo = getIntent().getStringExtra("orderNo");
        intoType = getIntent().getStringExtra("intoType");
        DataTypes types = new DataTypes();
        List<DataTypeName> list = new ArrayList<>();
        list.add(new DataTypeName(intoType));   //这里intoType的值跟接口定义中DataTypeName常量值保持一致，值传递和判断类别只看这一个变量。
        types.setTypes(list);
        Gson gson = new Gson();
        dialog.show();
        ServiceManager.doBaseDataRequest(gson.toJson(types).toString(), getSuccessListener(), errorListener());

    }

    private Response.Listener<BaseDataResult> getSuccessListener() {
        return new Response.Listener<BaseDataResult>() {
            @Override
            public void onResponse(BaseDataResult response) {
                dialog.dismiss();
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    payTypeList.addAll(response.getBody().getBase_data_list().get(0).data_list);
                    handler.sendEmptyMessage(REFRESH_LIST);
                } else {
                    ToastUtils.toastShort(getString(R.string.para_exception) + response.getHead().getReturn_message());
                }
            }
        };
    }

    public void initView() {
        dialog = DialogUtils.createLoadingDialog(this);
        mTxtTitle = (TextView) findViewById(R.id.top_name);
        mImgBack = (ImageView) findViewById(R.id.left_arrow_btn);
        mLvPay = (ListView) findViewById(R.id.select_pay_type_lv_list);
        mTxtTitle.setText(getResources().getString(R.string.select_pay_type_top_name));
        mTxtTitle.setVisibility(View.VISIBLE);
        mImgBack.setVisibility(View.VISIBLE);
        mImgBack.setOnClickListener(this);

        mLvPay = (ListView) findViewById(R.id.select_pay_type_lv_list);
        adapter = new PayTypeListAdapter(payTypeList, this);
        mLvPay.setAdapter(adapter);
        mLvPay.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (lastCheckedOption != null) {
                    lastCheckedOption.setChecked(false);
                }
                lastCheckedOption = (CheckBox) view.findViewById(R.id.select_pay_type_img_select);
                lastCheckedOption.setChecked(true);
                selectedPayType = payTypeList.get(position).getCode();
            }
        });
        mBtnPay = (Button) findViewById(R.id.select_pay_type_btn_pay);
        mBtnPay.setOnClickListener(this);
    }

    /**
     * 弹出取消支付对话框
     */
    private void showCancelPayDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View view = LayoutInflater.from(this).inflate(R.layout.cancel_pay_dialog_layout, null);
        builder.setView(view);
        mAlert = builder.create();
        mAlert.setCanceledOnTouchOutside(true);
        mAlert.show();
        mBtnCancel = (Button) view.findViewById(R.id.cancel_pay_btn_cancel);
        mBtnSure = (Button) view.findViewById(R.id.cancel_pay_btn_sure);
        mBtnCancel.setOnClickListener(this);
        mBtnSure.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
//        switch (v.getId()) {
//            case R.id.left_arrow_btn:
//                showCancelPayDialog();
//                break;
//            case R.id.select_pay_type_btn_pay:  //去支付
//                if (TextUtils.isEmpty(selectedPayType)) {
//                    ToastUtils.toastShort(R.string.select_pay_type_null_msg);
//                } else {
//                    toPay(Integer.parseInt(selectedPayType));
//                }
//                break;
//            case R.id.cancel_pay_btn_cancel:
//                mAlert.dismiss();
//                break;
//            case R.id.cancel_pay_btn_sure://确定取消支付
//                payCancle(intoType);
//                break;
//            default:
//                break;
//        }

        if (v.getId() == R.id.left_arrow_btn) {
            showCancelPayDialog();
        } else if (v.getId() == R.id.select_pay_type_btn_pay) {//去支付
            if (TextUtils.isEmpty(selectedPayType)) {
                ToastUtils.toastShort(R.string.select_pay_type_null_msg);
            } else {
                toPay(Integer.parseInt(selectedPayType));
            }
        } else if (v.getId() == R.id.cancel_pay_btn_cancel) {
            mAlert.dismiss();
        } else if (v.getId() == R.id.cancel_pay_btn_sure) {//确定取消支付
            payCancle(intoType);
        }

    }

    private void toPay(int type) {
        switch (type) {
            case TongleAppConst.PAY_TYPE_INT_ALIPAY:  //调起支付宝支付
                dialog.show();
                ServiceManager.AliPayRequest(TongleAppInstance.getInstance().getUserID(), orderNo, aliPaySuccessListener(), errorListener());
                break;
            case TongleAppConst.PAY_TYPE_INT_WECHAT:  //调起微信支付
                dialog.show();
                ServiceManager.WeChatPayRequest(TongleAppInstance.getInstance().getUserID(), orderNo, WeChatsuccessListener(), errorListener());
                break;
            default:
                ToastUtils.toastShort(R.string.select_pay_type_error_msg);
                break;
        }
    }

    /**
     * 功能：支付宝支付参数取得 网络响应成功，返回数据
     */
    private Response.Listener aliPaySuccessListener() {
        return new Response.Listener<AliPayResult>() {
            @Override
            public void onResponse(AliPayResult result) {
                dialog.dismiss();
                if (TongleAppConst.SUCCESS.equals(result.getHead().getReturn_flag())) {

                    final String orderInfo = result.getBody().getAlipay_info().toString(); // 订单信息
                    Runnable payRunnable = new Runnable() {
                        @Override
                        public void run() {
                            PayTask alipay = new PayTask(SelectPayTypeActivity.this);
                            String result = alipay.pay(orderInfo);
                            Message msg = new Message();
                            msg.what = SDK_PAY_FLAG;
                            msg.obj = result;
                            handler.sendMessage(msg);
                        }
                    };
                    // 异步调用
                    Thread payThread = new Thread(payRunnable);
                    payThread.start();
                } else {
                    ToastUtils.toastShort(getString(R.string.common_no_data));
                }
            }

        };
    }

    /**
     * 功能：微信支付参数取得 网络响应成功，返回数据
     */
    private Response.Listener WeChatsuccessListener() {
        return new Response.Listener<WeChatPayResult>() {
            @Override
            public void onResponse(WeChatPayResult userOrdersResult) {
                dialog.dismiss();
                if (TongleAppConst.SUCCESS.equals(userOrdersResult.getHead().getReturn_flag())) {
                    api = WXAPIFactory.createWXAPI(SelectPayTypeActivity.this, userOrdersResult.getBody().getWexin_pay_info().getApp_id());
                    api.registerApp(userOrdersResult.getBody().getWexin_pay_info().getApp_id());
                    //微信请求支付加参数
                    PayReq req = new PayReq();
                    req.appId = userOrdersResult.getBody().getWexin_pay_info().getApp_id();
                    req.partnerId = userOrdersResult.getBody().getWexin_pay_info().getPartner_id();
                    req.prepayId = userOrdersResult.getBody().getWexin_pay_info().getPrepay_id();
                    req.nonceStr = userOrdersResult.getBody().getWexin_pay_info().getNonce_str();
                    req.timeStamp = userOrdersResult.getBody().getWexin_pay_info().getTimestamp();
                    req.packageValue = "Sign=WXPay";//"Sign=" + packageValue;
                    req.sign = userOrdersResult.getBody().getWexin_pay_info().getSign();
                    // 在支付之前，如果应用没有注册到微信，应该先调用IWXMsg.registerApp将应用注册到微信
                    api.sendReq(req);
                } else {
                    ToastUtils.toastShort(getString(R.string.common_no_data));
                }
            }

        };
    }

    /**
     * 功能：接口请求响应失败回调
     */
    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                dialog.dismiss();
                ToastUtils.toastShort(getString(R.string.sys_exception));
            }
        };
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            showCancelPayDialog();//返回时弹出提示框
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(payFinishReceiver);
    }

    public static String payBroadcastAction = "com.ilingtong.app.tongle.activity.selectpayactivity";
    private BroadcastReceiver payFinishReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            int type = intent.getIntExtra("errCode", -1);
            switch (type) {
                case 0:
                    paySuccess(intoType);
                    break;
                case -1:
                    ToastUtils.toastShort(getString(R.string.common_pay_error));
                    break;
                case -2:
                    ToastUtils.toastShort(getString(R.string.common_pay_cancel));
                    break;
            }

        }
    };

    /**
     * 支付成功后逻辑处理
     *
     * @param intoType 为TongleAppConst.PAY_MODE 时表示是传统商品支付。
     *                 为TongleAppConst.GROUP_PAY_MODE 时表示是团购商品支付
     */
    private void paySuccess(String intoType) {
        if (TongleAppConst.PAY_MODE.equals(intoType)) {
            if (OrderDetailActivity.instance == null) {
                Intent intent = new Intent(SelectPayTypeActivity.this, OrderDetailActivity.class);
                intent.putExtra("order_no", orderNo);
                startActivity(intent);
                SelectPayTypeActivity.this.finish();
            } else {
                setResult(RESULT_OK);
                SelectPayTypeActivity.this.finish();
            }
        } else if (TongleAppConst.GROUP_PAY_MODE.equals(intoType)) {
            TicketOrderDetailActivity.launcher(SelectPayTypeActivity.this, orderNo);
            TicketOrderSubmitActivity.instance.finish();
            SelectPayTypeActivity.this.finish();
        }
    }

    /**
     * 取消支付后逻辑处理
     *
     * @param intoType 为TongleAppConst.PAY_MODE 时表示是传统商品支付。
     *                 为TongleAppConst.GROUP_PAY_MODE 时表示是团购商品支付
     */
    private void payCancle(String intoType) {
        if (TongleAppConst.PAY_MODE.equals(intoType)) {
            Intent intent = new Intent(SelectPayTypeActivity.this, OrderDetailActivity.class);
            intent.putExtra("order_no", orderNo);
            startActivity(intent);
            SelectPayTypeActivity.this.finish();
        } else if (TongleAppConst.GROUP_PAY_MODE.equals(intoType)) {
            SelectPayTypeActivity.this.finish();
        }
    }
}
