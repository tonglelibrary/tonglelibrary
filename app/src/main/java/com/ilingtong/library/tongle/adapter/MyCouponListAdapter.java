package com.ilingtong.library.tongle.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.activity.CollectExpertDetailActivity;
import com.ilingtong.library.tongle.activity.CollectForumDetailActivity;
import com.ilingtong.library.tongle.activity.CollectMStoreDetailActivity;
import com.ilingtong.library.tongle.activity.CollectProductDetailActivity;
import com.ilingtong.library.tongle.activity.MainActivity;
import com.ilingtong.library.tongle.activity.ProdIntegralActivity;
import com.ilingtong.library.tongle.protocol.MyCouponInfo;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

/**
 * author: liuting
 * Date: 2016/3/23
 * Time: 10:22
 * Email: liuting@ilingtong.com
 * Desc:我的优惠券列表Adapter
 */
public class MyCouponListAdapter extends BaseAdapter {
    private LayoutInflater mInflater;
    private List<MyCouponInfo> mLvCoupon;//优惠券列表
//    private MyCouponInfo mCouponItem = new MyCouponInfo();
    private Context mContext;

    public MyCouponListAdapter(Context context, List<MyCouponInfo> couponList) {
        mContext = context;
        this.mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mLvCoupon = couponList;
    }

    @Override
    public int getCount() {
        return mLvCoupon.size();
    }

    @Override
    public Object getItem(int position) {
        return mLvCoupon.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        final ViewHolder holder;
        if (view == null) {
            view = mInflater.inflate(R.layout.my_coupon_list_item, null);
            holder = new ViewHolder();
            holder.mRlyTop = (RelativeLayout) view.findViewById(R.id.my_coupon_list_item_rly_top);
            holder.mCouponProduct = (TextView) view.findViewById(R.id.my_coupon_list_item_tv_product);
            holder.mCouponTime = (TextView) view.findViewById(R.id.my_coupon_list_item_tv_time);
            holder.mCouponMoney = (TextView) view.findViewById(R.id.my_coupon_list_item_tv_money);
            holder.mCouponCondition = (TextView) view.findViewById(R.id.my_coupon_list_item_tv_condition);
            holder.mImgLogo = (ImageView) view.findViewById(R.id.my_coupon_list_item_img_logo);
            holder.mLlyUse = (LinearLayout) view.findViewById(R.id.my_coupon_list_item_lly_right);
            holder.mCouponStatus = (TextView) view.findViewById(R.id.my_coupon_list_item_tv_status);
            holder.mImgStatus = (ImageView) view.findViewById(R.id.my_coupon_list_item_img_status);
            holder.mImgUse = (ImageView) view.findViewById(R.id.my_coupon_list_item_img_use);
            holder.mCouponRMB = (TextView) view.findViewById(R.id.my_coupon_list_item_tv_rmb);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        final MyCouponInfo mCouponItem = (MyCouponInfo) getItem(position);
        if (mCouponItem.getVoucher_base().getIssue_flg().equals(TongleAppConst.FLAG_ISSUE_ALL)) {
            //全平台
            holder.mCouponProduct.setText(mCouponItem.getVoucher_base().getVouchers_name() + "-" + mContext.getString(R.string.adapter_my_coupon_issue_all));
        } else {
            //店铺发行
            holder.mCouponProduct.setText(mCouponItem.getVoucher_base().getVouchers_name() + "-" + mContext.getString(R.string.adapter_my_coupon_issue_store));
        }
        holder.mCouponTime.setText(mCouponItem.getVoucher_base().getExpiration_date_begin() + "-" + mCouponItem.getVoucher_base().getExpiration_date_end());
        holder.mCouponMoney.setText((int) mCouponItem.getVoucher_base().getMoney() + "");
        holder.mCouponCondition.setText(mCouponItem.getVoucher_base().getUse_conditions_memo());
        ImageLoader.getInstance().displayImage(mCouponItem.getVoucher_link_info().getPic(), holder.mImgLogo, TongleAppInstance.options);

        switch (mCouponItem.getVoucher_base().getVouchers_status()) {
            case TongleAppConst.COUPON_STATUS_NO_RECEIVE://未领取，不显示
                break;
            case TongleAppConst.COUPON_STATUS_RECEIVED://已领取
                if (mCouponItem.getVoucher_base().getIssue_flg().equals(TongleAppConst.FLAG_ISSUE_ALL)) {
                    //全平台
                    holder.mRlyTop.setBackgroundResource(R.drawable.coupon_top_tongle);
                    holder.mCouponMoney.setTextColor(mContext.getResources().getColor(R.color.my_coupon_list_item_money_txt_color));
                    holder.mCouponRMB.setTextColor(mContext.getResources().getColor(R.color.my_coupon_list_item_money_txt_color));
                } else {
                    //店铺发行
                    holder.mRlyTop.setBackgroundResource(R.drawable.coupon_top_dianpu);
                    holder.mCouponMoney.setTextColor(mContext.getResources().getColor(R.color.my_coupon_list_item_store_txt_color));
                    holder.mCouponRMB.setTextColor(mContext.getResources().getColor(R.color.my_coupon_list_item_store_txt_color));
                }
                holder.mImgUse.setVisibility(View.VISIBLE);
                holder.mCouponStatus.setText(mCouponItem.getVoucher_link_info().getTitle());
                holder.mLlyUse.setOnClickListener(new View.OnClickListener() {
                    Intent intent;
                    @Override
                    public void onClick(View v) {
                        switch (mCouponItem.getVoucher_link_info().getLink_type()) {
                            case TongleAppConst.COUPON_LINK_TYPE_PRODUCT:
                                //前往商品界面
                                intent = new Intent(mContext, CollectProductDetailActivity.class);
                                Bundle bundle = new Bundle();
                                bundle.putString("product_id", mCouponItem.getVoucher_link_info().getLink_id());
                                bundle.putString("tag", "coupon");
                                bundle.putString("relationId", "");
                                intent.putExtras(bundle);
                                mContext.startActivity(intent);
                                break;
                            case TongleAppConst.COUPON_LINK_TYPE_POST:
                                //前往帖子界面
                                intent = new Intent(mContext, CollectForumDetailActivity.class);
                                intent.putExtra("post_id", mCouponItem.getVoucher_link_info().getLink_id());
                                mContext.startActivity(intent);
                                break;
                            case TongleAppConst.COUPON_LINK_TYPE_STORE:
                                //前往魔店
                                intent = new Intent(mContext, CollectMStoreDetailActivity.class);
                                intent.putExtra("mstore_id", mCouponItem.getVoucher_link_info().getLink_id());
                                mContext.startActivity(intent);
                                break;
                            case TongleAppConst.COUPON_LINK_TYPE_MEMBER:
                                //前往会员
                                intent = new Intent(mContext, CollectExpertDetailActivity.class);
                                intent.putExtra("user_id", mCouponItem.getVoucher_link_info().getLink_id());
                                mContext.startActivity(intent);
                                break;
                            case TongleAppConst.COUPON_LINK_TYPE_HOME:
                                //前往首页
                                intent = new Intent(mContext, MainActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                intent.putExtra(TongleAppConst.MAINACTIVITY_NEWINTENT_FLAG_NAME, TongleAppConst.MAINACTIVITY_NEWINTENT_FLAG_MAIN);
                                mContext.startActivity(intent);
                                break;
                            case TongleAppConst.COUPON_LINK_TYPE_HTML:
                                //前往H5，领取优惠券
                                intent = new Intent(mContext, ProdIntegralActivity.class);
                                intent.putExtra("prod_url", mCouponItem.getVoucher_link_info().getLink_id());
                                intent.putExtra("myTitle", mContext.getString(R.string.adapter_my_coupon_link_title));
                                mContext.startActivity(intent);
                                break;
                        }
                    }
                });
                break;
            case TongleAppConst.COUPON_STATUS_USED://已使用
                holder.mRlyTop.setBackgroundResource(R.drawable.coupon_top_used_expired);
                holder.mCouponMoney.setTextColor(mContext.getResources().getColor(R.color.my_coupon_list_item_used_txt_color));
                holder.mCouponRMB.setTextColor(mContext.getResources().getColor(R.color.my_coupon_list_item_used_txt_color));
                holder.mCouponStatus.setText(mContext.getString(R.string.adapter_my_coupon_used));
                holder.mImgUse.setVisibility(View.GONE);
                break;
            case TongleAppConst.COUPON_STATUS_EXPIRED://已过期
                holder.mRlyTop.setBackgroundResource(R.drawable.coupon_top_used_expired);
                holder.mCouponMoney.setTextColor(mContext.getResources().getColor(R.color.my_coupon_list_item_used_txt_color));
                holder.mCouponRMB.setTextColor(mContext.getResources().getColor(R.color.my_coupon_list_item_used_txt_color));
                holder.mCouponStatus.setText(mContext.getString(R.string.ticket_order_detail_expire));
                holder.mImgUse.setVisibility(View.GONE);
                holder.mImgStatus.setVisibility(View.VISIBLE);//显示已过期图标
                break;
        }

        return view;
    }

    static class ViewHolder {
        RelativeLayout mRlyTop;//优惠券头部一栏
        TextView mCouponProduct;//优惠券名称-发行平台
        TextView mCouponTime;//优惠券有效时间
        TextView mCouponMoney;//优惠券金额
        TextView mCouponCondition;//优惠券限制条件
        ImageView mImgLogo;//优惠券图片
        LinearLayout mLlyUse;//优惠券使用一栏
        ImageView mImgUse;//优惠券使用图标
        TextView mCouponStatus;//优惠券状态显示：“已使用”和“已过期”状态时，直接显示“已使用”或“已过期”，否则显示接口返回的“title”
        ImageView mImgStatus;//“已过期”状态时显示
        TextView mCouponRMB;//RMB符号
    }
}

