package com.ilingtong.library.tongle.model;

import com.ilingtong.library.tongle.protocol.UserPostInfo;

/**
 * User: lengjiqiang
 * Date: 2015/5/20
 * Time: 13:59
 * Email: jqleng@isoftstone.com
 * Desc: 保存用户关注的帖子详情信息类
 */
public class PostDetailModel {
    public UserPostInfo user_post_info;
}
