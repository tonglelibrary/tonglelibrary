package com.ilingtong.library.tongle.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.activity.CollectExpertDetailActivity;
import com.ilingtong.library.tongle.activity.CollectForumDetailActivity;
import com.ilingtong.library.tongle.adapter.ForumListAdapter;
import com.ilingtong.library.tongle.external.maxwin.view.XListView;
import com.ilingtong.library.tongle.protocol.ExpertUpdateRequestParam;
import com.ilingtong.library.tongle.protocol.ExpertUpdateResult;
import com.ilingtong.library.tongle.protocol.UserFollowPostList;
import com.ilingtong.library.tongle.utils.ToastUtils;

import java.util.ArrayList;

/**
 * User: lengjiqiang
 * Date: 2015/5/18
 * Time: 17:07
 * Email: jqleng@isoftstone.com
 * Desc: 达人更新
 */
public class ExpertUpdateFragment extends BaseFragment implements XListView.IXListViewListener {
    private XListView postList;
    private ForumListAdapter postListAdaper;
    private ArrayList<UserFollowPostList> post_list = new ArrayList<>();
    private boolean flag = true;//上拉加载更多标识
    private ProgressDialog pDlg;
    private String userid;
    Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    postListAdaper.notifyDataSetChanged();
                    break;
            }
        }
    };

    public static ExpertUpdateFragment newInstance(String userId) {
        Bundle args = new Bundle();
        args.putString("user_id", userId);
        ExpertUpdateFragment fragment = new ExpertUpdateFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_expert_update_layout, null);
        getData();
        initView(rootView);
        doRequest();
        return rootView;
    }

    public void getData() {
        userid = getArguments().getString("user_id");
    }

    public void initView(View rootView) {
        pDlg = new ProgressDialog(getActivity());
        pDlg.setCancelable(false);
        pDlg.setMessage(getString(R.string.loading));
        pDlg.show();
        postList = (XListView) rootView.findViewById(R.id.expert_update_listview);
        postListAdaper = new ForumListAdapter(getActivity(), post_list);
        postList.setAdapter(postListAdaper);
        postList.setXListViewListener(this, 0);
        postList.setRefreshTime();
        postList.setPullLoadEnable(false);
        //点击列表Item，进入帖子详情
        postList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent detailIntent = new Intent(getActivity(), CollectForumDetailActivity.class);
                detailIntent.putExtra("post_id", post_list.get(position - 1).post_id);
                startActivityForResult(detailIntent, 10001);
            }
        });

    }

    public void doRequest() {
        if (!TextUtils.isEmpty(userid)) {
            ExpertUpdateRequestParam param = new ExpertUpdateRequestParam();
            param.user_id = TongleAppInstance.getInstance().getUserID();
            param.forward = "";
            param.post_id = "";
            param.expert_user_id = userid;
            param.fetch_count = TongleAppConst.FETCH_COUNT;
            param.action = "1";
            ServiceManager.doExpertUpdateRequest(param, successListener(), errorListener());
        }
    }

    /**
     * 功能：请求 收藏 -> 达人 -> 更新 属性页的网络数据，响应成功后，返回数据
     */
    private Response.Listener<ExpertUpdateResult> successListener() {
        return new Response.Listener<ExpertUpdateResult>() {
            @Override
            public void onResponse(ExpertUpdateResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    if (response.getBody().getUser_post_list().size() > 0) {
                        CollectExpertDetailActivity.top_name.setText(response.getBody().getUser_post_list().get(0).user_nick_name);
                    }
                    if (response.getBody().getUser_post_list().size() < Integer.parseInt(TongleAppConst.FETCH_COUNT)) {
                        flag = false;
                        postList.setPullLoadEnable(false);
                    } else {
                        postList.setPullLoadEnable(true);
                    }
                    post_list.addAll(response.getBody().getUser_post_list());
                } else {
                    ToastUtils.toastShort(getString(R.string.para_exception) + response.getHead().getReturn_message());
                }
                //调用handler，发送消息
                mHandler.sendEmptyMessage(0);
                pDlg.dismiss();
            }
        };
    }

    /**
     * 功能：请求 收藏 -> 达人 -> 更新 属性页的网络数据，响应失败
     */
    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ToastUtils.toastShort(getString(R.string.sys_exception));
                pDlg.dismiss();
            }
        };
    }

    @Override
    public void onRefresh(int id) {
        post_list.clear();
        doRequest();
    }

    @Override
    public void onLoadMore(int id) {
        //加载更多
        if (flag) {
            ExpertUpdateRequestParam param = new ExpertUpdateRequestParam();
            param.user_id = TongleAppInstance.getInstance().getUserID();
            param.post_id = post_list.get(post_list.size() - 1).post_id;
            param.expert_user_id = userid;
            param.fetch_count = TongleAppConst.FETCH_COUNT;
            param.forward = "1";
            param.action = "1";

            ServiceManager.doExpertUpdateRequest(param, successListener(), errorListener());
        } else {
            ToastUtils.toastShort(getString(R.string.common_list_end));
            postList.setPullLoadEnable(false);
        }
    }
}
