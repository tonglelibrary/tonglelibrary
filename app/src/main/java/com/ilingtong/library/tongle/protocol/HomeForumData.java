package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * User: lengjiqiang
 * Date: 2015/5/13
 * Time: 16:00
 * Email: jqleng@isoftstone.com
 * Dest:
 */
public class HomeForumData implements Serializable {
    public String data_total_count;
    public ArrayList<UserFollowPostList> user_follow_post_list;
}
