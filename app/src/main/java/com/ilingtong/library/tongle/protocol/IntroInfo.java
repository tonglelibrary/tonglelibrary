package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/6/24
 * Time: 11:08
 * Email: jqleng@isoftstone.com
 * Desc: 功能介绍信息类，该包含通过网络返回的数据
 */
public class IntroInfo implements Serializable {
    private String title_text;
    private String function_introduction_url;

    public String getTitle_text() { return title_text; }
    public String getFunction_introduction_url() {
        return function_introduction_url;
    }

    @Override
    public String toString() {
        return "title_text:" + title_text + "\r\n" +
                "function_introduction_url:" + function_introduction_url;
    }
}
