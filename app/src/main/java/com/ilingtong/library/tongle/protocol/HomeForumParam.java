package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/5/13
 * Time: 10:23
 * Email: jqleng@isoftstone.com
 * Dest:
 */
public class HomeForumParam implements Serializable {
    public String user_id;
    public String post_id;
    public String forward;
    public String fetch_count;
}
