package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * User: shiyuchong
 * Date: 2015/6/10
 * Time: 14:27
 * Email: ycshi@isoftstone.com
 * Desc:[1040]
 */
public class OrderListItemData implements Serializable {
    public String order_no;
    public String amount;
    public String fee_amount;
    public String order_time;
    public String status;
    public String pay_method;
    public String modify_flag;
    public String cancel_flag;
    public String evaluate_flag;
    public String goods_amount;       // add at 2016/04/07  商品总额
    public String tariff;     // add at 2016/04/07 关税
    public String pay_amount;     // add at 2016/04/07 应付金额
    public String vouchers_name;     // add at 2016/04/07 优惠券名称
    public String money;     // add at 2016/04/07 优惠金额
    public String use_conditions;     // add at 2016/04/07 优惠券使用条件
    public ArrayList<ProdDetailListItem> prod_detail;


}
