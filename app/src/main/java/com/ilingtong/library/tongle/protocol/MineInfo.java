package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/6/3
 * Time: 18:03
 * Email: jqleng@isoftstone.com
 * Desc:
 */
public class MineInfo implements Serializable {
    //根据用户ID取得用户的基本信息
    private  String token;
    private String user_id; // 用户ID
    private String user_name; //用户名称
    private String user_nick_name;  // 用户昵称
    private String user_phone;  // 手机号
    private String user_photo_url;// 头像URL
    private String user_sex;// 性别
    private String user_id_no;// 身份证号
    private String user_sales_point;// 用户消费积分余额
    private String user_rebate_point;// 用户返利积分余额
    private String user_account_balance;//用户账户余额
    private String user_coupons;// 用户优惠券
    private String user_fans;// 用户粉丝数
    private String user_follows;// 用户关注数
    private String user_qr_code_url;// 用户二维码URL
    private String user_customs_flag;// 个人积分开关

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_nick_name() {
        return user_nick_name;
    }

    public void setUser_nick_name(String user_nick_name) {
        this.user_nick_name = user_nick_name;
    }

    public String getUser_phone() {
        return user_phone;
    }

    public void setUser_phone(String user_phone) {
        this.user_phone = user_phone;
    }

    public String getUser_photo_url() {
        return user_photo_url;
    }

    public void setUser_photo_url(String user_photo_url) {
        this.user_photo_url = user_photo_url;
    }

    public String getUser_sex() {
        return user_sex;
    }

    public void setUser_sex(String user_sex) {
        this.user_sex = user_sex;
    }

    public String getUser_id_no() {
        return user_id_no;
    }

    public void setUser_id_no(String user_id_no) {
        this.user_id_no = user_id_no;
    }

    public String getUser_sales_point() {
        return user_sales_point;
    }

    public void setUser_sales_point(String user_sales_point) {
        this.user_sales_point = user_sales_point;
    }

    public String getUser_rebate_point() {
        return user_rebate_point;
    }

    public void setUser_rebate_point(String user_rebate_point) {
        this.user_rebate_point = user_rebate_point;
    }

    public String getUser_account_balance() {
        return user_account_balance;
    }

    public void setUser_account_balance(String user_account_balance) {
        this.user_account_balance = user_account_balance;
    }

    public String getUser_coupons() {
        return user_coupons;
    }

    public void setUser_coupons(String user_coupons) {
        this.user_coupons = user_coupons;
    }

    public String getUser_fans() {
        return user_fans;
    }

    public void setUser_fans(String user_fans) {
        this.user_fans = user_fans;
    }

    public String getUser_follows() {
        return user_follows;
    }

    public void setUser_follows(String user_follows) {
        this.user_follows = user_follows;
    }

    public String getUser_qr_code_url() {
        return user_qr_code_url;
    }

    public void setUser_qr_code_url(String user_qr_code_url) {
        this.user_qr_code_url = user_qr_code_url;
    }

    public String getUser_customs_flag() {
        return user_customs_flag;
    }

    public void setUser_customs_flag(String user_customs_flag) {
        this.user_customs_flag = user_customs_flag;
    }

    @Override
    public String toString() {
        return "user_id:" + user_id + "\r\n" +
                "user_name:" + user_name + "\r\n" +
                "user_nick_name:" + user_nick_name + "\r\n" +
                "phone:" + user_phone + "\r\n" +
                "user_photo_url:" + user_photo_url + "\r\n" +
                "user_sex:" + user_sex + "\r\n" +
                "user_id_no:" + user_id_no + "\r\n" +
                "user_sales_point:" + user_sales_point + "\r\n" +
                "user_rebate_point:" + user_rebate_point + "\r\n" +
                "user_account_balance:" + user_account_balance + "\r\n" +
                "user_coupons:" + user_coupons + "\r\n" +
                "user_fans:" + user_fans + "\r\n" +
                "user_qr_code_url:" + user_qr_code_url + "\r\n" +
                "user_customs_flag:" + user_customs_flag + "\r\n" +
                "user_follows:" + user_follows;
    }
}