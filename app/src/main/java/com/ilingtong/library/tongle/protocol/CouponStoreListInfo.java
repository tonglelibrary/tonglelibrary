package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;
import java.util.List;

/**
 * Created by wuqian on 2016/3/11.
 * mail: wuqian@ilingtong.com
 * Description:团购商户列表信息
 * use by 6008 6011 6007
 */
public class CouponStoreListInfo implements Serializable {
    private int store_total_count;     //商户总数量
    private List<CouponStoreInfo> store_list;   //适用商户列表

    public List<CouponStoreInfo> getStore_list() {
        return store_list;
    }

    public void setStore_list(List<CouponStoreInfo> store_list) {
        this.store_list = store_list;
    }

    public int getStore_total_count() {
        return store_total_count;
    }

    public void setStore_total_count(int store_total_count) {
        this.store_total_count = store_total_count;
    }


}
