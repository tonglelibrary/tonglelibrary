package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * User: lengjiqiang
 * Date: 2015/6/4
 * Time: 14:19
 * Email: jqleng@isoftstone.com
 * Desc:
 */
public class CollectProductInfo implements Serializable {

    private String data_total_count;
    private ArrayList<ProductListItemData> prod_list;//我收藏的商品列表
    private ArrayList<ProductListItemData> my_prod_list;//我的宝贝列表


    public String getData_total_count() {
        return data_total_count;
    }

    public ArrayList<ProductListItemData> getProd_list() {
        return prod_list;
    }
    public ArrayList<ProductListItemData> getMy_prod_list() {
        return my_prod_list;
    }

    @Override
    public String toString() {
        return "data_total_count:" + data_total_count + "\r\n" +
                "prod_list:" + prod_list+ "\r\n" +
                "my_prod_list:" + my_prod_list;
    }
}
