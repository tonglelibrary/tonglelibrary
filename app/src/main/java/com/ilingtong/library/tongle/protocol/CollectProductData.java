package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * User: lengjiqiang
 * Date: 2015/5/14
 * Time: 10:20
 * Email: jqleng@isoftstone.com
 * Dest:
 */
public class CollectProductData implements Serializable {
    public String data_total_count;
    public String mstore_name;
    public String function_id;
    public ArrayList<ProductListItemData> prod_list;
}
