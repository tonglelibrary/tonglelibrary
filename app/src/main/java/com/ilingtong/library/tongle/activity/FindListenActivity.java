package com.ilingtong.library.tongle.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.ilingtong.library.tongle.R;
import com.libra.sinvoice.SinVoiceRecognitionTwo;

import java.util.ArrayList;
import java.util.List;


/**
 * User:lengjiqiang
 * Date:2015/4/22
 * Time:AM 11:16
 * Email:jqleng@isoftstone.com
 * Desc:Tongle app MainActivity,this activity is the first activity that app start,it will call other activity it is working.
 * 听一听
 */
public class FindListenActivity extends BaseActivity implements View.OnClickListener, SinVoiceRecognitionTwo.Listener {
    private static final int ANIMATIONEACHOFFSET = 600; // 每个动画的播放时间间隔
    private AnimationSet aniSet, aniSet2, aniSet3;
    private ImageView wave1, wave2, wave3;
    private Button btn, animation_cancle;
    private ImageView arrow_left;


    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == 0x222) {
                wave2.startAnimation(aniSet2);
            } else if (msg.what == 0x333) {
                wave3.startAnimation(aniSet3);
            }
            super.handleMessage(msg);
        }
    };


    // 音频识别相关-----开始
    private final static String TAG = "AnimationActivity";
    private List<String> mVoiceCodeList;

    private final static int RECOGNITION_TYPE_1 = 1;
    private final static int RECOGNITION_TYPE_2 = 2;
    private final static int[] TOKENS = {32, 32, 32, 32, 32, 32};
    private final static int TOKEN_LEN = TOKENS.length;
    private SinVoiceRecognitionTwo mRecognition;
    private boolean mIsReadFromFile;
    private PowerManager.WakeLock mWakeLock;

    static {
        System.loadLibrary("sinvoice");
        Log.i(TAG, "sinvoice jnicall loadlibrary");
    }
    // 音频识别相关-----结束

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        aniSet = getNewAnimationSet();
        aniSet2 = getNewAnimationSet();
        aniSet3 = getNewAnimationSet();
        setContentView(R.layout.animation);
        //初始化
        arrow_left = (ImageView) findViewById(R.id.left_arrow_btn);//返回按钮
        arrow_left.setVisibility(View.VISIBLE);
        arrow_left.setOnClickListener(this);
        TextView top_name = (TextView) findViewById(R.id.top_name);//顶部标题
        top_name.setVisibility(View.VISIBLE);
        top_name.setText(getString(R.string.find));

        btn = (Button) findViewById(R.id.btn);
        animation_cancle = (Button) findViewById(R.id.animation_cancle);
        animation_cancle.setOnClickListener(this);
        wave1 = (ImageView) findViewById(R.id.wave1);
        wave2 = (ImageView) findViewById(R.id.wave2);
        wave3 = (ImageView) findViewById(R.id.wave3);

        // 动画播放
        showWaveAnimation();

        // 音频处理初始化
        initSinVoice();

        // 音频开始监听
        mRecognition.start(TOKEN_LEN, mIsReadFromFile);
        mVoiceCodeList = new ArrayList<String>();

        //中心按钮监听
        /*
        btn.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    //按下
                    case MotionEvent.ACTION_DOWN:
                        showWaveAnimation();

                        break;
                    //抬起
                    case MotionEvent.ACTION_UP:
                        cancalWaveAnimation();
                        break;
                    //取消
                    case MotionEvent.ACTION_CANCEL:
                        cancalWaveAnimation();
                        break;
                }
                return true;
            }
        });
        */

    }

    private AnimationSet getNewAnimationSet() {
        AnimationSet as = new AnimationSet(true);
        ScaleAnimation sa = new ScaleAnimation(1f, 2.3f, 1f, 2.3f,
                ScaleAnimation.RELATIVE_TO_SELF, 0.5f,
                ScaleAnimation.RELATIVE_TO_SELF, 0.5f);
//        ScaleAnimation sa = new ScaleAnimation(1.0f, 14.0f,1.0f,14.0f,1,0.5f,1,0.5f);
        sa.setDuration(ANIMATIONEACHOFFSET * 3);
        sa.setRepeatCount(-1);// 设置循环
        AlphaAnimation aniAlp = new AlphaAnimation(1, 0.1f);
        aniAlp.setRepeatCount(-1);// 设置循环
        as.setDuration(ANIMATIONEACHOFFSET * 3);
        as.addAnimation(sa);
        as.addAnimation(aniAlp);
        return as;
    }

    private void showWaveAnimation() {
        wave1.startAnimation(aniSet);
        handler.sendEmptyMessageDelayed(0x222, ANIMATIONEACHOFFSET);
        handler.sendEmptyMessageDelayed(0x333, ANIMATIONEACHOFFSET * 2);
    }

    private void cancalWaveAnimation() {
        wave1.clearAnimation();
        wave2.clearAnimation();
        wave3.clearAnimation();
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.left_arrow_btn) {
            cancalWaveAnimation();
            mRecognition.stop();
            mRecognition.uninit();
            finish();

        } else if (i == R.id.animation_cancle) {
            cancalWaveAnimation();
            mRecognition.stop();
            mRecognition.uninit();
            finish();

        }
    }


    private void initSinVoice() {
        mIsReadFromFile = false;
        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        mWakeLock = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, TAG);
        mRecognition = new SinVoiceRecognitionTwo(this, this);
    }

    @Override
    public void onResume() {
        super.onResume();

        mWakeLock.acquire();
    }

    @Override
    public void onPause() {
        super.onPause();

        mWakeLock.release();
        mRecognition.stop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        mRecognition.uninit();
    }

    @Override
    public void onSinVoiceRecognitionTwoStart() {
        Log.i(TAG, "onSinVoiceRecognitionTwoStart");
    }

    @Override
    public void onSinVoiceRecognitionTwoText(String text, int type) {
        Log.i(TAG, "onSinVoiceRecognitionTwoText text:" + text);
        if (RECOGNITION_TYPE_1 == type) {
            // 识别到音频码
            Log.i(TAG, "onSinVoiceRecognitionTwoText type 1:" + text);
            if (!mVoiceCodeList.contains(text)) {
                mVoiceCodeList.add(text);

                // 接收到的音频到跳转到Find Activity处理
                Intent intent = new Intent();
                intent.putExtra("LISTEN_RESULT", text);
                this.setResult(RESULT_OK, intent);
                this.finish();
            }
        } else if (RECOGNITION_TYPE_2 == type) {
            Log.i(TAG, "onSinVoiceRecognitionTwoText type 2:" + text);
            //mRecognisedTextView_2.setText(text);
        } else {
            Log.i(TAG, "onSinVoiceRecognitionTwoText type error");
        }

    }

    @Override
    public void onSinVoiceRecognitionTwoStop() {
        Log.i(TAG, "onSinVoiceRecognitionTwoStop");
    }

}