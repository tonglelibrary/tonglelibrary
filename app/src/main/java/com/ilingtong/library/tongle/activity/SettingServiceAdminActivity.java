package com.ilingtong.library.tongle.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.protocol.AppServiceResult;
import com.ilingtong.library.tongle.utils.ToastUtils;

/**
 * User: lengjiqiang
 * Date: 2015/6/23
 * Time: 11:17
 * Email: jqleng@isoftstone.com
 * Desc:  我的--》服务管家
 */
public class SettingServiceAdminActivity extends BaseActivity implements View.OnClickListener {
    private TextView top_name;
    private ImageView left_arrow_btn;
    private WebView webView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.webview_comm);
        initView();
        doRequest();
    }
    public void initView() {
        top_name = (TextView) findViewById(R.id.top_name);
        left_arrow_btn = (ImageView) findViewById(R.id.left_arrow_btn);
        webView = (WebView) findViewById(R.id.webview);
        left_arrow_btn.setOnClickListener(this);
        left_arrow_btn.setVisibility(View.VISIBLE);
        top_name.setVisibility(View.VISIBLE);
        top_name.setText(getString(R.string.setting_service_admin_top_name));
    }

    public void doRequest() {
        ServiceManager.doAppServiceRequest(successListener(), errorListener());
    }

    /**
     * 该页中的所有点击事件处理方法
     */
    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.left_arrow_btn) {
            finish();

        }
    }

    /**
     * 功能：网络响应成功，返回数据
     */
    private Response.Listener<AppServiceResult> successListener() {
        return new Response.Listener<AppServiceResult>() {
            @Override
            public void onResponse(AppServiceResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    String url = response.getBody().getQa_service_url();
                    if (!TextUtils.isEmpty(url)) {
                        webView.getSettings().setJavaScriptEnabled(true);
                        webView.loadUrl(url);
                    }
                } else {
                    ToastUtils.toastShort(getResources().getString(R.string.para_exception) + response.getHead().getReturn_message());
                }
            }
        };
    }

    /**
     * 功能：网络响应失败
     */
    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ToastUtils.toastShort(getResources().getString(R.string.sys_exception));
            }
        };
    }
}


