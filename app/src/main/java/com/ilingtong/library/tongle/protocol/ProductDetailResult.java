package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/6/5
 * Time: 11:25
 * Email: jqleng@isoftstone.com
 * Desc:
 */
public class ProductDetailResult implements Serializable {
    private BaseInfo head;
    private ProductDetailInfo body;

    public BaseInfo getHead() {
        return head;
    }

    public void setHead(BaseInfo head) {
        this.head = head;
    }

    public ProductDetailInfo getBody() {
        return body;
    }

    public void setBody(ProductDetailInfo body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return body.toString();
    }
}
