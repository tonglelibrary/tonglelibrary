package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: 率磊
 * Date: 2015/10/13
 * Time: 9:36
 * Email: leishuai@isoftstone.com
 * Desc:  微信result
 */
public class ModifyMessageResult implements Serializable {

    private BaseInfo head;
    private ModifyMessageInfo body;

    public BaseInfo getHead() {
        return head;
    }

    public void setHead(BaseInfo head) {
        this.head = head;
    }

    public ModifyMessageInfo getBody() {
        return body;
    }

    public void setBody(ModifyMessageInfo body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return body.toString();
    }

}
