package com.ilingtong.library.tongle.utils;

import android.content.Context;
import android.view.WindowManager;
import com.ilingtong.library.tongle.TongleAppInstance;

/**
 * 单位工具计算
 * 
 * @author GaiQS
 * @Date: 2015年1月13日
 * @Time: 上午11:13:56
 */
public class DipUtils {

	/**
	 * 根据手机的分辨率从 dip 的单位 转成为 px(像素)
	 */
	public static int dip2px(Context paramContext, float dipValue) {
		return (int) (0.5F + dipValue * paramContext.getResources().getDisplayMetrics().density);
	}

	public static int dip2px(float dipValue) {
		return (int) (0.5F + dipValue * TongleAppInstance.getAppContext().getResources().getDisplayMetrics().density);
	}

	/**
	 * 根据手机的分辨率从 px(像素) 的单位 转成为 dp
	 */
	public static int px2dip(Context paramContext, float pxValue) {
		return (int) (0.5F + pxValue / paramContext.getResources().getDisplayMetrics().density);
	}

	public static int px2dip(float pxValue) {
		return (int) (0.5F + pxValue * TongleAppInstance.getAppContext().getResources().getDisplayMetrics().density);
	}

	/**
	 * 将px值转换为sp值，保证文字大小不变
	 * 
	 * @param pxValue
	 *            （DisplayMetrics类中属性scaledDensity）
	 * @return
	 */
	public static int px2sp(Context context, float pxValue) {
		final float fontScale = context.getResources().getDisplayMetrics().scaledDensity;
		return (int) (pxValue / fontScale + 0.5f);
	}

	public static int px2sp(float pxValue) {
		final float fontScale = TongleAppInstance.getAppContext().getResources().getDisplayMetrics().scaledDensity;
		return (int) (pxValue / fontScale + 0.5f);
	}

	/**
	 * 将sp值转换为px值，保证文字大小不变
	 * 
	 * @param spValue
	 *            （DisplayMetrics类中属性scaledDensity）
	 * @return
	 */
	public static int sp2px(Context context, float spValue) {
		final float fontScale = context.getResources().getDisplayMetrics().scaledDensity;
		return (int) (spValue * fontScale + 0.5f);
	}

	public static int sp2px(float spValue) {
		final float fontScale = TongleAppInstance.getAppContext().getResources().getDisplayMetrics().scaledDensity;
		return (int) (spValue * fontScale + 0.5f);
	}

	/**
	 * 获取屏幕高度
	 * 
	 * @param paramContext
	 * @return
	 */
	public static int getScreenHeight(Context paramContext) {
		return ((WindowManager) paramContext.getSystemService("window")).getDefaultDisplay().getHeight();
	}

	/**
	 * 获取屏幕宽度
	 * 
	 * @param paramContext
	 * @return
	 */
	public static int getScreenWidth(Context paramContext) {
		return ((WindowManager) paramContext.getSystemService("window")).getDefaultDisplay().getWidth();
	}
}
