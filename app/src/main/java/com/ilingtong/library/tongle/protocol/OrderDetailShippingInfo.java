package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: shuailei
 * Date: 2015/6/10
 * Time: 13:01
 * Email: leishuai@isoftstone.com
 * Desc:
 */
public class OrderDetailShippingInfo implements Serializable {
    public String shipping_method_no;
    public String shipping_method_name;
    public String shipping_time_memo;

    @Override
    public String toString() {
        return "shipping_method_no:" + shipping_method_no + "\r\n" +
                "shipping_method_name:" + shipping_method_name + "\r\n" +
                "shipping_time_memo:" + shipping_time_memo;
    }
}
