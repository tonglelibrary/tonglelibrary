package com.ilingtong.library.tongle.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.protocol.CouponInfo;
import com.ilingtong.library.tongle.utils.Utils;

import java.util.List;

/**
 * author: liuting
 * Date: 2016/3/22
 * Time: 14:20
 * Email: liuting@ilingtong.com
 * Desc:订单详情中团购券券号列表Adapter
 */
public class GroupTicketListAdapter extends BaseAdapter {
    private LayoutInflater mInflater;
    private List<CouponInfo> mLvTicket;//团购券列表
//    private CouponInfo mTicketItem = new CouponInfo();
    private Context mContext;
    private String mType;

    public GroupTicketListAdapter(Context context, List<CouponInfo> ticketList,String type) {
        mContext = context;
        this.mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mLvTicket = ticketList;
        this.mType=type;
    }

    @Override
    public int getCount() {
        return mLvTicket.size();
    }

    @Override
    public Object getItem(int position) {
        return mLvTicket.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.my_ticket_list_item, null);
            viewHolder = new ViewHolder();
            viewHolder.ticketNumber = (TextView) convertView.findViewById(R.id.my_ticket_list_item_tv_number);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        final CouponInfo mTicketItem = (CouponInfo) getItem(position);
        viewHolder.ticketNumber.setText(Utils.formatStringSequence(mTicketItem.getCoupon_id(),TongleAppConst.COUPON_SEQUENCE_LENGHT));
        switch (mType){
            case TongleAppConst.GROUP_TICKET_NOT_USED://未使用
                viewHolder.ticketNumber.setTextColor(mContext.getResources().getColor(R.color.my_ticket_list_item_num_txt_color));
                break;
            case TongleAppConst.GROUP_TICKET_OUT_OF://过期
                viewHolder.ticketNumber.setTextColor(mContext.getResources().getColor(R.color.cart_list_second_line_color));
                break;
            case TongleAppConst.TICKET_STATUS_USED://已使用
                viewHolder.ticketNumber.setTextColor(mContext.getResources().getColor(R.color.my_ticket_list_item_num_txt_color));
                break;
            case TongleAppConst.TICKET_STATUS_REFUND://已退款
                viewHolder.ticketNumber.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);//中间加横线
                viewHolder.ticketNumber.setTextColor(mContext.getResources().getColor(R.color.cart_list_second_line_color));
                break;
            case TongleAppConst.TICKET_STATUS_REFUNDING://退款中
                viewHolder.ticketNumber.setTextColor(mContext.getResources().getColor(R.color.cart_list_second_line_color));
                break;
        }

        return convertView;
    }

    static class ViewHolder {
        TextView ticketNumber;//券号
    }

}

