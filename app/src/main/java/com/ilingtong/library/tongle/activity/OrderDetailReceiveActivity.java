//package com.ilingtong.app.tongle.activity;
//
//import android.app.Activity;
//import android.content.Intent;
//import android.os.Bundle;
//import android.view.KeyEvent;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.widget.Button;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.TextView;
//import com.android.volley.Response;
//import com.android.volley.VolleyError;
//import com.ilingtong.app.tongle.R;
//import com.ilingtong.app.tongle.ServiceManager;
//import com.ilingtong.app.tongle.TongleAppConst;
//import com.ilingtong.app.tongle.TongleAppInstance;
//import com.ilingtong.app.tongle.adapter.OrderDetailListAdaper;
//import com.ilingtong.app.tongle.external.maxwin.view.XListView;
//import com.ilingtong.app.tongle.model.OrderDetailModel;
//import com.ilingtong.app.tongle.protocol.CodeResult;
//import com.ilingtong.app.tongle.protocol.OrderDetailResult;
//import com.ilingtong.app.tongle.utils.ToastUtils;
//
///**
// * User: shuailei
// * Date: 2015/6/10
// * Time: 13:01
// * Email: leishuai@isoftstone.com
// * Desc:
// */
//public class OrderDetailReceiveActivity extends BaseActivity implements XListView.IXListViewListener, View.OnClickListener {
//    private ImageView left_arrow_btn;
//    private TextView top_name;
//    private XListView listview;
//    private View headView, footView;
//    private OrderDetailModel OrderDetailModel;
//    private TextView amount, fee_amount, consignee, address, order_no, order_time, orderdetail_statu, shipping_message, shipping_time;
//    private Button receive_order;
//    private String order;
//    private Button tariff_subBtn;
//    private TextView pass_msg;
//    private LinearLayout tariff_message_linear;
//    private TextView orderdetail_tariff;
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.xlistview_comm_layout);
//        getData();
//        initView();
//        doRequest();
//    }
//    public void getData(){
//        order = getIntent().getExtras().getString("order_no");
//    }
//    public void initView(){
//        top_name = (TextView) findViewById(R.id.top_name);
//        left_arrow_btn = (ImageView) findViewById(R.id.left_arrow_btn);
//        listview = (XListView) findViewById(R.id.xlistview);
//        left_arrow_btn.setOnClickListener(this);
//
//        left_arrow_btn.setVisibility(View.VISIBLE);
//        top_name.setVisibility(View.VISIBLE);
//        top_name.setText(getString(R.string.order_detail_delete_top_name));
//
//        headView = LayoutInflater.from(this).inflate(R.layout.orderdetail_headlayout, null);
//        footView = LayoutInflater.from(this).inflate(R.layout.orderdetail_footlayout, null);
//
//        //订单金额
//        amount = (TextView) headView.findViewById(R.id.orderdetail_amount);
//        //运费
//        fee_amount = (TextView) headView.findViewById(R.id.orderdetail_fee_amount);
//        //收件人
//        consignee = (TextView) headView.findViewById(R.id.orderdetail_consignee);
//        //地址
//        address = (TextView) headView.findViewById(R.id.orderdetail_address);
//        //通关信息
//        pass_msg = (TextView) footView.findViewById(R.id.pass_msg);
//        //关税信息linear
//        tariff_message_linear = (LinearLayout) footView.findViewById(R.id.tariff_message_linear);
//        //关税
//        orderdetail_tariff = (TextView) headView.findViewById(R.id.orderdetail_tariff);
//        //再次提交按钮
//        tariff_subBtn = (Button) footView.findViewById(R.id.tariff_subBtn);
//        tariff_subBtn.setOnClickListener(this);
//        //订单号
//        order_no = (TextView) footView.findViewById(R.id.orderdetail_order_no);
//        //下单时间
//        order_time = (TextView) footView.findViewById(R.id.orderdetail_order_time);
//        //确认收货
//        orderdetail_statu = (TextView) headView.findViewById(R.id.orderdetail_statu);
//        orderdetail_statu.setText(getString(R.string.common_order_confirm_receive));
//        //订单详情——确认收货按钮
//        receive_order = (Button) headView.findViewById(R.id.orderdetail_deleteorder);
//        receive_order.setText(getString(R.string.common_order_confirm_receive));
//        receive_order.setOnClickListener(this);
//
//        //快递信息
//        shipping_message = (TextView) headView.findViewById(R.id.shipping_message);
//        //快递时间
//        shipping_time = (TextView) headView.findViewById(R.id.shipping_time);
//
//        listview.addHeaderView(headView);
//        listview.addFooterView(footView);
//
//        //订单基本信息
//        if (OrderDetailModel == null)
//            OrderDetailModel = new OrderDetailModel();
//    }
//    public void doRequest(){
//        ServiceManager.OrderDetailRequest(TongleAppInstance.getInstance().getUserID(), order, successListener(), errorListener());
//    }
//    @Override
//    public void onClick(View v) {
//        if (v.getId() == R.id.orderdetail_deleteorder){
//            ServiceManager.OrderConfirmRequest(TongleAppInstance.getInstance().getUserID(), order, confirmsuccessListener(), errorListener());
//        }else if (v.getId() == R.id.left_arrow_btn){
////            Intent intent = new Intent( OrderDetailReceiveActivity.this,OrderMineActivity.class);
////            startActivity(intent);
//            finish();
//        }else if(v.getId()==R.id.tariff_subBtn){
//            ServiceManager.doReCustomers(TongleAppInstance.getInstance().getUserID(), order, comminttsuccessListener(), errorListener());
//        }
//    }
//
//    @Override
//    public void onRefresh(int id) {
//        doRequest();
//    }
//
//    @Override
//    public void onLoadMore(int id) {
//
//    }
//
//    /**
//     * 功能：订单详情网络响应成功，返回数据
//     */
//    private Response.Listener successListener() {
//        return new Response.Listener<OrderDetailResult>() {
//            @Override
//            public void onResponse(OrderDetailResult response) {
//                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
//                    OrderDetailModel.order_detail = response.getBody().order_info.order_detail;
//                    amount.setText(getString(R.string.common_fee_sum) + response.getBody().order_info.head_info.amount);
//                    order_no.setText(getString(R.string.common_order_number) + response.getBody().order_info.head_info.order_no);
//                    order_time.setText(getString(R.string.common_order_time)+ response.getBody().order_info.head_info.order_time);
//                    fee_amount.setText(getString(R.string.common_fee_transportation) + response.getBody().order_info.head_info.fee_amount);
//                    pass_msg.setText(response.getBody().order_info.head_info.customs_fail_reason);
//                    orderdetail_tariff.setText(getString(R.string.common_fee_tariff)+response.getBody().order_info.head_info.tariff);
//                    consignee.setText(getString(R.string.common_order_consignee)+ response.getBody().order_info.add_info.consignee);
//                    String a = response.getBody().order_info.add_info.province + response.getBody().order_info.add_info.area + response.getBody().order_info.add_info.address;
//                    address.setText(getString(R.string.common_order_address)+ a);
//
//                    //是否通关（0是，1否）
//                    String tag_flag=response.getBody().order_info.head_info.customs_flag;
//                    if(tag_flag.equals("0")){
//                        tariff_subBtn.setVisibility(View.GONE);
//                    }else if(tag_flag.equals("2")){
//                        tariff_message_linear.setVisibility(View.GONE);
//                    }else{
//                        tariff_subBtn.setVisibility(View.VISIBLE);
//                        orderdetail_statu.setText(getString(R.string.common_tariff_error));
//                        pass_msg.setTextColor(getResources().getColor(R.color.order_payway_press));
//                    }
//
//                    shipping_message.setText(response.getBody().order_info.logistics_info.shipping_company + "," + response.getBody().order_info.logistics_info.shipping_no);
//                    shipping_time.setText(response.getBody().order_info.logistics_info.shipping_time);
//                    updateListView();
//                } else {
//                    ToastUtils.toastShort(getString(R.string.para_exception)+response.getHead().getReturn_message());
//                }
//            }
//        };
//    }
//    /**
//     * 功能：再次提交网络响应成功，返回数据
//     */
//    private Response.Listener comminttsuccessListener() {
//        return new Response.Listener<CodeResult>() {
//            @Override
//            public void onResponse(CodeResult response) {
//                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
//                    tariff_subBtn.setVisibility(View.GONE);
//                    ToastUtils.toastShort(getString(R.string.common_tariff_success));
//                } else {
//                    ToastUtils.toastShort(getString(R.string.para_exception)+ response.getHead().getFunction_id() + response.getHead().getReturn_message());
//                }
//            }
//        };
//    }
//    public void updateListView() {
//        OrderDetailListAdaper adapter = new OrderDetailListAdaper(this, OrderDetailModel.order_detail);
//        listview.setPullLoadEnable(false);
//        listview.setPullRefreshEnable(true);
//        listview.setXListViewListener(this, 0);
//        listview.setRefreshTime();
//        listview.setAdapter(adapter);
//    }
//
//    /**
//     * 功能：订单详情请求网络响应失败
//     */
//    private Response.ErrorListener errorListener() {
//        return new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError volleyError) {
//                ToastUtils.toastShort(getString(R.string.sys_exception));
//            }
//        };
//    }
//    /**
//     * 功能：取消订单网络响应成功，返回数据
//     */
//    private Response.Listener confirmsuccessListener() {
//        return new Response.Listener<OrderDetailResult>() {
//            @Override
//            public void onResponse(OrderDetailResult response) {
//                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
//                    ToastUtils.toastShort(getString(R.string.common_order_confirm_receive_success));
//                } else {
//                    ToastUtils.toastShort(getString(R.string.para_exception)+response.getHead().getReturn_message());
//                }
//            }
//        };
//    }
//
//
//  //  @Override
////    public boolean onKeyDown(int keyCode, KeyEvent event) {
////        if (keyCode == KeyEvent.KEYCODE_BACK) {
////            Intent intent = new Intent( OrderDetailReceiveActivity.this,OrderMineActivity.class);
////            startActivity(intent);
////            finish();
////        }
////        return super.onKeyDown(keyCode, event);
////    }
//}