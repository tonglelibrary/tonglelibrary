package com.ilingtong.library.tongle.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.protocol.OrderDetailOrderDetailInfo;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

/**
 * User: shuailei
 * Date: 2015/6/10
 * Time: 10:45
 * Email: leishuai@isoftstone.com
 * Dest:订单详情列表adapter
 */
public class OrderDetailListAdaper extends BaseAdapter {
    private LayoutInflater inflater;
    private List<OrderDetailOrderDetailInfo> list;
    OrderDetailOrderDetailInfo dataItem = new OrderDetailOrderDetailInfo();
    Context mContext;
    public static String order_detail_no, prod_id;

    public OrderDetailListAdaper(Context context, List list) {
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mContext = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder viewholder;
        if (view == null) {
            view = inflater.inflate(R.layout.orderdetail_item, null);
            viewholder = new ViewHolder();

            viewholder.listItemImageView = (ImageView) view.findViewById(R.id.order_list_item_image);
            viewholder.desc = (TextView) view.findViewById(R.id.order_list_item_desc);
            viewholder.price = (TextView) view.findViewById(R.id.order_list_item_price);
            viewholder.num = (TextView) view.findViewById(R.id.order_list_item_num);
            viewholder.color = (TextView) view.findViewById(R.id.order_list_item_color);
            view.setTag(viewholder);
        } else {
            viewholder = (ViewHolder) view.getTag();
        }
        dataItem = list.get(position);
        ImageLoader.getInstance().displayImage(dataItem.prod_pic_url, viewholder.listItemImageView, TongleAppInstance.options);
        viewholder.desc.setText(dataItem.prod_name);
        viewholder.price.setText(mContext.getString(R.string.RMB) + dataItem.price);
        viewholder.num.setText(mContext.getString(R.string.common_string_number) + dataItem.quantity);
        order_detail_no = dataItem.order_detail_no;
        prod_id = dataItem.prod_id;
        if (dataItem.prod_spec_list.size() > 0) {
            for (int jk=0;jk<dataItem.prod_spec_list.size();jk++){
                viewholder.color.setText(dataItem.prod_spec_list.get(jk).prod_spec_name + ":" + dataItem.prod_spec_list.get(jk).spec_detail_name);
            }
        }
        return view;
    }

    static class ViewHolder {
        TextView price, num, desc, color;
        ImageView listItemImageView;
    }
}
