package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * User: lengjiqiang
 * Date: 2015/5/10
 * Time: 14:04
 * Email: jqleng@isoftstone.com
 * Desc: 购物车列表中的元素类型
 */
public class ShopCartListItem implements Serializable {
    public String seq_no;
    public String post_id;
    public String prod_pic_url;
    public String prod_id;
    public String prod_name;
    public String mstore_id;
    public String mstore_name;
    public String price;
    public String order_qty;
    public String amount;
    public String fee_amount;
    public String relation_id;
    public ArrayList<ProdSpecListItem> prod_spec_list;
    public String country_pic_url;
    public String import_info_desc;
    public String transfer_fee_desc;
    public String tariff_desc;
    public String tariff;
    public String import_goods_flag;
}
