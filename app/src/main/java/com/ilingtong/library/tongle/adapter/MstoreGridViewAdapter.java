package com.ilingtong.library.tongle.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.model.HomeModel;
import com.ilingtong.library.tongle.protocol.MStoreListItem;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

/**
 * User: shiyuchong
 * Date: 2015/7/17
 * Time: 17:05
 * Email: ycshi@isoftstone.com
 * Dest:  首页的魔店图片GridView的adapter
 */

public class MstoreGridViewAdapter extends BaseAdapter {
    private LayoutInflater inflater;
    private ArrayList<MStoreListItem> list;
    private Context mContext;

    public MstoreGridViewAdapter(ArrayList list, Context context) {
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mContext = context;
        this.list = list;
    }


    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.adapter_mstoregridview_layout, null);
            viewHolder = new ViewHolder();
            viewHolder.title = (TextView) convertView.findViewById(R.id.home_tv1);
            viewHolder.image = (ImageView) convertView.findViewById(R.id.home_mstore1);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        String url0 = list.get(position).mstore_pic_url;
        String text0 = HomeModel.mstore_list.get(position).mstore_name;

        ImageLoader.getInstance().displayImage(url0,  viewHolder.image, TongleAppInstance.options);
        viewHolder.title.setText(text0);
        return convertView;
    }

    class ViewHolder {
        public TextView title;
        public ImageView image;
    }

}