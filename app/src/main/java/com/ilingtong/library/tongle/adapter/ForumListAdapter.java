package com.ilingtong.library.tongle.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.protocol.FirstPostThumbnailPicUrl;
import com.ilingtong.library.tongle.protocol.UserFollowPostList;
import com.ilingtong.library.tongle.utils.DipUtils;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.ArrayList;
import java.util.List;

/**
 * User: lengjiqiang
 * Date: 2015/5/11
 * Time: 16:47
 * Email: jqleng@isoftstone.com
 * Desc: 帖子列表适配器类
 */
public class ForumListAdapter extends BaseAdapter {
    private LayoutInflater inflater;
    private List<UserFollowPostList> list;
    private Context mContext;

    public ForumListAdapter(Context context, List<UserFollowPostList> list) {
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mContext = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = new ViewHolder();
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.home_forum_list_item, null);
            viewHolder.img_headIcon = (ImageView) convertView.findViewById(R.id.headicon);
            viewHolder.txt_nickname = (TextView) convertView.findViewById(R.id.nickname);
            viewHolder.txt_comment = (TextView) convertView.findViewById(R.id.comments);
            viewHolder.txt_posttitle = (TextView) convertView.findViewById(R.id.forum_list_item_txt_post_title);
            viewHolder.txt_time = (TextView) convertView.findViewById(R.id.post_time);
            viewHolder.gridView = (GridView) convertView.findViewById(R.id.post_pic_grid);
            viewHolder.img_single_pic = (ImageView) convertView.findViewById(R.id.post_pic_img);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        ImageLoader.getInstance().displayImage(list.get(position).user_head_photo_url, viewHolder.img_headIcon, TongleAppInstance.headIcon_options);
        viewHolder.txt_time.setText(list.get(position).post_time + "");
        viewHolder.txt_nickname.setText(list.get(position).user_nick_name);
        //如果帖子id等于首发帖子id，则表示改帖子为首发。反之则为转发。
        if (!list.get(position).first_post_id.equals(list.get(position).post_id)) {
            //转发帖子，comment显示post_comment.txt_posttitle显示原帖标题
            viewHolder.txt_comment.setText(list.get(position).post_comment + "");
            viewHolder.txt_posttitle.setText(list.get(position).post_title);
        } else {
            //首发帖子，comment显示txt_posttitle.txt_posttitle隐藏
            viewHolder.txt_comment.setText(list.get(position).post_title);
            viewHolder.txt_posttitle.setVisibility(View.GONE);
        }

        //显示帖子图片墙
        ArrayList<FirstPostThumbnailPicUrl> picUrls = list.get(position).first_post_thumbnail_pic_url;
        if (picUrls != null && picUrls.size() > 0) {
            if (picUrls.size() == 1) {
                //当帖子图片集中只有一张图片时，隐藏gridview，加载imagevie。并且让图片宽度填充屏幕。等比拉伸/压缩后自适应高度
                viewHolder.gridView.setVisibility(View.GONE);
                viewHolder.img_single_pic.setVisibility(View.VISIBLE);
                viewHolder.img_single_pic.setScaleType(ImageView.ScaleType.FIT_XY);
                ImageLoader.getInstance().displayImage(picUrls.get(0).pic_url, viewHolder.img_single_pic, TongleAppInstance.options,new SimpleImageLoadingListener() {
                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        if (loadedImage == null) {
                            super.onLoadingComplete(imageUri,view,loadedImage);
                        } else {
                            float es = (float) (DipUtils.getScreenWidth(mContext) - mContext.getResources().getDimensionPixelSize(R.dimen.activity_vertical_margin) * 2) / (float) loadedImage.getWidth();
                            int height = (int) (loadedImage.getHeight() * es);
                            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) view.getLayoutParams();
                            params.height = height;
                            view.setLayoutParams(params);
                        }
                    }
                });
            } else {
                //当帖子中图片集中有多张图片时，隐藏imageView。加载gridview
                viewHolder.img_single_pic.setVisibility(View.GONE);
                viewHolder.gridView.setVisibility(View.VISIBLE);
                viewHolder.gridView.setClickable(false);
                viewHolder.gridView.setPressed(false);
                viewHolder.gridView.setEnabled(false);
                if (picUrls.size() < 3) {
                    viewHolder.gridView.setNumColumns(picUrls.size());
                    viewHolder.gridView.setHorizontalSpacing(2);
                } else {
                    viewHolder.gridView.setNumColumns(3);
                    viewHolder.gridView.setHorizontalSpacing(2);
                    viewHolder.gridView.setVerticalSpacing(2);
                }
                FirstPostPicGridAdapter gridAdapter = new FirstPostPicGridAdapter(mContext, picUrls);
                viewHolder.gridView.setAdapter(gridAdapter);
            }
        }
        return convertView;
    }

    class ViewHolder {
        ImageView img_headIcon; //头像
        TextView txt_nickname; //昵称
        TextView txt_time;  //转帖时间
        TextView txt_comment;  //转帖内容
        TextView txt_posttitle;   //帖子标题
        GridView gridView;  //帖子图片集
        ImageView img_single_pic;  //帖子图片集中只有一张图片时显示
    }
}
