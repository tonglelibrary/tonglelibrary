package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: lengjiqiang
 * Date: 2015/6/24
 * Time: 11:37
 * Email: jqleng@isoftstone.com
 * Desc: 用户协议信息类，该类包含通过网络返回的数据
 */
public class AgreementInfo implements Serializable {
    private String title_text;
    private String user_agreement_url;

    public String getTitle_text() { return title_text; }
    public String getUser_agreement_url() {
        return user_agreement_url;
    }

    @Override
    public String toString() {
        return "title_text:" + title_text + "\r\n" +
                "user_agreement_url:" + user_agreement_url;
    }
}
