package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;

/**
 * User: 率磊
 * Date: 2015/7/3
 * Time: 09：48
 * Email: leishuai@isoftstone.com
 * Desc: 我的关注人返回信息
 */
public class MyAttentionResult implements Serializable {
    public HeadJson head;
    public MyAttentionInfo body;
}
