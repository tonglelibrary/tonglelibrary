package com.ilingtong.library.tongle.protocol;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * User: shuailei
 * Date: 2015/6/24
 * Time: 22:05
 * Email: leishuai@isoftstone.com
 * Desc: 订单详细信息类型
 */
public class OrderJSONInfo implements Serializable {
    //订单信息（商品明细列表（明细序号，商品ID)
    public ArrayList<ProductListInfo> product_list;
    //收货地址编号
    public String address_no;
    //支付方式编号
    public String pay_type;
    //是否需要发票
    public String invoice_type;
    //配送信息（配送方式编号，配送时间）
    public ShippingMethodInfo shipping_method;
    //发票信息（发票类型编号，发票内容区分，发票抬头备注）
    public InvoiceInfo invoice_info;
    //订单备注
    public String order_memo;
    //关税
    public String tariff;
    //优惠券号 add  at 2016/04/05
    public String vouchers_number_id;
}
