package com.ilingtong.library.tongle.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.adapter.AddressListAdapter;
import com.ilingtong.library.tongle.model.AddressModel;
import com.ilingtong.library.tongle.protocol.AddressListItem;

/**
 * User: lengjiqiang
 * Date: 2015/6/9
 * Time: 17:15
 * Email: jqleng@isoftstone.com
 * Desc: 选择收货地址页
 */
public class AddressSelectActivity extends BaseActivity implements View.OnClickListener{
    private TextView top_name;
    private TextView top_btn_text;
    private ImageView left_arrow_btn;
    private ListView addressListview;
    AddressListAdapter addrAdapter;
    Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            addrAdapter = new AddressListAdapter(AddressSelectActivity.this,AddressModel.my_address_list);
            addressListview.setAdapter(addrAdapter);
        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.address_select_layout);
        initView();
    }
    public void initView(){
        top_name = (TextView)findViewById(R.id.top_name);
        left_arrow_btn = (ImageView)findViewById(R.id.left_arrow_btn);
        top_btn_text = (TextView)findViewById(R.id.top_btn_text);
        addressListview = (ListView)findViewById(R.id.address_listview);
        top_btn_text.setOnClickListener(this);
        left_arrow_btn.setOnClickListener(this);
        left_arrow_btn.setVisibility(View.VISIBLE);
        top_name.setText(R.string.select_address);
        top_name.setVisibility(View.VISIBLE);
        top_btn_text.setVisibility(View.VISIBLE);
        top_btn_text.setText(R.string.manage);
        if(AddressModel.my_address_list!=null) {
            addrAdapter = new AddressListAdapter(AddressSelectActivity.this, AddressModel.my_address_list);
            addressListview.setAdapter(addrAdapter);
        }
        addressListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                for (int i = 0; i < addressListview.getCount(); i ++ ) {
                    View itemview = addressListview.getChildAt(i);
                    ImageView selectIcon = (ImageView)itemview.findViewById(R.id.select_icon);
                    selectIcon.setVisibility(View.INVISIBLE);
                }
                View selectView = addressListview.getChildAt(position);
                ImageView selectIcon = (ImageView)selectView.findViewById(R.id.select_icon);
                selectIcon.setVisibility(View.VISIBLE);
                AddressModel.checkAddress = (AddressListItem)addrAdapter.getItem(position);

                setResult(2);
                finish();

            }
        });
    }
    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.left_arrow_btn){
            finish();
        }else if (v.getId() == R.id.top_btn_text){
            Intent intent = new Intent(AddressSelectActivity.this, AddressManageActivity.class);
            startActivityForResult(intent, 10001);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 10001) {
            mHandler.sendEmptyMessage(0);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
