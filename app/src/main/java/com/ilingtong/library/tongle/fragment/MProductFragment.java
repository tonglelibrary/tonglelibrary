package com.ilingtong.library.tongle.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.ilingtong.library.tongle.R;
import com.ilingtong.library.tongle.ServiceManager;
import com.ilingtong.library.tongle.TongleAppConst;
import com.ilingtong.library.tongle.TongleAppInstance;
import com.ilingtong.library.tongle.activity.CollectProductDetailActivity;
import com.ilingtong.library.tongle.activity.ProductTicketDetailActivity;
import com.ilingtong.library.tongle.activity.SearchActivity;
import com.ilingtong.library.tongle.adapter.MstoreRecyclerViewAdapter;
import com.ilingtong.library.tongle.external.SpacesItemDecoration;
import com.ilingtong.library.tongle.protocol.CollectProductResult;
import com.ilingtong.library.tongle.protocol.ProductListItemData;
import com.ilingtong.library.tongle.protocol.SearchResult;
import com.ilingtong.library.tongle.utils.ToastUtils;

import java.util.ArrayList;

/**
 * Created by wuqian on 2015/10/29.
 * mail: wuqian@ilingtong.com
 * Description:
 */
public class MProductFragment extends LazyFragment implements SearchActivity.FlushDataListener {
    // private XListView productListView;
    // private CollectProductListAdaper productListAdaper;
    public ArrayList<ProductListItemData> prod_list = new ArrayList<ProductListItemData>();
    private boolean flag = true;
    private int intoType = 0;

    public static boolean UPDATE_LIST_FLAG = false; //是否刷新列表的标志
    public static boolean MPRODUCTFRAGMENT_UPDATE_FLAG = false; //进入页面刷新标志。为ture时表示刷新，反之不刷新
    private int listIndex = -1; //表示是从 position位置跳转到M客详情的
    // 标志位，标志已经初始化完成。
    private boolean isPrepared;
    Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    //productListAdaper.notifyDataSetChanged();
                    recyclerViewAdapter.notifyDataSetChanged();
                    break;
            }
        }
    };

    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private GridLayoutManager gridLayoutManager;
    private int lastVisibleItem;
    private MstoreRecyclerViewAdapter recyclerViewAdapter;

    /**
     * 静态工厂方法需要一个int型的值来初始化fragment的参数，然后返回新的fragment到调用者
     *
     * @param intoType 在哪个页面创建
     * @return
     */
    public static MProductFragment newInstance(int intoType, ArrayList<ProductListItemData> prod_list) {
        MProductFragment fragment = new MProductFragment();
        Bundle args = new Bundle();
        args.putInt(TongleAppConst.INTO_TYPE, intoType);
        args.putSerializable("list", prod_list);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.mproduct_gridview_layout, null);
        initView(view);
        isPrepared = true;
        doRequest();
        return view;
    }

    private void initView(View view) {
        intoType = getArguments().getInt(TongleAppConst.INTO_TYPE);
        prod_list.clear();
        if (getArguments().get("list") != null) {
            prod_list = (ArrayList<ProductListItemData>) getArguments().get("list");
        }
        flag = true;
        recyclerView = (RecyclerView) view.findViewById(R.id.mproduct_gridview_recycler_view);
        recyclerView.addItemDecoration(new SpacesItemDecoration(8));
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.mproduct_gridview_swipeRefresh);
        gridLayoutManager = new GridLayoutManager(this.getActivity(), 2);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerViewAdapter = new MstoreRecyclerViewAdapter(prod_list);
        recyclerViewAdapter.setOnItemClickListener(new MstoreRecyclerViewAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                //点击item进行跳转，并把产品id传到产品详情界面
                if(prod_list.get(position).coupon_flag.equals(TongleAppConst.YES)) {//团购商品
                    ProductTicketDetailActivity.launchForResult(getActivity(), prod_list.get(position).prod_id, TongleAppConst.ACTIONID_COLLECT, prod_list.get(position).relation_id, "", "", intoType, 10001);
                }else{
                   //普通商品
                    CollectProductDetailActivity.launchForResult(getActivity(), prod_list.get(position).prod_id, TongleAppConst.ACTIONID_COLLECT, prod_list.get(position).relation_id, "", "", intoType, 10001);
                }
                listIndex = position;
            }
        });
        recyclerView.setAdapter(recyclerViewAdapter);

        swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright, android.R.color.holo_green_light,
                android.R.color.holo_orange_light, android.R.color.holo_red_light);
        //此处是android自带的只支持下拉刷新
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //刷新之前，清空list
                if (prod_list != null) {
                    prod_list.clear();
                }
                MPRODUCTFRAGMENT_UPDATE_FLAG = false;
                UPDATE_LIST_FLAG = false;
                ServiceManager.doCollectProductRequest(TongleAppInstance.getInstance().getUserID(), "", "1", TongleAppConst.FETCH_COUNT, successListener(), errorListener());
            }
        });
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                //SCROLL_STATE_DRAGGING  和   SCROLL_STATE_IDLE 两种效果自己看着来
                if (newState == RecyclerView.SCROLL_STATE_DRAGGING && lastVisibleItem + 1 == recyclerViewAdapter.getItemCount()) {
                    if (intoType == TongleAppConst.COLLECTFRAGMENT_INTO) {
                        swipeRefreshLayout.setRefreshing(true);
                        //加载更多
                        if (flag) {
                            ServiceManager.doCollectProductRequest(TongleAppInstance.getInstance().getUserID(), prod_list.get(prod_list.size() - 1).prod_id, "1", TongleAppConst.FETCH_COUNT, successListener(), errorListener());
                        } else {
                            ToastUtils.toastShort(getString(R.string.common_list_end));
                            swipeRefreshLayout.setRefreshing(false);
                            //productListView.setPullLoadEnable(false);
                        }
                    }

                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                lastVisibleItem = gridLayoutManager.findLastVisibleItemPosition();
            }
        });
    }

    /**
     * 点击M品列表进入详情后，返回的回调。如果在详情页面有取消关注，回到该页面时刷新列表。没有则不刷新
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (intoType == TongleAppConst.COLLECTFRAGMENT_INTO && requestCode == 10001 && UPDATE_LIST_FLAG) {
            if (listIndex > -1) {
                prod_list.remove(listIndex);
                mHandler.sendEmptyMessage(0);
            }
            UPDATE_LIST_FLAG = false;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * 调用接口，请求数据
     */
    public void doRequest() {
        if (intoType == TongleAppConst.COLLECTFRAGMENT_INTO) {
            ServiceManager.doCollectProductRequest(TongleAppInstance.getInstance().getUserID(), "", "1", TongleAppConst.FETCH_COUNT, successListener(), errorListener());
        } else if (intoType == TongleAppConst.FINDFRAGMENT_INTO) {
            //发现进入的产品，不可以有刷新和加载
            swipeRefreshLayout.setEnabled(false);
//            productListView.setPullRefreshEnable(false);
//            productListView.setPullLoadEnable(false);
            //热门搜索功能
            //ServiceManager.hotSearchRequest(TongleAppInstance.getInstance().getUserID(), hotSearchSuccessListener(), errorListener());
        } else if (intoType == TongleAppConst.SEARCH_INTO) { //关键字搜索
//            productListView.setPullLoadEnable(false);
//            productListView.setPullRefreshEnable(false);
            swipeRefreshLayout.setEnabled(false);
        }
    }

    /**
     * 刷新
     *
     * @param id
     */
//    @Override
//    public void onRefresh(int id) {
//        //刷新之前，清空list
//        if (prod_list != null) {
//            prod_list.clear();
//        }
//        MPRODUCTFRAGMENT_UPDATE_FLAG = false;
//        UPDATE_LIST_FLAG = false;
//        ServiceManager.doCollectProductRequest(TongleAppInstance.getInstance().getUserID(), "", "1", TongleAppConst.FETCH_COUNT, successListener(), errorListener());
//    }

    /**
     * 加载更多
     *
     * @param id
     */
//    @Override
//    public void onLoadMore(int id) {
//        //加载更多
//        if (flag) {
//            ServiceManager.doCollectProductRequest(TongleAppInstance.getInstance().getUserID(), prod_list.get(prod_list.size() - 1).prod_id, "1", TongleAppConst.FETCH_COUNT, successListener(), errorListener());
//        } else {
//            ToastUtils.toastShort("没有更多数据");
//            //productListView.setPullLoadEnable(false);
//        }
//    }

    /**
     * 功能：网络响应成功，返回数据 发现热门
     */
    private Response.Listener hotSearchSuccessListener() {
        return new Response.Listener<SearchResult>() {
            @Override
            public void onResponse(SearchResult searchResulte) {
                if (TongleAppConst.SUCCESS.equals(searchResulte.getHead().getReturn_flag())) {
                    prod_list.addAll(searchResulte.getBody().getProd_list());
                    //调用handler，发送消息
                    mHandler.sendEmptyMessage(0);
                } else {
                    ToastUtils.toastShort(getString(R.string.para_exception) + searchResulte.getHead().getReturn_message());
                }
            }

        };
    }

    /**
     * 功能：网络响应成功，返回数据
     */
    private Response.Listener successListener() {
        return new Response.Listener<CollectProductResult>() {
            @Override
            public void onResponse(CollectProductResult response) {
                if (TongleAppConst.SUCCESS.equals(response.getHead().getReturn_flag())) {
                    prod_list.addAll(response.getBody().getProd_list());
                    //第一次请求或刷新页面时候判断是否隐藏底部加载更多按钮
                    if (Integer.parseInt(response.getBody().getData_total_count()) > prod_list.size()) {
                        flag = true;
                        //productListView.setPullLoadEnable(true);
                    } else {
                        flag = false;
                        //productListView.setPullLoadEnable(false);
                    }
                    //调用handler，发送消息
                    mHandler.sendEmptyMessage(0);
                    swipeRefreshLayout.setRefreshing(false);
                } else {
                    ToastUtils.toastShort(getString(R.string.para_exception) + response.getHead().getReturn_message());
                    swipeRefreshLayout.setRefreshing(false);
                }
            }
        };
    }

    /**
     * 功能：网络响应失败
     */
    private Response.ErrorListener errorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                ToastUtils.toastShort(getString(R.string.sys_exception));
                swipeRefreshLayout.setRefreshing(false);
            }
        };
    }

    @Override
    public void flush(Object data) {
        if (recyclerViewAdapter != null) {
            prod_list = (ArrayList<ProductListItemData>) data;
            recyclerViewAdapter.notifyDataSetChanged();
        }
    }

    @Override
    protected void lazyLoad() {
        if (!isPrepared || !isVisible) {
            return;
        }
        if (intoType == TongleAppConst.COLLECTFRAGMENT_INTO) {
            if (MPRODUCTFRAGMENT_UPDATE_FLAG || UPDATE_LIST_FLAG) {
                //onRefresh(0);
                //刷新之前，清空list
                if (prod_list != null) {
                    prod_list.clear();
                }
                MPRODUCTFRAGMENT_UPDATE_FLAG = false;
                UPDATE_LIST_FLAG = false;
                ServiceManager.doCollectProductRequest(TongleAppInstance.getInstance().getUserID(), "", "1", TongleAppConst.FETCH_COUNT, successListener(), errorListener());
            }
        }
    }
}
