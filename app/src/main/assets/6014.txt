{"head": {
    "function_id": "1004",
    "return_flag": "0",
    "return_message": "success"
  },"body":{
             "voucher":[
               {
                 "voucher_base":
                 {
                   "vouchers_number_id":"16020001000011",
                   "vouchers_name":"通乐抵用券",
                   "issue_flg": "1",
                   "issue_store_name": "",
                   "money": 30,
                   "use_conditions":200,
                   "use_conditions_memo":"满200元使用",
                   "expiration_date_begin": "2016-03-01",
                   "expiration_date_end": "2016-06-30",
                   "vouchers_status":"2"
                 },
                 "voucher_link_info":
                 {
                   "title":"去看看",
                   "pic":"http://172.18.0.78/TongLe/P1.jpg",
                   "link_type":"4",
                   "link_id":""
                 }
               },
               {
                 "voucher_base":
                 {
                   "vouchers_number_id":"16020002000101",
                   "vouchers_name":"小家电抵用券",
                   "issue_flg": "2",
                   "issue_store_name": "XX店铺",
                   "money": 50,
                   "use_conditions":300,
                   "use_conditions_memo":"满300元使用",
                   "expiration_date_begin": "2016-02-01",
                   "expiration_date_end": "2016-05-30",
                   "vouchers_status":"4"
                 },
                 "voucher_link_info":
                 {
                   "title":"去看看",
                   "pic":"http://172.18.0.78/TongLe/P1.jpg",
                   "link_type":"4",
                   "link_id":""
                 }
               }
             ]
           }
}
